#ifndef DCM_DUMMYPROCESSOR_H
#define DCM_DUMMYPROCESSOR_H

#include "dcm/Processor.h"

#include "eformat/StreamTag.h"

namespace daq
{
namespace dcm
{

// Forward declarations
namespace dal { class DcmDummyProcessor; }

/*! \class Manager of event fragment fetching and event processing.
 *
 *  \brief Fetches L1Results and ROBs on demand by of the Trigger, on processing completion it
 *      forwards the event to the EventBuilder and releases event storage on notification of its the
 *      safe storage.
 *
 *  \author Christophe Meessen <meessen@cppm.in2p3.fr>
 */
class DummyProcessor : public Processor
{
protected:
  struct HLTPU;

public:
  DummyProcessor(Main& main, const dal::DcmDummyProcessor& config);
  void initialize(L1Source* l1Source, DataCollector* dataCollector, EventBuilder* eventBuilder) override;
  void asyncStart() override;
  void startHLTPU();

protected:
  bool setDynamicParameter(const std::string& key, const std::string& value) override;
  void processL1R  (Processor::HLTPU* hltpu) override;
  void processROBs (Processor::HLTPU* hltpu) override;
  void setStateIdle(Processor::HLTPU* hltpu) override;
  void terminate   (Processor::HLTPU* hltpu) override;

  void l2DataCollection(HLTPU* hltpu);
  void l2Processing    (HLTPU* hltpu);
  void efDataCollection(HLTPU* hltpu);
  void efProcessing    (HLTPU* hltpu);

  struct HLTPU : Processor::HLTPU
  {
    enum class Operation
    {
      L1R_FETCHING,
      L2_DATA_COLLECTION,
      L2_PROCESSING,
      EF_DATA_COLLECTION,
      EF_PROCESSING
    };

    HLTPU(boost::asio::io_service& ioService) : operation(Operation::L1R_FETCHING), timer(ioService) {}
    ~HLTPU() override { timer.cancel(); }

    Operation operation;
    boost::asio::steady_timer timer;
  };

  class Histograms : public Processor::Histograms
  {
  public:

    Histograms(boost::asio::io_service& ioService,
        std::chrono::steady_clock::duration updateInterval,
        float maxTime, float procTimeOut);

    void initialize() override;
    void finalize() override;
    void clear() override;
    void updateRecents() override;

    void fillL2DataCollectionTime_ms(double x);
    void fillEbDataCollectionTime_ms(double x);

  private:
    monsvc::ptr<TH1I>     m_L2DataCollectionTime_ms;
    monsvc::ptr<TH1I>     m_L2DataCollectionTime_ms_recent;
    std::unique_ptr<TH1I> m_L2DataCollectionTime_ms_last;

    monsvc::ptr<TH1I>     m_EbDataCollectionTime_ms;
    monsvc::ptr<TH1I>     m_EbDataCollectionTime_ms_recent;
    std::unique_ptr<TH1I> m_EbDataCollectionTime_ms_last;
  };

  Histograms*                       m_histograms;
  const dal::DcmDummyProcessor&     m_config;
  std::vector<std::string>          m_robSources;
  float                             m_l2Acceptance;
  float                             m_efAcceptance;
  std::unique_ptr<DC::Distribution> m_l2ProcessingTimeDistribution_ms;
  std::unique_ptr<DC::Distribution> m_l2RequestsDistribution;
  std::unique_ptr<DC::Distribution> m_l2RequestsPerSourceDistribution;
  std::unique_ptr<DC::Distribution> m_efProcessingTimeDistribution_ms;
  eformat::helper::StreamTag        m_streamTag;
};

} // namespace dcm
} // namespace daq

#endif // DCM_DUMMYPROCESSOR_H

