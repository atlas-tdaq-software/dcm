#ifndef DCM_FILEOUTPUT_H
#define DCM_FILEOUTPUT_H

#include "dcm/Output.h"

#include "eformat/StreamTag.h"
#include "EventStorage/DataWriter.h"

#include <string>
#include <vector>
#include <unordered_map>

namespace daq
{
namespace dcm
{

class Main;

namespace dal
{
class DcmFileOutput;
}

class FileOutput: public Output
{

public:

  FileOutput(Main& main, const dal::DcmFileOutput& config);

  ~FileOutput();

  //! Implementation of Output::initialize()
  void initialize(EventBuilder* eventBuilder) override;

  //! Implementation of Output::asyncConnect()
  void asyncConnect() override;

  //! Implementation of Output::asyncStart()
  void asyncStart() override;

  //! Implementation of Output::asyncStop()
  void asyncStop() override;

  //! Implementation of Output::asyncDisconnect()
  void asyncDisconnect() override;

  //! Implementation of Output::asyncEventSend()
  void asyncEventSend(std::unique_ptr<Event> event) override;

protected:

  //! Open the EventStorage file
  EventStorage::DataWriter& getWriter(const std::string& type, const std::string& name);

  Main& m_main;
  const dal::DcmFileOutput& m_config;
  EventBuilder* m_eventBuilder = nullptr;

  EventStorage::run_parameters_record m_runParameters; ///< Run parameters from IS
  std::vector<std::string> m_userMetaData; ///< User-defined meta-data strings from IS
  std::unordered_map<std::string, EventStorage::DataWriter> m_dataWriters; ///< data writers
  double m_acceptanceRemainder; ///< remainder of event storage acceptance computation

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_FILEOUTPUT_H)
