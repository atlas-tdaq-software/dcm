#ifndef DCM_EVENT_BUILDER_H
#define DCM_EVENT_BUILDER_H

#include "dcm/Event.h"
#include "eformat/StreamTag.h"

#include <boost/system/error_code.hpp>

namespace daq
{
  namespace dcm
  {
    // Forward declarations
    class Processor;
    class Output;

    /*! @class EventBuilder is a pure abstract class for Event building
        @brief Build events given by the Processor and pass them to the Output
        @author Christophe Meessen <meessen@cppm.in2p3.fr>
        @author Tommaso Colombo <Tommaso.Colombo@cern.ch>
        @author Andrea Negri <Andrea.Negri@pv.infn.it>

        The EventBuilder class is intended to interconnect the Processor and Output object
        instances with the following call scenarios

        <pre>
        Processor                EventBuilder                 Output
           |                          |                          |
           |----asyncBuildAndSend---->|                          |
           |                          |------asyncEventSend----->|
           =                          =                          =
           |                          |<-------onEventSent-------| (callback)
           |<-------onEventSent-------|                          |
         </pre>

         The Processor calls the EventBuilder::asyncBuildAndSend()
         method. This method builds the FullEventHeader fragment(s) for the events, pass them to
         the Monitoring system and then forward the event to the Output.

         Some time later, when the event has been sent, or failed to be sent because of an error,
         the Output calls back the method EventBuilder::onEventSent() to notify that the event has
         been sent. When the event data may be discarded and its storage space may be reused, the
         EventBuilder forwards the call back to the Processor by calling Processor::onEventSent().
      */
    class EventBuilder
    {
    public:
      //! Virtual destructor doing nothing
      virtual ~EventBuilder() {}

      /*! @brief Configures the EventBuilder
          @param processor Pointer to the Processor submitting Events to build and send to Output
          @param output Pointer to the Output to which built Events will be submitted

          The EventBuilder instance must be configured with non null processor and output pointer,
          otherwise a misconfiguration issue must be raised.

          The EventBuilder may be reconfigured, but the behavior is undefined if there are pending
          callbacks.
       */
      virtual void initialize( Processor* processor, Output* output ) = 0;

      /*! @brief Build the submitted event and forward it to the output
          @param event The event to build and pass to the output instance

          The method Processor::onEventSent(event) is called when the event has been successfuly
          sent, or Processor::onEventSent(error, event) is called when an error occured.
       */
      virtual void asyncBuildAndSend( std::unique_ptr<Event> event ) = 0;

      /*! @brief Send the already built event directly to the output
          @param event The event to forward to the output instance

          To be called when the event has been returned by onEventSentError and needs to be
          resubmitted to the output instance without needing to rebuild the event.

          The method Processor::onEventSent(event) is called when the event has been successfuly
          sent, or Processor::onEventSent(error, event) is called when an error occured.
       */
      virtual void asyncSend( std::unique_ptr<Event> event ) = 0;

      /*! @brief Informs the EventBuilder that the given event was successfully sent by the Output
          @param event The event that was sent by the Output

          The EventBuilder must forward the call to the Processor instance.
       */
      virtual void onEventSent( std::unique_ptr<Event> event ) = 0;

      /*! @brief Informs the EventBuilder that the given event failed to be sent by the Output
          @param error The error code
          @param event Event that failed to be sent by the Output

          If the Event doesn't have a Debug StreamTag, the stream tag must be added and the event
          must be resubmitted to the Output. Otherwise the EventBuilder must forward the call to the
          Processor.
       */
      virtual void onEventSentError( const ers::Issue& issue, std::unique_ptr<Event> event ) = 0;

      /*! @brief Serialize the event using the header found in the given event, and set of robIds
          @param event[in,out] The event to serialized
          @param streamTag[in] The vector of robIds to include in the event
          @param templateHeader[in] The header to be used as template for the serialized event
          @param data[out] Pointer on the first byte of the serialized event
          @param nbrBytes[out] Size in bytes of the serialized event

          The Header of the event must have been previously serialized and reference in the event object.
          The serialized event is appended to the SharedBlockArray block.
       */
      virtual void serializeEvent(const Event &event,
                           const eformat::helper::StreamTag& streamTag,
                           const std::uint32_t *templateHeader,
                           std::uint32_t*& data,
                           std::uint32_t& nbrBytes) =0;


    };
  } // namespace dcm
} // namespace daq

#endif // !defined( DCM_EVENT_BUILDER_H )
