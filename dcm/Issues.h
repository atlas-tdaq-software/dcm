#ifndef DCM_ISSUES_H
#define DCM_ISSUES_H

#include <ers/ers.h>
#include <string>
#include <sstream>

#include "boost/system/error_code.hpp"

namespace daq
{

  /// System error issue
  ERS_DECLARE_ISSUE(dcm, SystemError,
    message << " " << "(" << category << ": " << number << ")",
    ((std::string) message) ((std::string) category) ((int) number)
  )

  /// System error issue helper function
  namespace dcm
  {
    inline SystemError makeSystemError(const ers::Context& context, const boost::system::error_code& error)
    {
      return SystemError(context, error.message(), error.category().name(), error.value());
    }
  }

  /// Base issue ("abstract")
  ERS_DECLARE_ISSUE( dcm, Issue, ERS_EMPTY, ERS_EMPTY )

  /// Generic Issue
  ERS_DECLARE_ISSUE_BASE( dcm, GenericIssue, dcm::Issue,
    "Generic DCM problem: " << reason,
    ERS_EMPTY,
    ((std::string) reason)
  )

  /// DCM specific issues ...

  /// Base Issue for SharedBlockArray
  ERS_DECLARE_ISSUE_BASE( dcm, SharedBlockArrayIssue, dcm::Issue, ERS_EMPTY, ERS_EMPTY, ERS_EMPTY )

  /// SBA specific issue:
  ERS_DECLARE_ISSUE_BASE( dcm, SBAInvalidArgument, dcm::SharedBlockArrayIssue,
    "SBA Invalid Argument: " << reason,
    ERS_EMPTY,
    ((std::string) reason)
  )

  /// SBA specific issue:
  ERS_DECLARE_ISSUE_BASE( dcm, SBARunTimeError, dcm::SharedBlockArrayIssue,
    "SBA RunTimeError: " << reason,
    ERS_EMPTY,
    ((std::string) reason)
  )

  /// DCM Invalid argument issue:
  ERS_DECLARE_ISSUE_BASE( dcm, InvalidArgument, dcm::Issue,
    "DCM Invalid Argument: " << reason,
    ERS_EMPTY,
    ((std::string) reason)
  )

  /// DCM Runtime error issue:
  ERS_DECLARE_ISSUE_BASE( dcm, RunTimeError, dcm::Issue,
    "DCM RunTimeError: " << reason,
    ERS_EMPTY,
    ((std::string) reason)
  )

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  CONFIGURATION

  /// Configuration issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ConfigurationIssue,
                          dcm::Issue,
                          "DCM Configuration issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Configuration issue: Unknown parameter
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ConfigUnknownParameter,
                          dcm::ConfigurationIssue,
                          "Unknown parameter " << param << ". Ignoring.",
                          ERS_EMPTY,
                          ((std::string) param))

  /// Configuration issue: Unknown parameter
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ConfigInvalidValue,
                          dcm::ConfigurationIssue,
                          "Value '" << value << "'' is invalid for parameter " << param << ". Ignoring.",
                          ERS_EMPTY,
                          ((std::string) param)
                          ((std::string) value))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  DATA INTEGRITY

  /// DataIntegrity issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          DataIntegrityIssue,
                          dcm::Issue,
                          "DCM Data Integrity issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// L1IDMismatch issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          L1IDMismatchIssue,
                          dcm::DataIntegrityIssue,
                          "LVL1ID mismatch detected: event fragment from ROB 0x"
                          << std::hex << robid << std::dec
                          << " (" << detName << ") from " << rosName
                          << " with Level1Id: 0x" << std::hex << robl1id << std::dec
                          << " doesn't match the event LVL1ID: 0x"
                          << std::hex << eventl1id << std::dec,
                          ERS_EMPTY,
                          ((uint32_t) robid)
                          ((uint64_t) robl1id)
                          ((uint64_t) eventl1id)
                          ((const char*) detName)
                          ((const char*) rosName))

  /// BCIDMismatch issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          BCIDMismatchIssue,
                          dcm::DataIntegrityIssue,
                          "BCID mismatch detected: event fragment from ROB 0x"
                          << std::hex << robid << std::dec
                          << " (" << detName << ") from " << rosName
                          << " with Level1Id: 0x" << std::hex << l1id << std::dec
                          << " and BCID: " << robbcid
                          << " doesn't match the event BCID: " << eventbcid
                          << " event BCID - ROB BCID: " << (eventbcid-robbcid) << " ",
                          ERS_EMPTY,
                          ((uint32_t) robid)
                          ((int32_t) robbcid)
                          ((int32_t) eventbcid)
                          ((uint64_t) l1id)
                          ((const char*) detName)
                          ((const char*) rosName))

  /// Run number issue
  ERS_DECLARE_ISSUE_BASE( dcm,
                          RunNoMismatchIssue,
                          dcm::DataIntegrityIssue,
                          "Run number mismatch detected in CTP rod_run_no"
                          << " (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec
                          << "; RunNo: " << runNo
                          << "; CTPRodRunNo: " << rodRunNo << "). Ignored. ",
                          ERS_EMPTY,
                          ((std::uint32_t) runNo)
                          ((std::uint32_t) rodRunNo)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  LEVEL 1 RESULT FETCHING

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          L1RFetchingIssue,
                          dcm::Issue,
                          "Level1 result fetching issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          L1RFetchingError,
                          dcm::Issue,
                          "" << reason,
                          ERS_EMPTY,
                          ((std::string) reason))

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          NoHLTSVFound,
                          dcm::Issue,
                          "No enabled HLTSV found. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          L1RRequestCanceled,
                          dcm::Issue,
                          "L1R request canceled. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ManyHLTSVFound,
                          dcm::Issue,
                          "More than one enabled HLTSV found. Using " << hltsvName,
                          ERS_EMPTY,
                          ((std::string) hltsvName))

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          L1RSessionOpenError,
                          dcm::L1RFetchingIssue,
                          "Failed opening session with " << remoteName << " (" << remoteEndpoint << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint))

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          HLTSVSessionError,
                          dcm::L1RFetchingIssue,
                          "" << reason << " (name: " << remoteName << "; addr: " << remoteEndpoint << "). "
                          << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint)
                          ((std::string) action))

  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          HLTSVSessionBadMessage,
                          dcm::L1RFetchingIssue,
                          "Unexpected message type (name: " << remoteName << "; addr: " << remoteEndpoint
                          << "; msgTypeId: " << typeId << "). "
                          << action << ". ",
                          ERS_EMPTY,
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint)
                          ((std::uint32_t) typeId)
                          ((std::string) action))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  DATA COLLECTING

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          DataCollectingIssue,
                          dcm::Issue,
                          "DCM Data collecting issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          DataCollectingError,
                          dcm::DataCollectingIssue,
                          "" << reason,
                          ERS_EMPTY,
                          ((std::string) reason))

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ROSSessionError,
                          dcm::DataCollectingIssue,
                          "" << reason << " (name: " << remoteName << "; addr: " << remoteEndpoint << "). "
                          << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint)
                          ((std::string) action))

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ROSSessionBadMessage,
                          dcm::DataCollectingIssue,
                          "Unexpected message type (name: " << remoteName << "; addr: " << remoteEndpoint
                          << "; msgTypeId: " << typeId << "). "
                          << action << ". ",
                          ERS_EMPTY,
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint)
                          ((std::uint32_t) typeId)
                          ((std::string) action))

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ROSSessionEventError,
                          dcm::DataCollectingIssue,
                          "" << reason << " (name: " << remoteName << "; addr: " << remoteEndpoint
                          << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). "
                          << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) remoteName)
                          ((std::string) remoteEndpoint)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id)
                          ((std::string) action))

  /// Data collecting issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          DataCollectingBadRobId,
                          dcm::DataCollectingIssue,
                          "Could not fetch ROB with non-existent ID (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec
                          << "; ROBId: " << robId << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id)
                          ((std::uint32_t) robId))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  PROCESSOR

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ProcessorIssue,
                          dcm::Issue,
                          "DCM Processor issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ProcessingIssue,
                          dcm::ProcessorIssue,
                          "" << reason,
                          ERS_EMPTY,
                          ((std::string) reason))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ProcessorWatchDogError,
                          dcm::ProcessorIssue,
                          "Processor watch dog timer error. Ignoring. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ReceivedL1RWhileStopping,
                          dcm::ProcessorIssue,
                          "Received Level 1 result while stopping (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec <<  "). Discard. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FetchLvl1Error,
                          dcm::ProcessorIssue,
                          "Error while fetching new Level 1 result. Retrying. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FetchLvl1HLTPUError,
                          dcm::ProcessorIssue,
                          "Error while fetching new Level 1 Result (HLTPU: " << hltpuName << "). Retrying. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FetchROBsError,
                          dcm::ProcessorIssue,
                          "Error while fetching ROBs (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FetchROBsHLTPUError,
                          dcm::ProcessorIssue,
                          "Error while fetching ROBs (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec
                          << "). Force accept partial event and close connection. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ProcessingSimulationError,
                          dcm::ProcessorIssue,
                          "Error simulating " << processingType << " processing (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) processingType)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          AcceptHLTPUSessionError,
                          dcm::ProcessorIssue,
                          "Accept HLTPU session error. Ignoring. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          OpenHLTPUSessionError,
                          dcm::ProcessorIssue,
                          "Open HLTPU session error. Ignoring. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          CloseHLTPUSessionError,
                          dcm::ProcessorIssue,
                          "Close HLTPU session error (HLTPU: " << hltpuName << "). Force close. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          RejectHLTPUConnection,
                          dcm::ProcessorIssue,
                          "State disallow HLTPU connections (HLTPU: " << hltpuName
                          << "). Ignore connection request. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          MaxHLTPUConnections,
                          dcm::ProcessorIssue,
                          "Maximum number of HLTPU connections reached (Max: " << max << "; HLTPU: " << hltpuName
                          << "). Ignore connection request. ",
                          ERS_EMPTY,
                          ((std::uint32_t) max)
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SessionClosedByIdleHLTPU,
                          dcm::ProcessorIssue,
                          "Session closed by idle HLTPU (HLTPU: " << hltpuName << "). Clean up. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SessionClosedByActiveHLTPU,
                          dcm::ProcessorIssue,
                          "Session closed by active HLTPU (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Force accept and clean up. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ProcessingTimedOut,
                          dcm::ProcessorIssue,
                          "Event processing timed out (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Force accept and closing session. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          Level1RequestTimedOut,
                          dcm::ProcessorIssue,
                          "Level 1 request timed out (HLTPU: " << hltpuName << "). Closing session. ",
                          ERS_EMPTY,
                          ((std::string) hltpuName))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          IdleHLTPUSessionError,
                          dcm::ProcessorIssue,
                          "" << reason << " (HLTPU: " << hltpuName << "). " << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) hltpuName)
                          ((std::string) action))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          ActiveHLTPUSessionError,
                          dcm::ProcessorIssue,
                          "" << reason << " (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). " << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id)
                          ((std::string) action))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          InvalidRobIdInGetRobs,
                          dcm::ProcessorIssue,
                          "" << reason << " (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "; robIds: " << robIds
                          << "). " << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id)
                          ((std::string) robIds)
                          ((std::string) action))

  /// Processor issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          InvalidRobIdOrDetInStreamTag,
                          dcm::ProcessorIssue,
                          "" << reason << " (HLTPU: " << hltpuName << "; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "; streamTag: " << streamTag
                          << "; robIds: " << robIds << "; detectors: " << dets << "). " << action << ". ",
                          ERS_EMPTY,
                          ((std::string) reason)
                          ((std::string) hltpuName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id)
                          ((std::string) streamTag)
                          ((std::string) robIds)
                          ((std::string) dets)
                          ((std::string) action))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  EVENT BUILDER

  /// EventBuilder issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EventBuilderIssue,
                          dcm::Issue,
                          "DCM Event Builder issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// EventBuilder Error thrown: Failed serializing event header:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EBFailedSerializingEventHeader,
                          dcm::EventBuilderIssue,
                          "DCM Event Builder: Failed serializing event header (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// EventBuilder Error thrown: Failed serializing event:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EBFailedSerializingEvent,
                          dcm::EventBuilderIssue,
                          "DCM Event Builder: Failed serializing event (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// EventBuilder Error thrown: Event without ROBs:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EBEventWithoutROBs,
                          dcm::EventBuilderIssue,
                          "DCM Event Builder: Event without ROBs (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// EventBuilder warning: No ROBs for detector
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EBNoROBsForDetector,
                          dcm::EventBuilderIssue,
                          "DCM Event Builder: No ROBs for detector " << detStr << " (0x:" << std::hex << det << std::dec
                          << ") in stream with tagType " << tagType << " and tagName " << tagName << " ( GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) detStr)
                          ((std::uint32_t) det)
                          ((std::string) tagType)
                          ((std::string) tagName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// EventBuilder warning: Missing ROB data
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EBMissingROBData,
                          dcm::EventBuilderIssue,
                          "DCM Event Builder: Missing data for ROB " << srcId << " (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) srcId)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  OUTPUT

  /// Output issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          OutputIssue,
                          dcm::Issue,
                          "DCM Output issue. ",
                          ERS_EMPTY,
                          ERS_EMPTY)

  /// Output issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          PossibleDuplicateError,
                          dcm::OutputIssue,
                          "DCM Output: Event output error (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Force accept as possibleDuplicate. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Output issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          BuildAndSendError,
                          dcm::OutputIssue,
                          "DCM Output: Event output error (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Output issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EventMaybeDelivered,
                          dcm::OutputIssue,
                          "DCM Output: Event maybe delivered (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec
                          << "). Flag as possible duplicate and retry sending. ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// Output issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          EventNotDelivered,
                          dcm::OutputIssue,
                          "DCM Output: Event not delivered (GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec
                          << "). ",
                          ERS_EMPTY,
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// FileOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FileOutputOpenIssue,
                          dcm::OutputIssue,
                          "Error opening output stream file (fullTag: '" << fullTag << "'; tagType '" << tagType
                          << "'; tagName '" << tagName << "'). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) fullTag)
                          ((std::string) tagType)
                          ((std::string) tagName))

  /// FileOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          FileOutputWriteIssue,
                          dcm::OutputIssue,
                          "Error writing event to stream (tagType '" << tagType
                          << "'; tagName '" << tagName << "'; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Ignoring. ",
                          ERS_EMPTY,
                          ((std::string) tagType)
                          ((std::string) tagName)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoHostNotFoundIssue,
                          dcm::OutputIssue,
                          "Error SFO host not found (local name'" << localName
                          << "'; remote name '" << remoteName << "'). ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName))


  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoAssignMessageIssue,
                          dcm::OutputIssue,
                          "Error sending Assign message (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((std::string) errorMessage)
                          ((int)         errorCode))


  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoTransferMessageIssue,
                          dcm::OutputIssue,
                          "Error sending Transfer message (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((std::string) errorMessage)
                          ((int)         errorCode))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoUpdateMessageSizeIssue,
                          dcm::OutputIssue,
                          "Error Unexpected update message size (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; size " << size << "). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((unsigned int)size))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoMessageTypeIssue,
                          dcm::OutputIssue,
                          "Error Unexpected message type (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; type " << type << "). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((unsigned int)type))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoRequestEventIssue,
                          dcm::OutputIssue,
                          "Error SFO requested more events that were assigned to it (local name'"
                          << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoReportEventIssue,
                          dcm::OutputIssue,
                          "Error SFO reports that it recorded an event that was not sent (local name'"
                          << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoUpdateMessageIssue,
                          dcm::OutputIssue,
                          "Error receiving Update message (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((std::string) errorMessage)
                          ((int)         errorCode))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoEventRequestTimeoutIssue,
                          dcm::OutputIssue,
                          "Error Timeout while waiting for event request (local name'"
                          << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoAcknowledgmentTimeoutIssue,
                          dcm::OutputIssue,
                          "Error Timeout while waiting for acknowledgment (local name'"
                          << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'). Aborting. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoConnectIssue,
                          dcm::OutputIssue,
                          "Error could not connect (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "). Retry. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((std::string) errorMessage)
                          ((int)         errorCode))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoEventTransferIssue,
                          dcm::OutputIssue,
                          "Error could not transfer Event (local name'" << localName
                          << "'; remote name '" << remoteName << "', remoteEndPoint ,"
                          << remoteEndPoint << "'; errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "'; GlobalID: " << gid
                          << "; Level1Id: 0x" << std::hex << l1id << std::dec << "). Retry. ",
                          ERS_EMPTY,
                          ((std::string) localName)
                          ((std::string) remoteName)
                          ((std::string) remoteEndPoint)
                          ((std::string) errorMessage)
                          ((int)         errorCode)
                          ((std::uint64_t) gid)
                          ((std::uint32_t) l1id))

  /// SfoOutput issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          SfoWatchdogTimerIssue,
                          dcm::OutputIssue,
                          "SFO connection watchdog timer error (errorMessage '" << errorMessage
                          << "'; errorCode " << errorCode << "). ",
                          ERS_EMPTY,
                          ((std::string) errorMessage)
                          ((int)         errorCode))


  /// L1R fetching issue:
  ERS_DECLARE_ISSUE_BASE( dcm,
                          NoSFOFound,
                          dcm::OutputIssue,
                          "No enabled SFO found. ",
                          ERS_EMPTY,
                          ERS_EMPTY)


  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // These two are needed by the RC. The "from" is mandatory

  /// Issue for HLTPU crash
  ERS_DECLARE_ISSUE_BASE( dcm, HltpuCrashIssue, dcm::Issue,
    "HltpuCrashIssue: " << reason << "; from " << from,
    ERS_EMPTY,
    ((const char*) reason) ((std::string) from)
  )

  /// Issue for HLTPU crash
  ERS_DECLARE_ISSUE_BASE( dcm, HltpuTimeoutIssue, dcm::Issue,
    "HltpuTimeoutIssue: " << reason << "; from " << from,
    ERS_EMPTY,
    ((const char*) reason) ((std::string) from)
  )

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  #define DCM_WARNING( ISSUE, MESSAGE )     \
  do {                                      \
    std::ostringstream out;                 \
    out << MESSAGE;                         \
    ISSUE i( ERS_HERE, out.str().c_str() ); \
    ers::warning(i);                        \
  } while(0)

  #define DCM_ERROR( ISSUE, MESSAGE )       \
  do {                                      \
    std::ostringstream out;                 \
    out << MESSAGE;                         \
    ISSUE i( ERS_HERE, out.str().c_str() ); \
    ers::error(i);                          \
  } while(0)

  #define DCM_FATAL( ISSUE, MESSAGE )       \
  do {                                      \
    std::ostringstream out;                 \
    out << MESSAGE;                         \
    ISSUE i( ERS_HERE, out.str().c_str() ); \
    ers::fatal(i);                          \
  } while(0)




  //#define EFD_INFO( MESSAGE )     ERS_INFO(     "[" << efd::name() << "]{" << efd::gettid() << "} " << MESSAGE )
  //#define EFD_LOG( MESSAGE )      ERS_LOG(      "[" << efd::name() << "]{" << efd::gettid() << "} " << MESSAGE )
  //#define EFD_DEBUG( L, MESSAGE ) ERS_DEBUG( L, "[" << efd::name() << "]{" << efd::gettid() << "} " << MESSAGE )
}

#endif
