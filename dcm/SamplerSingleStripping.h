#ifndef SAMPLERSINGLESTRIPPING_H
#define SAMPLERSINGLESTRIPPING_H

#include "dcm/Sampler.h"
#include <atomic>
#include <vector>
#include <deque>
#include <mutex>
#include <memory>
#include <chrono>
#include <thread>
#include <condition_variable>
#include "emon/EventSampler.h"
#include "emon/EventChannel.h"
#include "emon/SelectionCriteria.h"
#include "emon/PullSampling.h"
#include "emon/PullSamplingFactory.h"
#include "emon/Util.h"


namespace daq
{
namespace dcm
{

// Forward declarations
class EventBuilder;


/*!
  \class SamplerSingleStripping is a sampler for single stripped events
  \brief Samples single stripped events submitted by the EventBuilder
  \author Christophe Meessen <meessen@cppm.in2p3.fr>

  This class samples single stripped events for emon.
*/
class SamplerSingleStripping : public Sampler
{
public:
  /*!
    \brief Construct a sampler for single stripping events
    \param partition a reference on the partition in which the dcm runs
    \param maxChannels maximum number of active channels
    \param maxEvents maximum number of events in the event queues
    \param builder event builder

    If the sampler selects an event according to a criteria but the output queue is full,
    the event is skipped and will not be sent to the monitoring.
    If the monitoring is not ready to receive an event queued for output, the event is
    skipped as well and will not be sent to the monitoring.
  */
  SamplerSingleStripping(const IPCPartition & partition, std::uint32_t maxChannels,
                         uint32_t maxEvents, EventBuilder* builder);

  //! Virtual destructor
  ~SamplerSingleStripping() override
  {
    m_sampler.reset(); // Probably not required
    ERS_LOG("Destructing Sampler");
  }

  /*! \brief Submit a DCM event for sampling
      \param event DEC Event to sample
   */
  void sample( const Event& event ) override;

protected:

  /*! DCM Event Sampler architectur and behavior
   *
   *  _________        _________        ____________        ______________
   * |         | 1  N |         | 1  1 |            | N  1 |              |
   * | Sampler | <--- | Channel | <--> | EventQueue | <--- | EventSampler |
   * |_________|      |_________|      |____________|      |______________|
   *
   * The Sampler constructor creates the EventSampler object of libemon and
   * passes a singleton ChannelFactory to it as an EventQueue object factory.
   * The EventQueue object is derived from PullSampler. It contains a queue of
   * selected events waiting to be sent out to emon client.
   *
   * The Sampler destructor deletes first the EventSampler, then all Channel objects.
   * The EventSampler destructor deletes the ChannelFactory and all EventQueue
   * objects.
   *
   * When an emon client connects, the method ChannelFactory::startSampling() is
   * called by a libemon thread. It creates a pending Channel, queued for later
   * activation by the Sampler thread. It doesn't directly append the new Channel
   * to the list active channels because the Sampler thread may be iterating over
   * that list. Also, when a client disconnects from the EventSampler, the
   * corresponding EventQueue is deleted by the libemon thread. To avoid that this
   * pulls the rug under the feets of the Sampler thread, the EventQueue is
   * decoupled from the Channel. Its destructor simply clears the pointer to the
   * EventQueue in the Channel. The Sampler thread can thus safely detect the
   * EventQueue destruction (channel closing) by the libemon thread.
   * The libemon thread polls (sic) the EventQueues at a 1Hz rate for pending
   * events to send.
   *
   * Events are serialized with singleStripping strategy. This means that
   * sampled events contain the union of all ROBs specified by the streamTags.
   *
   * Each Channel has an associated EventQueue with a storage size limit (maxEvents).
   * When the queue is full no new event is added to that queue.
   *
   * A channel may become stalled if a client doesn't read data from the socket.
   * The queue will fill up with events and the queue size limit will ensure
   * that memory usage won't increase because of that channel. However, users
   * may create other stalled channels. The worst case memory usage is then
   * maxChannels x maxEvents.
   */

  //! Buffer to store a copy of a serialized Event
  struct Buffer : public std::enable_shared_from_this<Buffer>
  {
    std::vector<std::uint32_t> data; // use data.assign() to write into buffer
    Buffer()
    {
      static size_t id = 0;
      m_id = ++id;
      ERS_LOG("Sampler: Construct Buffer id:" << m_id);
    }
    ~Buffer()
    {
      ERS_LOG("Sampler: Destruct Buffer id:" << m_id
              << " (capacity=" << data.capacity() << " words)");
    }
    size_t m_id;
  };

  class EventQueue;

  class Channel : public std::enable_shared_from_this<Channel>
  {
  public:
    Channel(const emon::SelectionCriteria &criteria) :
      m_criteria(criteria), m_eventQueue(NULL)
    {
      static size_t id = 0;
      m_id = ++id;
      ERS_LOG("Sampler: Construct Channel id:" << m_id
              << " : " << emon::construct_name(m_criteria));
    }
    ~Channel()
    {
      ERS_LOG("Sampler: Destruct Channel id:" << m_id
              << " : " << emon::construct_name(m_criteria));
      ERS_ASSERT(m_eventQueue == NULL);
    }
    bool is_closed() const
    { // Called by sampler thread
      std::lock_guard<std::mutex> lock(m_channelMtx);
      return m_eventQueue == NULL;
    }
    bool is_selected(const Event& event);
    void queue_event(std::shared_ptr<Buffer> event)
    { // Called by sampler thread
      std::lock_guard<std::mutex> lock(m_channelMtx);
      if (m_eventQueue)
        m_eventQueue->queue_event(event);
    }
    void set_eventQueue(EventQueue *eventQueue)
    { // Called by libemon thread
      std::lock_guard<std::mutex> lock(m_channelMtx);
      m_eventQueue = eventQueue;
    }
    const emon::SelectionCriteria& criteria() const
    {
      return m_criteria;
    }
    size_t id() const
    {
      return m_id;
    }
  private:
    mutable std::mutex            m_channelMtx;
    const emon::SelectionCriteria m_criteria;
    EventQueue                   *m_eventQueue;
    size_t                        m_id;
  };

  class EventQueue : public emon::PullSampling
  {
  public:
    EventQueue(std::shared_ptr<Channel> channel, size_t maxEvents) :
      m_channel(channel), m_maxEvents(maxEvents)
    {
      static size_t id = 0;
      m_id = ++id;
      ERS_LOG("Sampler: Construct EventQueue id:" << m_id);
    }
    ~EventQueue()
    { // Called by libemon thread
      m_channel->set_eventQueue(NULL);
      ERS_LOG("Sampler: Destruct EventQueue id:" << m_id);
    }
    void queue_event(std::shared_ptr<Buffer> event)
    { // Called by sampler thread
      std::lock_guard<std::mutex> lock(m_queueMtx);
      m_queue.push_back(event);
      m_queueCond.notify_all();
    }
    bool is_full() const
    { // Called by sampler thread
      std::lock_guard<std::mutex> lock(m_queueMtx);
      if (m_queue.size() >= m_maxEvents) {
        //ERS_LOG("Sampler: EventQueue id:" << m_id << " is full (skip)");
        return true;
      }
      return false;
    }

/*
    void sampleEvent(emon::EventChannel &ch) override
    { // Called by libemon thread
      std::unique_lock<std::mutex> lock(m_queueMtx);
      if (m_queueCond.wait_for(lock, std::chrono::seconds(1),
                               [this](){return !this->m_queue.empty();})) {
        std::shared_ptr<Buffer> buffer = m_queue.front();
        m_queue.pop_front();
        ch.pushEvent(buffer->data.data(), buffer->data.size());
      }
      // Do nothing if timed out
    }
*/
    void sampleEvent(emon::EventChannel &ch) override     
    { // Called by libemon thread       
      std::shared_ptr<Buffer> buffer;             
      {         
        std::unique_lock<std::mutex> lock(m_queueMtx);         
        if (m_queueCond.wait_for(lock, std::chrono::seconds(1),                                  
                                 [this](){return !this->m_queue.empty();})) {           
          buffer.swap(m_queue.front());           
          m_queue.pop_front();         
        } else {           // Do nothing if timed out           
          return;         
        }       
      }       
      // This relies on the previous section of code to always       
      // return a valid 'buffer' object. Make sure not       
      // to break this assumption     
      ch.pushEvent(buffer->data.data(), buffer->data.size());     
    }

    size_t id() const
    {
      return m_id;
    }
  private:
    std::shared_ptr<Channel>            m_channel;
    mutable std::mutex                  m_queueMtx;
    std::condition_variable             m_queueCond;
    std::deque<std::shared_ptr<Buffer>> m_queue;
    size_t                              m_maxEvents;
    size_t                              m_id;
  };

  struct ChannelFactory : emon::PullSamplingFactory
  {
    ChannelFactory(SamplerSingleStripping& sampler, size_t maxEvents) :
      m_sampler(sampler), m_maxEvents(maxEvents)
    {
      ERS_LOG("Sampler: Construct Channel factory; max events in EventQueue : "
              << maxEvents);
    }
    emon::PullSampling* startSampling(const emon::SelectionCriteria& criteria)
        override
    { // Called by libemon thread
      std::shared_ptr<Channel> channel = std::make_shared<Channel>(criteria);
      EventQueue *eventQueue = new EventQueue(channel, m_maxEvents);
      channel->set_eventQueue(eventQueue);
      std::lock_guard<std::mutex> lock(m_sampler.m_newChannelsMtx);
      m_sampler.m_newChannels.push_back(channel);
      ERS_LOG("Sampler: Add new pending channel id:" << channel->id()
              << " with EventQueue id:" << eventQueue->id()
              << "; number of pending channels: " << m_sampler.m_newChannels.size());
      return eventQueue;
    }
    SamplerSingleStripping& m_sampler;
    size_t                  m_maxEvents;
  };

  //! Add a free buffer to the free buffer recylcing storage
  void addFreeBuffer(Buffer* buffer);

  //! Get a free buffer able to hold the specified amount of data
  std::shared_ptr<Buffer> getFreeBuffer();

  //! Return event as singleStripped to send to emon
  std::shared_ptr<Buffer> getSingleStrippedEvent(const Event& event);

  EventBuilder*                         m_builder;         //!< Event builder
  std::unique_ptr<emon::EventSampler>   m_sampler;         //!< Event sampler object
  std::vector<std::shared_ptr<Channel>> m_channels;        //!< Vector of active sampling channels
  std::vector<std::shared_ptr<Channel>> m_newChannels;     //!< Vector of new channels
  std::mutex                            m_newChannelsMtx;  //!< Mutex securing access to newChannels
  std::vector<std::unique_ptr<Buffer>>  m_buffers;         //!< Free buffer stack for recycling
  std::mutex                            m_buffersMtx;      //!< Mutex for m_freeBuffers access
  const std::uint32_t                   m_maxEvents;       //!< Maximum number of events in EventQueue
};
} // namespace dcm
} // namespace daq

#endif // SAMPLERSINGLESTRIPPING_H
