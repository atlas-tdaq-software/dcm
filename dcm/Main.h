#ifndef DCM_MAIN_H
#define DCM_MAIN_H

#include "dcm/dal/DcmApplication.h"
#include "dcm/info/DCM.h"
#include "dcm/SharedBlockArray/Writable.h"

#include "config/Configuration.h"
#include "dal/Partition.h"
#include "dal/Segment.h"
#include "ipc/partition.h"
#include "is/callbackinfo.h"
#include "is/inforeceiver.h"
#include "monsvc/PublishingController.h"
#include "monsvc/ptr.h"
#include "rc/RunParams.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/OnlineServices.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/circular_buffer.hpp>
#include <TH1I.h>
#include <chrono>
#include <cstdint>
#include <future>
#include <map>
#include <memory>
#include <string>
#include <utility>

namespace daq
{
namespace dcm
{

// Forward declarations
class DataCollector;
class EventBuilder;
class L1Source;
class Processor;
class Trigger;
class Output;

class EventHistograms
{
public:

  EventHistograms(boost::asio::io_service& ioService,
      std::chrono::steady_clock::duration updateInterval,
      float maxSize);
  ~EventHistograms();

  void clear();
  void updateRecents();

  void setEventLocationBin(int bin, double value);
  void setPUsStatusBin(int bin, double value);
  void fillEventSize_kB(double value);

private:

  void timer(const boost::system::error_code& error);

  boost::asio::steady_timer m_timer;
  std::chrono::steady_clock::duration m_updateInterval;

  monsvc::ptr<TH1I> m_EventSize_kB;
  monsvc::ptr<TH1I> m_EventSize_kB_recent;
  std::unique_ptr<TH1I> m_EventSize_kB_last;

  monsvc::ptr<TH1I> m_EventLocation;
  monsvc::ptr<TH1I> m_PUsStatus;

};

/*! \brief Implements the Run Control interface for the DCM
 *
 *  The class inherits from daq::rc::Controllable and implements the methods corresponding
 *  to the transitions relevant for the DCM.
 *
 *  At each transition the class is supposed to coordinate and synchronize the operations
 *  among the DCM modules taking account of the priorities and the "location" of events
 *  inside the application.
 *
 */
class Main: public daq::rc::Controllable
{

public:

  Main();

  ~Main() noexcept;

  // Global services

  //! Get the ASIO I/O service
  boost::asio::io_service& ioService()
  {
    return m_ioService;
  }

  //! Get the Shared Block Array
  SharedBlockArray::Writable& sba()
  {
    ERS_ASSERT(m_sba != nullptr);
    return *m_sba;
  }

  // Partition infrastructure

  //! Get the IPC partition
  const IPCPartition& partition() const
  {
    return daq::rc::OnlineServices::instance().getIPCPartition();
  }

  // Static configuration

  //! Get the application name
  std::string applicationName() const
  {
     return daq::rc::OnlineServices::instance().applicationName();
  }

  //! Get the name of the user running the application
  const std::string& userName() const
  {
    return m_userName;
  }

  //! Get the configuration database
  Configuration& confDatabase() const
  {
    return daq::rc::OnlineServices::instance().getConfiguration();
  }

  //! Get the partition object from the configuration database
  const daq::core::Partition& partitionConf() const
  {
    return daq::rc::OnlineServices::instance().getPartition();
  }

  //! Get the DCM application object from the configuration database
  const dal::DcmApplication& applicationConf() const
  {
    return *daq::rc::OnlineServices::instance().getApplication().cast<dal::DcmApplication>();
  }

  // Dynamic configuration

  const std::string& dfConfigServer() const
  {
    return m_dfConfigServer;
  }

  //! Get the run parameters (changing at PREPAREFORRUN)
  const RunParams& runParams() const
  {
    return m_runParams;
  }

  // Monitoring

  //! Get the IS info object
  monsvc::ptr<info::DCM> isInfo()
  {
    return m_dcmInfo;
  }

  //! Get the OH histograms
  EventHistograms& eventHistograms()
  {
    return *m_eventHistograms;
  }

  // Transitions

  void configure(const daq::rc::TransitionCmd& cmd);

  void connect(const daq::rc::TransitionCmd& cmd);

  void prepareForRun(const daq::rc::TransitionCmd& cmd);

  void stopHLT(const daq::rc::TransitionCmd& cmd);

  void stopGathering(const daq::rc::TransitionCmd& cmd);

  void disconnect(const daq::rc::TransitionCmd& cmd);

  void unconfigure(const daq::rc::TransitionCmd& cmd);

  void user(const daq::rc::UserCmd& cmd);

  void onExit(daq::rc::FSM_STATE state) noexcept;

  // Operations

  //! Resolve a remote service using asyncmsg's NameService
  boost::asio::ip::tcp::endpoint resolveServer(const std::string& serverName);

  // L1Source completion methods

  /*! \brief Reports that the L1Source successfully completed the CONNECT transition.
   *
   *  This method is a completion method for L1Source::asyncConnect().
   */
  void onL1SourceConnect();

  /*! \brief Reports that the L1Source failed to complete the CONNECT transition.
   *
   *  This method is a completion method for L1Source::asyncConnect().
   */
  void onL1SourceConnectError(const boost::system::error_code& error);

  /*! \brief Reports that the L1Source successfully completed the PREPAREFORRUN transition.
   *
   *  This method is a completion method for L1Source::asyncStart().
   */
  void onL1SourceStart();

  /*! \brief Reports that the L1Source failed to complete the PREPAREFORRUN transition.
   *
   *  This method is a completion method for L1Source::asyncStart().
   */
  void onL1SourceStartError(const boost::system::error_code& error);

  /*! \brief Reports that the L1Source successfully completed the STOPHLT transition.
   *
   *  This method is a completion method for L1Source::asyncStop().
   */
  void onL1SourceStop();

  /*! \brief Reports that the L1Source failed to complete the STOPHLT transition.
   *
   *  This method is a completion method for L1Source::asyncStop().
   */
  void onL1SourceStopError(const boost::system::error_code& error);

  /*! \brief Reports that the L1Source successfully completed the DISCONNECT transition.
   *
   *  This method is a completion method for L1Source::asyncDisconnect().
   */
  void onL1SourceDisconnect();

  /*! \brief Reports that the L1Source failed to complete the DISCONNECT transition.
   *
   *  This method is a completion method for L1Source::asyncDisconnect().
   */
  void onL1SourceDisconnectError(const boost::system::error_code& error);

  // DataCollector completion methods

  /*! \brief Reports that the DataCollector successfully completed the CONNECT transition.
   *
   *  This method is a completion method for DataCollector::asyncConnect().
   */
  void onDataCollectorConnect();

  /*! \brief Reports that the DataCollector failed to complete the CONNECT transition.
   *
   *  This method is a completion method for DataCollector::asyncConnect().
   */
  void onDataCollectorConnectError(const boost::system::error_code& error);

  /*! \brief Reports that the DataCollector successfully completed the PREPAREFORRUN transition.
   *
   *  This method is a completion method for DataCollector::asyncStart().
   */
  void onDataCollectorStart();

  /*! \brief Reports that the DataCollector failed to complete the PREPAREFORRUN transition.
   *
   *  This method is a completion method for DataCollector::asyncStart().
   */
  void onDataCollectorStartError(const boost::system::error_code& error);

  /*! \brief Reports that the DataCollector successfully completed the STOPHLT transition.
   *
   *  This method is a completion method for DataCollector::asyncStop().
   */
  void onDataCollectorStop();

  /*! \brief Reports that the DataCollector failed to complete the STOPHLT transition.
   *
   *  This method is a completion method for DataCollector::asyncStop().
   */
  void onDataCollectorStopError(const boost::system::error_code& error);

  /*! \brief Reports that the DataCollector successfully completed the DISCONNECT transition.
   *
   *  This method is a completion method for DataCollector::asyncDisconnect().
   */
  void onDataCollectorDisconnect();

  /*! \brief Reports that the DataCollector failed to complete the DISCONNECT transition.
   *
   *  This method is a completion method for DataCollector::asyncDisconnect().
   */
  void onDataCollectorDisconnectError(const boost::system::error_code& error);

  // Output completion methods

  /*! \brief Reports that the Output successfully completed the CONNECT transition.
   *
   *  This method is a completion method for Output::asyncConnect().
   */
  void onOutputConnect();

  /*! \brief Reports that the Output failed to complete the CONNECT transition.
   *
   *  This method is a completion method for Output::asyncConnect().
   */
  void onOutputConnectError(const boost::system::error_code& error);

  /*! \brief Reports that the Output successfully completed the PREPAREFORRUN transition.
   *
   *  This method is a completion method for Output::asyncStart().
   */
  void onOutputStart();

  /*! \brief Reports that the Output failed to complete the PREPAREFORRUN transition.
   *
   *  This method is a completion method for Output::asyncStart().
   */
  void onOutputStartError(const boost::system::error_code& error);

  /*! \brief Reports that the Output successfully completed the STOPHLT transition.
   *
   *  This method is a completion method for Output::asyncStop().
   */
  void onOutputStop();

  /*! \brief Reports that the Output failed to complete the STOPHLT transition.
   *
   *  This method is a completion method for Output::asyncStop().
   */
  void onOutputStopError(const boost::system::error_code& error);

  /*! \brief Reports that the Output successfully completed the DISCONNECT transition.
   *
   *  This method is a completion method for Output::asyncDisconnect().
   */
  void onOutputDisconnect();

  /*! \brief Reports that the Output failed to complete the DISCONNECT transition.
   *
   *  This method is a completion method for Output::asyncDisconnect().
   */
  void onOutputDisconnectError(const boost::system::error_code& error);

  // Processor completion methods

  /*! \brief Reports that the Processor successfully completed the CONNECT transition.
   *
   *  This method is a completion method for Processor::asyncConnect().
   */
  void onProcessorConnect();

  /*! \brief Reports that the Processor failed to complete the CONNECT transition.
   *
   *  This method is a completion method for Processor::asyncConnect().
   */
  void onProcessorConnectError(const boost::system::error_code& error);

  /*! \brief Reports that the Processor successfully completed the PREPAREFORRUN transition.
   *
   *  This method is a completion method for Processor::asyncStart().
   */
  void onProcessorStart();

  /*! \brief Reports that the Processor failed to complete the PREPAREFORRUN transition.
   *
   *  This method is a completion method for Processor::asyncStart().
   */
  void onProcessorStartError(const boost::system::error_code& error);

  /*! \brief Reports that the Processor successfully completed the STOPHLT transition.
   *
   *  This method is a completion method for Processor::asyncStop().
   */
  void onProcessorStop();

  /*! \brief Reports that the Processor failed to complete the STOPHLT transition.
   *
   *  This method is a completion method for Processor::asyncStop().
   */
  void onProcessorStopError(const boost::system::error_code& error);

  /*! \brief Reports that the Processor successfully completed the STOPGATHERING transition.
   *
   *  This method is a completion method for Processor::asyncReset().
   */
  void onProcessorReset();

  /*! \brief Reports that the Processor failed to complete the STOPGATHERING transition.
   *
   *  This method is a completion method for Processor::asyncReset().
   */
  void onProcessorResetError(const boost::system::error_code& error);

  /*! \brief Reports that the Processor successfully completed the DISCONNECT transition.
   *
   *  This method is a completion method for Processor::asyncDisconnect().
   */
  void onProcessorDisconnect();

  /*! \brief Reports that the Processor failed to complete the DISCONNECT transition.
   *
   *  This method is a completion method for Processor::asyncDisconnect().
   */
  void onProcessorDisconnectError(const boost::system::error_code& error);

private:

  enum Transition
  {
    CONNECT,
    START,
    STOP,
    RESET,
    DISCONNECT
  };

  enum TransitionStatus
  {
    L1_SOURCE_DONE = (1 << 0),
    DATA_COLLECTOR_DONE = (1 << 1),
    OUTPUT_DONE = (1 << 2),
    PROCESSOR_DONE = (1 << 3),
    COMPONENT_FAILED = (1 << 31)
  };

  //! Get action timeout
  int32_t actionTimeout() const
  {
    return applicationConf().get_ActionTimeout();
  }

  //! Reset IsInfo
  void resetIsInfo();
  //! Initialize monitoring infrastructure
  void initializeMonitoring();
  //! Monsvc callback function.
  void isInfoCallBack(const std::string& name, info::DCM* dcm);

  void doTransition(Transition transition);
  void updateTransition(std::uint32_t status);

  // Global services

  boost::asio::io_service m_ioService;
  std::unique_ptr<std::thread> m_ioServiceThread;
  std::unique_ptr<boost::asio::io_service::work> m_ioServiceWork;

  std::unique_ptr<ISInfoReceiver> m_infoReceiver;

  std::unique_ptr<SharedBlockArray::Writable> m_sba;

  // Global variables

  std::string m_dfConfigServer;
  std::uint32_t m_fakeRunNumber;
  RunParams m_runParams;
  std::map<std::string, std::pair<boost::asio::ip::tcp::endpoint, bool>> m_serverToEndpoint;
  std::string m_userName;
  std::uint32_t m_maxOutEvts;

  // Components

  std::unique_ptr<L1Source> m_l1Source;
  std::unique_ptr<DataCollector> m_dataCollector;
  std::unique_ptr<Processor> m_processor;
  std::unique_ptr<EventBuilder> m_eventBuilder;
  std::unique_ptr<Output> m_output;

  // Transition status

  Transition m_transition;
  unsigned int m_transitionStatus;
  std::promise<void> m_transitionDone;

  // Monitoring

  monsvc::ptr<info::DCM> m_dcmInfo;
  boost::circular_buffer<std::pair<
      std::chrono::time_point<std::chrono::steady_clock>, info::DCM>> m_oldInfos;
  std::unique_ptr<EventHistograms> m_eventHistograms;
  std::unique_ptr<monsvc::PublishingController> m_publisher;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_MAIN_H)
