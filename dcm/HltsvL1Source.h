#ifndef DCM_HLTSVL1SOURCE_H
#define DCM_HLTSVL1SOURCE_H

#include "dcm/L1Source.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <deque>
#include <memory>
#include <set>
#include <utility>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class HltsvSession;
class Main;
class Processor;

namespace dal
{
class DcmHltsvL1Source;
}

/*! \brief A client retrieving L1Result data from the HLTSV.
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class HltsvL1Source: public L1Source
{

public:

  HltsvL1Source(Main& main, const dal::DcmHltsvL1Source& config);

  //! Implementation of L1Source::initialize()
  void initialize(Processor* processor) override;

  //! Implementation of L1Source::asyncConnect()
  void asyncConnect() override;

  //! Implementation of L1Source::asyncStart()
  void asyncStart() override;

  //! Implementation of L1Source::asyncStop()
  void asyncStop() override;

  //! Implementation of L1Source::asyncDisconnect()
  void asyncDisconnect() override;

  //! Implementation of L1Source::asyncFetchL1R()
  void asyncFetchL1R(std::unique_ptr<Event> event, void* context) override;

  //! Implementation of L1Source::markDone()
  void markDone(const Event* event) override;

private:

  Main& m_main;
  const dal::DcmHltsvL1Source& m_config;

  std::string m_hltsvName;
  std::shared_ptr<HltsvSession> m_session;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_HLTSVL1SOURCE_H)
