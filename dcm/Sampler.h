#ifndef SAMPLER_H
#define SAMPLER_H

#include "Event.h"

namespace daq
{
namespace dcm
{

/*!
  \class Sampler is a pure abstract class for event sampling
  \brief Samples events submitted by the EventBuilder
  \author Christophe Meessen <meessen@cppm.in2p3.fr>

  The Sampler class is given events to sample from the EventBuilder. Events musts
  have been serialized by the EventBuider before the call. Sampled events will be
  submitted to emon.
*/
class Sampler
{
public:
  //! Virtual destructor doing nothing
  virtual ~Sampler() {}

  /*! \brief Samples the given event
      \param event to sample
   */
  virtual void sample( const Event& event ) = 0;
};
} // namespace dcm
} // namespace daq

#endif // SAMPLER_H
