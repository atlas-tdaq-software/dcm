#ifndef DCM_EVENT_H
#define DCM_EVENT_H

#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/StreamTag.h"
#include "dcm/SharedBlockArray/Writable.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/ROBFragment.h"

#include <cstdint>
#include <vector>
#include <chrono>
#include <ostream>

#include <boost/date_time.hpp>

namespace bpt = boost::posix_time;

namespace daq
{
namespace dcm
{

/*!
 @class Event
 @brief Event object moved around the DCM internal dataflow
 @author Christophe Meessen <meessen@cppm.in2p3.fr>
 @author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 @author Andrea Negri <Andrea.Negri@pv.infn.it>

 The Event class is defined as a structure to provide direct access to member variables
 instead of using getter and setter methods. The rationale is that it's interface is not
 exposed to third party code.
 */

class Event
{
public:

  /*!
   @struct Stream
   @brief Serialized event with streamTags
   */
  struct Stream
  {
    /*!
     @struct Info
     @brief StreamTag relevant information
     */
    struct Info
    {
      Info(const std::string& name, const std::string& type, size_t idx) :
          name(name), type(type), idx(idx)
      {
      }
      std::string name; //!< StreamTag name
      std::string type; //!< StreamTag type
      size_t idx; //!< Index of the streamTag in the streamTag vector
    };

    Stream(const std::string& name, const std::string& type, size_t idx, const void* data, size_t size) :
        data(data), size(size)
    {
      tags.emplace_back(name, type, idx);
    }
    void addTag(const std::string& name, const std::string& type, size_t idx)
    {
      tags.emplace_back(name, type, idx);
    }

    std::vector<Info> tags; //!< Vector of streamTag relevant information
    const void* data; //!< Pointer on first byte of event data
    size_t size; //!< Data size in bytes
  };

  /*!
   @brief Construct an Event data container instance
   @param sba Reference on the SharedBlockArray in which to allocate the block

   The constructor allocates a block in the SharedBlockArray and the destructor
   deallocates it.
   */
  Event(SharedBlockArray::Writable &sba);

  //! Destructor clears the block
  ~Event();

  //! Reference on the Shared block array containing the block
  SharedBlockArray::Writable& sba;

  //! Reference to a SharedBlockArray block containing the event ROBs
  SharedBlockArray::Writable::Block& block;

  //! Allocate a block of nbr_uint32 uint32 garbage collected by the Event destructor
  std::uint32_t *allocate(size_t nbr_uint32) const;

  //! True if the event has been marked done
  bool markedDone;

  //! Types of force accept
  class ForceAccept {
  public:
    ForceAccept() : m_code(0) {}
    bool closeSession() { return (m_code & 0x4000) != 0; }
    bool fetchAllRobs() { return (m_code & 0x2000) != 0; }
    static const ForceAccept NONE;
    static const ForceAccept HLTSV_REASSIGN;
    static const ForceAccept HLTPU_CRASH;
    static const ForceAccept HLTPU_TIMEOUT;
    static const ForceAccept FETCHROBS_ERROR;
    static const ForceAccept L1ID_MISMATCH;
    static const ForceAccept DFINTERFACE_ERROR;
    static const ForceAccept INVALID_ROBID_ERROR;
    std::string str() const;
    bool operator==(const ForceAccept val) { return val.m_code == m_code; }
    bool operator!=(const ForceAccept val) { return val.m_code != m_code; }
    friend std::ostream& operator<<(std::ostream& os, const Event::ForceAccept val);
  private:
    ForceAccept(int code) : m_code(code) {}
    std::uint32_t m_code;
  };

  //! Set the current type of forceAccept (set to HLTSV_REASSIGN by L1Source)
  ForceAccept forceAccept;

  //! Level-1 result ROBs
  std::vector<SharedBlockArray::RobInfo*> l1Result;

  //! Global ID of the event
  uint64_t globalId;

  //! Level-1 ID of the event
  std::uint32_t l1Id;

  //! Luminosity block
  std::uint16_t lumiBlock;

  //! Bunch crossing ID of the event
  std::uint32_t bcId;

  //! True if some fragments in the event have a different Level-1 ID
  bool hasL1IdMismatch;

  //! True if some fragments in the event have a different Level-1 ID
  bool hasBcIdMismatch;

  //! Event Filter trigger info for this event, returned by the HLTPU
  std::vector<std::uint32_t> hltpuTriggerInfo;

  //! Stream tags for this event, normally returned by the HLTPU
  std::vector<eformat::helper::StreamTag> streamTags;

  //! Stream tags stripped of monitoring streamTags
  std::vector<eformat::helper::StreamTag> outputStreamTags;

  //! Status error codes, normally returned by the HLTPU
  std::vector<std::uint32_t> pscErrors;

  //! HLT result ROBs
  std::vector<SharedBlockArray::RobInfo*> hltResult;

  //! Serialized full event without any robs used as header template
  const std::uint32_t* eventHeader;

  //! Vector of serialized events
  std::vector<Stream> streams;

  //! Event life start time (period ends when event is destroyed)
  std::chrono::steady_clock::time_point lifeStartTime;

  //! Event processing start time (period ends when processing ends)
  std::chrono::steady_clock::time_point processingStartTime;
  
  //! Event acknowledge time (ack sent to hltsv)
  std::chrono::steady_clock::time_point ackTime;

  //! Event data collection start time (period ends when ROBs are collected)
  std::chrono::steady_clock::time_point lastDataCollectionStartTime;

  //! Total data collection duration
  std::chrono::steady_clock::duration dataCollectionDuration;

  //! CTP ROB fragment (ctpROB.start() is NULL if undefined)
  eformat::ROBFragment<const std::uint32_t*> ctpROB;

  //! Pointer to the after last data words stored in the block
  const std::uint32_t* lastDataEnd;

  //! True if the event is a possible duplicate
  bool possibleDuplicate;

  //! Number of retries to submit to SFO when not RUNNING
  std::uint32_t nbrRetries;

  // Debugging hack
  const uint32_t *inputEvent;

private:
  mutable std::vector<std::unique_ptr<std::uint32_t[]>> m_gcBlocks;

};

inline std::ostream& operator<<(std::ostream& os, const Event::ForceAccept val) {
  os << val.str();
  return os;
}

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_EVENT_H)
