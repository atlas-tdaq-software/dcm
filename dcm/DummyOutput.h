#ifndef DUMMYOUTPUT_H
#define DUMMYOUTPUT_H

#include "dcm/Output.h"
#include <string>
#include <cstdio>

namespace daq
{
  namespace dcm
  {

    class EventBuilder;

    class DummyOutput : public Output
    {
    public:
      /*! @brief Construct DummyOutput with binary file in which to write received events
          @param outputFileName Name of file used as output (must not exist)
       */
      DummyOutput( const std::string& outputFileName );

      /*! @brief Close the output file
       */
      ~DummyOutput() { ::close(m_fd); }

      /*! @brief Configures the Output
          @param eventBuilder Pointer to the EventBuilder submitting Events to send

          The Output instance must be configured with a non null EventBuilder pointer
          otherwise a misconfiguration issue must be raised.

          The Output may be reconfigured, but the behavior is undefined if there are pending
          callbacks.
       */
      void initialize( EventBuilder* eventBuilder ) { m_eventBuilder = eventBuilder; }

      /*! @brief Writes the reveived eformat event into the binary file
          @param event Event to write into the binary file

          The Ouput instance must call EventBuilder::onEventSent() or
          EventBuilder::onEventSentError() as a callback on completion.
       */
      void asyncEventSend( std::unique_ptr<Event> event );

    protected:
      EventBuilder *m_eventBuilder;  //!< Pointer to event buuilder instance
      int           m_fd;            //!< File descriptor to output file
    };
  }
}

#endif // DUMMYOUTPUT_H
