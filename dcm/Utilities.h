#ifndef DCM_UTILITIES_H
#define DCM_UTILITIES_H

#include <cstdint>
#include <memory>
#include <string>
#include <utility>


namespace daq
{
namespace dcm
{ 



/*!
 * \brief  Read diskIO write_bytes (from /proc/self/io)
 * \param  label: field to return (eg: "write_bytes:")
 * \return Number of bytes this process caused to be sent to the storage layer
 */
uint64_t procSelfIo(std::string label);


/*!
 * \brief  Calculate the node CPU usage (from /proc/stat)
 * \return Cpu usage in percentage
 */
float cpuUsageByNode();

/*!
 * \brief  Calculate the process CPU usage
 * \return Cpu usage in percentage
 */
float cpuUsageByProcess(); 

/*!
 * \brief  Utility to monitor the traffic on network interface
 * \return Cpu usage in percentage
 * \param[in] Interface  Name of the NIC to monitor
 * \param[out] recv      Received bytes
 * \param[out] sent      Sent bytes
 */
bool getEthTraffic(std::string& interface, std::uint64_t& recv, std::uint64_t& sent);

#if __cplusplus <= 201103L

template<class T>
struct _Unique_if
{
  typedef std::unique_ptr<T> _Single_object;
};

template<class T>
struct _Unique_if<T[]>
{
  typedef std::unique_ptr<T[]> _Unknown_bound;
};

template<class T, size_t N>
struct _Unique_if<T[N]>
{
  typedef void _Known_bound;
};

template<class T, class ... Args>
typename _Unique_if<T>::_Single_object make_unique(Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template<class T>
typename _Unique_if<T>::_Unknown_bound make_unique(size_t n)
{
  typedef typename std::remove_extent<T>::type U;
  return std::unique_ptr<T>(new U[n]());
}

template<class T, class ... Args>
typename _Unique_if<T>::_Known_bound make_unique(Args&&...) = delete;

#else

using std::make_unique;

#endif

template <class T1, class T2>
std::unique_ptr<T1> static_pointer_cast(std::unique_ptr<T2>&& ptr)
{
  return std::unique_ptr<T1>(static_cast<T1*>(ptr.release()));
}

// Fisher-Yates shuffle
template<typename ForwardIterator>
ForwardIterator random_shuffle_n(ForwardIterator begin, ForwardIterator end, std::size_t num_random)
{
  size_t left = std::distance(begin, end);
  while (num_random--) {
    ForwardIterator r = begin;
    std::advance(r, std::rand() % left);
    using std::swap;
    swap(*begin, *r);
    ++begin;
    --left;
  }
  return begin;
}

template<class TimeValueContainer>
static typename TimeValueContainer::value_type::second_type time_weighted_average(const TimeValueContainer& pairs)
{
  typedef typename TimeValueContainer::const_iterator RandomAccessIterator;
  typedef typename TimeValueContainer::value_type::first_type TimePoint;
//  typedef typename TimeValueContainer::value_type::second_type Value;
  typedef decltype(TimePoint() - TimePoint()) Duration;

  if (pairs.begin() == pairs.end()) {
    return 0;
  } else if ((pairs.begin() + 1) == pairs.end()) {
    return pairs.front().second;
  }
  // c_{i} = pairs[i].second
  // t_{i} = pairs[i].first
  // sum = \sum_{i=0}^{N-1} c_{i} * (t_{i+1} - t_{i})
  Duration sum = Duration();
  for (RandomAccessIterator it = pairs.begin(); (it + 1) != pairs.end(); ++it) {
    sum += ((it + 1)->first - it->first) * it->second;
  }
  return sum / (pairs.back().first - pairs.front().first);
}


} // namespace dcm
} // namespace daq

#endif
