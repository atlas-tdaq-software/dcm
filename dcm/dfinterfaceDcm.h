#ifndef DF_INTERFACE_DCM_H
#define DF_INTERFACE_DCM_H

#include <sys/syscall.h>
#include "eformat/eformat.h"
#include "dcm/SharedBlockArray/Readable.h"
#include "boost/asio.hpp"
#include <cstdint>
#include "dfinterface/dfinterface.h"
#include "hltinterface/DCM_ROBInfo.h"

#include <mutex>

//redefinition of the ERS_LOG with the printing of the tid and the name of the application
#undef  ERS_LOG
#define ERS_LOG( message ) do { \
{ \
   ERS_REPORT_IMPL( ers::log, ers::Message, "{" << daq::dfinterface::gettid() << "} " << message, ERS_EMPTY ); \
} } while(0)


namespace daq
{

/// System error issue
ERS_DECLARE_ISSUE(dfinterface, SystemError,
  message << " " << "(" << category << ": " << number << ")",
  ((std::string) message) ((std::string) category) ((int) number)
)

/// System error issue helper function
namespace dfinterface
{
  inline SystemError makeSystemError(const ers::Context& context, const boost::system::error_code& error)
  {
    return SystemError(context, error.message(), error.category().name(), error.value());
  }

   inline pid_t gettid(){ return syscall(SYS_gettid); }
}


namespace dfinterface
{

// Forward declaration
class DCMEvent;

// Protocol version
const std::uint32_t PROTOCOL_VERSION = 0xEFDF0101;

// asyncmsg Message typeId
enum struct MsgTypeId : std::uint32_t {
  INIT_REQ = 0x00EFDF00,
  INIT_RSP = 0x00EFDF01,

  EVENT_REQ = 0x00EFDF02,
  EVENT_RSP = 0x00EFDF03,

  EVENT_ACCEPTED = 0x00EFDF04, // One way message

  EVENT_REJECTED = 0x00EFDF06, // One way message

  ROBIDS_REQ = 0x00EFDF08,
  ROBIDS_RSP = 0x00EFDF09,

  MAY_GET_ROBS = 0x00EFDF0A, // One way message

  GET_ROBS_REQ = 0x00EFDF0C, // ALL ROBS is with empty robIds list
  GET_ROBS_RSP = 0x00EFDF0D
};

/*! \brief Represents a dfinterface communication Session in the HLTPU to the DCM.
 *
 *  It enables the interaction with a service that provides event data to be analysed (via the
 *  getNext(), tryGetNextUntil() or tryGetNextFor() methods) and is informed of the final
 *  trigger decision (via the accept() or reject() methods).
 *
 *  This class is designed to support only one event processing at the time. All io calls are
 *  blocking forever, except for event requests with timeout.
 *
 *  The createSession factory function expects that the property "dfinterfaceDCM.portNumFile"
 *  is passed to createSession with a string as value providing the file name (with path) that
 *  will contain the tcp port number managed by the DCM to which the Hltpu has to connect to.
 *
 *  In absence of this property, the dfinterfaceDCM will use the default value
 *  "/tmp/DCM_portNum.txt".
 *
 */
class DCMSession : public Session
{
public:
  friend class DCMEvent;

  /*! \brief Session DCM constructor
   *
   *  \param[in] portNoFileName is the name of the file that contains the TCP port number
   *            to which the DCMSession will connect to.
   */
  DCMSession( const std::string& portNoFileName, bool robPreFetching );

  //! \brief Session DCM destructor (closes the Session)
  ~DCMSession();

  /*! \brief Opens the Session.
   *
   *  \param[in] clientName Name of the application opening the Session. The application name
   *      must be suitable to report a misbehaving application to the Run Control infrastructure.
   *
   *  \exception CommunicationError is thrown if the Session can not be created due to
   *      communication issues with the service, e.g. the service is unreachable or does not respond.
   */
  void open(const std::string& clientName) override;

  /*! \brief Determines whether the Session is open.
   */
  bool isOpen() const override;

  /*! \brief Closes the Session.
   */
  void close() override;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Blocks indefinitely while waiting for new events.
   *
   *  \exception NoMoreEvents is thrown if there are no more events to be processed.
   *  \exception CommunicationError is thrown in case of network communication issues.
   */
  std::unique_ptr<Event> getNext() override;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Blocks until an event can be obtained, or the specified time is reached.
   *
   *  \exception OperationTimedOut is thrown if an event could not be obtained before \c absTime.
   *  \exception NoMoreEvents is thrown if there are no more events to be processed.
   *  \exception CommunicationError is thrown in case of network communication issues.
   */
  std::unique_ptr<Event> tryGetNextUntil(
      const std::chrono::steady_clock::time_point& absTime) override;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Equivalent to:
   *  \code
   *  tryGetNextUntil(std::chrono::steady_clock::now() + relTime)
   *  \endcode
   */
  std::unique_ptr<Event> tryGetNextFor(
      const std::chrono::steady_clock::duration& relTime) override;

  /*! \brief Send an event request
   */
  void sendEventRequest();

  /*! \brief Receive the event data
   *  \param[in] hdr The message header
   */
  std::unique_ptr<Event> receiveEventData(std::uint32_t *hdr );

  /*! \brief Marks the event as accepted by the High-Level Trigger.
   *
   *  \param[in] event An \ref Event object obtained from getNext(), tryGetNextUntil() or
   *      tryGetNextFor()
   *  \param[in] triggerInfo Vector of High-Level Trigger information 32-bit words to be stored in
   *      the event header.
   *  \param[in] streamTags Vector of stream tags to be stored in the event header. The stream tags
   *      can contain partial event building lists.
   *  \param[in] pscErrors Vector of 32-bit PSC error words to be stored as additional status words
   *      in the event header.
   *  \param[in] hltFragments Vector of ROBFragment objects containing High-Level Trigger information
   *      to be appended to the event.
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   *
   *  \remark This method performs a deep copy of the \c hltFragment. The memory referenced by it
   *  can be safely freed after the method call.
   */
  void accept(std::unique_ptr<Event> event,
              std::unique_ptr<uint32_t[]> hltr) override;

  /*! \brief Marks the event as rejected by the High-Level Trigger.
   *
   *  \param[in] event An \ref Event object obtained from getNextEvent(),
   *      tryGetNextEventUntil() or  tryGetNextEventFor()
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   */
  void reject(std::unique_ptr<Event> event) override;

  /*! \brief Read size bytes of data from the socket and write them into buf
   *
   *  \param[in] buf Pointer of the first byte of the buffer where to write the received data
   *  \param[in] byteSize Size in bytes of the data to read
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   *
   *  The read operation is atomic and will block until all size bytes have been read
   */
  void read( void* buf, size_t byteSize );

  /*! \brief Write the size bytes of data to the socket
   *
   *  \param[in] data Pointer of the first byte of data to write to the socket
   *  \param[in] byteSize Size in bytes of the data to write
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   *
   *  The write operation is atomic and will block until all size bytes have been written
   */
  void write( const void* data, size_t byteSize );

  /*! \brief Return the list of robIds expected to be requested to the DCM
   *
   *  \return a vector of robIds expected to be requested to be DCMEvent
   */
  virtual std::vector<uint32_t> getRobIds();

  /*! \brief Make sure the buffer has room for at least byteSize bytes
   *
   *  \param[in] byteSize Size in bytes of the buffer
   */
  void adjustBufferSize( std::uint32_t byteSize )
  {
    if( byteSize > m_bufferByteSize )
    {
      m_bufferByteSize = (byteSize + 3) & ~3; // Upper rounding to multiple of word size
      m_buffer.reset(new std::uint32_t[m_bufferByteSize>>2]);
    }
  }

  /*! \brief return pointer on buffer data
   *
   *  \return pointer on buffer data
   */
  std::uint32_t* buffer() const { return m_buffer.get(); }

  /*! \brief return pointer on sharedBlockArray read instance
   *
   *  \return pointer on SharedBlockArray::Read instance
   */
  daq::dcm::SharedBlockArray::Readable* sba() { return m_sba.get(); }

private:
  enum State
  {
    CLOSED,
    IDLE,
    WAITING_EVENT,
    PROCESSING_EVENT
  };


  boost::asio::io_service m_ioService;   //!< asio service used by this Session
  boost::asio::ip::tcp::socket m_socket; //!< session socket to DCM server
  std::string m_portNumFileName;         //!< File containing DCM TCP port number (set by constructor)
  bool m_robPreFetching;                 //!< If true DCMEvent::mayGetRobs actually calls DCMEvent::getRobs
  std::unique_ptr<dcm::SharedBlockArray::Readable> m_sba; //!< Read only SharedBlockArray
  std::unique_ptr<DCMEvent> m_event;     //!< cached event for single threaded event processing
  std::unique_ptr<uint32_t[]> m_buffer;  //!< data receiving Buffer grown as needed
  std::uint32_t m_bufferByteSize;        //!< Buffer size in bytes
  std::vector<uint32_t> m_robIds;        //!< vector of robIds we may ask to the DCM
  std::string m_remoteName;              //!< Name of remote peer end
  DCMSession::State m_state;             //!< Current internal state of the dfinterface
};

/*! \brief Provides access to the event data.
 *
 *  \remark \ref Event pointers are obtained from a \ref Session instance and they must be returned
 *  to that instance when the processing is complete.
 */
class DCMEvent : public Event
{
public:
  /*! \brief Constructor
   *
   *  \param[in] fd is file descriptor of open and initialized tcp connection to DCM
   *  \param[in] data Pointer on first word of Event data in the SharedBlockArray
   *  \param[in] l1Id level 1 id identifying this event
   *  \param[in] offsets to ROI ROBs (l1Result) relative to eventData
   */
  DCMEvent( DCMSession& session );

  /*! \brief Constructor
   *
   *  \param[in] data Array of data words received as response to an event request
   *  \param[in] size Number of data received
   */
  void initialize( const std::uint32_t* data, std::uint32_t byteSize );

  /*! \brief Clear the cached transient event data
   */
  void clear();

  //! \brief Destructor
  ~DCMEvent();

  /*! \brief Returns the Level-1 ID of the event.
   *  \note  The interface implementation has to keep track of the Level-1 ID
   */
  std::uint32_t l1Id() override;

  /*! \brief Returns the flobal ID of the event.
  */
  std::uint64_t gid() override;

  /*! \brief Returns the luminosity block of the event.
   */
  std::uint16_t lumiBlock() override;

  /*! \brief Request l1results of the next event
   *
   * A FullEventFragment is filled with partial event data containing at the
   * minimum the L1 RoI ROBs and a correctly filled event header. The fragment
   * is serialised and the ownership is passed to \p l1Result. The caller is
   * responsible for releasing the pointer after use.
   *
   * The following fields of the header should be correctly set to the
   * respective values for a given event:
   * version, global_id, run_type, run_no, lumi_block, bc_id, bc_time_seconds,
   * bc_time_nanoseconds, lvl1_id, lvl1_trigger_type, lvl1_trigger_info
   *
   * In addition:
   * source_id should be FULL_SD_EVENT
   * status should be normally 0
   * compression_type should be UNCOMPRESSED
   * checksum can be NO_CHECKSUM
   *
   *  \param[out] l1Result reference to a unique_ptr which should take ownership of a
   *      serialized FullEventFragment containing LVL1 result ROBs and event header
  */
  void getL1Result(std::unique_ptr<uint32_t[]>& l1Result) override;

  /*! \brief Informs the dfinterface implementation that the specified ROBs are likely to be requested in
   *      the future.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   */
  void mayGetRobs(const std::vector<uint32_t>& robIds) override;

  /*! \brief Retrieves the specified ROB fragments.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *  \param[out] robs The requested ROBFragment objects are pushed back into this vector.
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead.
   */
  void getRobs(const std::vector<uint32_t>& robIds,
               std::vector<hltinterface::DCM_ROBInfo>& robInfos) override;

  /*! \brief Actually performing the retrieving of the specified ROB fragments.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *  \param[out] robs The requested ROBFragment objects are pushed back into this vector.
   *
   *  See getRobs for more info.
   *  NB: this function is not protected by any mutex. The function MUST be called from a protected function (getRobs, mayGetRobs)
   */
  void getRobs_unprotected(const std::vector<uint32_t>& robIds,
    std::vector<hltinterface::DCM_ROBInfo>& robInfos );

  /*! \brief Retrieves all the available ROB fragments.
   *
   *  \param[out] robInfos The requested ROBFragments object are pushed back into this vector
   *              together with DC time statistics according to the structure hltinterface::DCM_ROBInfo
   *
   *  \exception CommunicationError is thrown in case of network communication issues.
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead. Fragments already retrieved with get_robs() are nevertheless included in
   *  \c robs.
   */
  virtual void getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robInfos) override;

  //! Return the event identifier
  std::uint32_t eventId() const { return m_eventId; }

  //! Tryes to lock a timed_mutex for n minutes, returning false if not succeed
  bool attempt_lock_for_n_minutes( std::unique_lock<std::timed_mutex>&, const uint32_t );

private:
  void getTheRobs(const std::vector<uint32_t>& robIds);

  DCMSession &m_session;                              //!< Reference to session
  dcm::SharedBlockArray::Readable::Block m_block;     //!< Event data block
  std::uint32_t m_l1Id;                               //!< Level 1 identifier
  std::uint32_t m_eventId;                            //!< Event identifier
  std::uint64_t m_globalId;                           //!< Event global identifier
  std::uint16_t m_lumiBlock;                          //!< Luminosity Block number
  std::vector<eformat::read::ROBFragment> m_l1Result; //!< Level 1 result (ROI)
  eformat::write::FullEventFragment m_fullEvent;      //!< Full event representation
  bool m_fetchedAllRobs;                              //!< True if fetched all robs
  std::timed_mutex m_local_mtx;                       //!< Mutex to block concurrent getRobs requests
};
} // namespace dfinterface
} // namespace daq
#endif // DF_INTERFACE_DCM_H

