#ifndef DCM_DUMMYL1SOURCE_H
#define DCM_DUMMYL1SOURCE_H

#include "dcm/L1Source.h"

#include "dccommon/Distribution.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <deque>
#include <memory>
#include <set>
#include <utility>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class Main;
class Processor;

namespace dal
{
class DcmDummyL1Source;
}

/*! \brief A provider of internally generated L1Result data
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class DummyL1Source: public L1Source
{

public:

  DummyL1Source(Main& main, const dal::DcmDummyL1Source& config);

  //! Implementation of L1Source::initialize()
  void initialize(Processor* processor) override;

  //! Implementation of L1Source::asyncConnect()
  void asyncConnect() override;

  //! Implementation of L1Source::asyncStart()
  void asyncStart() override;

  //! Implementation of L1Source::asyncStop()
  void asyncStop() override;

  //! Implementation of L1Source::asyncDisconnect()
  void asyncDisconnect() override;

  //! Implementation of L1Source::asyncFetchL1R()
  void asyncFetchL1R(std::unique_ptr<Event> event, void* context) override;

  //! Implementation of L1Source::markDone()
  void markDone(const Event* event) override;

private:

  struct FetchL1RRequest
  {
    FetchL1RRequest(std::unique_ptr<Event> event_, void* context_) :
        event(std::move(event_)),
        context(context_)
    {
    }

    std::unique_ptr<Event> event;
    void* context;
  };

  void startTimer();

  Main& m_main;
  const dal::DcmDummyL1Source& m_config;
  Processor* m_processor;

  DC::Distribution m_delayDistribution_us;
  DC::Distribution m_sizeDistribution_words;

  std::deque<FetchL1RRequest> m_requests;
  std::uint64_t m_lastGlobalId;
  std::set<std::uint32_t> m_pendingL1Ids;
  boost::asio::steady_timer m_timer;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_DUMMYL1SOURCE_H)
