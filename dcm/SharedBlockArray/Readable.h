#ifndef READABLE_H
#define READABLE_H

#include <string>

#include "dcm/SharedBlockArray/Block.h"

namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

/*!
 * \brief The Readable class provides read only access to the RobInfo and ROBs in the
 *        SharedBlockArray file. It can also be used to recover data.
 *
 * Access to data is performed by mapping a Readable::Block object.
 *
 * The recover() method returns a vector of pair<blockId, words> where words is the
 * number of recoverable data words in the block which contains robInfo structure and
 * ROBs data.
 *
 * The user may then iterate on the vector, and for blocks with a non zero number of
 * recoverable words he can instantiate a Readable::Block with the method Readable::block(blockId)
 * and then call appendAndInsertRobInfos(words) to fill the robInfoIndex.
 * He may then iterate the robInfoIndex to access the ROBs. The information that was
 * stored in the robInfo can be recovered as well.
 */
class Readable
{
public:

  //! Recoverable info returned by recover
  struct RecoverableInfo { std::uint32_t blockId; std::uint32_t words; };


  /*!
   * \brief The Block class provides read only access to a mapped block
   *
   * It encapsulates a shared_ptr to the mapped block so that it can be efficiently
   * copied and passed as function argument by value. The mapping is destroyed when
   * the last copy of the block is destroyed.
   *
   * This allows to only map in memory the portion of the file covered by the Block.
   *
   * The block has its own robInfoIndex which may be desynchronized with the
   * Writable::Block mapping the same portion of the file. The Readable::Block robInfoIndex
   * may be resynchronized by calling appendAndInsertRobInfos(newDataWordSize) where
   * dataWordSize is the number of words in the writable data block obtained by the call
   * Writable::Block::dataWordSize().
   *
   * It may also be the number of recoverable words returned by the Readable::recover()
   * method.
   */
  class Block
  {
    friend class Readable;
  public:

    //! Block factory method
    static Readable::Block create( int fd, size_t blockByteOffset, size_t blockByteSize );

    //! Default constructor required by vector
    Block() {}

    //! Return reference to const RobInfo index
    const RobInfoIndex& robInfoIndex() const
    {
      static RobInfoIndex emptyRobInfoIndex;
      return m_block?m_block->robInfoIndex():emptyRobInfoIndex;
    }

    //! Return a non modifiable RobInfo associate to robId or nullptr if not in index
    const RobInfo* robInfo(RobId robId) const { return m_block?m_block->robInfo(robId):nullptr; }

    //! Subscript operator returning a non modifiable RobInfo or nullptr if not in index
    const RobInfo* operator[] (RobId robId) const { return robInfo(robId); }

    /*!
   * \brief if newDataWordSize is bigger than the current block data size in words, insert found
   *        RobInfos into robInfoIndex, and set the block data size to the given newDataWordSize
     * \param newDataWordSize New number of data words in the block
     *
     * warning: This method may modify the robIndIndex and will then invalidate iterators
     * in the robInfoIndex assigned before the call
     */
    void appendAndInsertRobInfos( std::uint32_t newDataWordSize )
    {
      if (m_block)
        m_block->appendAndInsertRobInfos(newDataWordSize);
    }

  protected:
    //! Construct block
    Block(std::shared_ptr<SharedBlockArray::Block> block) : m_block(block) {}

    mutable std::shared_ptr<SharedBlockArray::Block> m_block; //!< The mapped block
  };

  /*!
   * \brief Open the SharedBlockArray file for read only access
   * \param fileName File name of the SharedBlockArray
   * \throw SBAInvalidArgument if fails to open the file or it isn't a valid SharedBlockArray
   */
  Readable( const std::string& fileName );

  //! Close the SharedBlockArray file
  ~Readable();

  /*!
   * \brief Returns a pointer on the non modifiable block with the specified block identifier
   * \param blockId Identifier of block in the SharedBlockArray [0..nBlocks-1]
   * \return a pointer on the mapped block
   * \throws SBAInvalidArgument if the index is out of range
   */
  Readable::Block block(std::uint32_t blockId);

  /*!
   * \brief Returns a Readable::Block with recovered data as specified by recoverInfo
   * \param recoverInfo A RecoverableInfo entry from the vector returned by Readable::recoverInfos()
   * \return a pointer on the mapped block
   * \throws SBAInvalidArgument if the index is out of range
   *
   * block.robInfoIndex().empty() will return true if no data was recovered. Note that if
   * recoverInfo.words is 0 there won't be any data to recover for sure. But it if it's not
   * 0 it is uncertain if the recoverable data contains robInfo records and ROBs.
   */
  Readable::Block recover( const Readable::RecoverableInfo& recoverInfo )
  {
    Readable::Block b = block(recoverInfo.blockId);
    b.appendAndInsertRobInfos(recoverInfo.words);
    return b;
  }


  /*!
   * \brief Return the memory mapped file name
   * \return the memory mapped file name
   */
  const std::string& fileName() const { return m_fileName; }

  /*!
   * \brief Return a vector of RecoverableInfo for each block in the SharedBlockArray
   * \return a vector of RecoverableInfo for each block in the SharedBlockArray
   *
   * The inteded use is to iterate on the vector to process blocks containing recoverable data.
   * \code
   * Readable readable("myFile.data");
   * for (const auto& recoverInfo : readable.recoverInfos())
   * {
   *   Readable::Block block = readable.recover(recoverInfo);
   *   if (!block.robInfoIndex().empty())
   *   {
   *     < process recovered robInfo data and ROBs >
   *   }
   * }
   * \endcode
   */
  std::vector<RecoverableInfo> recoverInfos() const;

private:
  int           m_fd;                 //!< File descriptor
  std::string   m_fileName;           //!< file name of the SharedBlockArray
  std::uint32_t m_blockByteSize;      //!< block size in bytes
  size_t        m_minBlockByteOffset; //!< Minimum valid offset value
  size_t        m_maxBlockByteOffset; //!< Minimum invalid offset value
  std::uint32_t m_nbrBlocks;          //!< Number of blocks
};

}
}
}

#endif // READABLE_H
