#ifndef RecoverableWordsIndex_H
#define RecoverableWordsIndex_H

#include <cstdint>
#include <cstring>
#include <sys/mman.h>

#include "dcm/Issues.h"


namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{


/*!
 * \brief The RecoverableWordsIndex class keeps track of the number of words recoverable in all Blocks
 */
class RecoverableWordsIndex
{
public:
  /*!
   * \brief Construct the RecoverableWordsIndex initialized to 0 and memory protected
   * \param nBlocks Number of blocks to for whitch to track the number of recoverable bytes
   * \param indexStorage Pointer on the index value storage
   *
   * Requires: the memory is previously cleared to zero for a new file and its protection
   * is the to read only access
   */
  RecoverableWordsIndex( std::uint32_t* header ) : m_memory(header),
    m_byteSize(header[5]), m_nBlocks(header[3]), m_index(header+header[1])
  {
    ERS_ASSERT(::memcmp(header, "SBA\x01", 4) == 0);
  }

  /*!
   * \brief set Sets the number of recoverable words for the block with blockId
   * \param blockId Block identifier to which the value applies
   * \param recoverableDataWordSize Number of recoverable words
   */
  void set( std::uint32_t blockId, std::uint32_t recoverableDataWordSize )
  {
    if (blockId >= m_nBlocks )
      throw dcm::SBAInvalidArgument(ERS_HERE, "BlockId out of range" );
    ::mprotect( m_memory, m_byteSize, PROT_READ|PROT_WRITE );
    m_index[blockId] = recoverableDataWordSize;
    ::mprotect( m_memory, m_byteSize, PROT_READ );
  }

  /*!
   * \brief get Return the number of recoverable words for the block with blockId
   * \param blockId Block identifier from which to get the number of recoverable words
   * \return the number of recoverable words in the block with the given blockId
   */
  std::uint32_t get( std::uint32_t blockId ) const
  {
    if (blockId >= m_nBlocks )
      throw dcm::SBAInvalidArgument(ERS_HERE, "BlockId out of range" );
    return m_index[blockId];
  }

  //! Return the number of blocks in the index
  std::uint32_t numberOfBlocks() const { return m_nBlocks; }

private:
  std::uint32_t*       m_memory;   //!< Pointer on memory protection is controlled
  const std::uint32_t  m_byteSize; //!< Memory section byte size to protect
  const std::uint32_t  m_nBlocks;  //!< Number of blocks in SharedBlockArray
  std::uint32_t* const m_index;    //!< Pointer on first Recoverable value in index
};

}
}
}
#endif // RecoverableWordsIndex_H
