#ifndef ROBINDEX_H
#define ROBINDEX_H

#include <map>
#include <chrono>
#include <cstdint>

#include "ers/Assertion.h"
#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/HeaderMarker.h"

namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

//! The RobId type (with 8 most significant bits set to 0)
typedef std::uint32_t RobId;

/*! The RobInfo struct contains ROB meta information stored in the SharedBlockArray block
 *
 *  Note: RobInfo struct is stored in the SharedBlockArray with 64bit alignment
 */
class RobInfo
{
  const std::int32_t offsetToRob;                //!< Offset to rob data block relative to this* in 32bit word units
public:
  const RobId robId;                             //!< Identifier of the ROB
  std::chrono::steady_clock::time_point reqTime; //!< time point when request was issued by DataCollector
  std::chrono::steady_clock::time_point rspTime; //!< time point when response was recieved by DataCollector

  //! Construct the RobInfo with its robId and pointer to rob data set
  RobInfo(RobId robId, const std::uint32_t* robPtr) :
    offsetToRob(reinterpret_cast<const std::uint32_t*>(this) - robPtr), robId(robId)
  {
  }

  //! Return a const pointer on the ROBFragment data (checks the ROB marker validity)
  const std::uint32_t* robPtr() const
  {
    const std::uint32_t* robPtr = reinterpret_cast<const std::uint32_t*>(this) - offsetToRob;
    ERS_ASSERT(*robPtr == eformat::HeaderMarker::ROB);
    return robPtr;
  }

  //! Return the ROB as a const ROBFragment
  const eformat::read::ROBFragment robFragment() const
  {
    return eformat::read::ROBFragment(robPtr());
  }

};

//! Index of RobInfo* with its RobId as key
typedef std::map<RobId, RobInfo*> RobInfoIndex;


}
}
}

#endif // ROBINDEX_H
