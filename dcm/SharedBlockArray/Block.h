#ifndef BLOCK_H
#define BLOCK_H

#include "dcm/SharedBlockArray/RobInfo.h"
#include "dcm/SharedBlockArray/RecoverableWordsIndex.h"
#include "eformat/ROBFragment.h"


namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

/*!
 * \brief The Block class implements a Writable
 */
class Block
{
public:
  /*!
   * \brief Construct Block for writable access
   * \param start Pointer on first data word in Block
   * \param byteSize Size in bytes of the Block. Must be a multiple of the system memory page
   * \param blockId The identifier of the block. Must be a unique value between 0 and nBlocks -1
   * \param recoverableBytesIndex Pointer on the recoverableBytesIndex of nullptr if not used
   */
  Block( std::uint32_t* start, std::uint32_t byteSize, std::uint32_t blockId,
         RecoverableWordsIndex* recoverableWordsIndex );

  /*!
   * \brief Construct Block for read only access
   * \param start Pointer on first data word in Block
   * \param byteSize Size in bytes of the Block. Must be a multiple of the system memory page
   *
   * This constructor is used for read only access in the HLTPU or as result of recovery
   */
  Block( std::uint32_t* start, std::uint32_t byteSize ) : Block(start, byteSize, 0, nullptr) {}

  //! Clear the block after use
  void clear();

  //! Initialize the block to be used
  void init();

  //! Return true if the block is deallocated
  bool deallocated() const { return m_state == Block::CLEARED; }

  //! Return the block identifier
  std::uint32_t blockId() const { return m_blockId; }

  //! Return pointer on first word of block data
  const std::uint32_t* dataStart() const { return m_dataStart; }

  //! Return pointer on after last word of block data
  const std::uint32_t* dataEnd() const { return m_dataEnd; }

  //! Return the number of data words currently stored in the block
  std::uint32_t dataWordSize() const { return m_dataEnd - m_dataStart; }

  //! Return the number of free bytes remaining in the block
  std::uint32_t freeSpace() const { return m_freeSpace; }

  //! Return the block size in bytes
  std::uint32_t byteSize() const { return m_byteSize; }

  //! Return reference to RobInfo index
  RobInfoIndex& robInfoIndex() { return m_robInfoIndex; }

  //! Return reference to const RobInfo index
  const RobInfoIndex& robInfoIndex() const { return m_robInfoIndex; }

  //! Return a modifiable RobInfo structure associate to robId or nullptr if not in index
  RobInfo* robInfo(RobId robId)
  {
    RobInfoIndex::iterator it = m_robInfoIndex.find(robId);
    return it == m_robInfoIndex.end() ? nullptr : it->second;
  }

  //! Return a non modifiable RobInfo associate to robId or nullptr if not in index
  const RobInfo* robInfo(RobId robId) const
  {
    RobInfoIndex::const_iterator it = m_robInfoIndex.find(robId);
    return it == m_robInfoIndex.end() ? nullptr : it->second;
  }

  //! Subscript operator returning a modifiable RobInfo or nullptr if not in index
  RobInfo* operator[] (RobId robId) { return robInfo(robId); }

  //! Subscript operator returning a non modifiable RobInfo or nullptr if not in index
  const RobInfo* operator[] (RobId robId) const { return robInfo(robId); }

  //! Mark the data appended so far to the block as recoverable
  void setRecoverable()
  {
    if (m_recoverableWords)
      m_recoverableWords->set( m_blockId, m_dataEnd - m_dataStart);
  }

  /*!
    \brief Append one or more ROBs data bytes to the block
    \param data Pointer on the first word of ROB data to append
    \param byteSize Byte size of the ROB data to append to the block content
    \return a vector of pointers to the RobInfo of the robs found in data
    \throws SBAInvalidArgument if the robs are not complete and contiguous
    \throws SBAInvalidArgument if there is not enough free space in the block

    The rob data is copied in the block and the RobInfoIndex is updated.
    More than one rob may be appended with a single call provided the robs are valid and
    contiguous in the the given data block.
   */
  std::vector<RobInfo*> appendRobData( const std::uint32_t* data, std::uint32_t byteSize );

  /*!
    \brief Return a pointer in the block storage where ROB data may be written in place
    \param maxByteSize Maximum number of bytes that might be written in place
    \return 32bit word aligned pointer in the block storage where ROB data may be written in place
    \throws SBAInvalidArgument if maxByteSize is bigger than the amount of free space

    Mulitple ROBs may be written in place. A call to the endAppendRobData(actualByteSize)
    is required to complete appending ROB data in this way.
   */
  std::uint32_t* startAppendRobData( std::uint32_t maxByteSize );

  /*!
    \brief completes appending rob data in place and updates the RobInfoIndex
    \param actualByteSize The actual number of bytes written in place
    \return a vector of pointers to the RobInfo of the robs found in data
    \throws SBAInvalidArgument if the robs are not complete and contiguous
    \throws SBAInvalidArgument if actualByteSize is bigger than maxByteSize

    More than one rob may be appended with a single call provided the robs are valid and
    contiguous in the the given data block.
   */
  std::vector<RobInfo*> endAppendRobData( std::uint32_t actualByteSize );

  /*!
    \brief Append the raw data bytes to the block leaving RobInfoIndex unmodified
    \param data Pointer on the first byte of raw data to append
    \param byteSize Byte size of the raw data to append to the block content
    \param marker Data block marker to use. By default it uses a RAW_DATA_MARKER.
    \return pointer on the first byte of raw data copied inside of the block
    \throws SBAInvalidArgument if there is not enough free space in the block

    The data stored in the block is 32bit word aligned.
   */
  std::uint8_t* appendRawData( const void* data, std::uint32_t byteSize );

  /*!
    \brief Return a pointer in the block storage where raw data may be written in place
    \param maxByteSize Maximum number of bytes that might be written in place
    \return 64bit word aligned pointer in the block storage where raw data may be written in place
    \throws SBAInvalidArgument if maxByteSize is bigger than the amount of free space

    A call to the endAppendRawData(actualByteSize) is required to complete writing raw data.
   */
  std::uint8_t* startAppendRawData( std::uint32_t maxByteSize )
  {
    return startAppendRawData(maxByteSize, Block::RAW_DATA_MARKER);
  }

  /*!
    \brief completes appending rob data in place leaving RobInfoIndex unmodified
    \param actualByteSize The actual number of bytes written in place
    \throws SBAInvalidArgument if actualByteSize is bigger than maxByteSize
   */
  void endAppendRawData( std::uint32_t actualByteSize );

  /*!
   * \brief if newDataWordSize is bigger than the current block data size in words, insert found RobInfos into robInfoIndex,
   *        and set the block data size to the given newDataWordSize
   * \param newDataWordSize New size in words of data stored in the block
   *
   * The size of data currently stored in the block is changed only if newDataWordSize is bigger
   * than the current size. In this case RobInfo records are searched in the appended data
   * and inserted in the robInfoIndex.
   *
   * This method is used by the dfinterfaceDcm to update its local robInfo index after a request
   * for data method returns.
   * It is also intended to be used when recovering data by passing the number of words to recover to this method.
   */
  void appendAndInsertRobInfos( std::uint32_t newDataWordSize );

  /*!
   * \brief Fill the vector of RobFragments found between the start and end data pointer in the block
   *
   * \param[in] begin Pointer on the start of data blocks
   * \param[in] end Pointer on the data word where to stop the search
   * \param[out] robs Vector of RobFragments found between start and end
   * \return a reference on robs
   */
  std::vector<eformat::read::ROBFragment>& getRobFragments( const std::uint32_t* begin, const std::uint32_t* end,
                                                            std::vector<eformat::read::ROBFragment>& robs ) const;

  /*!
   * \brief Fill the vector of RobFragments found between the start and the data end in the block
   *
   * \param[in] begin Pointer on the start of data blocks
   * \param[out] robs Vector of RobFragments found between start and end
   * \return a reference on robs
   */
  std::vector<eformat::read::ROBFragment>& getRobFragments( const std::uint32_t* begin,
                                                            std::vector<eformat::read::ROBFragment>& robs ) const
  {
    return getRobFragments(begin, m_dataEnd, robs);
  }

private:

  /*!
    \brief Parse ROBs found in the data block at dataEnd, append the data block and
           a RobInfo for each found ROB and insert it in the robInfoIndex
    \return a vector of pointers to the RobInfo just inserted in the robInfoIndex
    \throws SBARunTimeError if found an invalid ROB
   */
  std::vector<RobInfo*> parseAndInsertROBs();

  /*!
    \brief Return a pointer in the block storage where raw data may be written in place
    \param maxByteSize Maximum number of bytes that might be written in place
    \return 64bit word aligned pointer in the block storage where a RobInfo can be written in place
    \throws SBAInvalidArgument if maxByteSize is bigger than the amount of free space

    A call to the endAppendRawData(actualByteSize) is required to complete writing raw data.
   */
  std::uint8_t* startAppendRobInfo( std::uint32_t maxByteSize )
  {
    return startAppendRawData(maxByteSize, Block::ROB_INFO_MARKER);
  }

  /*!
    \brief Return a pointer in the block storage where raw data may be written in place
    \param maxByteSize Maximum number of bytes that might be written in place
    \param marker Data block marker to use
    \return 64bit word aligned pointer in the block storage where raw data may be written in place
    \throws SBAInvalidArgument if maxByteSize is bigger than the amount of free space

    A call to the endAppendRawData(actualByteSize) is required to complete writing raw data.
   */
  std::uint8_t* startAppendRawData( std::uint32_t maxByteSize, std::uint32_t marker );

  //! Internal state of the Block appending mod
  enum State { CLEARED, IDLE, APPENDING_ROB_DATA, APPENDING_RAW_DATA };

  /*! Data block markers for data blocks stored in the SharedBlockArray block storage
   * Data blocks are all 32bit word alligned. The byte size of a data block is a multiple of 32bit words.
   * Raw data blocks are padded with random bytes as needed.
   * Some data blocks are 64bit word alligned. The NULL_MARKER may be inserted in front of a data block
   * to obtain this alignement.
   * All markers except the NULL_MARKER are followed by a 32bit unsigned int coding the data block size
   * in words. The marker and the block size are thus included.
   */
  enum Tags {
    NULL_MARKER      = 0xAFFFFFFA, // 32bit padding word marker followed by another marker
    RAW_DATA_MARKER  = 0xBFFFFFFB, // 64bit alligned raw data block marker
    ROB_INFO_MARKER  = 0xCFFFFFFC, // 64bit alligned RobInfo structure
    ROB_DATA_MARKER  = 0xDFFFFFFD  // 32bit alligned block of 1 or more ROBs
  };

  std::uint32_t*         m_dataStart;        //!< Pointer on the first word of the block
  std::uint32_t*         m_dataEnd;          //!< Pointer on after last word of data
  const size_t           m_byteSize;         //!< Size of a block in bytes
  std::uint32_t          m_pageSize;         //!< Size of a system page in bytes
  std::uint32_t          m_freeSpace;        //!< Free space left in block (bytes)
  const std::uint32_t    m_blockId;          //!< Block identifier
  RecoverableWordsIndex* m_recoverableWords; //!< Index of recoverable words in each block. null if unused
  RobInfoIndex           m_robInfoIndex;     //!< Index of RobInfos stored in the Block
  State                  m_state;            //!< Current state of appending mode
};


}
}
}

#endif // BLOCK_H
