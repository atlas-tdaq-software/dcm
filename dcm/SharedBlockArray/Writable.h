#ifndef WRITE_H
#define WRITE_H

#include <vector>
#include <queue>
#include <string>

#include "dcm/SharedBlockArray/Block.h"

namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

class Writable
{
public:
  typedef SharedBlockArray::Block Block;

  /*!
   * \brief Creates a writable SharedBlockArray with its memory mapped file
   * \param fileName Name to give to the memory mapped file
   * \param nomberOfBlocks Number of blocks to allocate in the memory mapped file
   * \param blockMiBSize The size in Mebibyte (1024*1024 bytes) of a block
   * \param trigger MAP_ANONYMOUS memory mapping: i.e. the shared memory can be accessed only by one process
   * \throw SBAInvalidArgument if an argument is invalid
   * \throw SBARunTimeError if fails to instantiate the memory mapped file
   *
   * For safety reason the instantiation will fail if a file with that name already exist.
   * If the desired behavior is to overwrite the existing file if any, than call
   * ::unlink( fileName.c_str() ); before instantiating this class.
   *
   * If an error occurs when attempting to create the writable SharedBlockArray,
   * an exception is thrown with an explicit message of the error cause.
   * If this occurs the instance is invalid and not usable. In this case
   * fileName() returns an empty string or nomberOfBlocks() returns 0.
   */
  Writable( const std::string& fileName, std::uint32_t numberOfBlocks, std::uint32_t blockMiBSize, bool MapAnonymous = false );

  //! Close the mapped file
  ~Writable();

  //! Return the memory mapped file name
  const std::string& fileName() const { return m_fileName; }

  //! Return the number of blocks in the SharedBlockArray
  std::uint32_t numberOfBlocks() const { return m_header?m_header[3]:0; }

  //! Return the size in bytes of a block
  std::uint32_t blockByteSize() const { return m_header?m_header[2]:0; }

  //! Return true if there is no allocated blocks
  bool empty() const { return m_freeBlocksIndex.size() == numberOfBlocks(); }

  //! Return true if there are no more blocks to allocate
  bool isFull() const { return m_freeBlocksIndex.empty(); }

  //! Return the number of allocated blocks
  std::uint32_t size() const { return numberOfBlocks() - m_freeBlocksIndex.size(); }

  /*!
   * \brief Return the reference on a newly allocated block
   * \return a reference on an empty block
   * \throws SBARunTimeError if no more blocks to allocate
   */
  Block& allocate();

  /*!
   * \brief Deallocate the specified block
   * \param block A reference on a non deallocated block
   * \throws SBAInvalidArgument if the block is already deallocated
   */
  void deallocate( Block& block );

  //! Return the occupacy (from 0. to 1.) of the most used block
  float maxBlockOccupancy() const;

private:

  typedef std::priority_queue<std::uint32_t, std::vector<std::uint32_t>, std::greater<std::uint32_t> > FreeBlockIndex;

  std::uint32_t*                         m_header;           //!< SharedBlockArray header
  size_t                                 m_mmapByteSize;     //!< Memory mapped zone size
  std::unique_ptr<RecoverableWordsIndex> m_recoverableWords; //!< Index of recoverable bytes in blocks
  std::vector<Block>                     m_blocks;           //!< Vector of blocks in the SharedBlockArray
  std::string                            m_fileName;         //!< Memory mapped file name
  FreeBlockIndex                         m_freeBlocksIndex;  //!< Index of blockIds of free blocks
};

}
}
}

#endif // WRITE_H
