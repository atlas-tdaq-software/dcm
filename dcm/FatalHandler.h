#ifndef DCM_FATAL_HANDLER_H
#define DCM_FATAL_HANDLER_H

namespace daq
{
namespace dcm
{
namespace FatalHandler
{

/** Writes a backtrace to the file referred to by the file descriptor fd */
void writeBacktrace(int fd);

/** Writes the process memory map to the file referred to by the file descriptor fd */
void writeMaps(int fd);

/** \brief Handler function for fatal signals.
 *  Writes a description of the signal, a backtrace, and the process memory map to stderr.
 *  After this, resets the signal handler to the system default.
 */
void fatalHandler(int signum);

/** \brief Installs fatalHandler().
 *  Sets fatalHandler() as signal handler for SIGABRT, SIGSEGV, SIGBUS, SIGILL, SIGFPE, SIGPIPE.
 */
void install();

} // namespace FatalHandler
} // namespace dcm
} // namespace daq

#endif // defined(DCM_FATAL_HANDLER_H)
