#ifndef DCM_OUTPUT_H
#define DCM_OUTPUT_H

#include <memory>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class EventBuilder;

/*! \class Dispatcher of events to a Data Logger.
 *
 *  \brief TODO.
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 *  \author Christophe Meessen <meessen@cppm.in2p3.fr>
 *  \author Andrea Negri <Andrea.Negri@pv.infn.it>
 */
class Output
{

public:

  //! Virtual destructor doing nothing
  virtual ~Output()
  {
  }

  /*! \brief Initializes the Output.
   *
   *  \param eventBuilder Pointer to the EventBuilder preparing events for dispatch.
   *
   *  The Output may be initialized only once.
   */
  virtual void initialize(EventBuilder* eventBuilder) = 0;

  // Transitions

  /*! \brief Request to perform the CONNECT transition (used by the Main).
   *
   *  When the operation is completed, the Output calls Main::onOutputConnect() or
   *  Main::onOutputConnectError() in case of failure.
   */
  virtual void asyncConnect() = 0;

  /*! \brief Request to perform the PREPAREFORRUN transition (used by the Main).
   *
   *  When the operation is completed, the Output calls Main::onOutputStart or
   *  Main::onOutputStartError() in case of failure.
   */
  virtual void asyncStart() = 0;

  /*! \brief Request to perform the STOP transition (used by the Main).
   *
   *  When the operation is completed, the Output calls Main::onOutputStop() or
   *  Main::onOutputStopError() in case of failure.
   */
  virtual void asyncStop() = 0;

  /*! \brief Request to perform the DISCONNECT transition (used by the Main).
   *
   *  When the operation is completed, the Output calls Main::onOutputDisconnect() or
   *  Main::onOutputDisconnectError() in case of failure.
   */
  virtual void asyncDisconnect() = 0;

  // Operations

  /*! \brief Request to dispatch an Event.
   *
   *  \param event The event to be dispatched. The ownership of the event is
   *      transferred to the Output.
   *
   *  When the operation is completed, the Output calls
   *  EventBuilder::onEventSent() or EventBuilder::onEventSentError() in case
   *  of failure.
   */
  virtual void asyncEventSend(std::unique_ptr<Event> event) = 0;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_OUTPUT_H)
