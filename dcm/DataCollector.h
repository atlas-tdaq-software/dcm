#ifndef DCM_DATACOLLECTOR_H
#define DCM_DATACOLLECTOR_H

#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class Processor;

/*! \brief A pure abstract class representing the interface between the DCM and a provider of
 *    ROB fragments data (e.g. the ROS)
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class DataCollector
{

public:

  //! Virtual destructor doing nothing
  virtual ~DataCollector()
  {
  }

  /*! \brief Initializes the DataCollector
   *
   *  \param processor Pointer to the Processor requesting the ROB fragments.
   *
   *  The DataCollector may be initialized only once.
   */
  virtual void initialize(Processor* processor) = 0;

  /*! \brief Marks a fragment source as disabled.
   *  If the DataCollector receives a request for fragments served by a disabled source, dummy
   *  empty fragments will be provided instead.
   *
   *  \param name Name of the fragment source
   */
  virtual void disableSource(const std::string& name) = 0;

  /*! \brief Marks a fragment source as enabled.
   *  Reverts the effects of a previous call to disableSource()
   *
   *  \param source Name of the fragment source
   */
  virtual void enableSource(const std::string& name) = 0;

  /*! \brief Set configuration parameters that are user-controllable at run-time
   *
   *  \param parameters Map of parameter names to values. An empty string as parameter value means
   *      that the parameter must be reset to its original value from the configuration database.
   */
  virtual void setDynamicParameters(const std::map<std::string, std::string>& parameters) = 0;

  //! Returns the names of the configured fragment sources.
  virtual const std::vector<std::string>& sources() = 0;

  //! Returns the ROB IDs served by the DataCollector.
  virtual const std::vector<std::uint32_t>& robIds() = 0;

  //! Returns a map associating the served ROB IDs with the source providing them.
  virtual const std::map<std::uint32_t, std::string>& robIdToSource() = 0;

  //! Returns a map associating the sources with the ROB IDs they provide.
  virtual const std::multimap<std::string, std::uint32_t>& sourceToRobIds() = 0;

  // Transitions

  /*! \brief Request to perform the CONNECT transition (used by the Main).
   *
   *  When the operation is completed, the DataCollector calls Main::onDataCollectorConnect() or
   *  Main::onDataCollectorConnectError() in case of failure.
   */
  virtual void asyncConnect() = 0;

  /*! \brief Request to perform the PREPAREFORRUN transition (used by the Main).
   *
   *  When the operation is completed, the DataCollector calls Main::onDataCollectorStart or
   *  Main::onDataCollectorStartError() in case of failure.
   */
  virtual void asyncStart() = 0;

  /*! \brief Request to perform the STOP transition (used by the Main).
   *
   *  When the operation is completed, the DataCollector calls Main::onDataCollectorStop() or
   *  Main::onDataCollectorStopError() in case of failure.
   */
  virtual void asyncStop() = 0;

  /*! \brief Request to perform the DISCONNECT transition (used by the Main).
   *
   *  When the operation is completed, the DataCollector calls Main::onDataCollectorDisconnect() or
   *  Main::onDataCollectorDisconnectError() in case of failure.
   */
  virtual void asyncDisconnect() = 0;

  // Operations

  /*! \brief Request to retrieve the fragments with the given ROB IDs (used by the Processor).
   *
   *  \param event The event for which to collect the ROBs. The ownership of the event does not
   *      change.
   *  \param robIds The list of ROB IDs to collect. An empty list is interpreted as a request to
   *      collect all configured ROB IDs.
   *  \param context Argument to be passed to the completion method.
   *
   *  When the operation is completed, the DataCollector calls Processor::onFetchRobs() or
   *  Processor::onFetchRobsError() in case of failure.
   */
  virtual void asyncFetchRobs(Event* event, const std::vector<std::uint32_t>& robIds,
      void* context) = 0;

  /*! \brief Inform the DataCollector that the fragments with the given ROB IDs may be later requested
   *
   *  \param event The event for which we may collect ROBs. The ownership of the event does not
   *      change.
   *  \param robIds The list of ROB IDs we may collect in susequent calls.
   */
  virtual void mayGetRobs(Event* event, const std::vector<std::uint32_t>& robIds) = 0;

  /*! \brief Clean up internal cache for ROB prefetching.
   *
   *  \param event The event whose processing is done.
   */
  virtual void clearEvent(Event *) = 0;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_DATACOLLECTOR_H)
