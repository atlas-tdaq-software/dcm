#ifndef DCM_L1SOURCE_H
#define DCM_L1SOURCE_H

#include <memory>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class Processor;

/*! \brief A pure abstract class representing the interface between the DCM and a provider of
 *    L1Result data (e.g. the HLTSV)
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class L1Source
{

public:

  //! Virtual destructor doing nothing
  virtual ~L1Source()
  {
  }

  /*! \brief Initializes the L1Source
   *
   *  \param processor Pointer to the Processor controlling the flow of L1Result data.
   *
   *  The L1Source may be initialized only once.
   */
  virtual void initialize(Processor* processor) = 0;

  // Transitions

  /*! \brief Request to perform the CONNECT transition (used by the Main).
   *
   *  When the operation is completed, the L1Source calls Main::onL1SourceConnect() or
   *  Main::onL1SourceConnectError() in case of failure.
   */
  virtual void asyncConnect() = 0;

  /*! \brief Request to perform the PREPAREFORRUN transition (used by the Main).
   *
   *  When the operation is completed, the L1Source calls Main::onL1SourceStart or
   *  Main::onL1SourceStartError() in case of failure.
   */
  virtual void asyncStart() = 0;

  /*! \brief Request to perform the STOP transition (used by the Main).
   *
   *  When the operation is completed, the L1Source calls Main::onL1SourceStop() or
   *  Main::onL1SourceStopError() in case of failure.
   */
  virtual void asyncStop() = 0;

  /*! \brief Request to perform the DISCONNECT transition (used by the Main).
   *
   *  When the operation is completed, the L1Source calls Main::onL1SourceDisconnect() or
   *  Main::onL1SourceDisconnectError() in case of failure.
   */
  virtual void asyncDisconnect() = 0;

  // Operations

  /*! \brief Request to retrieve a new L1Result.
   *
   *  \param event The event that will contain the L1Result. The ownership of the event is
   *      transferred to the L1Source.
   *  \param context Argument to be passed to the completion method.
   *
   *  When the operation is completed, the L1Source calls Processor::onFetchL1R() or
   *  Processor::onFetchL1RError() in case of failure.
   */
  virtual void asyncFetchL1R(std::unique_ptr<Event> event, void* context) = 0;

  /*! \brief Mark an event as done.
   *
   *  This method informs the L1Result provider that no further data needs to be collected for the
   *  given event.
   *
   *  \param event The event.
   */
  virtual void markDone(const Event* event) = 0;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_L1SOURCE_H)
