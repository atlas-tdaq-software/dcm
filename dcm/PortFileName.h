#ifndef PORT_FILE_NAME_H
#define PORT_FILE_NAME_H

#include <sys/types.h>
#include <pwd.h>
#include <string>
#include <stdlib.h>


namespace daq
{
  namespace dcm
  {

    /// Thread safe function to get the username
    inline std::string username_r()
    {
      std::string uname("unknown");
      struct passwd pw;
      struct passwd *result;

      long bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
      if (bufsize == -1)
        bufsize = 16384;

      char *buf = (char*) malloc(bufsize);
      if (buf == NULL) 
        return uname;

      getpwuid_r(geteuid(), &pw, buf, bufsize, &result);
      if (result != NULL)
        uname = std::string(pw.pw_name);

      free(buf);
      return uname;
    }


    /*! @brief Calculate the name of the file supposed to host the port name
     *  @param Base path name, default "/tmp"
     *  Convention: <path>/.dcmport.<partition_name>.<username>.txt
     *  The <partition_name> is read from the environment, 
     *  the username is obtained from a thread safe function  
     */ 
    inline std::string getPortFileName(std::string path = "/tmp")
    {
      // get $TDAQ_PARTITION and make sure it is define with some value
      const char* partitionName = ::getenv( "TDAQ_PARTITION" );
      if(partitionName == nullptr || *partitionName == '\0')
        return std::string("");

      // get $TMPDIR and if defined with some value and path is /tmp then replace path with its value
      const char *tmpDir = ::getenv("TMPDIR");
      if (path == "/tmp" && tmpDir != nullptr && *tmpDir != '\0')
        path.assign(tmpDir);

      // remove trailing '/'
      while (!path.empty() && path.back() == '/' )
        path.pop_back();

      path += "/dcmport.";
      path += std::string(partitionName);
      path += ".";
      path += username_r();
      path += ".txt";
      return path;
    }
  } // namespace dcm
} // namespace daq

#endif
