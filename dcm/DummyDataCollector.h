#ifndef DCM_DUMMYDATACOLLECTOR_H
#define DCM_DUMMYDATACOLLECTOR_H

#include "dcm/DataCollector.h"

#include "dccommon/Distribution.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>
#include <cstdint>
#include <deque>
#include <list>
#include <map>
#include <string>
#include <vector>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class Main;
class Processor;

namespace dal
{
class DcmDummyDataCollector;
}

/*! \brief A provider of internally generated ROB fragments data.
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class DummyDataCollector: public DataCollector
{

public:

  DummyDataCollector(Main& main, const dal::DcmDummyDataCollector& config);

  //! Implementation of DataCollector::initialize()
  void initialize(Processor* processor) override;

  //! Implementation of DataCollector::disableSource()
  void disableSource(const std::string& source) override;

  //! Implementation of DataCollector::enableSource()
  void enableSource(const std::string& source) override;

  //! Implementation of DataCollector::setDynamicParameters()
  void setDynamicParameters(const std::map<std::string, std::string>& parameters) override;

  //! Implementation of DataCollector::sources()
  const std::vector<std::string>& sources() override;

  //! Implementation of DataCollector::robIds()
  const std::vector<std::uint32_t>& robIds() override;

  //! Implementation of DataCollector::robIdToSource()
  const std::map<std::uint32_t, std::string>& robIdToSource() override;

  //! Implementation of DataCollector::sourceToRobIds()
  const std::multimap<std::string, std::uint32_t>& sourceToRobIds() override;

  // Transitions

  //! Implementation of DataCollector::asyncConnect()
  void asyncConnect() override;

  //! Implementation of DataCollector::asyncStart()
  void asyncStart() override;

  //! Implementation of DataCollector::asyncStop()
  void asyncStop() override;

  //! Implementation of DataCollector::asyncDisconnect()
  void asyncDisconnect() override;

  // Operations

  //! Implementation of DataCollector::asyncFetchRobs()
  void asyncFetchRobs(Event* event, const std::vector<std::uint32_t>& robIds,
      void* context) override;

  //! Implementation of DataCollector::mayGetRobs()
  void mayGetRobs(Event* event, const std::vector<std::uint32_t>& robIds) override;

  //! Clean up internal cache for ROB prefetching
  void clearEvent(Event *event) override;

private:

  struct DcRequest
  {
    DcRequest(Event* event_, void* context_) :
        event(event_),
        context(context_),
        nPendingRosRequests(0)
    {
    }

    Event* event;
    void* context;
    std::uint32_t nPendingRosRequests;
  };

  struct RosRequest
  {
    RosRequest(const std::list<DcRequest>::iterator& dcRequest_,
        std::vector<std::uint32_t>&& robIds_) :
      dcRequest(dcRequest_),
      robIds(std::move(robIds_))
    {
    }

    std::list<DcRequest>::iterator dcRequest;
    std::vector<std::uint32_t> robIds;
  };

  struct Ros
  {
    Ros(const std::string& name_, boost::asio::io_service& ioService) :
        name(name_),
        timer(ioService)
    {
    }

    std::string name;
    std::deque<RosRequest> requests;
    boost::asio::steady_timer timer;
  };

  static const std::map<std::string, std::vector<std::uint32_t>> s_sourceToRobIdsVec;

  void startTimer(Ros& ros);

  Main& m_main;
  const dal::DcmDummyDataCollector& m_config;
  Processor* m_processor;

  std::vector<std::string> m_sources;
  std::vector<std::uint32_t> m_robIds;
  std::map<std::uint32_t, std::string> m_robIdToSource;
  std::multimap<std::string, std::uint32_t> m_sourceToRobIds;

  DC::Distribution m_delayDistribution_us;
  DC::Distribution m_sizeDistribution_words;

  std::list<DcRequest> m_dcRequests;
  std::map<std::uint32_t, Ros*> m_robIdToRos;
  std::list<Ros> m_roses;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_DUMMYDATACOLLECTOR_H)
