#ifndef DCM_SFOOUTPUT_H
#define DCM_SFOOUTPUT_H

#include "dcm/Output.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <deque>
#include <memory>
#include <vector>

namespace daq
{
namespace dcm
{

class Event;
class EventBuilder;
class Main;

namespace dal
{
class DcmSfoOutput;
}

/*! \brief A client for multiple Data Logger (SFO) servers.
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class SfoOutput: public Output
{
public:

  class Transaction;
  class Session;

  SfoOutput(Main& main, const dal::DcmSfoOutput& config);

  //! Implementation of Output::initialize()
  void initialize(daq::dcm::EventBuilder* eventBuilder) override;

  //! Implementation of Output::asyncConnect()
  void asyncConnect() override;

  //! Implementation of Output::asyncStart()
  void asyncStart() override;

  //! Implementation of Output::asyncStop()
  void asyncStop() override;

  //! Implementation of Output::asyncDisconnect()
  void asyncDisconnect() override;

  //! Implementation of Output::asyncEventSend()
  void asyncEventSend(std::unique_ptr<Event> event) override;

private:

  enum State
  {
    NONE,
    CONNECTING,
    DISCONNECTING,
    CONNECTED,
    STARTING,
    STOPPING,
    RUNNING
  };

  void startTransfers();
  void onSessionConnect(Session* session);
  void onSessionConnectError(Session* session, const boost::system::error_code& error);
  void onSessionDisconnect(Session* session);
  void onSessionDisconnectError(Session* session, const boost::system::error_code& error);
  void onSessionTransfer(Session* session, std::unique_ptr<Transaction> transaction);
  void onSessionTransferError(Session* session, const boost::system::error_code& error,
      std::unique_ptr<Transaction> transaction);
  void onTimer(const boost::system::error_code& error);
  void checkConnect();
  void checkDisconnect();
  void checkStart();
  void checkStop();

  Main& m_main;
  const dal::DcmSfoOutput& m_config;
  EventBuilder* m_eventBuilder;

  std::vector<std::string> m_sinks;

  State m_state;
  std::deque<std::unique_ptr<Transaction>> m_queuedTransactions;
  std::vector<std::shared_ptr<Session>> m_sessions;

  boost::asio::steady_timer m_timer;
  const std::chrono::seconds m_requestTimeout;
  const std::chrono::seconds m_ackTimeout;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_SFOOUTPUT_H)
