#ifndef DCM_PROCESSOR_H
#define DCM_PROCESSOR_H

#include "dccommon/Distribution.h"
#include "monsvc/ptr.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <TH1.h>
#include <chrono>
#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "dcm/Event.h"

namespace daq
{
namespace dcm
{

// Forward declarations
class Main;
class L1Source;
class DataCollector;
class EventBuilder;
class Event;
namespace dal { class DcmProcessor; }

/*! \class Manager of event fragment fetching and event processing.
 *
 *  \brief Fetches L1Results and ROBs on demand by of the Trigger, on processing completion it
 *      forwards the event to the EventBuilder and releases event storage on notification of its the
 *      safe storage.
 *
 *  \author Christophe Meessen <meessen@cppm.in2p3.fr>
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 *  \author Andrea Negri <Andrea.Negri@pv.infn.it>
 */
class Processor
{
protected:
  class Histograms
  {
  public:

    Histograms(boost::asio::io_service& ioService,
        std::chrono::steady_clock::duration updateInterval,
        float maxTime, float procTimeOut);
    virtual ~Histograms();

    virtual void initialize();
    virtual void finalize();
    virtual void clear();
    virtual void updateRecents();

    void fillInterAssignmentTime_us(double x);
    void fillLifeTime_ms(double x);
    void fillOwnTime_ms(double x);
    void fillWaitTime_ms(double x);
    void fillProcessingTime_ms(double x);
    void fillDataCollectionTime_ms(double x);

  private:
    void timer(const boost::system::error_code& error);
    boost::asio::steady_timer m_timer;

    std::chrono::steady_clock::duration m_updateInterval;

    monsvc::ptr<TH1I> m_InterAssignmentTime_us;
    monsvc::ptr<TH1I> m_InterAssignmentTime_us_recent;
    std::unique_ptr<TH1I> m_InterAssignmentTime_us_last;

    monsvc::ptr<TH1I> m_LifeTime_ms;
    monsvc::ptr<TH1I> m_LifeTime_ms_recent;
    std::unique_ptr<TH1I> m_LifeTime_ms_last;
    
    monsvc::ptr<TH1I> m_OwnedTime_ms;
    monsvc::ptr<TH1I> m_OwnedTime_ms_recent;
    std::unique_ptr<TH1I> m_OwnedTime_ms_last;

    monsvc::ptr<TH1I> m_WaitTime_ms;
    monsvc::ptr<TH1I> m_WaitTime_ms_recent;
    std::unique_ptr<TH1I> m_WaitTime_ms_last;

    monsvc::ptr<TH1I> m_ProcessingTime_ms;
    monsvc::ptr<TH1I> m_ProcessingTime_ms_recent;
    std::unique_ptr<TH1I> m_ProcessingTime_ms_last;
    
    monsvc::ptr<TH1I> m_ProcessingTimeWideView_ms;
    monsvc::ptr<TH1I> m_ProcessingTimeWideView_ms_recent;
    std::unique_ptr<TH1I> m_ProcessingTimeWideView_ms_last;

    monsvc::ptr<TH1I> m_DataCollectionTime_ms;
    monsvc::ptr<TH1I> m_DataCollectionTime_ms_recent;
    std::unique_ptr<TH1I> m_DataCollectionTime_ms_last;

    float m_maxTime;
    float m_procTimeOut;
  };

  struct HLTPU;

public:
  Processor(Main& main, std::uint32_t maxHLTPUs, std::unique_ptr<Histograms> histograms, const dal::DcmProcessor& config);

  //! Virtual destructor doing nothing
  virtual ~Processor();

  /*! \brief Initializes the Processor
   *
   *  \param l1source Pointer to the L1Source providing L1Results.
   *  \param dataCollector Pointer to the DataCollector providing the ROBFragments.
   *  \param processor Pointer to the Trigger driving the trigger.
   *  \param eventBuilder Pointer to the EventBuilder assembling accepted events.
   *
   *  The Processor may be initialized only once.
   */
  virtual void initialize(L1Source* l1Source, DataCollector* dataCollector, EventBuilder* eventBuilder);

  /*! \brief Set configuration parameters that are user-controllable at run-time
   *
   *  \param parameters Map of parameter names to values. An empty string as parameter value means
   *      that the parameter must be reset to its original value from the configuration database.
   */
  void setDynamicParameters(const std::map<std::string, std::string>& parameters);

  // Transitions

  /*! \brief Request to perform the CONNECT transition (used by the Main).
   *
   *  When the operation is completed, the Processor calls Main::onProcessorConnect() or
   *  Main::onProcessorConnectError() in case of failure.
   */
  virtual void asyncConnect();

  /*! \brief Request to perform the PREPAREFORRUN transition (used by the Main).
   *
   *  When the operation is completed, the Processor calls Main::onProcessorStart or
   *  Main::onProcessorStartError() in case of failure.
   */
  virtual void asyncStart();

  /*! \brief Request to perform the STOPHLT transition (used by the Main).
   *
   *  When the operation is completed, the Processor calls Main::onProcessorStop() or
   *  Main::onProcessorStopError() in case of failure.
   */
  virtual void asyncStop();

  /*! \brief Request to perform the STOPGATHERING transition (used by the Main).
   *
   *  When the operation is completed, the Processor calls Main::onProcessorReset() or
   *  Main::onProcessorResetError() in case of failure.
   */
  virtual void asyncReset();

  /*! \brief Request to perform the DISCONNECT transition (used by the Main).
   *
   *  When the operation is completed, the Processor calls Main::onProcessorDisconnect() or
   *  Main::onProcessorDisconnectError() in case of failure.
   */
  virtual void asyncDisconnect();

  // Completion methods

  /*! \brief Informs the Processor that a new L1Result was successfully retrieved.
   *
   *  \param event The event containing the L1Result. The ownership of the event is transferred
   *      to the Processor.
   *  \param context Argument provided to the initiation method.
   *
   *  This method is a completion method for L1Source::asyncFetchL1R().
   */
  void onFetchL1R(std::unique_ptr<Event> event, void* context);

  /*! \brief Informs the Processor that a new L1Result could not be retrieved because the STOP transition is ongoing.  */
  void onFetchL1RStop(HLTPU*);

  /*! \brief Informs the Processor that a new L1Result could not be retrieved.
   *
   *  \param issue The issue causing the error.
   *  \param event The event containing the L1Result. The ownership of the event is transferred
   *      to the Processor.
   *  \param context Argument provided to the initiation method.
   *
   *  This method is a completion method for L1Source::asyncFetchL1R().
   */
  void onFetchL1RError(const ers::Issue& issue, std::unique_ptr<Event> event, void* context);

  /*! \brief Informs the Processor that the requested ROB fragments were successfully retrieved.
   *
   *  \param event The event to which the fragments belong. The ownership of the event does not
   *      change.
   *  \param context Argument provided to the initiation method.
   *
   *  This method is a completion method for DataCollector::asyncFetchRobs().
   */
  void onFetchRobs(Event* event, void* context);

  /*! \brief Informs the Processor that the requested ROB fragments could not be retrieved.
   *
   *  \param issue The issue causing the error.
   *  \param event The event to which the fragments belong. The ownership of the event does not
   *      change.
   *  \param context Argument provided to the initiation method.
   *
   *  This method is a completion method for DataCollector::asyncFetchRobs().
   */
  void onFetchRobsError(const ers::Issue& issue, Event* event, void* context);

  /*! \brief Informs the Processor that the given event was successfully built and dispatched.
   *
   *  \param event The event.
   *
   *  This method is a completion method for EventBuilder::asyncBuildAndSend(). The ownership of
   *  the event is given back to the Processor.
   */
  void onBuildAndSend(std::unique_ptr<Event> event);

  /*! \brief Informs the Processor that the given event could not be built or dispatched.
   *
   *  \param issue The issue causing the error.
   *  \param event The event.
   *
   *  This method is a completion method for EventBuilder::asyncBuildAndSend(). The ownership of
   *  the event is given back to the Processor.
   */
  void onBuildAndSendError(const ers::Issue& issue, std::unique_ptr<Event> event);

  /*! \brief Try attaching the hltpu as active hltpu, return false if doesn't satisfy conditions
   *
   *  \param hltpu PU that sent the ROBs request and receiving it back.
   *  \return false if PU doesn't satisfy conditions to be attached
   *  hltpu is a reference on a unique_ptr. If the attachement succeeds, hltpu will be reset.
   *  Otherwise, hltpu is left unchanged.
   */
  virtual bool attachHLTPU (std::unique_ptr<HLTPU>& hltpu); // Override to add conditions

protected:

  struct HLTPU
  {
    enum class State
    {
      INITIALIZED,    // Before the hltpu is in use
      IDLE,           // While an event has just been accepted or rejected and before the next L1R has been requested
      SUSPENDED,      // While a L1R has been requested but the hltpu has to wait due to backpressure or not RUNNING
      FETCHING_L1R,   // While L1RSource is waiting for a L1R
      PROCESSING,     // When it received a L1R to process and while ROBs may be fetched
      FORCE_ACCEPTING,// When an event is force accepted
      ACCEPTING,      // When it accepted the event and fetching the missing robs is in progress
      TERMINATED      // When it is terminated and should not be used anymore
    };

    HLTPU() : state(State::INITIALIZED), fetchedAllRobs(false), fetchingROBs(false) {}
    virtual ~HLTPU() {}
    const char* stateStr();

    State                                 state;          //!< Current state of the hltpu
    std::unique_ptr<Event>                event;          //!< Event currently processed by the Hltpu
    bool                                  fetchedAllRobs; //!< Fetched all robs
    bool                                  fetchingROBs;   //!< A fetchROBs call is in progress
    boost::asio::steady_timer::time_point deadline;       //!< Time when the IDLE or PROCESSING operation will time out
    std::string                           name;           //!< HLTPU name
  };

  void detachHLTPU (HLTPU* hltpu);
  void tryFinalizeStop();
  void tryFinalizeReset();
  void tryFetchL1R(HLTPU* hltpu);
  void unsuspendAllHtpu();
  void fetchL1R(HLTPU* hltpu);
  void fetchROBs(HLTPU* hltpu, const std::vector<std::uint32_t> &robIds);
  void fetchAllROBs(HLTPU* hltpu);
  void mayGetROBs(HLTPU* hltpu, const std::vector<std::uint32_t> &robIds);
  void reject(HLTPU* hltpu);
  void accept(HLTPU* hltpu);
  void finalizeAccept(HLTPU* hltpu);
  void finalizeProcessing(HLTPU* hltpu);
  void finalizeEvent(std::unique_ptr<Event> event);
  void forceAccept(HLTPU* hltpu, Event::ForceAccept type);
  void finalizeForceAccept(HLTPU* hltpu);
  // If is not urgent: merge markDone with L1R request that must follow immediately
  void markDone(Event* event);
  // Request markDone just after event building (all robs collected)
  void markDoneAfterEB(Event* event)
  {
    if (m_markDoneAfterEB)
      markDone(event);
  }
  // Request markDone after the event has been sent to SFO
  void markDoneAfterES(Event* event)
  {
    if (!m_markDoneAfterEB)
      markDone(event);
  }
  void readCTPFragment(Event *event);
  bool ignoreBcIdCheck(Event* event, std::uint32_t robId) const;
  void checkL1IdMismatch(Event* event, std::uint32_t robId, std::uint64_t l1Id);
  void checkBcIdMismatch(Event* event, std::uint32_t robId, std::uint32_t bcId);
  void checkBcIdAndL1IdMismatch(HLTPU* hltpu);
  void timer(const boost::system::error_code& error);
  static std::string robIdsAsString( const std::vector<std::uint32_t>& robIds);
  static std::string detsAsString( const std::vector<eformat::SubDetector>& dets);

  /*! \brief Try set a configuration parameter that is user-controllable at run-time
   *
   *  \param key Name of the parameter we want to change.
   *  \param value Value of the parameter we want to set.
   *  \return false if the parameter name (key) was not recognized.
   */
  virtual bool setDynamicParameter(const std::string& key, const std::string& value);

  /*! \brief Process the L1R received in response of a L1R requested by hltpu
   *
   *  \param hltpu PU that sent the L1R request and receiving it back.
   */
  virtual void processL1R  (HLTPU* hltpu) = 0;

  /*! \brief Process the ROBs received in response of ROBs requested by hltpu
   *
   *  \param hltpu PU that sent the ROBs request and receiving it back.
   */
  virtual void processROBs (HLTPU* hltpu) = 0;

  /*! \brief Operation performed after an HLTPU is attached, or an event has been accepted or rejected
   *
   *  \param hltpu PU that was attached, or that accepted or rejected an event
   */
  virtual void setStateIdle(HLTPU* hltpu) = 0;

  /*! \brief Informs the hltpu that there are no more events to be processed
   *
   *  \param hltpu PU to notify that there are no more events to process
   */
  virtual void noMoreEvents(HLTPU* hltpu); // Overring method must call this method before ending

  /*! \brief Terminates the activity of the hltpu
   *
   *  \param hltpu PU to terminate
   */
  virtual void terminate(HLTPU* hltpu);    // Overring method must call this method before ending

  enum class State
  {
    UNCONNECTED, // initial state
    CONNECTED,   // after asyncConnect() or stop is completed
    RUNNING,     // after asyncStart(), while processing events
    STOPPING,    // after asyncStop(), while waiting for m_nEventsInDCM to become 0
    STOPPED,     // when nEventsInDCM is 0 and waiting for asyncReset() to be called
    RESETTING    // after asyncReset(), while waiting for all HLTPUs to be disconnected
  };

  Main&                       m_main;                        //!< Reference to the main instance
  const dal::DcmProcessor&    m_config;                      //!< Reference to the processor configuration
  L1Source*                   m_l1Source;                    //!< Pointer to the L1Source
  DataCollector*              m_dataCollector;               //!< Pointer to the Data Collector
  EventBuilder*               m_eventBuilder;                //!< Pointer to the Event Builder
  State                       m_state;                       //!< Current state of the processor
  std::uint32_t               m_nEventsInDCM;                //!< Number of events currently in the DCM
  std::uint32_t               m_nOutputEvents;               //!< Number of accepted events not yet sent out
  std::uint32_t               m_maxOutputEvents;             //!< Threshold value for backpressure activation
  std::chrono::milliseconds   m_maxProcessingTime_ms;        //!< Processing time limit
  std::chrono::milliseconds   m_maxIdleTime_ms;              //!< Max waiting time for an event request while stopping
  std::uint32_t               m_maxHLTPUs;                   //!< Maximum number of HLTPUs
  std::unique_ptr<Histograms> m_histograms;                  //!< Object used to manage histograms
  std::vector<std::uint32_t>  m_robIds;                      //!< Sorted copy of Data Collector's robId list
  std::set<std::uint8_t>      m_bcidCheckIgnoreSubdetectors; //!< Sub-detectors not checked for BcId mismatches
  std::set<std::uint8_t>      m_bcidCheckSilentSubdetectors; //!< Sub-detectors whose BcId mismatches are not reported in ERS
  boost::asio::steady_timer   m_timer;                       //!< Timer used for periodic processing timeout check
  std::vector<std::unique_ptr<HLTPU>>   m_hltpus;            //!< Vector of connected HLTPUs
  std::chrono::steady_clock::time_point m_lastL1RAssignmentTime; //!< Time stamp of last L1R assignement
  bool                        m_markDoneAfterEB;             //!< True if markDone should be called after event building
};

} // namespace dcm
} // namespace daq

#endif // DCM_PROCESSOR_H

