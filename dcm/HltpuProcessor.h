#ifndef DCM_HLTPUPROCESSOR_H
#define DCM_HLTPUPROCESSOR_H

#include "dcm/Processor.h"
#include "asyncmsg/asyncmsg.h"
#include <boost/asio/steady_timer.hpp>

namespace daq
{
namespace dcm
{

// Forward declarations
namespace dal { class DcmHltpuProcessor; }

/*! \class Manager of event fragment fetching and event processing.
 *
 *  \brief Fetches L1Results and ROBs on demand by of the Trigger, on processing completion it
 *      forwards the event to the EventBuilder and releases event storage on notification of its the
 *      safe storage.
 *
 *  \author Christophe Meessen <meessen@cppm.in2p3.fr>
 */
class HltpuProcessor : public Processor
{
protected:
  struct HLTPU;

public:
  HltpuProcessor(Main& main, const dal::DcmHltpuProcessor& config);
  ~HltpuProcessor();

protected:
  void processL1R  (Processor::HLTPU* hltpu) override;
  void processROBs (Processor::HLTPU* hltpu) override;
  void setStateIdle(Processor::HLTPU* hltpu) override;
  void noMoreEvents(Processor::HLTPU* hltpu) override;
  void terminate   (Processor::HLTPU* hltpu) override;

  class HLTPUSrv : public asyncmsg::Server
  {
    friend class HltpuProcessor;
  public:
    HLTPUSrv(HltpuProcessor* processor, boost::asio::io_service& ioService);
    void initialize();
    void onAccept(std::shared_ptr<asyncmsg::Session> session) override;
    void onAcceptError(const boost::system::error_code & error,
                       std::shared_ptr<asyncmsg::Session> session) override;
  private:
    HltpuProcessor* processor;
  };

  struct HLTPU;
  class HLTPUSes : public asyncmsg::Session
  {
    friend class HltpuProcessor::HLTPU;
  public:
    HLTPUSes(HltpuProcessor* processor, boost::asio::io_service& ioService);
    void onOpen() override;
    void onOpenError(const boost::system::error_code& error) override;
    std::unique_ptr<asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
                                                          std::uint32_t transactionId,
                                                          std::uint32_t payloadSize) override;
    void onReceive(std::unique_ptr<asyncmsg::InputMessage> message) override;
    void onReceiveError(const boost::system::error_code& error,
                        std::unique_ptr<asyncmsg::InputMessage> message) override;
    void onSend(std::unique_ptr<const asyncmsg::OutputMessage> message) override;
    void onSendError(const boost::system::error_code& error,
                     std::unique_ptr<const asyncmsg::OutputMessage> message) override;
    void onClose() override;
    void onCloseError(const boost::system::error_code& error) override;

    void sendFetchedL1R();
    void sendFetchedROBs();
    void sendNoMoreEvents();

  private:
    HltpuProcessor* processor;
    HltpuProcessor::HLTPU* hltpu;
    std::uint32_t      transactionId;
    bool               sentNoMoreEvents;
  };

  struct HLTPU : Processor::HLTPU
  {
    HLTPU(std::shared_ptr<HLTPUSes> hltpuSes) : hltpuSes(hltpuSes) {}
    std::shared_ptr<HLTPUSes> hltpuSes;
  };

  const dal::DcmHltpuProcessor& m_config;
  std::shared_ptr<HLTPUSrv>     m_server;
};

} // namespace dcm
} // namespace daq

#endif // DCM_HLTPUPROCESSOR_H

