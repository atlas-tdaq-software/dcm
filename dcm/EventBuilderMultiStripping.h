#ifndef EVENTBUILDERMULTISTRIPPING_H
#define EVENTBUILDERMULTISTRIPPING_H

#include "dcm/EventBuilder.h"
#include "dcm/Main.h"
#include "dcm/Processor.h"
#include "dcm/Output.h"
#include "dcm/SamplerSingleStripping.h"

#include "eformat/compression.h"

#include <deque>
#include <mutex>
#include <memory>
#include <chrono>
#include <thread>
#include <atomic>
#include <condition_variable>

//#include "dcm/BuildSingleStrippingEvent.h"


namespace daq
{
namespace dcm
{
  // Forward declaration
  class Processor;  // still needed ?
  class Output;     // still needed ?
  class Main;


/*! @class EventBuilder is a pure abstract class for Event building
    @brief Build events given by the Processor and pass them to the Output
    @author Christophe Meessen <meessen@cppm.in2p3.fr>
    @author Tommaso Colombo <Tommaso.Colombo@cern.ch>
    @author Andrea Negri <Andrea.Negri@pv.infn.it>

    The EventBuilder class is intended to interconnect the Processor and Output object
    instances with the following call scenarios

    <pre>
    Processor                EventBuilder                 Output
       |                          |                          |
       |----asyncBuildAndSend---->|                          |
       |                          |------asyncEventSend----->|
       =                          =                          =
       |                          |<-------onEventSent-------| (callback)
       |<-------onEventSent-------|                          |
     </pre>

     The Processor calls the EventBuilder::asyncBuildAndSend() method.
     This method builds the FullEventHeader fragment(s) for the events, pass them to
     the Monitoring system and then forward the event to the Output.

     Some time later, when the event has been sent, or failed to be sent because of an error,
     the Output calls back the method EventBuilder::onEventSent() to notify that the event has
     been sent. When the event data may be discarded and its storage space may be reused, the
     EventBuilder forwards the call back to the Processor by calling Processor::onEventSent().
  */
class EventBuilderMultiStripping : public EventBuilder
{
public:
  //! Constructor initializes EventBuilder with NULL processor and output (robIds is not used)
  EventBuilderMultiStripping( Main& main, uint32_t runType, const std::vector<std::uint32_t>& robIds );

  //! The destructor (terminates the event builder and sampler threads)
  ~EventBuilderMultiStripping();

  /*! @brief Configures the EventBuilder
      @param processor Pointer to the Processor submitting Events to build and send to Output
      @param output Pointer to the Output to which built Events will be submitted

      The EventBuilder instance must be configured with non null processor and output pointer,
      otherwise a misconfiguration issue must be raised.

      The EventBuilder may be reconfigured, but the behavior is undefined if there are pending
      callbacks.

      If output is null, the event is silently deleted after construction and the processor is
      notified of a successfull event build and send operation.
   */
  void initialize( Processor* processor, Output* output ) override;

  /*! @brief Build the submitted event and forward it to the output
      @param event The event to build and pass to the output instance

      The method Processor::onEventSent(event) is called when the event has been successfuly
      sent, or Processor::onEventSent(error, event) is called when an error occured.
   */
  void asyncBuildAndSend( std::unique_ptr<Event> event ) override;

  /*! @brief Send the already built event directly to the output
      @param event The event to forward to the output instance

      To be called when the event has been returned by onEventSentError and needs to be
      resubmitted to the output instance without needing to rebuild the event.

      The method Processor::onEventSent(event) is called when the event has been successfuly
      sent, or Processor::onEventSent(error, event) is called when an error occured.
   */
  virtual void asyncSend( std::unique_ptr<Event> event ) override;

  /*! @brief Informs the EventBuilder that the given event was successfully sent by the Output
      @param event The event that was sent by the Output

      Forwards the call to the Processor instance, if any.
   */
  void onEventSent( std::unique_ptr<Event> event ) override;

  /*! @brief Informs the EventBuilder that the given event failed to be sent by the Output
      @param error The error code
      @param event Event that failed to be sent by the Output

      Forwards the call to the Processor instance, if any
   */
  void onEventSentError( const ers::Issue& issue, std::unique_ptr<Event> event ) override;

  //! Method executed by the event builder thread
  void builderTask();

  /*! @brief Process the built event
      @param event The event that has been built
   */
  void eventBuilt (std::unique_ptr<Event> event);

  /*! @brief Return true if the two streamTags have exactly the same set of robs and dets
      @param tag1 The streamTag to compare with tag2
      @param tag2 The streamTag to compare with tag1
      @return true if the two streamTags have exactly the same set of robs and dets
   */
  bool hasSameSetOfRobs( const eformat::helper::StreamTag& tag1, const eformat::helper::StreamTag& tag2)
  {
    return tag1.dets == tag2.dets && tag1.robs == tag2.robs;
  }

  /*! @brief Serialize the event using the header found in the given event, and set of robIds
      @param event[in,out] The event to serialized
      @param streamTag[in] The vector of robIds to include in the event
      @param templateHeader[in] The header to be used as template for the serialized event
      @param data[out] Pointer on the first byte of the serialized event
      @param nbrBytes[out] Size in bytes of the serialized event

      The Header of the event must have been previously serialized and reference in the event object.
      The serialized event is appended to the SharedBlockArray block.
   */
  void serializeEvent(const Event &event,
                      const eformat::helper::StreamTag& streamTag,
                      const std::uint32_t *templateHeader,
                      std::uint32_t*& data,
                      std::uint32_t& nbrBytes);


  Main & getmain() {return m_main;}                    

private:

  /*! @brief Build the serialized event header used as template for all events
      @param event The event for which to build the event header
   */
  void buildEventHeader(Event* event);

  /*! @brief Build the events, pass it to the sampler and send it to the output
      @param event The event to build, sample and send to the output
   */
  void buildEvents (std::unique_ptr<Event> event);

  Main &                                  m_main;          //!< Reference on main variables ("globals")
  Processor*                              m_processor;     //!< Pointer to event input
  Output*                                 m_output;        //!< Pointer to event output
  std::unique_ptr<SamplerSingleStripping> m_sampler;       //!< Pointer to the sampler
  std::deque<std::unique_ptr<Event>>      m_builderQueue;  //!< Queue of events to build
  std::condition_variable                 m_builderCond;   //!< Queue state condition variable
  std::mutex                              m_builderMtx;    //!< Mutex to synchronize m_builderQueue
  std::atomic<bool>                       m_builderStop;   //!< Flag to tell the builder thread to stop
  std::thread                             m_builderThread; //!< Event builder thread
  const std::uint32_t                     m_runType;       //!< The current run type
  const std::vector<std::uint32_t>        m_robIds;        //!< Vector of all robIds
  eformat::Compression                    m_compAlg;       //!< Compression algorithm
  bool                                    m_ctpFromRoI;    //!< Force CTP data from RoI

  //BuildSingleStrippingEvent m_build;//!< Builder of single stripping events
};

} // namespace dcm
} // namespace daq


#endif // EVENTBUILDERMULTISTRIPPING_H
