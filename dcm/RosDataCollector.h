#ifndef DCM_ROSDATACOLLECTOR_H
#define DCM_ROSDATACOLLECTOR_H

#include "dcm/DataCollector.h"

#include "monsvc/ptr.h"

#include <boost/asio/steady_timer.hpp>
#include <TH1.h>
#include <TH2.h>
#include <cstdint>
#include <deque>
#include <list>
#include <memory>
#include <map>
#include <string>
#include <vector>
#include <set>

namespace daq
{
namespace dcm
{

// Forward declarations
class Event;
class Main;
class Processor;

namespace dal
{
class DcmRosDataCollector;
}

/*! \brief A provider ROB fragments data fetched from the ROS.
 *
 *  \author Tommaso Colombo <Tommaso.Colombo@cern.ch>
 */
class RosDataCollector: public DataCollector
{

public:

  class Transaction;
  class Session;

  friend class Session;

  RosDataCollector(Main& main, const dal::DcmRosDataCollector& config);

  //! Implementation of DataCollector::initialize()
  void initialize(Processor* processor) override;

  //! Implementation of DataCollector::disableSource()
  void disableSource(const std::string& source) override;

  //! Implementation of DataCollector::enableSource()
  void enableSource(const std::string& source) override;

  //! Implementation of Processor::setDynamicParameters()
  void setDynamicParameters(const std::map<std::string, std::string>& parameters) override;

  //! Implementation of DataCollector::sources()
  const std::vector<std::string>& sources() override;

  //! Implementation of DataCollector::robIds()
  const std::vector<std::uint32_t>& robIds() override;

  //! Implementation of DataCollector::robIdToSource()
  const std::map<std::uint32_t, std::string>& robIdToSource() override;

  //! Implementation of DataCollector::sourceToRobIds()
  const std::multimap<std::string, std::uint32_t>& sourceToRobIds() override;

  // Transitions

  //! Implementation of DataCollector::asyncConnect()
  void asyncConnect() override;

  //! Implementation of DataCollector::asyncStart()
  void asyncStart() override;

  //! Implementation of DataCollector::asyncStop()
  void asyncStop() override;

  //! Implementation of DataCollector::asyncDisconnect()
  void asyncDisconnect() override;

  // Operations

  //! Implementation of DataCollector::asyncFetchRobs()
  void asyncFetchRobs(Event* event, const std::vector<std::uint32_t>& robIds,
      void* context) override;

  //! Implementation of DataCollector::mayGetRobs()
  void mayGetRobs(Event* event, const std::vector<std::uint32_t>& robIds) override;

  //! Clean up internal cache for ROB prefetching
  void clearEvent(Event *event) override;

private:

  class Histograms
  {
  public:

    Histograms();
    ~Histograms();

    void initialize(const std::vector<std::string>& sources, std::size_t maxRobIdsPerSource,
        std::uint32_t maxCredits);
    void clear();
    void updateRecents();

    void fillRobSize(const std::string& x, double y);
    void fillRobsPerRequest(const std::string& x, double y);
    void fillDcResponseTime_us(const std::string& x, double y);
    void fillDcAvailableCredits(double x, double weight);

  private:

    monsvc::ptr<TH2I> m_RobSize;
    monsvc::ptr<TH2I> m_RobSize_recent;
    std::unique_ptr<TH2I> m_RobSize_last;

    monsvc::ptr<TH2I> m_RobsPerRequest;
    monsvc::ptr<TH2I> m_RobsPerRequest_recent;
    std::unique_ptr<TH2I> m_RobsPerRequest_last;

    monsvc::ptr<TH2I> m_DcResponseTime_us;
    monsvc::ptr<TH2I> m_DcResponseTime_us_recent;
    std::unique_ptr<TH2I> m_DcResponseTime_us_last;

    monsvc::ptr<TH1I> m_DcAvailableCredits;
    monsvc::ptr<TH1I> m_DcAvailableCredits_recent;
    std::unique_ptr<TH1I> m_DcAvailableCredits_last;

  };

  void onSessionConnect(Session* session);
  void onSessionConnectError(Session* session, const boost::system::error_code& error);
  void onSessionDisconnect(Session* session);
  void distributeCredits();
  void returnCredits(std::uint32_t nCredits);
  void applyCreditDelta();
  void onSessionFetch(Session* session, Transaction* transaction);
  void onSessionFetchError(Session* session, const boost::system::error_code& error,
      Transaction* transaction);
  void onTimer(const boost::system::error_code& error);

  Main& m_main;
  const dal::DcmRosDataCollector& m_config;
  Processor* m_processor;

  std::vector<std::string> m_sources;
  std::vector<std::uint32_t> m_robIds;
  std::map<std::uint32_t, std::string> m_robIdToSource;
  std::multimap<std::string, std::uint32_t> m_sourceToRobIds;

  std::uint32_t m_creditsAvailable;
  std::uint32_t m_creditsTotal;
  std::int64_t m_creditsDelta;
  std::list<Transaction> m_transactions;

  std::size_t m_nOpenSessions;
  std::vector<std::shared_ptr<Session>> m_sessions;
  std::map<std::string, Session*> m_sourceToSession;
  std::map<std::uint32_t, Session*> m_robIdToSession;

  std::map<std::uint64_t, std::map<std::string, std::set<std::uint32_t>>> m_eventToPrefetchRobs;

  boost::asio::steady_timer m_timer;

  Histograms m_histograms;
  std::chrono::steady_clock::time_point m_recentHistogramsEpoch;
  std::deque<std::pair<std::chrono::steady_clock::time_point, std::uint32_t>> m_creditsAvailableHistory;

};

} // namespace dcm
} // namespace daq

#endif // !defined(DCM_ROSDATACOLLECTOR_H)
