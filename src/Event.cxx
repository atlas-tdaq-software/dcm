#include <sstream>

#include "dcm/Event.h"

namespace daq
{
namespace dcm
{


const Event::ForceAccept Event::ForceAccept::NONE = Event::ForceAccept(0x0000);
const Event::ForceAccept Event::ForceAccept::HLTSV_REASSIGN = Event::ForceAccept(0x2001);
const Event::ForceAccept Event::ForceAccept::HLTPU_CRASH = Event::ForceAccept(0x6002);
const Event::ForceAccept Event::ForceAccept::HLTPU_TIMEOUT = Event::ForceAccept(0x6003);
const Event::ForceAccept Event::ForceAccept::FETCHROBS_ERROR = Event::ForceAccept(0x6004);
const Event::ForceAccept Event::ForceAccept::L1ID_MISMATCH = Event::ForceAccept(0x2005);
const Event::ForceAccept Event::ForceAccept::DFINTERFACE_ERROR = Event::ForceAccept(0x6006);
const Event::ForceAccept Event::ForceAccept::INVALID_ROBID_ERROR = Event::ForceAccept(0x2007);
// Convert ForceAccept value to string
std::string Event::ForceAccept::str() const {
  std::stringstream s;
  s << " (0x" << std::hex << m_code << std::dec << ")";
  std::string code = s.str();
  static const char* str[] = {
    "ForceAccept::NONE",
    "ForceAccept::HLTSV_REASSIGN",
    "ForceAccept::HLTPU_CRASH",
    "ForceAccept::HLTPU_TIMEOUT",
    "ForceAccept::FETCHROBS_ERROR",
    "ForceAccept::L1ID_MISMATCH",
    "ForceAccept::DFINTERFACE_ERROR",
    "ForceAccept::INVALID_ROBID_ERROR",
    "ForceAccept::???"
  };
  int val = m_code & 0x1FFF;
  if (val < 0 || val > 8) {
    val = 8;
  }
  return str[val] + code;
}

Event::Event(SharedBlockArray::Writable &sba) :
    sba(sba),
    block(sba.allocate()),
    markedDone(false),
    forceAccept(ForceAccept::NONE),
    globalId(0),
    l1Id(0),
    lumiBlock(0),
    bcId(0),
    hasL1IdMismatch(false),
    hasBcIdMismatch(false),
    eventHeader(0),
    dataCollectionDuration(std::chrono::steady_clock::duration::zero()),
    lastDataEnd(0),
    possibleDuplicate(false),
    nbrRetries(0),
    inputEvent(0)
{
  m_gcBlocks.reserve(50);
}


Event::~Event()
{
  if (!block.robInfoIndex().empty()) {
    ERS_ASSERT(markedDone);
  } else {
    ERS_ASSERT(!markedDone);
  }
  sba.deallocate(block);
}


std::uint32_t *Event::allocate(size_t nbr_uint32) const
{
  m_gcBlocks.emplace_back(new std::uint32_t[nbr_uint32]);
  return m_gcBlocks.back().get();
}


} // namespace dcm
} // namespace daq

