#include "dcm/DummyL1Source.h"

#include "dcm/Event.h"
#include "dcm/Main.h"
#include "dcm/Processor.h"
#include "dcm/dal/DcmDummyL1Source.h"

#include "eformat/SourceIdentifier.h"
#include "eformat/Status.h"
#include "eformat/write/ROBFragment.h"
#include "is/infodictionary.h"

#include <boost/system/error_code.hpp>
#include <algorithm>
#include <chrono>
#include <cstdint>
#include <cstring> // for std::memcpy
#include <memory>

namespace daq
{
namespace dcm
{

DummyL1Source::DummyL1Source(Main& main, const dal::DcmDummyL1Source& config) :
    m_main(main),
    m_config(config),
    m_processor(nullptr),
    m_delayDistribution_us(config.get_delayDistribution_us()),
    m_sizeDistribution_words(config.get_sizeDistribution_words()),
    m_lastGlobalId(0),
    m_timer(m_main.ioService())
{
}

void DummyL1Source::initialize(Processor* processor)
{
  m_processor = processor;
}

void DummyL1Source::asyncConnect()
{
  m_lastGlobalId = 0;
  m_main.ioService().post(std::bind(&Main::onL1SourceConnect, &m_main));
}

void DummyL1Source::asyncStart()
{
  m_main.ioService().post(std::bind(&Main::onL1SourceStart, &m_main));
}

void DummyL1Source::asyncStop()
{
  for (auto& request : m_requests) {
    m_processor->onFetchL1RError(dcm::L1RRequestCanceled(ERS_HERE),
        std::move(request.event), request.context);
  }
  m_requests.clear();
  m_timer.cancel();
  m_main.ioService().post(std::bind(&Main::onL1SourceStop, &m_main));
}

void DummyL1Source::asyncDisconnect()
{
  m_main.ioService().post(std::bind(&Main::onL1SourceDisconnect, &m_main));
}

void DummyL1Source::asyncFetchL1R(std::unique_ptr<Event> event, void* context)
{
  bool empty = m_requests.empty();
  m_requests.emplace_back(std::move(event), context);
  if (empty) {
    startTimer();
  }
  m_main.isInfo()->L1SourceRequestedEvents++;
  m_main.isInfo()->L1SourceReceivedEvents++;
}

void DummyL1Source::markDone(const Event* event)
{
  auto it = m_pendingL1Ids.find(event->l1Id);
  ERS_ASSERT(it != m_pendingL1Ids.end());
  m_pendingL1Ids.erase(it);
  m_main.isInfo()->L1SourceDoneEvents++;
}

void DummyL1Source::startTimer()
{
  m_timer.expires_from_now(std::chrono::microseconds(m_delayDistribution_us.next_value()));

  m_timer.async_wait([this] (const boost::system::error_code& error) {

    if (m_requests.empty()) {
      // This only happens at STOP
      return;
    }

    namespace efh = eformat::helper;
    namespace efw = eformat::write;

    auto& request = m_requests.front();
    auto& event = *request.event;

    // TODO: better error handling
    if (error) {
      m_processor->onFetchL1RError(makeSystemError(ERS_HERE,error),
                                   std::move(request.event), request.context);
      return;
    }

    ++m_lastGlobalId;

    event.globalId = m_lastGlobalId;
    // Use the least significant 32 bits of the global ID as Level-1 ID
    event.l1Id = m_lastGlobalId & 0x00000000FFFFFFFFull;
    m_pendingL1Ids.insert(event.l1Id);

    namespace efh = eformat::helper;
    namespace efw = eformat::write;

    // Manufacture a RoI

    const std::uint32_t bunchCrossingId = 0x1;
    // L1 Trigger type is an 8-bit field. If the most significant bit (bit 8) is set, the event
    // was selected for physics, and the bits 7 to 0 are a bitmask detailing the trigger type. If
    // the MSB is not set, the event was selected for calibration, and bits 7 to 3 are the integer
    // detector ID.
    // Here we manufacture a physics L1 trigger type, with one detail bit set.
    const std::uint32_t l1TriggerType = 0x81;
    // The event type is a sub-detector specific 32-bit field. We leave it empty.
    const std::uint32_t eventType = 0x0;

    // For the CTP ROB we use a fixed, "real" payload
    static const std::uint8_t ctpRodPayload[] = {
      0x00, 0x5d, 0x47, 0x5e, 0x13, 0xf0, 0xcc, 0x50, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x3d, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x02, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x02, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x01, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x40, 0x82, 0x00, 0x3e, 0x10, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x20, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x90, 0x00, 0x3f, 0x00, 0x00, 0x00,
      0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
      0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x02, 0x00, 0x00, 0x02, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x02, 0x00, 0x00, 0x02, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0xf9, 0x06, 0x00, 0x00, 0x3f, 0xa2, 0x88, 0x04, 0x56, 0x26, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
      0x00, 0x00, 0xf7, 0x00
    };
    // For the other ROBs we choose a payload size from the configured distribution and use
    // uninitialized memory as payload.
    std::array<std::uint32_t, 7> sizes_words;
    std::generate(sizes_words.begin(), sizes_words.end(),
        std::bind(&DC::Distribution::next_value, &m_sizeDistribution_words));
    std::unique_ptr<std::uint32_t[]> data(
        new std::uint32_t[*std::max_element(sizes_words.begin(), sizes_words.end()) * 4]);

    std::array<efw::ROBFragment, 8> robs { {
      { efh::SourceIdentifier(eformat::TDAQ_CTP, 1).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizeof(ctpRodPayload) / 4, (const uint32_t*)ctpRodPayload, eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_CLUSTER_PROC_ROI, 0).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[0], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_CLUSTER_PROC_ROI, 1).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[1], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_CLUSTER_PROC_ROI, 2).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[2], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_CLUSTER_PROC_ROI, 3).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[3], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_JET_PROC_ROI, 0).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[4], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_CALO_JET_PROC_ROI, 1).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[5], data.get(), eformat::STATUS_FRONT },
      { efh::SourceIdentifier(eformat::TDAQ_MUON_CTP_INTERFACE, 0).code(),
        m_main.runParams().run_number, event.l1Id, bunchCrossingId, l1TriggerType, eventType,
        sizes_words[6], data.get(), eformat::STATUS_FRONT }
    } };


    // Serialize to SBA
    // TODO: catch SBA exceptions
    std::uint32_t robsSize = 0;
    for (auto& rob: robs) {
      robsSize += rob.size_word() * 4;
    }
    std::uint32_t* buffer = event.block.startAppendRobData(robsSize);
    for (auto& rob: robs) {
      auto node = rob.bind();
      do {
        std::memcpy(buffer, node->base, node->size_word * 4);
        buffer += node->size_word;
        node = node->next;
      } while (node);
    }
    event.l1Result = event.block.endAppendRobData(robsSize);

    // TODO: fill the request.event's bcId field

    m_processor->onFetchL1R(std::move(request.event), request.context);

    m_requests.pop_front();
    if (!m_requests.empty()) {
      startTimer();
    }

  });
}

} // namespace dcm
} // namespace daq
