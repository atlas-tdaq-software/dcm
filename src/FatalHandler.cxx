#include "dcm/FatalHandler.h"

#define _GNU_SOURCE 1

#include <stdio.h>

#include <execinfo.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace daq
{
namespace dcm
{
namespace FatalHandler
{

void writeBacktrace(int fd)
{
  void* addresses[256];
  int count;
  write(fd, "======= Backtrace: =========\n", 29);
  count = backtrace(addresses, 256);
  backtrace_symbols_fd(addresses, count, fd);
}

void writeMaps(int fd)
{
  int fd2;
  write(fd, "======= Memory map: ========\n", 29);
  if ((fd2 = open("/proc/self/maps", O_RDONLY)) >= 0) {
    char buf[1024];
    ssize_t n;
    while ((n = read(fd2, buf, sizeof(buf))) > 0)
      if (write(fd, buf, n) != n)
        break;
    close(fd2);
  }
}

void fatal(int signum)
{
  const char* signame = NULL;
  int fd = STDERR_FILENO;
  char str[128];
  int n;
  struct sigaction sa;

  /* Associate each signal with a signal signame string. */
  switch (signum) {
    case SIGABRT:
      signame = "SIGABRT";
      break;
    case SIGSEGV:
      signame = "SIGSEGV";
      break;
    case SIGBUS:
      signame = "SIGBUS";
      break;
    case SIGILL:
      signame = "SIGILL";
      break;
    case SIGFPE:
      signame = "SIGFPE";
      break;
  }
  if (signame)
    n = sprintf(str, "Caught signal %d (%s)\n", signum, signame);
  else
    n = sprintf(str, "Caught signal %d\n", signum);
  write(fd, str, n);

  writeBacktrace(fd);

  writeMaps(fd);

  /* Re-arm default handler */
  sa.sa_handler = SIG_DFL;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  sigaction(signum, &sa, NULL);
}

void install()
{
  static const int fatal_signals[] = { SIGABRT, SIGSEGV, SIGBUS, SIGILL, SIGFPE };
  static const int n_signals = sizeof(fatal_signals) / sizeof(int);

  int i;
  struct sigaction sa;

  sa.sa_handler = fatal;
  sigemptyset(&sa.sa_mask);
  for (i = 0; i < n_signals; ++i)
    sigaddset(&sa.sa_mask, fatal_signals[i]);
  sa.sa_flags = SA_RESTART;
  for (i = 0; i < n_signals; ++i)
    sigaction(fatal_signals[i], &sa, NULL);
}

} // namespace FatalHandler
} // namespace dcm
} // namespace daq
