#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>

#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/FullEventFragmentNoTemplates.h"
#include "eformat/write/FullEventFragment.h"
#include "dcm/DummyOutput.h"
#include "dcm/Issues.h"


using namespace std;
namespace daq
{
  namespace dcm
  {

    void printValues( const char* fmtLabel, uint32_t valWidth, const char* fmtVal,
                      const uint32_t *ie, const uint32_t* be, uint32_t &kie, uint32_t &kbe,
                      const char* label, uint32_t nie = 1, uint32_t nbe = 1 )
    {
      // check if values are different
      if( nie == nbe )
      {
        bool same = true;
        for( uint32_t n = nie, i = 0; i < n && same; ++i )
          if( ie[kie+i] != be[kbe+i] )
            same = false;
        if( same )
        {
          kie += nie;
          kbe += nbe;
          return;
        }
      }

      // print values
      char padding[100];
      for( uint32_t i = 0; i < valWidth; ++i )
        padding[i] = ' ';
      padding[valWidth] = '\0';
      while( nie || nbe )
      {
        bool same = nie && nbe && (ie[kie] == be[kbe]);
        printf( fmtLabel, label );
        if( nie )
        {
          printf( fmtVal, ie[kie++] );
          nie--;
        }
        else
          printf( "%s", padding );
        if( nbe )
        {
          printf( fmtVal, be[kbe++] );
          nbe--;
        }
        else
          printf( "%s", padding );
        printf( same ? "\n" : "  !\n");
      }
    }

    // Construct DummyOutput with binary file in which to write received events
    DummyOutput::DummyOutput( const std::string& outFileName ) : m_eventBuilder(0), m_fd(-1)
    {
      // Try creating the file, fails if the file already exist
      int fd = ::open( outFileName.c_str(), O_CREAT|O_RDWR|O_EXCL,
                       S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH );
      if( fd < 0 )
      {
        std::stringstream s;
        s << "Failed to open or create the file '"
          << outFileName << "' as writable : " << strerror( errno );
        throw dcm::OutputIssue(ERS_HERE, s.str());
      }
      m_fd = fd;
    }

    // Writes the reveived eformat event into the binary file
    void DummyOutput::asyncEventSend( std::unique_ptr<Event> inEvent )
    {
      std::cout << "Start DummyOutput::asyncEventSend()" << std::endl;

      if( m_eventBuilder == nullptr )
        throw dcm::OutputIssue(ERS_HERE, "Output has null EventBuilder pointer" );

      // Check parameter integrity
      if( inEvent == nullptr )
        throw dcm::OutputIssue(ERS_HERE, "DummyOutput::asyncEventSend() received null event");
      if( inEvent->serializedEvents == nullptr )
        throw dcm::OutputIssue(ERS_HERE, "DummyOutput::asyncEventSend() received null event data");
      try
      {
        ssize_t res = ::write( m_fd, inEvent->serializedEvents, inEvent->serializedEventsByteSize );
        if( res != (ssize_t)inEvent->serializedEventsByteSize )
        {
          std::stringstream s;
          s << "DummyOutput::asyncEventSend() write failed: " << strerror(errno);
          throw dcm::OutputIssue(ERS_HERE, s.str() );
        }

        // If the initial input event is provided, check for differences
        if( inEvent->inputEvent )
        {
          // Compare the builtEvent with the inputEvent
          const uint32_t *ie = inEvent->inputEvent;
          const uint32_t *be = inEvent->serializedEvents;
          char fmtLabel[] = "%25s :";
          char fmtValHex[] = " 0x%08X";
          char fmtValDec[] = " %10d";
          uint32_t valWidth = 11;

          uint32_t kie = 0, kbe = 0;
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Marker" );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "EventSize" );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "HeaderSize" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Version" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "SourceId" );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "nbrStatus" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "| Status", ie[kie-1], be[kbe-1] );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "CheckSum" );
          // Specific header
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "BunchCrossing(s)" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "BunchCrossing(ns)" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Global id" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Run type" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Run number" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Luminosity block" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Extended lvl1 id" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "BCID" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Lvl1 trigger type" );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "nbr lvl1 info words" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Lvl1 info | ", ie[kie-1], be[kbe-1] );
          // Skip lvl2 difference
          kie += ie[kie]+1;
          kbe += be[kbe]+1;
          // printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "nbr lvl2 info words" );
          // printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Lvl2 info | ", ie[kie-1], be[kbe-1] );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "nbr lvl3 info words" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "Lvl3 info | ", ie[kie-1], be[kbe-1] );
          printValues( fmtLabel, valWidth, fmtValDec, ie, be, kie, kbe, "nbr streamTag words" );
          printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "streamTag | ", ie[kie-1], be[kbe-1] );
          // printValues( fmtLabel, valWidth, fmtValHex, ie, be, kie, kbe, "ROB data | ", ie[kie+2], be[kbe+2] );
        }
        // TODO queue callback to m_eventBuilder->onEventSent( move( inEvent ) );
      }
      catch( std::exception& e )
      {
        // TODO: queue callback to m_eventBuilder->onEventSentError( issue, move( inEvent ) );
        throw;
      }
    }
  }
}
