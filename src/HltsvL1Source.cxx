#include "dcm/HltsvL1Source.h"

#include "dcm/Event.h"
#include "dcm/Main.h"
#include "dcm/Processor.h"
#include "dcm/Utilities.h"
#include "dcm/dal/DcmHltsvL1Source.h"

#include "asyncmsg/Error.h"
#include "asyncmsg/Session.h"
#include "dal/Component.h"
#include "DFdal/DFParameters.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/Status.h"
#include "eformat/write/ROBFragment.h"
#include "is/exceptions.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <algorithm>
#include <chrono>
#include <cstdint>
#include <cstring> // for std::memcpy
#include <memory>

namespace daq
{
namespace dcm
{

class HltsvSession: public asyncmsg::Session
{

private:

  struct FetchRequest
  {
    std::unique_ptr<Event> event;
    void* context = nullptr;
  };

  struct UpdateMessage: public asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF00;

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      // FIXME: these assume we are on a little-endian arch
      buffers.emplace_back(static_cast<const void*>(&nRequestedL1Rs), sizeof(nRequestedL1Rs));
      for (auto& l1Id : doneL1Ids) {
        buffers.emplace_back(static_cast<const void*>(&l1Id), sizeof(l1Id));
      }
    }

    std::uint32_t nRequestedL1Rs = 0;
    std::vector<std::uint32_t> doneL1Ids;
  };

  class L1RMessage: public daq::asyncmsg::InputMessage
  {
  public:

    static const std::uint32_t TYPE_ID = 0x00DCDF01;
    static const std::uint32_t REASSIGNMENT_TYPE_ID = 0x00DCDF0F;

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      // FIXME: these assume we are on a little-endian arch
      buffers.emplace_back(static_cast<void*>(&event->globalId), sizeof(event->globalId));
      buffers.emplace_back(static_cast<void*>(&event->l1Id), sizeof(event->l1Id));
      buffers.emplace_back(l1ResultData, l1ResultSize);
    }

    void* context;
    std::unique_ptr<Event> event;
    void* l1ResultData;
    std::uint32_t l1ResultSize;

  };

public:

  HltsvSession(Main& main, const dal::DcmHltsvL1Source& config, Processor& processor,
      const std::string& remoteName) :
      asyncmsg::Session(main.ioService()),
      m_main(main),
      m_processor(processor),
      m_localName(main.applicationName()),
      m_remoteName(remoteName),
      m_maxClearDelay(std::chrono::milliseconds(config.get_maxClearDelay_ms())),
      m_timer(m_main.ioService()),
      m_inTransition(false),
      m_running(false)
  {
  }

  const std::string& remoteName() const
  {
    return m_remoteName;
  }

  const boost::asio::ip::tcp::endpoint& remoteEndpoint() const
  {
    return m_remoteEndpoint;
  }

  std::string remoteEndpointStr() const
  {
    std::ostringstream out;
    out << remoteEndpoint();
    return out.str();
  }

  void asyncConnect()
  {
    m_inTransition = true;
    doOpen();
    m_timer.expires_at(std::chrono::steady_clock::now());
    m_timer.async_wait(std::bind(&HltsvSession::onTimer, this, std::placeholders::_1));
  }

private:

  void doOpen()
  {
    try {
      m_remoteEndpoint = m_main.resolveServer(m_remoteName);
    }
    catch (ers::Issue& ex) {
      ers::error(ex);
      auto self = shared_from_this();
      getStrand().post([this, self] ()
      {
        onOpenError(make_error_code(boost::asio::error::host_not_found));
      });
      return;
    }
    asyncOpen(m_localName, m_remoteEndpoint);
  }

  void onOpen() override
  {
    ERS_LOG("Connection to " << remoteName() << " (" << remoteEndpoint() << ") open.");
    if (m_inTransition) {
      m_main.ioService().post(std::bind(&Main::onL1SourceConnect, &m_main));
      m_inTransition = false;
    }
    if (!m_fetchRequests.empty() || !m_pendingDoneL1Ids.empty()) {
      asyncSendUpdate(m_fetchRequests.size());
    }
    asyncReceive();
  }

  void onOpenError(const boost::system::error_code& error) override
  {
    ers::error(dcm::L1RSessionOpenError(ERS_HERE, remoteName(), remoteEndpointStr(),
                                        makeSystemError(ERS_HERE, error)));
    if (m_inTransition) {
      m_main.ioService().post(std::bind(&Main::onL1SourceConnectError, &m_main, error));
      m_inTransition = false;
    }
  }

public:

  void asyncDisconnect()
  {
    ERS_ASSERT(m_fetchRequests.empty());
    ERS_ASSERT(m_pendingDoneL1Ids.empty());

    m_timer.cancel();
    if (state() == State::CLOSED) {
      m_main.ioService().post(std::bind(&Main::onL1SourceDisconnect, &m_main));
    }
    else {
      m_inTransition = true;
      if (state() != State::CLOSE_PENDING) {
        asyncClose();
      }
    }
  }

private:

  void onClose() override
  {
    ERS_LOG("Connection to " << remoteName() << " (" << remoteEndpoint() << ") closed.");
    if (m_inTransition) {
      m_main.ioService().post(std::bind(&Main::onL1SourceDisconnect, &m_main));
      m_inTransition = false;
    }
  }

  void onCloseError(const boost::system::error_code& error) override
  {
    DCM_ERROR(dcm::GenericIssue, "session close failed: " << error.message());
  }

public:

  void asyncStart()
  {
    ERS_ASSERT(m_fetchRequests.empty());

    // Clear leftover done L1IDs
    m_pendingDoneL1Ids.clear();

    m_running = true;

    m_main.ioService().post(std::bind(&Main::onL1SourceStart, &m_main));
  }

  void asyncStop()
  {
    ERS_DEBUG(3,"Starting HLTsvL1Source asyncStop");
    m_running = false;

    // Flush remaining done L1IDs
    if (state() == State::OPEN && !m_pendingDoneL1Ids.empty()) {
      asyncSendUpdate(0);
    }

    auto self = shared_from_this();
    m_main.ioService().post([this, self] () {
      // Abort remaining fetch requests
      //for (auto& fetchRequest : m_fetchRequests) {
         ////TODO: use a proper error code
        //m_processor.onFetchL1RError(dcm::L1RRequestCanceled(ERS_HERE),
            //std::move(fetchRequest.event), fetchRequest.context);
      //}
      m_fetchRequests.clear();
      m_main.onL1SourceStop();
    });
  }

  void asyncFetchL1R(std::unique_ptr<Event> event, void* context)
  {
    ERS_DEBUG(3, "Entered asyncFetchL1R."); 
    m_fetchRequests.emplace_back();
    m_fetchRequests.back().event = std::move(event);
    m_fetchRequests.back().context = context;
    if (state() == State::OPEN) {
      asyncSendUpdate(1);
    }
    ERS_DEBUG(3,"Done");
  }

  void markDone(const Event* event)
  {
    if (m_pendingDoneL1Ids.empty()) {
      m_oldestPendingDoneL1IdTime = std::chrono::steady_clock::now();
    }
    m_pendingDoneL1Ids.push_back(event->l1Id);
    if (state() == State::OPEN && m_maxClearDelay == m_maxClearDelay.zero()) {
      asyncSendUpdate(0);
    }
  }

private:

  void asyncSendUpdate(std::uint32_t nRequestedL1Rs) {
    auto update = make_unique<UpdateMessage>();
    update->nRequestedL1Rs = nRequestedL1Rs;
    std::swap(m_pendingDoneL1Ids, update->doneL1Ids);
    asyncSend(std::move(update));
  }

  std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t /* transactionId */, std::uint32_t size) override
  {
    if (typeId == L1RMessage::TYPE_ID || typeId == L1RMessage::REASSIGNMENT_TYPE_ID) {
      if (!m_fetchRequests.empty()) {
        if (size > (sizeof(Event::l1Id) + sizeof(Event::globalId))) {
          auto l1R = make_unique<L1RMessage>();
          l1R->context = m_fetchRequests.front().context;
          l1R->event = std::move(m_fetchRequests.front().event);
          l1R->l1ResultSize = size - sizeof(l1R->event->l1Id) - sizeof(l1R->event->globalId);
          l1R->l1ResultData = l1R->event->block.startAppendRobData(l1R->l1ResultSize);
          m_fetchRequests.pop_front();
          if (typeId == L1RMessage::REASSIGNMENT_TYPE_ID) {
            l1R->event->forceAccept = Event::ForceAccept::HLTSV_REASSIGN;
          }
          return std::move(l1R);
        }
        else {
          ers::warning(dcm::HLTSVSessionError(ERS_HERE, "Malformed (too short) L1Result received",
                                              remoteName(), remoteEndpointStr(), "Ignoring"));
          asyncReceive();
          return std::unique_ptr<asyncmsg::InputMessage>();
        }
      }
      else {
        ers::warning(dcm::HLTSVSessionError(ERS_HERE, "Unsolicited L1Result received",
                                            remoteName(), remoteEndpointStr(), "Ignoring"));
        asyncReceive();
        return std::unique_ptr<asyncmsg::InputMessage>();
      }
    }
    else {
      ers::warning(dcm::HLTSVSessionBadMessage(ERS_HERE, remoteName(), remoteEndpointStr(), typeId, "Ignoring"));
      asyncReceive();
      return std::unique_ptr<asyncmsg::InputMessage>();
    }
  }

  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    ERS_DEBUG(3,"onReceive HLTSV L1 source.");
    auto l1R = static_pointer_cast<L1RMessage>(std::move(message));
    try {
      l1R->event->l1Result = l1R->event->block.endAppendRobData(l1R->l1ResultSize);
      m_processor.onFetchL1R(std::move(l1R->event), l1R->context);
    }
    catch (SBAInvalidArgument& ex) {
      dcm::L1RFetchingError issue(ERS_HERE, "Error appending ROBs to SBA", ex);
      ers::error(issue);
      m_processor.onFetchL1RError(issue, std::move(l1R->event), l1R->context);
    }
    catch (SBARunTimeError& ex) {
      dcm::L1RFetchingError issue(ERS_HERE, "Error appending ROBs to SBA", ex);
      ers::error(issue);
      m_processor.onFetchL1RError(issue, std::move(l1R->event), l1R->context);
    }
    asyncReceive();
    m_main.isInfo()->L1SourceReceivedEvents++;
  }

  void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    if (message) {
      dcm::HLTSVSessionError issue(ERS_HERE, "Error receiving L1 Result 'Assign'",
                                   remoteName(), remoteEndpointStr(), "Aborting",
                                   makeSystemError(ERS_HERE, error));
      ers::error(issue);
      auto l1R = static_pointer_cast<L1RMessage>(std::move(message));
      l1R->event->block.endAppendRobData(0);
      if (m_running) {
        m_fetchRequests.emplace_back();
        m_fetchRequests.back().event = std::move(l1R->event);
        m_fetchRequests.back().context = l1R->context;
      }
      else {
        m_processor.onFetchL1RError(issue, std::move(l1R->event), l1R->context);
      }
    }
    else if (!(m_inTransition && error == boost::asio::error::operation_aborted)) {
      ers::error(dcm::HLTSVSessionError(ERS_HERE, "Error receiving message",
                                        remoteName(), remoteEndpointStr(), "Aborting",
                                        makeSystemError(ERS_HERE, error)));
    }
    if (state() == State::OPEN) {
      ++m_main.isInfo()->L1SourceConnectionErrors;
      asyncClose();
    }
  }

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override
  {
    ERS_DEBUG(3, "onSend HLTSV L1 source.");
    auto update = static_pointer_cast<const UpdateMessage>(std::move(message));
    m_main.isInfo()->L1SourceRequestedEvents += update->nRequestedL1Rs;
    m_main.isInfo()->L1SourceDoneEvents += update->doneL1Ids.size();
  }

  void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override
  {
    ers::error(dcm::HLTSVSessionError(ERS_HERE, "Error sending Update Message",
                                      remoteName(), remoteEndpointStr(), "Aborting",
                                      makeSystemError(ERS_HERE, error)));
    if (m_running) {
      auto update = static_pointer_cast<const UpdateMessage>(std::move(message));
      m_pendingDoneL1Ids.insert(m_pendingDoneL1Ids.end(),
          update->doneL1Ids.begin(), update->doneL1Ids.end());
    }
    if (state() == State::OPEN) {
      ++m_main.isInfo()->L1SourceConnectionErrors;
      asyncClose();
    }
  }

  void onTimer(const boost::system::error_code& error)
  {
    if (error) {
      if (error != boost::asio::error::operation_aborted) {
        ers::error(dcm::L1RFetchingError(ERS_HERE, "HLTSV connection watchdog timer error. Aborting",
                                         makeSystemError(ERS_HERE, error)));
      }
      return;
    }

    if (state() == State::CLOSED) {
      // Automatic reconnection
      doOpen();
    }

    if (state() == State::OPEN &&
        !m_pendingDoneL1Ids.empty() &&
        std::chrono::steady_clock::now() >= m_oldestPendingDoneL1IdTime + m_maxClearDelay) {
      asyncSendUpdate(0);
    }

    m_timer.expires_at(m_timer.expires_at() + std::chrono::seconds(1));
    m_timer.async_wait(std::bind(&HltsvSession::onTimer, this, std::placeholders::_1));

  }

  Main& m_main;
  Processor& m_processor;

  std::string m_localName;
  std::string m_remoteName;
  boost::asio::ip::tcp::endpoint m_remoteEndpoint;
  const std::chrono::steady_clock::duration m_maxClearDelay;

  std::deque<FetchRequest> m_fetchRequests;
  std::vector<std::uint32_t> m_pendingDoneL1Ids;
  std::chrono::steady_clock::time_point m_oldestPendingDoneL1IdTime;

  boost::asio::steady_timer m_timer;
  bool m_inTransition;
  bool m_running;

};

HltsvL1Source::HltsvL1Source(Main& main, const dal::DcmHltsvL1Source& config) :
    m_main(main),
    m_config(config)
{
  std::set<std::string> types = { "HLTSVApplication" };
  std::vector<const daq::core::BaseApplication *> apps = m_main.partitionConf().get_all_applications(&types);
  std::vector<std::string> hltsvs;
  for (auto& app : apps) {
    auto component = m_main.confDatabase().cast<daq::core::Component>(app->get_base_app());
    if (component == nullptr || !component->disabled(m_main.partitionConf())) {
      hltsvs.push_back(app->UID());
    }
  }

  if (hltsvs.empty()) {
    // TODO: use a better exception and throw, so that we fail the CONFIGURE transition
    ers::fatal(dcm::NoHLTSVFound(ERS_HERE));
    return;
  }

  m_hltsvName = hltsvs[0];
  if (hltsvs.size() > 1) {
    // TODO: use a better exception
    ers::warning(dcm::ManyHLTSVFound(ERS_HERE, m_hltsvName));
  }
}

void HltsvL1Source::initialize(Processor* processor)
{
  m_session = std::make_shared<HltsvSession>(m_main, m_config, *processor, m_hltsvName);
}

void HltsvL1Source::asyncConnect()
{
  m_session->asyncConnect();
}

void HltsvL1Source::asyncStart()
{
  m_session->asyncStart();
}

void HltsvL1Source::asyncStop()
{
  m_session->asyncStop();
}

void HltsvL1Source::asyncDisconnect()
{
  m_session->asyncDisconnect();
}

void HltsvL1Source::asyncFetchL1R(std::unique_ptr<Event> event, void* context)
{
  m_session->asyncFetchL1R(std::move(event), context);
}

void HltsvL1Source::markDone(const Event* event)
{
  m_session->markDone(event);
}

} // namespace dcm
} // namespace daq
