
#include "dcm/HltpuProcessor.h"
#include "dcm/dal/DcmHltpuProcessor.h"
#include "dcm/Main.h"
#include "dcm/Utilities.h"
#include "dcm/PortFileName.h"
#include "dcm/dfinterfaceDcm.h"
#include <memory>


namespace daq
{
namespace dcm
{

// --------------------------------- HLTPUServ class  ---------------------------------


HltpuProcessor::HLTPUSrv::HLTPUSrv(HltpuProcessor*processor, boost::asio::io_service& ioService) :
  asyncmsg::Server(ioService),
  processor(processor)
{
}


void HltpuProcessor::HLTPUSrv::initialize()
{
  // Listen on localhost (ipv4) with port 0 (the server will pick an available port)
  listen(processor->m_main.applicationName(),
         boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 0));

  // Get the server listening port number
  unsigned short port = localEndpoint().port();

  // Calculate the file name
  std::string portNumberFileName = ::daq::dcm::getPortFileName();
  if (portNumberFileName.empty())
  {
    throw dcm::InvalidArgument(ERS_HERE, "Missing $TDAQ_PARTITION in the enviroment. Can't construct port file name");
  }

  // Write the port number in the file
  std::ofstream f(portNumberFileName);
  if (!f)
  {
    std::stringstream s;
    s << "Failed opening '" << portNumberFileName << "' for writing the DCM listening port number : " << strerror( errno );
    throw dcm::InvalidArgument(ERS_HERE, s.str() );
  }
  f << "DCM_HltpuServer_portNumber " << port << std::endl;
  f.close();

  ERS_LOG( "Created DCM Server port number file '" << portNumberFileName << "' containing port number " << port );

  asyncAccept(std::make_shared<HLTPUSes>(processor, processor->m_main.ioService()));
}


void HltpuProcessor::HLTPUSrv::onAccept(std::shared_ptr<asyncmsg::Session> /*session*/)
{
  asyncAccept(std::make_shared<HLTPUSes>(processor, processor->m_main.ioService()));
}


void HltpuProcessor::HLTPUSrv::onAcceptError(const boost::system::error_code & error,
                                                std::shared_ptr<asyncmsg::Session> /*session*/ )
{
  if (error == boost::system::errc::operation_canceled)
    return;
  ers::warning(dcm::AcceptHLTPUSessionError(ERS_HERE, makeSystemError(ERS_HERE, error)));
  asyncAccept(std::make_shared<HLTPUSes>(processor, processor->m_main.ioService()));
}


// --------------------------------- HLTPUCli class  ---------------------------------

//        Request messages received from the hltpu
class RequestMessage : public asyncmsg::InputMessage
{
public:
  // Constructor
  RequestMessage( std::uint32_t typeId, std::uint32_t transactionId, std::uint32_t size ):
    m_typeId(typeId),
    m_transactionId(transactionId),
    m_size(size),
    m_data(new std::uint8_t[size])
  {
  }

  // Return the message type id
  std::uint32_t typeId() const override { return m_typeId; }
  // Return the message transaction Id
  std::uint32_t transactionId() const override { return m_transactionId; }
  // Return the message payload size in bytes
  std::uint32_t size() const { return m_size; }
  // Return a pointer on the message data
  const std::uint8_t* data() const { return m_data.get(); }
  // Provide the buffers were to write the received data
  void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
    { buffers.emplace_back( m_data.get(), m_size ); }

private:
  std::uint32_t m_typeId;
  std::uint32_t m_transactionId;
  std::uint32_t m_size;
  std::unique_ptr<std::uint8_t[]> m_data;
};


//        Response message sent to the hltpu
class ResponseMessage : public asyncmsg::OutputMessage
{
public:
  // Constructor
  ResponseMessage( std::uint32_t typeId, std::uint32_t transactionId, std::uint32_t size ):
    m_typeId(typeId),
    m_transactionId(transactionId),
    m_size(size),
    m_data(new std::uint8_t[size])
  {
  }

  // Return the message type id
  std::uint32_t typeId() const override { return m_typeId; }
  // Return the message transaction Id
  std::uint32_t transactionId() const override { return m_transactionId; }
  // Return the message payload size in bytes
  std::uint32_t size() const { return m_size; }
  // Return a pointer on the message data
  std::uint8_t* data() { return m_data.get(); }
  // Provide the buffers containing the data to send as response
  void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
    { buffers.emplace_back( m_data.get(), m_size ); }

private:
  std::uint32_t m_typeId;
  std::uint32_t m_transactionId;
  std::uint32_t m_size;
  std::unique_ptr<std::uint8_t[]> m_data;
};


HltpuProcessor::HLTPUSes::HLTPUSes(HltpuProcessor* processor, boost::asio::io_service& ioService) :
  asyncmsg::Session(ioService),
  processor(processor),
  hltpu(nullptr),
  transactionId(0),
  sentNoMoreEvents(false)
{}


void HltpuProcessor::HLTPUSes::onOpen()
{
  std::shared_ptr<HLTPUSes> hltpuSes = std::dynamic_pointer_cast<HLTPUSes>(shared_from_this());
  ERS_ASSERT(hltpuSes != nullptr);
  this->hltpu = new HLTPU(hltpuSes);
  this->hltpu->name = remoteName();
  std::unique_ptr<Processor::HLTPU> hltpu(this->hltpu);
  if (!processor->attachHLTPU(hltpu))
  {
    ERS_LOG("Connection from '" << remoteName() << "' rejected!" );

    this->hltpu = nullptr; // so that onClose will not call terminate and detach
    asyncClose();
    return;
  }
}


void HltpuProcessor::HLTPUSes::onOpenError(const boost::system::error_code& error)
{
  ers::warning(dcm::OpenHLTPUSessionError(ERS_HERE, makeSystemError(ERS_HERE, error)));
}


std::unique_ptr<asyncmsg::InputMessage> HltpuProcessor::HLTPUSes::createMessage(
    std::uint32_t typeId, std::uint32_t transactionId, std::uint32_t payloadSize)
{
  return std::move(std::unique_ptr<RequestMessage>(new RequestMessage(typeId, transactionId, payloadSize)));
}


void HltpuProcessor::HLTPUSes::onClose()
{
  if (hltpu == nullptr) return; // Session closed, ignore callback
  HLTPU* tmp(hltpu);
  hltpu = nullptr; // Mark session closed
  processor->Processor::terminate(tmp);
}


void HltpuProcessor::HLTPUSes::onCloseError(const boost::system::error_code& error)
{
  ers::warning(dcm::CloseHLTPUSessionError(ERS_HERE, hltpu->name, makeSystemError(ERS_HERE, error)));

  if (error != daq::asyncmsg::Error::SESSION_NOT_OPEN)
    onClose();
}


void HltpuProcessor::HLTPUSes::onSend(std::unique_ptr<const asyncmsg::OutputMessage> /*message*/)
{
  if (hltpu == nullptr) return; // Session closed, ignore callback
  // Wait for the next request message to process unless we just sent noMoreEvents
  if (!sentNoMoreEvents)
    asyncReceive();
}


void HltpuProcessor::HLTPUSes::onSendError(const boost::system::error_code& error,
                 std::unique_ptr<const asyncmsg::OutputMessage> /*message*/)
{
  if (hltpu == nullptr) return; // Session closed, ignore callback
  if (error == boost::asio::error::operation_aborted)
    return;
  ERS_LOG("onSendError: " << error.message() << " with " << hltpu->name);

  if (hltpu->event) // was processing an event
  {
    ers::warning(dcm::HltpuCrashIssue(ERS_HERE, error.message().c_str(), hltpu->name,
                                      makeSystemError(ERS_HERE, error)));
    processor->forceAccept(hltpu, Event::ForceAccept::HLTPU_CRASH);
  }
  else
  {
    ers::warning(dcm::HltpuCrashIssue(ERS_HERE, error.message().c_str(), hltpu->name,
                                      makeSystemError(ERS_HERE, error)));
    asyncClose();
  }
}


void HltpuProcessor::HLTPUSes::onReceiveError(const boost::system::error_code& error,
                    std::unique_ptr<asyncmsg::InputMessage> /*message*/)
{
  if (hltpu == nullptr) return; // Session closed, ignore callback
  ERS_LOG("onReceiveError: " << error.message() << " with " << hltpu->name);

  if (error == boost::asio::error::operation_aborted)
    return;
  // If a forceAccept was initiated while an asyncReceive was in progress
  if (hltpu->event && hltpu->event->forceAccept != Event::ForceAccept::NONE)
  {
    // The error may be ignored because the connection will be closed
    ERS_LOG( "onReceiveError called while forceAccept " << hltpu->event->forceAccept << " with " << hltpu->name <<
             " Ignoring error" );
    // This is the only forceAccept type that can be in progress with an asyncReceive is in progress
    ERS_ASSERT(hltpu->event->forceAccept == Event::ForceAccept::HLTPU_TIMEOUT);
    return;
  }

  if (hltpu->event) // was processing an event
  {
    ers::warning(dcm::HltpuCrashIssue(ERS_HERE, error.message().c_str(), hltpu->name,
                                      makeSystemError(ERS_HERE, error)));
    processor->forceAccept(hltpu, Event::ForceAccept::HLTPU_CRASH);
  }
  else
  {
    ers::warning(dcm::HltpuCrashIssue(ERS_HERE, error.message().c_str(), hltpu->name,
                                      makeSystemError(ERS_HERE, error)));
    asyncClose();
  }
}


void HltpuProcessor::HLTPUSes::onReceive(std::unique_ptr<asyncmsg::InputMessage> message)
{
  if (hltpu == nullptr) return; // Session closed, ignore callback
  RequestMessage* req = dynamic_cast<RequestMessage*>(message.get());
  ERS_ASSERT(req != nullptr);

  // If a forceAccept was initiated while an asyncReceive was in progress
  if (hltpu->event && hltpu->event->forceAccept != Event::ForceAccept::NONE)
  {
    // The HLTPU request may be ignored because the connection will be closed
    ERS_LOG( "onReceive called while forceAccept " << hltpu->event->forceAccept << " with " << hltpu->name <<
             " Ignoring HLTPU requests" );
    return;
  }

  // --- Initialization request : check version and return the shareBlockArray file name immediatly
  if (req->typeId() == std::uint32_t(dfinterface::MsgTypeId::INIT_REQ))
  {
    if (hltpu->state != Processor::HLTPU::State::INITIALIZED) {
      if (!hltpu->event) {
        ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Received unexpected initialization request",
                                                hltpu->name, "Closing connection"));
      } else {
        ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Received unexpected initialization request while an event is processed",
                                                hltpu->name, "Closing connection"));
        processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      }
      asyncClose();
      return; // onClose() destroys the session
    }

    ERS_ASSERT(!hltpu->event);

    // Check protocol version
    if (req->size() != sizeof(std::uint32_t))
    {
      ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Received invalid initialization message",
                                              hltpu->name, "Closing connection"));
      asyncClose();
      return; // onClose() destroys the session
    }

    auto data = reinterpret_cast<const std::uint32_t*>(req->data());
    if (data[0] != dfinterface::PROTOCOL_VERSION)
    {
      std::stringstream str;
      str << "Invalid Protocol Version (expected: Ox" << dfinterface::PROTOCOL_VERSION
          << ", received 0x" << std::hex << data[0] << std::dec << ")";
      ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, str.str(), hltpu->name, "Closing connection"));
      asyncClose();
      return; // onClose() destroys the session
    }

    // Send back SharedBlockArray file name
    const std::string &sbaFileName = processor->m_main.sba().fileName();
    std::unique_ptr<ResponseMessage> rsp(
          new ResponseMessage(std::uint32_t(dfinterface::MsgTypeId::INIT_RSP),
                              req->transactionId(),
                              sbaFileName.size()));
    ::memcpy(rsp->data(), sbaFileName.c_str(), sbaFileName.size());
    hltpu->state = Processor::HLTPU::State::IDLE;
    hltpu->deadline = std::chrono::steady_clock::now() + processor->m_maxIdleTime_ms;
    asyncSend(std::move(rsp)); //onSend() will call asyncReceive()
  }


  // --- RobId List request : return the list immediatly
  else if (req->typeId() == std::uint32_t(dfinterface::MsgTypeId::ROBIDS_REQ) )
  {
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::IDLE);
    ERS_ASSERT(!hltpu->event);

    if (req->size() != 0)
    {
      ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Received invalid RobIdList request message",
                                              hltpu->name, "Closing connection"));
      asyncClose();
      return; // onClose() destroys the session
    }

    const std::vector<std::uint32_t>& allRobIds = processor->m_robIds;
    std::uint32_t byteSize = std::uint32_t(allRobIds.size()*sizeof(std::uint32_t));
    std::unique_ptr<ResponseMessage> rsp(
          new ResponseMessage(std::uint32_t(dfinterface::MsgTypeId::ROBIDS_RSP),
                              req->transactionId(),
                              byteSize));
    ::memcpy(rsp->data(), &allRobIds[0], byteSize);
    asyncSend(std::move(rsp)); //onSend() will call asyncReceive()
  }


  // --- Next Event request : calls tryFetchL1R()
  else if( req->typeId() == std::uint32_t(dfinterface::MsgTypeId::EVENT_REQ) )
  {
    ERS_DEBUG(3, "Received a Next event request from pu: " << hltpu->name);
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::IDLE);
    ERS_ASSERT(!hltpu->event);

    if (req->size() != 0)
    {
      ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Received invalid NextEvent request message",
                                              hltpu->name, "Closing connection"));
      asyncClose();
      return; // onClose() destroys the session
    }

    transactionId = req->transactionId();
    processor->tryFetchL1R(hltpu);
  }


  // --- Accept event : decode processing result
  else if (req->typeId() == std::uint32_t(dfinterface::MsgTypeId::EVENT_ACCEPTED))
  {
    ERS_DEBUG(3, "Received an Event Accept from pu: " << hltpu->name);
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::PROCESSING );
    ERS_ASSERT(processor->m_state == Processor::State::RUNNING ||
               processor->m_state == Processor::State::STOPPING);
    ERS_ASSERT(hltpu->event);

    // Decode the message payload
    auto p = reinterpret_cast<const std::uint32_t*>(req->data());
    std::uint32_t wordSize, checkWordSize = req->size();
    if ((checkWordSize < 6*sizeof(std::uint32_t))||
        (checkWordSize%sizeof(std::uint32_t) != 0)||
        (p[0] != 0)||
        (p[1] != hltpu->event->l1Id))
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize /= sizeof(std::uint32_t);

    // skip the checked eventId and l1Id
    checkWordSize -= 2;
    p += 2;

    // Decode the triggerInfo
    checkWordSize--;
    wordSize = *p++;
    if (checkWordSize < wordSize)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Trigger info overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize -= wordSize;
    hltpu->event->hltpuTriggerInfo.resize(wordSize);
    std::copy( p, p + wordSize, hltpu->event->hltpuTriggerInfo.begin() );
    p += wordSize;

    // Decode the streamTags
    if (checkWordSize == 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "StreamTags overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize--;
    wordSize = *p++;
    if (checkWordSize < wordSize)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "StreamTags overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize -= wordSize;
    eformat::helper::decode(wordSize, p, hltpu->event->streamTags);
    p += wordSize;

    // Decode the pscError
    if (checkWordSize == 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "pscError overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize--;
    wordSize = *p++;
    if (checkWordSize < wordSize)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "pscError overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize -= wordSize;
    hltpu->event->pscErrors.resize(wordSize);
    std::copy(p, p + wordSize, hltpu->event->pscErrors.begin());
    p += wordSize;

    // Decode the Hlt Rob Fragments and store them in the event block
    if (checkWordSize == 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Hlt Rob Fragments overflows EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    checkWordSize--;
    std::uint32_t nbrRobs = *p++;
    while (nbrRobs--)
    {
      eformat::read::ROBFragment rob(p);
      if (rob.marker() != eformat::HeaderMarker::ROB )
      {
        ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received Hlt Rob Fragments with invalid Header",
                                                  hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                  "Force accept and closing session"));
        processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
        return; // onClose() destroys the session
      }
      wordSize = rob.fragment_size_word();
      if (checkWordSize < wordSize)
      {
        ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Hlt Rob Fragments overflows EventAccepted message",
                                                  hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                  "Force accept and closing session"));
        processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
        return; // onClose() destroys the session
      }
      hltpu->event->hltResult.push_back(hltpu->event->block.appendRobData(p, wordSize*sizeof(std::uint32_t)).at(0));
      checkWordSize -= wordSize;
      p += wordSize;
    }

    if (checkWordSize > 0)
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Unused data at end of EventAccepted message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id, "Ignoring"));
    processor->accept(hltpu);
  }


  // --- Reject event : discard the event
  else if (req->typeId() == std::uint32_t(dfinterface::MsgTypeId::EVENT_REJECTED))
  {
    ERS_DEBUG(3, "Received an Event Reject from pu: " << hltpu->name);
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::PROCESSING);
    ERS_ASSERT(processor->m_state == Processor::State::RUNNING ||
               processor->m_state == Processor::State::STOPPING);
    ERS_ASSERT(hltpu->event);

    //ERS_LOG("Start Reject Event request for " << hltpu->name);
    auto data = reinterpret_cast<const std::uint32_t*>(req->data());
    if ((req->size() != 2*sizeof(std::uint32_t))||
        (data[0] != 0)||
        (data[1] != hltpu->event->l1Id))
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid EventRejected message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    processor->reject(hltpu);
  }


  // --- get robs request : forward the request to the data collector
  else if( req->typeId() == std::uint32_t(dfinterface::MsgTypeId::GET_ROBS_REQ) )
  {
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::PROCESSING );
    ERS_ASSERT(processor->m_state == Processor::State::RUNNING ||
               processor->m_state == Processor::State::STOPPING);
    ERS_ASSERT(hltpu->event);

    transactionId = req->transactionId();
    // Decode and check the message payload
    if ((req->size() % sizeof(std::uint32_t)) != 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid GetRobs request message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    std::uint32_t nbrDataWords = req->size() / sizeof(std::uint32_t);
    auto data = reinterpret_cast<const std::uint32_t*>(req->data());
    if (nbrDataWords < 1 || data[0] != 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid GetRobs request message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    ++data;
    --nbrDataWords;
    if (nbrDataWords == 0) // list is empty, request to fetch all robs
      processor->fetchAllROBs(hltpu);
    else
    {
      std::vector<std::uint32_t> robIds, invalidRobIds;
      const auto& dcRobIds = processor->m_robIds;
      robIds.reserve(nbrDataWords);
      while (nbrDataWords--)
      {
        if (std::binary_search(dcRobIds.begin(), dcRobIds.end(), *data))
          robIds.push_back(*data++);
        else
          invalidRobIds.push_back(*data++);
      }
      if (!invalidRobIds.empty())
      {
        ers::warning(dcm::InvalidRobIdInGetRobs(ERS_HERE, "Received invalid robIds in GetRobs request",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                robIdsAsString(invalidRobIds), "Ignoring"));
      }
      if (!robIds.empty())
        processor->fetchROBs(hltpu, robIds); // onFetchRobs() is called when completed
      else
      {
        hltpu->event->lastDataEnd = hltpu->event->block.dataEnd();
        sendFetchedROBs(); // Return a response with empty ROB data
      }
    }
  }


  // --- may get some robs request : forward the request to the data collector
  else if( req->typeId() == std::uint32_t(dfinterface::MsgTypeId::MAY_GET_ROBS) )
  {
    ERS_ASSERT(hltpu->state == Processor::HLTPU::State::PROCESSING);
    ERS_ASSERT(processor->m_state == Processor::State::RUNNING ||
               processor->m_state == Processor::State::STOPPING);
    ERS_ASSERT(hltpu->event);

    // Decode and check the message payload
    if ((req->size() % sizeof(std::uint32_t)) != 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid MayGetRob request message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    std::uint32_t nbrDataWords = req->size() / sizeof(std::uint32_t);
    auto data = reinterpret_cast<const std::uint32_t*>(req->data());
    if (nbrDataWords < 1 || data[0] != 0)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Received invalid MayGetRob request message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
      return; // onClose() destroys the session
    }
    ++data;
    --nbrDataWords;
    if (nbrDataWords == 0) // list is empty, request to prefetch all robs
      processor->mayGetROBs(hltpu, processor->m_robIds);
    else
    {
      std::vector<std::uint32_t> robIds, invalidRobIds;
      const auto& dcRobIds = processor->m_robIds;
      robIds.reserve(nbrDataWords);
      while (nbrDataWords--)
      {
        if (std::binary_search(dcRobIds.begin(), dcRobIds.end(), *data))
          robIds.push_back(*data++);
        else
          invalidRobIds.push_back(*data++);
      }
      if (!invalidRobIds.empty())
      {
        ers::warning(dcm::InvalidRobIdInGetRobs(ERS_HERE, "Received invalid robIds in MayGetRob request",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                robIdsAsString(invalidRobIds), "Ignoring"));
      }
      if (!robIds.empty())
        processor->mayGetROBs(hltpu, robIds);
    }
    asyncReceive();
  }


  // Unrecognized request message typeId
  else
  {
    if (hltpu->event)
    {
      ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Unrecognized request message",
                                                hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                "Force accept and closing session"));
      processor->forceAccept(hltpu, Event::ForceAccept::DFINTERFACE_ERROR);
    }
    else
    {
      ers::warning(dcm::IdleHLTPUSessionError(ERS_HERE, "Unrecognized request message",
                                              hltpu->name, "Closing session"));
      asyncClose();
    }
    return; // onClose() destroys the session
  }
}


void HltpuProcessor::HLTPUSes::sendFetchedL1R()
{
  ERS_DEBUG(3, "Sending L1R (GID: " << hltpu->event->globalId << ") to " << hltpu->name);
  std::unique_ptr<ResponseMessage> rsp(
        new ResponseMessage(std::uint32_t(dfinterface::MsgTypeId::EVENT_RSP),
                            transactionId,
                            7*sizeof(uint32_t) ) );
  auto data = reinterpret_cast<std::uint32_t*>(rsp->data());
  data[0] = 0; // eventId
  data[1] = hltpu->event->block.blockId();
  data[2] = hltpu->event->block.dataWordSize();
  data[3] = (std::uint32_t)(hltpu->event->globalId>>32);
  data[4] = (std::uint32_t)(hltpu->event->globalId);
  data[5] = hltpu->event->l1Id;
  data[6] = hltpu->event->lumiBlock;
  asyncSend(std::move(rsp));
}


void HltpuProcessor::HLTPUSes::sendFetchedROBs()
{
  ERS_ASSERT(hltpu->event);
  std::unique_ptr<ResponseMessage> rsp(
        new ResponseMessage(std::uint32_t(dfinterface::MsgTypeId::GET_ROBS_RSP),
                            transactionId,
                            2*sizeof(uint32_t)));
  auto data = reinterpret_cast<std::uint32_t*>(rsp->data());
  data[0] = 0; // eventId
  data[1] = hltpu->event->block.dataWordSize();
  asyncSend(std::move(rsp));
}


void HltpuProcessor::HLTPUSes::sendNoMoreEvents()
{
  ERS_ASSERT(!hltpu->event);
  std::unique_ptr<ResponseMessage> rsp(
        new ResponseMessage(std::uint32_t(dfinterface::MsgTypeId::EVENT_RSP),
                            transactionId,
                            0));
  asyncSend(std::move(rsp));
  sentNoMoreEvents = true;
 }


// --------------------------------- HltpuProcessor class  ---------------------------------


HltpuProcessor::HltpuProcessor(Main& main, const dal::DcmHltpuProcessor& config) :
  Processor(main,
               m_main.applicationConf().get_sbaNumOfBlocks() - config.get_maxOutputEvents(),
               make_unique<Processor::Histograms>(main.ioService(),
                        std::chrono::seconds(main.applicationConf().get_infoAverageInterval_s()),
                        m_main.applicationConf().get_UpperEdgeTimeHistos(),
                        config.get_processingTimeOut_ms()
                        ),
               config),
  m_config(config),
  m_server(std::make_shared<HLTPUSrv>(this, m_main.ioService()))
{
  m_server->initialize();
}


HltpuProcessor::~HltpuProcessor()
{
  if (m_server)
  {
    m_server->processor = nullptr;
    m_server->close();
  }
  m_timer.cancel();
}


void HltpuProcessor::processL1R  (Processor::HLTPU* h)
{
  ERS_ASSERT(h != nullptr);
  ERS_ASSERT_MSG(h->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);
  hltpu->hltpuSes->sendFetchedL1R();
}


void HltpuProcessor::processROBs (Processor::HLTPU* h)
{
  ERS_ASSERT(h != nullptr);
  ERS_ASSERT_MSG(h->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);
  hltpu->hltpuSes->sendFetchedROBs();
}


void HltpuProcessor::setStateIdle(Processor::HLTPU* h)
{
  ERS_ASSERT(h != nullptr);
  ERS_ASSERT_MSG(h->state == HLTPU::State::INITIALIZED ||
                 h->state == HLTPU::State::PROCESSING ||
                 h->state == HLTPU::State::ACCEPTING ||
                 h->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);

  // Don't set state to IDLE if come from Initialized state.
  // It will be set to IDLE once the HLTPU has send the Initialization message
  if (hltpu->state != HLTPU::State::INITIALIZED) {
    hltpu->state = HLTPU::State::IDLE;
  }
  hltpu->hltpuSes->asyncReceive();
}


void HltpuProcessor::noMoreEvents(Processor::HLTPU* h)
{
  ERS_ASSERT(h != nullptr);
  ERS_ASSERT_MSG(h->state == HLTPU::State::IDLE ||
                 h->state == HLTPU::State::FETCHING_L1R,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);
  hltpu->hltpuSes->sendNoMoreEvents();
  Processor::noMoreEvents(hltpu);
}


void HltpuProcessor::terminate(Processor::HLTPU* h)
{
  auto hltpu = static_cast<HLTPU*>(h);
  hltpu->hltpuSes->asyncClose();
  // Processor::terminate is called in onClose()
}


} // namespace dcm
} // namespace daq
