#include "dcm/RosDataCollector.h"

#include "dcm/Event.h"
#include "dcm/Issues.h"
#include "dcm/Main.h"
#include "dcm/Processor.h"
#include "dcm/Utilities.h"
#include "dcm/dal/DcmRosDataCollector.h"

#include "asyncmsg/Error.h"
#include "asyncmsg/Session.h"
#include "dal/Computer.h"
#include "dal/Detector.h"
#include "DFdal/DFParameters.h"
#include "DFdal/InputChannel.h"
#include "DFdal/ROS.h"
#include "eformat/Status.h"
#include "eformat/write/ROBFragment.h"
#include "is/exceptions.h"
#include "monsvc/MonitoringService.h"
#include "oh/OHRootMutex.h"

#include <boost/asio/error.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <TH1I.h>
#include <TH2I.h>
#include <chrono>
#include <functional>
#include <limits>
#include <set>
#include <sstream>
#include <unordered_map>

namespace daq
{
namespace dcm
{


struct RosDataCollector::Transaction
{
  Transaction(Event* event_, void* context_) :
      event(event_),
      context(context_),
      nPendingRosTransactions(0)
  {
  }

  Event* event;
  void* context;
  std::uint32_t nPendingRosTransactions;
};


class RosDataCollector::Session: public asyncmsg::Session
{

private:

  struct Transaction
  {
    std::uint32_t id;
    RosDataCollector::Transaction* parent;
    std::vector<std::uint32_t> robIds;
    std::chrono::steady_clock::time_point sendTime;
  };

  struct RequestMessage: public asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF20;

    explicit RequestMessage(Transaction&& transaction_) :
        transaction(std::move(transaction_))
    {
    }

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return transaction.id;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      // FIXME: these assume we are on a little-endian arch
      buffers.emplace_back(static_cast<const void*>(&transaction.parent->event->l1Id),
          sizeof(transaction.parent->event->l1Id));
      buffers.emplace_back(transaction.robIds.data(),
          transaction.robIds.size() * sizeof(std::uint32_t));
    }

    Transaction transaction;

  };

  struct ResponseMessage: public daq::asyncmsg::InputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF21;

    ResponseMessage(Transaction&& transaction_, std::uint32_t dataSize) :
        transaction(std::move(transaction_)),
        data(dataSize)
    {
    }

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return transaction.id;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      buffers.emplace_back(data.data(), data.size());
    }

    Transaction transaction;
    std::vector<std::uint8_t> data;

  };

public:

  Session(RosDataCollector& dataCollector, boost::asio::io_service& ioService,
      const std::string& localName, const std::string& remoteName) :
      asyncmsg::Session(ioService),
      m_dataCollector(dataCollector),
      m_ioService(ioService),
      m_localName(localName),
      m_remoteName(remoteName),
      m_inTransition(false),
      m_reconnect(true),
      m_quiet(false),
      m_credits(0),
      m_nextTransactionId(0),
      m_openAttemptCount(0),
      m_openAttemptTimer(ioService)
  {
  }

  const std::string& remoteName() const
  {
    return m_remoteName;
  }

  const boost::asio::ip::tcp::endpoint& remoteEndpoint() const
  {
    return m_remoteEndpoint;
  }

  std::string remoteEndpointStr() const
  {
    std::ostringstream out;                      \
    out << remoteEndpoint();                              \
    return out.str();
  }

  void asyncConnect()
  {
    m_inTransition = true;
    doOpen();
  }

private:

  void doOpen()
  {
    ERS_ASSERT(m_pendingTransactions.empty());
    ERS_ASSERT(m_runningTransactions.empty());
    m_openAttemptCount = 0;
    try {
      m_remoteEndpoint = m_dataCollector.m_main.resolveServer(m_remoteName);
    }
    catch (ers::Issue& ex) {
      reportError(ex);
      auto self = shared_from_this();
      getStrand().post([this, self] ()
      {
        onOpenError(make_error_code(boost::asio::error::host_not_found));
      });
      return;
    }
    asyncOpen(m_localName, m_remoteEndpoint);
  }

  void onOpen() override
  {
    ERS_LOG("Connection to " << remoteName() << " (" << remoteEndpoint() << ") open.");
    m_dataCollector.m_main.isInfo()->DcActiveConnections++;
    if (m_inTransition)
    {
      m_inTransition = false;
      m_dataCollector.onSessionConnect(this);
    }
    // Re-enable error reporting possibly disabled by disable()
    m_quiet = false;
  }

  void onOpenError(const boost::system::error_code& error) override
  {
    // retry open after OpenAttemptIntervalDuration_sec seconds at most MaxOpenAttemptCount times
    if (m_openAttemptCount < MaxOpenAttemptCount) {
      ++m_openAttemptCount;
      m_openAttemptTimer.expires_from_now(boost::posix_time::seconds(OpenAttemptIntervalDuration_sec));
      m_openAttemptTimer.async_wait([this](const boost::system::error_code& error){
        if (!error) {
          this->asyncOpen(this->m_localName, this->m_remoteEndpoint);
        } else { // wait terminated with an error (could have been aborted)
          // make sure it falls through error handling
          this->m_openAttemptCount = MaxOpenAttemptCount;
          onOpenError(error);
        }
      });
    } else {
      reportWarning(dcm::ROSSessionError(ERS_HERE, "Could not connect", remoteName(), remoteEndpointStr(), "Ignoring",
                                       makeSystemError(ERS_HERE, error)));
      if (m_inTransition)
      {
        m_inTransition = false;
        m_dataCollector.onSessionConnectError(this, error);
      }
    }
  }

public:

  void asyncDisconnect()
  {
    if (state() == State::CLOSED) {
      m_ioService.post(std::bind(&RosDataCollector::onSessionDisconnect, &m_dataCollector, this));
    }
    else {
      m_inTransition = true;
      if (state() != State::CLOSE_PENDING) {
        asyncClose();
      }
    }
  }

private:

  void onClose() override
  {
    ERS_LOG("Connection to " << remoteName() << " (" << remoteEndpoint() << ") closed.");
    m_dataCollector.m_main.isInfo()->DcActiveConnections--;

    // Clear all running and pending transactions
    decltype(m_runningTransactions) runningTransactions;
    std::swap(m_runningTransactions, runningTransactions);
    decltype(m_pendingTransactions) pendingTransactions;
    std::swap(m_pendingTransactions, pendingTransactions);
    std::uint32_t credits = m_credits;
    m_credits = 0;
    for (auto& entry : runningTransactions)
    {
      appendDummyRobs(entry.second.parent, entry.second.robIds);
      credits += entry.second.robIds.size();
    }
    for (auto& entry : pendingTransactions)
    {
      appendDummyRobs(entry.second.parent, entry.second.robIds);
    }
    m_dataCollector.returnCredits(credits);
    for (auto& entry : runningTransactions)
    {
      reportError(dcm::ROSSessionError(ERS_HERE, "Could not fetch ROBs", remoteName(), remoteEndpointStr(),
                                       "Connection closed"));
      m_dataCollector.onSessionFetch(this, entry.second.parent);
    }
    for (auto& entry : pendingTransactions)
    {
      reportError(dcm::ROSSessionError(ERS_HERE, "Could not fetch ROBs", remoteName(), remoteEndpointStr(),
                                       "Connection closed"));
      m_dataCollector.onSessionFetch(this, entry.second.parent);
    }

    if (m_inTransition)
    {
      m_inTransition = false;
      m_dataCollector.onSessionDisconnect(this);
    }
  }

  void onCloseError(const boost::system::error_code& error) override
  {
    DCM_ERROR(dcm::GenericIssue, "session close failed: " << error.message());
  }

public:

  void asyncFetch(RosDataCollector::Transaction* parent, std::vector<std::uint32_t>&& robIds)
  {
    if (state() != Session::State::OPEN) {
      reportError(dcm::ROSSessionError(ERS_HERE, "Could not fetch ROBs", remoteName(), remoteEndpointStr(),
                                       "session not open"));
      appendDummyRobs(parent, robIds);
      m_ioService.post(std::bind(&RosDataCollector::onSessionFetch, &m_dataCollector,
          this, parent));
      return;
    }
    auto& transaction = m_pendingTransactions[m_nextTransactionId];
    transaction.id = m_nextTransactionId;
    transaction.parent = parent;
    transaction.robIds = std::move(robIds);
    std::sort(transaction.robIds.begin(), transaction.robIds.end());
    ++m_nextTransactionId;
    doFetch();
  }

  void giveCredits(std::uint32_t n)
  {
    m_credits += n;
    doFetch();
  }

  std::uint32_t minCreditsNeeded()
  {
    if (m_pendingTransactions.empty()) {
      return 0;
    }
    if (m_credits >= m_pendingTransactions.begin()->second.robIds.size()) {
      return 0;
    }
    else {
      return m_pendingTransactions.begin()->second.robIds.size() - m_credits;
    }
  }

  std::uint32_t totCreditsNeeded()
  {
    std::uint32_t nPendingFragments = 0;
    for (auto& entry : m_pendingTransactions) {
      nPendingFragments += entry.second.robIds.size();
    }
    if (m_credits >= nPendingFragments) {
      return 0;
    }
    else {
      return nPendingFragments - m_credits;
    }
  }

private:

  void doFetch()
  {
    if (state() != Session::State::OPEN) {
      return;
    }
    for (;;) {
      auto it = m_pendingTransactions.begin();
      if (it == m_pendingTransactions.end() || it->second.robIds.size() > m_credits) {
        break;
      }
      it->second.sendTime = std::chrono::steady_clock::now();
      m_credits -= it->second.robIds.size();
      asyncSend(make_unique<RequestMessage>(std::move(it->second)));
      m_pendingTransactions.erase(it);
    }
  }

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override
  {
    auto request = static_pointer_cast<const RequestMessage>(std::move(message));
    m_runningTransactions.insert(
        std::make_pair(request->transaction.id, std::move(request->transaction)));
    ++m_dataCollector.m_main.isInfo()->DcROBRequests;
    asyncReceive();
  }

  void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override
  {
    // TODO: better error handling
    auto request = static_pointer_cast<const RequestMessage>(std::move(message));
    reportError(dcm::ROSSessionError(ERS_HERE, "Error sending Fragment Request", remoteName(), remoteEndpointStr(),
                                     "Aborting", makeSystemError(ERS_HERE, error)));
    appendDummyRobs(request->transaction.parent, request->transaction.robIds);
    if (state() == State::OPEN) {
      asyncClose();
    }
    m_dataCollector.returnCredits(request->transaction.robIds.size());
    m_dataCollector.onSessionFetch(this, request->transaction.parent);
  }

  std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t transactionId, std::uint32_t size) override
  {
    if (typeId == ResponseMessage::TYPE_ID) {
      auto it = m_runningTransactions.find(transactionId);
      if (it != m_runningTransactions.end()) {
        auto response = make_unique<ResponseMessage>(std::move(it->second), size);
        m_runningTransactions.erase(it);
        return std::move(response);
      }
      else {
        ers::warning(dcm::ROSSessionError(ERS_HERE, "Unsolicited ROB data received",
                                          remoteName(), remoteEndpointStr(), "Ignoring"));
        asyncReceive();
        return std::unique_ptr<asyncmsg::InputMessage>();
      }
    }
    else {
      ers::warning(dcm::ROSSessionBadMessage(ERS_HERE, "Unexpected message",
                                        remoteName(), remoteEndpointStr(), typeId, "Ignoring"));
      asyncReceive();
      return std::unique_ptr<asyncmsg::InputMessage>();
    }
  }

  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    auto response = static_pointer_cast<const ResponseMessage>(std::move(message));

    auto now = std::chrono::steady_clock::now();
    m_dataCollector.m_main.isInfo()->DcReceivedData += response->data.size();
    auto responseTime = std::chrono::duration_cast<std::chrono::microseconds>(
        (now - response->transaction.sendTime));
    m_dataCollector.m_histograms.fillDcResponseTime_us(remoteName(), responseTime.count());

    bool error = false;
    std::vector<SharedBlockArray::RobInfo*> robInfos;
    try {
      robInfos = response->transaction.parent->event->block.appendRobData(
          reinterpret_cast<const std::uint32_t*>(response->data.data()), response->data.size());
    }
    catch (SBAInvalidArgument& ex) {
      ers::error(dcm::DataCollectingError(ERS_HERE, "Error appending ROBs to SBA. ", ex));
      error = true;
    }
    catch (SBARunTimeError& ex) {
      ers::error(dcm::DataCollectingError(ERS_HERE, "Error appending ROBs to SBA. ", ex));
      error = true;
    }

    m_dataCollector.returnCredits(response->transaction.robIds.size());

    if (error) {
      // TODO: use a better error
      m_dataCollector.onSessionFetchError(this,
          make_error_code(boost::system::errc::illegal_byte_sequence),
          response->transaction.parent);
    }
    else {
      std::vector<std::uint32_t> responseRobIds;
      std::vector<std::uint32_t> responseRobSizes;
      for (auto robInfo : robInfos) {
        robInfo->reqTime = response->transaction.sendTime;
        robInfo->rspTime = now;
        responseRobIds.push_back(robInfo->robId);
        responseRobSizes.push_back(robInfo->robFragment().fragment_size_word() * 4);
      }
      std::sort(responseRobIds.begin(), responseRobIds.end());
      std::vector<std::uint32_t> missingRobIds;
      std::set_difference(response->transaction.robIds.begin(), response->transaction.robIds.end(),
          responseRobIds.begin(), responseRobIds.end(), std::back_inserter(missingRobIds));
      if (!missingRobIds.empty()) {
        ers::error(dcm::ROSSessionEventError(ERS_HERE,"Fragment Response with missing fragment(s) received",
                                             remoteName(), remoteEndpointStr(),
                                             response->transaction.parent->event->globalId,
                                             response->transaction.parent->event->l1Id, "Aborting"));
        m_dataCollector.onSessionFetchError(this,
            make_error_code(boost::system::errc::protocol_error),
            response->transaction.parent);
      }
      else {
        for (auto size : responseRobSizes) {
          m_dataCollector.m_histograms.fillRobSize(remoteName(), size);
        }
        m_dataCollector.onSessionFetch(this, response->transaction.parent);
      }
    }
  }

  void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    if (message) {
      reportError(dcm::ROSSessionError(ERS_HERE, "Error receiving Fragment Response",
                                       remoteName(), remoteEndpointStr(),
                                       "Aborting", makeSystemError(ERS_HERE, error)));
      auto response = static_pointer_cast<ResponseMessage>(std::move(message));
      appendDummyRobs(response->transaction.parent, response->transaction.robIds);
      m_dataCollector.returnCredits(response->transaction.robIds.size());
      m_dataCollector.onSessionFetch(this, response->transaction.parent);
    }
    else {
      reportError(dcm::ROSSessionError(ERS_HERE, "Error receiving message",
                                       remoteName(), remoteEndpointStr(),
                                       "Aborting", makeSystemError(ERS_HERE, error)));
    }
    if (state() == State::OPEN) {
      asyncClose();
    }
  }

public:

  void disable()
  {
    if (m_inTransition) {
      ers::warning(dcm::ROSSessionError(ERS_HERE, "Cannot disable ROS during FSM transition",
          remoteName(), remoteEndpointStr(), "Ignoring"));
    }
    else {
      // Prevent the periodic timer from re-opening the session
      m_reconnect = false;
      // Disable error reporting
      m_quiet = true;
      if (state() == State::OPEN_PENDING || state() == State::OPEN) {
        asyncClose();
      }
      ERS_LOG("Connection to " << remoteName() << " disabled.");
    }
  }

  void enable()
  {
    if (m_inTransition) {
      ers::warning(dcm::ROSSessionError(ERS_HERE, "Cannot enable ROS during FSM transition",
          remoteName(), remoteEndpointStr(), "Ignoring"));
    }
    else {
      // Let the periodic timer re-open the session
      m_reconnect = true;
      ERS_LOG("Connection to " << remoteName() << " enabled.");
      // Error reporting will be re-enabled once the session is again open (see onOpen())
    }
  }

  void onTimer(const std::chrono::steady_clock::time_point& now)
  {
    if (state() == State::OPEN) {
      // Timeout detection
      for (auto& entry : m_runningTransactions) {
        auto deadline = entry.second.sendTime +
            std::chrono::seconds(m_dataCollector.m_config.get_requestTimeout_s());
        if (now > deadline) {
          ers::error(dcm::ROSSessionError(ERS_HERE, "Timeout while waiting for response",
                                          remoteName(), remoteEndpointStr(), "Aborting"));
          asyncClose();
          break;
        }
      }
    }
    else if (m_reconnect && state() == State::CLOSED) {
      // Automatic reconnection
      doOpen();
    }
  }

private:

  void appendDummyRobs(RosDataCollector::Transaction* parent,
      const std::vector<std::uint32_t>& robIds)
  {
    namespace efh = eformat::helper;
    namespace efw = eformat::write;

    // Manufacture the requested ROBs

    const std::uint32_t bunchCrossingId = 0x0;
    const std::uint32_t l1TriggerType = 0x0;
    const std::uint32_t eventType = 0x0;
    const std::uint32_t dataWords = 0x0;
    const std::uint32_t* data = nullptr;
    const std::uint32_t status =
        efh::Status(eformat::DUMMY_FRAGMENT, eformat::COLLECTION_TIMEOUT).code();

    for (auto robId : robIds) {
      efw::ROBFragment rob(robId, m_dataCollector.m_main.runParams().run_number, parent->event->l1Id,
          bunchCrossingId, l1TriggerType, eventType, dataWords, data, eformat::STATUS_FRONT);
      rob.status(1, &status);

      // Serialize to SBA
      // TODO: catch SBA exceptions
      std::uint32_t* buffer = parent->event->block.startAppendRobData(rob.size_word() * 4);
      auto node = rob.bind();
      do {
        std::memcpy(buffer, node->base, node->size_word * 4);
        buffer += node->size_word;
        node = node->next;
      } while (node);
      parent->event->block.endAppendRobData(rob.size_word() * 4);
    }
  }

  void reportError(const ers::Issue& issue)
  {
    if (m_quiet) {
      ers::log(issue);
    } else {
      ers::error(issue);
    }
  }

  void reportWarning(const ers::Issue& issue)
  {
    if (m_quiet) {
      ers::log(issue);
    } else {
      ers::warning(issue);
    }
  }
private:

  RosDataCollector& m_dataCollector;
  boost::asio::io_service& m_ioService;

  std::string m_localName;
  std::string m_remoteName;
  boost::asio::ip::tcp::endpoint m_remoteEndpoint;

  bool m_inTransition;
  bool m_reconnect;
  bool m_quiet;

  std::uint32_t m_credits;
  std::uint32_t m_nextTransactionId;
  std::map<std::uint32_t, Transaction> m_pendingTransactions;
  std::map<std::uint32_t, Transaction> m_runningTransactions;

  int m_openAttemptCount;
  const int MaxOpenAttemptCount = 3;
  const int OpenAttemptIntervalDuration_sec = 1;
  boost::asio::deadline_timer m_openAttemptTimer;

};


RosDataCollector::RosDataCollector(Main& main, const dal::DcmRosDataCollector& config) :
    m_main(main),
    m_config(config),
    m_processor(nullptr),
    m_creditsAvailable(0),
    m_creditsTotal(config.get_nRequestCredits()),
    m_creditsDelta(0),
    m_nOpenSessions(0),
    m_timer(m_main.ioService())
{
  if (m_creditsTotal == 0) {
    m_creditsTotal = std::numeric_limits<std::uint32_t>::max();
  }
  m_creditsAvailable = m_creditsTotal;

  std::set<std::string> types = { "ROS" };
  std::vector<const daq::core::BaseApplication *> apps = m_main.partitionConf().get_all_applications(&types);
  for (auto& app : apps) {
    if (app->get_host()->get_State() != 0) {
      auto ros = m_main.confDatabase().cast<daq::df::ROS>(app);
      if (ros != nullptr && !ros->disabled(m_main.partitionConf())) {
        m_sources.emplace_back(ros->UID());
        auto detId = ros->get_Detector()->get_LogicalId();
        for (auto rosResource : ros->get_Contains()) {
          auto module = m_main.confDatabase().cast<daq::core::ResourceSet>(rosResource);
          if (module != nullptr && !module->disabled(m_main.partitionConf())) {
            for (auto moduleResource : module->get_Contains()) {
              auto channel = m_main.confDatabase().cast<daq::df::InputChannel>(moduleResource);
              if (channel != nullptr && !channel->disabled(m_main.partitionConf())) {
                // XXX: the following logical OR works in the following cases:
                // - The ROS sub-detector ID is 0 and the channel ID contains the full ROB ID
                // - The ROS sub-detector ID is set and the channel ID contains just the module ID
                // - The ROS sub-detector ID is set and the channel ID contains the full ROB ID,
                //   with its sub-detector ID part equal to the sub-detector ID.
                std::uint32_t robId = channel->get_Id() | ((std::uint32_t) detId << 16);
                m_robIds.push_back(robId);
                m_robIdToSource.insert({robId, ros->UID()});
                m_sourceToRobIds.insert({ros->UID(), robId});
              }
            }
          }
        }
      }
    }
  }

  // TODO: throw, so that we fail the CONFIGURE transition
  if (m_sources.empty()) {
    DCM_FATAL(dcm::DataCollectingError, "No configured ROS found.");
    return;
  }
  if (m_robIds.empty()) {
    DCM_FATAL(dcm::DataCollectingError, "No configured input channels found.");
    return;
  }
  std::sort(m_sources.begin(), m_sources.end());
  std::sort(m_robIds.begin(), m_robIds.end());
  auto dupIt = std::unique(m_robIds.begin(), m_robIds.end());
  if (dupIt != m_robIds.end()) {
    std::stringstream ss;
    for (; dupIt != m_robIds.end(); ++dupIt) {
      ss << *dupIt << ", ";
    }
    DCM_FATAL(dcm::DataCollectingError, "Duplicate ROB ID(s) found: " << ss.str());
    return;
  }

  std::size_t maxRobIdsPerSource = 0;
  for (auto& source : m_sources)
  {
    maxRobIdsPerSource = std::max(maxRobIdsPerSource, m_sourceToRobIds.count(source));
  }
  if (m_creditsTotal < maxRobIdsPerSource) {
    DCM_ERROR(dcm::DataCollectingError,
        "The configured amount of data collection credits (" << m_creditsTotal << ") is smaller " <<
        "than the maximum number of ROB IDs served by a single ROS (" << maxRobIdsPerSource << "). " <<
        "This can lead to deadlocks!");
  }
  m_histograms.initialize(m_sources, maxRobIdsPerSource, config.get_nRequestCredits());
  m_recentHistogramsEpoch = std::chrono::steady_clock::now();
}

void RosDataCollector::initialize(Processor* processor)
{
  m_processor = processor;
}

void RosDataCollector::disableSource(const std::string& source)
{
  auto it = m_sourceToSession.find(source);
  if (it == m_sourceToSession.end()) {
    ers::warning(dcm::DataCollectingError(ERS_HERE,
        "Cannot disable non-existent ROS " + source));
  } else {
    it->second->disable();
  }
}

void RosDataCollector::enableSource(const std::string& source)
{
  auto it = m_sourceToSession.find(source);
  if (it == m_sourceToSession.end()) {
    ers::warning(dcm::DataCollectingError(ERS_HERE,
        "Cannot enable non-existent ROS " + source));
  } else {
    it->second->enable();
  }
}

void RosDataCollector::setDynamicParameters(const std::map<std::string, std::string>& parameters)
{
  for (const auto& kv : parameters)
  {
    try
    {
      if (kv.first == "nRequestCredits")
      {
        auto v = (kv.second != "") ?
            boost::lexical_cast<std::uint32_t>(kv.second) : m_config.get_nRequestCredits();
        auto newCreditsTotal = v != 0 ?
            v : std::numeric_limits<std::uint32_t>::max();
        m_creditsDelta += std::int64_t(newCreditsTotal) - std::int64_t(m_creditsTotal);
        m_creditsTotal = newCreditsTotal;
        applyCreditDelta();
        distributeCredits();
      }
      else
      {
        ers::warning(dcm::ConfigUnknownParameter(ERS_HERE, kv.first));
        continue;
      }
      ERS_INFO("RosDataCollector dynamic parameter " << kv.first << " changed to: " << kv.second << ".");
    }
    catch (boost::bad_lexical_cast& ex)
    {
      ers::warning(dcm::ConfigInvalidValue(ERS_HERE, kv.first, kv.second, ex));
    }
  }
}

const std::vector<std::string>& RosDataCollector::sources()
{
  return m_sources;
}

const std::vector<std::uint32_t>& RosDataCollector::robIds()
{
  return m_robIds;
}

const std::map<std::uint32_t, std::string>& RosDataCollector::robIdToSource()
{
  return m_robIdToSource;
}

const std::multimap<std::string, std::uint32_t>& RosDataCollector::sourceToRobIds()
{
  return m_sourceToRobIds;
}

void RosDataCollector::asyncConnect()
{
  for (auto& source : m_sources) {
    m_sessions.emplace_back(std::make_shared<Session>(*this, m_main.ioService(),
        m_main.applicationName(), source));
    m_sourceToSession[source] = m_sessions.back().get();
    for (auto it = m_sourceToRobIds.lower_bound(source);
        it != m_sourceToRobIds.upper_bound(source); ++it) {
      m_robIdToSession.insert({it->second, m_sessions.back().get()});
    }
  }
  for (auto& session : m_sessions) {
    session->asyncConnect();
  }
}

void RosDataCollector::asyncStart()
{
  ERS_ASSERT(m_transactions.empty());
  m_histograms.clear();
  m_recentHistogramsEpoch = std::chrono::steady_clock::now();
  m_creditsAvailableHistory.clear();
  m_main.ioService().post(std::bind(&Main::onDataCollectorStart, &m_main));
}

void RosDataCollector::asyncStop()
{
  m_main.ioService().post(std::bind(&Main::onDataCollectorStop, &m_main));
}

void RosDataCollector::asyncDisconnect()
{
  m_timer.cancel();
  for (auto& session : m_sessions) {
    session->asyncDisconnect();
  }
}

void RosDataCollector::asyncFetchRobs(Event* event, const std::vector<std::uint32_t>& robIds,
    void* context)
{
  std::unordered_map<Session*, std::vector<std::uint32_t> > requests;
  
  for (auto robId : robIds) {
    auto it = m_robIdToSession.find(robId);
    // Check for non-existent ROB IDs
    if (it == m_robIdToSession.end()) {
      dcm::DataCollectingBadRobId issue(ERS_HERE, event->globalId, event->l1Id, robId);
      ers::error(issue);
      m_main.ioService().post(std::bind(&Processor::onFetchRobsError, m_processor,
          issue, event, context));
      return;
    }
    if (!event->block[robId]) {
      // The ROB wasn't already collected. Queue it for fetching.
      requests[it->second].push_back(robId);
    }
  }

  if (requests.empty()) {
    // All ROBs were already collected. Notify the caller.
    m_main.ioService().post(
        std::bind(&Processor::onFetchRobs, m_processor, event, context));
    return;
  }

  // Add prefetched ROBs to Sessions in requests

  // Are there prefetched ROBs for this event ?
  if(m_eventToPrefetchRobs.find(event->globalId) != m_eventToPrefetchRobs.end()) {

    // Get map of prefetched ROBs for this event
    auto& m = m_eventToPrefetchRobs[event->globalId];

    for(auto& request : requests) {
      // For each ROS, do we have prefetched ROBs at all ? 
      if(m.find(request.first->remoteName()) != m.end()) {
            
        // yes, we have
        for(auto robId : m[request.first->remoteName()]) {
          // If the robId is not yet collected and not part of the request, add it.
          // Not sure if the uniqueness is checked later
          if(!event->block[robId] && std::find(request.second.begin(), request.second.end(), robId) == request.second.end()) {
            request.second.push_back(robId);
          }
        }
        // remove the data for this Session
        m.erase(request.first->remoteName());
      }
    }
  }

  // We have a legitimate request. Do it.
  m_transactions.emplace_back(event, context);
  auto& transaction = m_transactions.back();
  for (auto& request : requests) {
    ++transaction.nPendingRosTransactions;
    ++m_main.isInfo()->DcPendingRequests;
    m_histograms.fillRobsPerRequest(request.first->remoteName(), request.second.size());
    request.first->asyncFetch(&transaction, std::move(request.second));
  }
  distributeCredits();
}

// Implementation of DataCollector::mayGetRobs()
void RosDataCollector::mayGetRobs(Event* event, const std::vector<std::uint32_t>& robIds)
{
  auto& m = m_eventToPrefetchRobs[event->globalId];
  for(auto robId : robIds) {

      auto it = m_robIdToSession.find(robId);
      if(it == m_robIdToSession.end()) {
          // handle error, here ignore ROB
          continue;
      }
      m[it->second->remoteName()].insert(robId);
  }
}

void RosDataCollector::clearEvent(Event *event)
{
    m_eventToPrefetchRobs.erase(event->globalId);
}

void RosDataCollector::onSessionConnect(Session* /* session */)
{
  if (++m_nOpenSessions == m_sessions.size()) {
    m_main.onDataCollectorConnect();
    // Start watchdog timer
    m_timer.expires_at(std::chrono::steady_clock::now());
    m_timer.async_wait(std::bind(&RosDataCollector::onTimer, this, std::placeholders::_1));
  }
}

void RosDataCollector::onSessionConnectError(Session* session,
    const boost::system::error_code& /* error */)
{
  onSessionConnect(session);
}

void RosDataCollector::onSessionDisconnect(Session* /* session */)
{
  if (--m_nOpenSessions == 0) {
    m_main.onDataCollectorDisconnect();
    m_sessions.clear();
    m_sourceToSession.clear();
    m_robIdToSession.clear();
  }
}

void RosDataCollector::distributeCredits()
{
  std::vector<Session*> selectedSessions;
  do {
    selectedSessions.clear();
    for (auto& session : m_sessions) {
      if (session->minCreditsNeeded() != 0 && m_creditsAvailable >= session->minCreditsNeeded()) {
        selectedSessions.push_back(session.get());
      }
    }
    std::random_shuffle(selectedSessions.begin(), selectedSessions.end());
    for (auto& session : selectedSessions) {
      auto creditsGiven = session->minCreditsNeeded();
      if (m_creditsAvailable >= creditsGiven) {
        session->giveCredits(creditsGiven);
        m_creditsAvailable -= creditsGiven;
      }
    }
  } while (!selectedSessions.empty());
  // Credit history monitoring
  m_creditsAvailableHistory.emplace_back(std::chrono::steady_clock::now(), m_creditsAvailable);
  if (m_creditsAvailableHistory.size() > 1) {
    auto value = (m_creditsAvailableHistory.end() - 2)->second;
    auto timeStart = (m_creditsAvailableHistory.end() - 2)->first;
    auto timeEnd = (m_creditsAvailableHistory.end() - 1)->first;
    typedef std::chrono::duration<std::int32_t, std::ratio<1, 10000>> tens_of_milliseconds;
    auto weight = std::chrono::duration_cast<tens_of_milliseconds>(timeEnd - timeStart);
    m_histograms.fillDcAvailableCredits(value, weight.count());
  }
}

void RosDataCollector::returnCredits(std::uint32_t n)
{
  m_creditsAvailable += n;
  applyCreditDelta();
  distributeCredits();
}

void RosDataCollector::applyCreditDelta()
{
  if (m_creditsDelta != 0)
  {
    ERS_LOG("m_creditsAvailable = " << m_creditsAvailable << "; m_creditsDelta = " << m_creditsDelta);
    if (m_creditsAvailable + m_creditsDelta >= 0)
    {
      m_creditsAvailable += m_creditsDelta;
      m_creditsDelta = 0;
    }
    else
    {
      m_creditsDelta += m_creditsAvailable;
      m_creditsAvailable = 0;
    }
    ERS_LOG("m_creditsAvailable = " << m_creditsAvailable << "; m_creditsDelta = " << m_creditsDelta);
  }
}

void RosDataCollector::onSessionFetch(Session* /* session */, Transaction* transaction)
{
  --m_main.isInfo()->DcPendingRequests;
  if (--transaction->nPendingRosTransactions == 0) {
    m_processor->onFetchRobs(transaction->event, transaction->context);
    auto it = std::find_if(
        m_transactions.begin(), m_transactions.end(), [transaction] (Transaction& element) {
          return &element == transaction;
        });
    ERS_ASSERT(it != m_transactions.end());
    m_transactions.erase(it);
  }
}

void RosDataCollector::onSessionFetchError(Session* session, const boost::system::error_code& error,
    Transaction* transaction)
{
  ers::error(dcm::ROSSessionError(ERS_HERE, "Error receiving Fragment Response",
                                  session->remoteName(), session->remoteEndpointStr(),
                                  "Ignoring", makeSystemError(ERS_HERE, error)));
  onSessionFetch(session, transaction);
}

void RosDataCollector::onTimer(const boost::system::error_code& error)
{
  if (error) {
    if (error != boost::asio::error::operation_aborted) {
      ers::error(dcm::DataCollectingError(ERS_HERE, "ROS connection watchdog timer error. Aborting",
                                          makeSystemError(ERS_HERE, error)));
    }
    return;
  }

  auto now = std::chrono::steady_clock::now();

  for (auto& session : m_sessions) {
    session->onTimer(now);
  }

  if ((now - m_recentHistogramsEpoch) >=
      std::chrono::seconds(m_main.applicationConf().get_infoAverageInterval_s())) {
    m_histograms.updateRecents();
    m_recentHistogramsEpoch = now;
  }

  if (!m_creditsAvailableHistory.empty()) {
    m_main.isInfo()->DcTrafficShapingCredits = time_weighted_average(m_creditsAvailableHistory);
    // Erase all elements but the last
    m_creditsAvailableHistory.erase(
        m_creditsAvailableHistory.begin(),
        m_creditsAvailableHistory.begin() + m_creditsAvailableHistory.size() - 1);
  }
  else {
    m_main.isInfo()->DcTrafficShapingCredits = m_creditsAvailable;
  }

  m_timer.expires_at(m_timer.expires_at() + std::chrono::seconds(1));
  m_timer.async_wait(std::bind(&RosDataCollector::onTimer, this, std::placeholders::_1));
}


RosDataCollector::Histograms::Histograms()
{
}

RosDataCollector::Histograms::~Histograms()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  monsvc::MonitoringService::instance().remove_object(m_RobSize.get());
  monsvc::MonitoringService::instance().remove_object(m_RobSize_recent.get());

  monsvc::MonitoringService::instance().remove_object(m_RobsPerRequest.get());
  monsvc::MonitoringService::instance().remove_object(m_RobsPerRequest_recent.get());

  monsvc::MonitoringService::instance().remove_object(m_DcResponseTime_us.get());
  monsvc::MonitoringService::instance().remove_object(m_DcResponseTime_us_recent.get());

  monsvc::MonitoringService::instance().remove_object(m_DcAvailableCredits.get());
  monsvc::MonitoringService::instance().remove_object(m_DcAvailableCredits_recent.get());
}

void RosDataCollector::Histograms::initialize(const std::vector<std::string>& sources,
    std::size_t maxRobIdsPerSource, std::uint32_t maxCredits)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  m_RobSize = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/RobSize",
      new TH2I("RobSize",
          "ROB size (B)",
          sources.size(), 0, sources.size(), 400, 0, 4000),
      true);
  m_RobSize_recent = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/RobSize_recent",
      new TH2I("RobSize_recent",
          "ROB size (B) [recent content]",
          sources.size(), 0, sources.size(), 400, 0, 4000),
      true);
  m_RobSize_last.reset(
      new TH2I("RobSize_last",
          "ROB size (B) [last content]",
          sources.size(), 0, sources.size(), 400, 0, 4000));

  m_RobsPerRequest = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/RobsPerRequest",
      new TH2I("RobsPerRequest",
          "N. of ROB fragments asked to the same ROS per request",
          sources.size(), 0, sources.size(), maxRobIdsPerSource, 0.5, maxRobIdsPerSource + 0.5),
      true);
  m_RobsPerRequest_recent = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/RobsPerRequest_recent",
      new TH2I("RobsPerRequest_recent",
          "N. of ROB fragments asked to the same ROS per request [recent content]",
          sources.size(), 0, sources.size(), maxRobIdsPerSource, 0.5, maxRobIdsPerSource + 0.5),
      true);
  m_RobsPerRequest_last.reset(
      new TH2I("RobsPerRequest_last",
          "N. of ROB fragments asked to the same ROS per request [last content]",
          sources.size(), 0, sources.size(), maxRobIdsPerSource, 0.5, maxRobIdsPerSource + 0.5));

  m_DcResponseTime_us = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/DcResponseTime_us",
      new TH2I("DcResponseTime_us",
          "ROS response time (us)",
          sources.size(), 0, sources.size(), 10000, 0, 1000000),
      true);
  m_DcResponseTime_us_recent = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/DcResponseTime_us_recent",
      new TH2I("DcResponseTime_us_recent",
          "ROS response time (us) [recent content]",
          sources.size(), 0, sources.size(), 10000, 0, 1000000),
      true);
  m_DcResponseTime_us_last.reset(
      new TH2I("DcResponseTime_us_last",
          "ROS response time (us) [last content]",
          sources.size(), 0, sources.size(), 10000, 0, 1000000));

  m_DcAvailableCredits = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/DcAvailableCredits",
      new TH1I("DcAvailableCredits",
          "Available traffic shaping credits (time weighted)",
          maxCredits + 1, -0.5, maxCredits + 0.5),
      true);
  m_DcAvailableCredits_recent = monsvc::MonitoringService::instance().register_object(
      "/DEBUG/DcAvailableCredits_recent",
      new TH1I("DcAvailableCredits_recent",
          "Available traffic shaping credits (time weighted) [recent content]",
          maxCredits + 1, -0.5, maxCredits + 0.5),
      true);
  m_DcAvailableCredits_last.reset(
      new TH1I("DcAvailableCredits_last",
          "Available traffic shaping credits (time weighted) [last content]",
          maxCredits + 1, -0.5, maxCredits + 0.5));

  // Prepare histogram labels
  auto prepareLabels = [&] (TH2I* h)
  {
    int bin = 1;
    for (const auto& source : sources)
    {
      h->GetXaxis()->SetBinLabel(bin, source.c_str());
      ++bin;
    }
  };
  auto prepareLabelsLock = [&] (monsvc::ptr<TH2I>& h)
  {
    std::lock_guard<monsvc::ptr<TH2I>> lock(h);
    prepareLabels(h.get());
  };
  prepareLabelsLock(m_RobSize);
  prepareLabelsLock(m_RobSize_recent);
  prepareLabels(m_RobSize_last.get());
  prepareLabelsLock(m_RobsPerRequest);
  prepareLabelsLock(m_RobsPerRequest_recent);
  prepareLabels(m_RobsPerRequest_last.get());
  prepareLabelsLock(m_DcResponseTime_us);
  prepareLabelsLock(m_DcResponseTime_us_recent);
  prepareLabels(m_DcResponseTime_us_last.get());
}

void RosDataCollector::Histograms::clear()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  m_RobSize->Reset();
  m_RobSize_recent->Reset();
  m_RobSize_last->Reset();

  m_RobsPerRequest->Reset();
  m_RobsPerRequest_recent->Reset();
  m_RobsPerRequest_last->Reset();

  m_DcResponseTime_us->Reset();
  m_DcResponseTime_us_recent->Reset();
  m_DcResponseTime_us_last->Reset();

  m_DcAvailableCredits->Reset();
  m_DcAvailableCredits_recent->Reset();
  m_DcAvailableCredits_last->Reset();
}

void RosDataCollector::Histograms::updateRecents()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  {
    std::lock_guard<monsvc::ptr<TH2I>> l1(m_RobSize);
    std::lock_guard<monsvc::ptr<TH2I>> l2(m_RobSize_recent);
    m_RobSize_recent.get()->Add(m_RobSize.get(), m_RobSize_last.get(), 1, -1);
    m_RobSize.get()->Copy(*m_RobSize_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH2I>> l1(m_RobsPerRequest);
    std::lock_guard<monsvc::ptr<TH2I>> l2(m_RobsPerRequest_recent);
    m_RobsPerRequest_recent.get()->Add(m_RobsPerRequest.get(), m_RobsPerRequest_last.get(), 1, -1);
    m_RobsPerRequest.get()->Copy(*m_RobsPerRequest_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH2I>> l1(m_DcResponseTime_us);
    std::lock_guard<monsvc::ptr<TH2I>> l2(m_DcResponseTime_us_recent);
    m_DcResponseTime_us_recent.get()->Add(m_DcResponseTime_us.get(), m_DcResponseTime_us_last.get(), 1, -1);
    m_DcResponseTime_us.get()->Copy(*m_DcResponseTime_us_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_DcAvailableCredits);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_DcAvailableCredits_recent);
    m_DcAvailableCredits_recent.get()->Add(m_DcAvailableCredits.get(), m_DcAvailableCredits_last.get(), 1, -1);
    m_DcAvailableCredits.get()->Copy(*m_DcAvailableCredits_last.get());
  }
}

void RosDataCollector::Histograms::fillRobSize(const std::string& x, double y)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_RobSize->Fill(x.c_str(), y, 1);
}

void RosDataCollector::Histograms::fillRobsPerRequest(const std::string& x, double y)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_RobsPerRequest->Fill(x.c_str(), y, 1);
}

void RosDataCollector::Histograms::fillDcResponseTime_us(const std::string& x, double y)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_DcResponseTime_us->Fill(x.c_str(), y, 1);
}

void RosDataCollector::Histograms::fillDcAvailableCredits(double x, double weight)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_DcAvailableCredits->Fill(x, weight);
}


} // namespace dcm
} // namespace daq
