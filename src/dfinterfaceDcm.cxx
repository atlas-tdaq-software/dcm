#include <memory>
#include <fstream>
#include <sstream>
#include <cctype>
#include <chrono>
#include <cstdint>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <sys/types.h>
#include <pwd.h>
#include "dcm/SharedBlockArray/Readable.h"
#include "dcm/dfinterfaceDcm.h"
#include "dcm/PortFileName.h"
#include "boost/property_tree/ptree.hpp"
#include "boost/optional/optional.hpp"
#include "boost/system/system_error.hpp"
#include "boost/asio.hpp"
#include "ers/ers.h"
#include "eformat/HeaderMarker.h"
#include "boost/asio/steady_timer.hpp"
#include "CTPfragment/CTPfragment.h"


namespace daq
{
namespace dfinterface
{

// DLL entry point
std::unique_ptr<Session> createSession(const boost::property_tree::ptree& config )
{
  ERS_LOG("Creating Session object ...");

  // Parse parameters
  std::string portFileName;
  bool robPreFetching = false;
  auto parametersPath = "Configuration.HLTMPPUApplication.DataSource.HLTDFDCMBackend.extraParams";
  auto parametersTree = config.get_child_optional(parametersPath);
  if (parametersTree)
  {
    for (auto& child : *parametersTree)
    {
      if (child.first == "parameter")
      {
        auto p = child.second.data();
        if (p.compare(0, 13, "portFileName=") == 0)
        {
          portFileName = p.substr(13);
        }
        else if (p.compare(0, 15, "robPreFetching=") == 0)
        {
          try
          {
            robPreFetching = std::stoi(p.substr(15)) != 0;
          }
          catch (std::exception& ex)
          {
            ERS_LOG("Malformed extra parameter: " << p);
          }
        }
        else
        {
          ERS_LOG("Unknown extra parameter: " << p);
        }
      }
    }
  }

  if (portFileName.empty()) {
    // Calculate the fileName containing the dcm listening port number
    portFileName = daq::dcm::getPortFileName();
    if(portFileName.empty())
    {
      DCM_FATAL(dcm::ProcessingIssue, "Missing $TDAQ_PARTITION in the enviroment");
      throw dfinterface::BadConfiguration( ERS_HERE, "Missing $TDAQ_PARTITION in the enviroment" );
    }
  }

  // Create a new SessionDcm
  std::unique_ptr<Session> s(new DCMSession(portFileName, robPreFetching));

  ERS_LOG("Created new DCMSession using file '" << portFileName << "'" <<
      ", with ROB pre-fetching hack " << (robPreFetching ? "enabled" : "disabled"));

  return s;
}

// Even Message typeId are sent message, odd Message typeId are received message
// A request and response message must differ only by the less significant bit


// Session DCM constructor
DCMSession::DCMSession( const std::string& portNumFileName, bool robPreFetching ) :
  m_socket(m_ioService), m_portNumFileName(portNumFileName), m_robPreFetching(robPreFetching),
  m_bufferByteSize(0), m_state(DCMSession::CLOSED)
{
  m_bufferByteSize = 1024;
  m_buffer.reset(new std::uint32_t[m_bufferByteSize/sizeof (std::uint32_t)]);
}

// Session destructor
DCMSession::~DCMSession()
{
  if( isOpen() )
    close();
}


/*! \brief Return DCM HLTInterface port Number read from specified file or a negative value if failed
 *  \param[in] fileName file to read the DCM HLT interface port number from
 *  \return the port number in the range 1..65535 or a negative number if failed
  *
 *  The file is expected to contain a single line with the string
 *    DCM_HLTInterface_portNumber <portNumber>
 *
 *  Returns:
 *    -1 if the file couldn't be opened
 *    -2 if couldn't read two words from the file
 *    -3 if the label (first word) is not 'DCM_HltpuServer_portNumber'
 *    -4 if the port number is not digits only
 *    -5 if the port number is not in the range 1 .. 65535
 */
static int read_DCM_HltpuServer_portNum( const std::string& fileName )
{
  const std::string LABEL = "DCM_HltpuServer_portNumber";

  // try opening the file. If fails return an empty portNumber string
  std::ifstream file(fileName);
  if( !file )
    return -1;

  // Try reading the label and the value
  std::string label, portNumberStr;
  file >> label >> portNumberStr;
  if( !file )
    return -2;

  // Check if label is the expected value
  if( label != LABEL )
    return -3;

  ERS_LOG( "Found '" << label << " " << portNumberStr << "'' in file " << fileName );

  // Check if portNumber string is all digits
  if( portNumberStr.empty() || std::find_if(portNumberStr.begin(), portNumberStr.end(),
                                 [](char c) { return !::isdigit(c); }) != portNumberStr.end() )
    return -4;

  // Check if the value is in the range 1 to 65535
  int portNumber = atoi(portNumberStr.c_str());
  if( portNumber < 1 || portNumber > 65535 )
    return -5;

  return portNumber;
}


// Opens the Session.
// Call blocks until connection succeeds or fails by timeout
void DCMSession::open(const std::string& clientName)
{
  namespace aip = boost::asio::ip;

  if( m_state != DCMSession::CLOSED )
    throw dfinterface::CommunicationError( ERS_HERE, "Attempt to reopen dfinterface. Ignored" );

  // Build localhost address
  aip::address localhost(aip::address::from_string("127.0.0.1"));
  int DCM_HltpuServer_portNum = 0;
  std::string sharedBlockArrayFileName;

  // Retry while the socket is not opened
  while( !m_socket.is_open() )
  {
    // Try reading the DCM HltpuServer port number
    DCM_HltpuServer_portNum = read_DCM_HltpuServer_portNum(m_portNumFileName);
    if( DCM_HltpuServer_portNum < 0 )
    {
      // ERS_LOG( "Got invalid port number " << DCM_HltpuServer_portNum << ". Retry after 1 second" );
      // It failed, retry after a second
      sleep(1);
      continue;
    }

    // Try connecting to remote port on localhost
    boost::system::error_code ec;
    m_socket.connect(aip::tcp::endpoint(localhost, static_cast<unsigned short>(DCM_HltpuServer_portNum)), ec);
    if( ec )
    {
//    ERS_LOG( "Connection to port " << DCM_HltpuServer_portNum << " failed with error " << ec.message() << "! Wait 1 second and retry" )

      m_socket.close();
      sleep(1);
      continue;
    }

    ERS_LOG( "Connection to port " << DCM_HltpuServer_portNum << " successful!" );

    // Disable the nagel algorithm
    boost::system::error_code ignoredError;
    m_socket.set_option(aip::tcp::no_delay(true), ignoredError);

    ERS_LOG( "Start asyncmsg's HELLO hand shake ..." );

    try
    {
      // Send the asyncmsg HELLO hand shake with the HLTPU client name as payload
      std::uint32_t hdr[3] = { std::uint32_t(0x80000001), 0, 0 };
      hdr[2] = clientName.size();
      write( hdr, sizeof(hdr) );
      write( clientName.c_str(), hdr[2] );

      // Get the asyncmsg HELLO hand shake with the DCM server name
      read( hdr, sizeof(hdr) );
      if( hdr[0] != std::uint32_t(0x80000001) )
      {
        close();
        std::stringstream s;
        s << "Received unexpected Message type id : 0x" << std::hex << hdr[0] << std::dec <<
             " during asyncmsg HELLO hand shake";
        throw dfinterface::CommunicationError( ERS_HERE, s.str() );
      }

      // Read data into the buffer
      adjustBufferSize( hdr[2] );
      read( m_buffer.get(), hdr[2] );

      // Set the remote Name strong to the received string
      m_remoteName.assign(reinterpret_cast<char*>(m_buffer.get()), hdr[2]);
    }
    catch( std::exception &ex )
    {
//    ERS_LOG( "HELLO transaction failed with error " << ex.what() << "! Wait 1 second and retry" )
      m_socket.close();
      sleep(1);
      continue;
    }

    ERS_LOG( "asyncmsg HELLO completed ! " << clientName
        << "(" << std::hex << this << std::dec << ")"
        << " is connected to " << m_remoteName );

    // HLTPU <-> DCM connection initialization (request the ShareBlockArray file name)

    {
      // Send the initialization hand shake with the HLTPU client name
      std::uint32_t hdr[4] = { std::uint32_t(MsgTypeId::INIT_REQ), 0, sizeof(std::uint32_t), PROTOCOL_VERSION };
      write( hdr, sizeof(hdr) );

      // Get the SharedBlockArray file name from DCM
      read( hdr, 3*sizeof(std::uint32_t) );
      if( hdr[0] != std::uint32_t(MsgTypeId::INIT_RSP) )
      {
        close();
        std::stringstream s;
        s << "Received unexpected Message type id : 0x" << std::hex << hdr[0] << std::dec <<
             " during HLTPU <-> DCM connection initialization";
        throw dfinterface::CommunicationError( ERS_HERE, s.str() );
      }

      ERS_LOG( "Receiving SBA filename of length " << hdr[2] );
      // Read data into the buffer
      adjustBufferSize( hdr[2] );
      read( m_buffer.get(), hdr[2] );

      try
      {
        // Open the SharedBlockArray for read only access (May throw as well)
        sharedBlockArrayFileName = std::string( reinterpret_cast<char*>(m_buffer.get()), hdr[2] );
        m_sba.reset( new dcm::SharedBlockArray::Readable(sharedBlockArrayFileName));
      }
      catch( dcm::SharedBlockArrayIssue& sbae )
      {
        close();
        std::stringstream s;
        s << "SharedBlockArray::open(" << sharedBlockArrayFileName << ") error : " << sbae.what();
        throw dfinterface::CommunicationError( ERS_HERE, s.str() );
      }
    }
  }

  // Instantiate the cached dcm event
  m_event.reset( new DCMEvent(*this) );

  // Set the state to IDLE (ready to get event requests or getRobIds)
  m_state = DCMSession::IDLE;

  ERS_LOG( clientName << " is connected to " << m_remoteName << ":" <<
           DCM_HltpuServer_portNum << " using SharedBlockArray file " << sharedBlockArrayFileName );
}


// Determines whether the Session is open.
bool DCMSession::isOpen() const { return m_state != DCMSession::CLOSED; }

// Closes the Session.
void DCMSession::close()
{
  ERS_DEBUG(3, "DCMSession close: closing " << std::hex << this);
  if(isOpen())
  {
    m_event.reset();
    m_sba.reset();
    m_socket.close();
    m_state = DCMSession::CLOSED;
  }
  ERS_DEBUG(3, "DCMSession close: closed " << std::hex << this);
}


// Send an event request
void DCMSession::sendEventRequest()
{
  // Make sure an event request is not already in progress
  if( m_state == DCMSession::IDLE )
  {
    // Send the event request message
    std::uint32_t hdr[3] = { std::uint32_t(MsgTypeId::EVENT_REQ), 0, 0 };
    write( hdr, sizeof(hdr) );
    m_state = DCMSession::WAITING_EVENT;
  }
  else if( m_state != DCMSession::WAITING_EVENT )
  {
    close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }
  // else without effect if already waiting for an event
  //      this is needed for getEvent with timeout
}


// Receive the event data
std::unique_ptr<Event> DCMSession::receiveEventData(std::uint32_t *hdr )
{
  if( m_state != DCMSession::WAITING_EVENT )
  {
    close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }

  if( hdr[0] != std::uint32_t(MsgTypeId::EVENT_RSP) )
  {
    close();
    std::stringstream s;
    s << "Received unexpected Message type id : 0x" << std::hex << hdr[0] << std::dec <<
         " instead of MsgTypeId::EVENT_RSP (0xEFDF03)";
    throw dfinterface::CommunicationError( ERS_HERE, s.str() );
  }

  // Throw NoMoreEvents if the message signals that there are no more events to process
  if( hdr[2] == 0 )
  {
    ERS_LOG("Received noMoreEvents as nextEvent response " << std::hex << this);
    close();
    ERS_LOG("Received noMoreEvents as nextEvent response: after close " << std::hex << this);
    throw dfinterface::NoMoreEvents(ERS_HERE, "No more events to process" );
  }

  // Read data into the buffer
  adjustBufferSize( hdr[2] );
  read( m_buffer.get(), hdr[2] );

  // Check that the DCMEvent pointer is valid before trying to dereference it
  ERS_ASSERT_MSG( m_event , "Receiving an event while processing an event" );

  // Initialize the cached event with the received data
  m_event->initialize( m_buffer.get(), hdr[2] );

  // Set the new dfinterface state
  m_state = DCMSession::PROCESSING_EVENT;

  return std::move(m_event);
}


// Returns a pointer to the next \ref Event object to be processed.
std::unique_ptr<Event> DCMSession::getNext()
{
  sendEventRequest();

  // Receive response header using a blocking read
  std::uint32_t hdr[3];
  read( hdr, sizeof(hdr) );

  // Return the received event
  return receiveEventData(hdr);
}


// Replace optional error code a with error code b
static void set_result(boost::optional<boost::system::error_code>* a, boost::system::error_code b) { a->reset(b); }


// Returns a pointer to the next \ref Event object to be processed.
std::unique_ptr<Event> DCMSession::tryGetNextUntil(const std::chrono::steady_clock::time_point& absTime)
{
  using namespace boost::placeholders;

  ERS_DEBUG(3,"Starting tryGetNextUntil function: " << std::hex << this);
  sendEventRequest();

  // Synchronous read with timeout based on code inspired by the code at
  // http://stackoverflow.com/a/13144834/75517
  boost::asio::steady_timer timer(m_ioService, absTime);
  boost::optional<boost::system::error_code> timer_result;
  timer.async_wait(boost::bind(set_result, &timer_result, _1));

  // Queue an asynchronous socket read
  boost::optional<boost::system::error_code> read_result;
  std::uint32_t hdr[3];
  boost::asio::async_read( m_socket, boost::asio::buffer(hdr, sizeof(hdr)),
                     boost::bind(set_result, &read_result, _1));

  // Reset the io_service to run and return when the first one of both queued task ends
  m_ioService.reset();
  while( m_ioService.run_one() )
  {
    // If the read completed, cancel the timer
    if( read_result )
      timer.cancel();
    // Else if the timer completed, cancel the read (this is not an error condition)
    else if( timer_result )
      m_socket.cancel();
  }

  // In the timeout case, operation aborted. Need to send OperationTimedOut.
  if( *read_result == boost::asio::error::operation_aborted)
    throw dfinterface::OperationTimedOut(ERS_HERE, "Socket read aborted");
  else if( *read_result )
    throw dfinterface::CommunicationError( ERS_HERE, "Socket read error",
                                           makeSystemError(ERS_HERE, *read_result) );

  ERS_DEBUG(3,"tryGetNextUntil: received event to process: " << std::hex << this);
  return receiveEventData(hdr);
}


// Returns a pointer to the next \ref Event object to be processed.
std::unique_ptr<Event> DCMSession::tryGetNextFor(const std::chrono::steady_clock::duration& relTime)
{
  using namespace boost::placeholders;

  sendEventRequest();

  // Synchronous read with timeout based on code inspired by the code at
  // http://stackoverflow.com/a/13144834/75517

  boost::asio::steady_timer timer(m_ioService);
  timer.expires_from_now(relTime);
  boost::optional<boost::system::error_code> timer_result;
  timer.async_wait(boost::bind(set_result, &timer_result, _1));

  // Queue an asynchronous socket read
  boost::optional<boost::system::error_code> read_result;
  std::uint32_t hdr[3];
  boost::asio::async_read( m_socket, boost::asio::buffer(hdr, sizeof(hdr)),
                     boost::bind(set_result, &read_result, _1));

  // Reset the io_service to run and return when the first one of both queued tasks ended
  m_ioService.reset();
  while( m_ioService.run_one() )
  {
    // If the read completed, cancel the timer
    if( read_result )
      timer.cancel();
    // Else if the timer completed, cancel the read (this is not an error condition)
    else if( timer_result )
      m_socket.cancel();
  }

  // If there was an error throw it as a system error
  if( *read_result == boost::asio::error::operation_aborted)
    throw dfinterface::CommunicationError( ERS_HERE, "Socket read aborted",
                                           makeSystemError(ERS_HERE, *read_result) );
  else if( *read_result )
    throw dfinterface::CommunicationError( ERS_HERE, "Socket read error",
                                           makeSystemError(ERS_HERE, *read_result) );

  return receiveEventData(hdr);
}


// Marks the event as accepted by the High-Level Trigger.
void DCMSession::accept(std::unique_ptr<Event> event,
                        std::unique_ptr<uint32_t[]> hltr)
{
  ERS_DEBUG(3,"Starting accept function: " << std::hex << this);
  if( m_state != DCMSession::PROCESSING_EVENT )
  {
    close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }

  ERS_ASSERT_MSG( event, "event pointer must not be nullptr" );
  DCMEvent* dcmEvent = dynamic_cast<DCMEvent*>(event.release());
  ERS_ASSERT_MSG( dcmEvent, "event pointer must point to a DCMEvent" );
  if( m_event )
      throw dfinterface::CommunicationError( ERS_HERE, "accept() called while not processing an event" );
  m_event.reset(dcmEvent);

  // Read the FullEventFragment
  eformat::read::FullEventFragment result(hltr.get());

  // Compute the full message size in bytes (multiple of word size)
  std::uint32_t byteSize = 3; // message header size in words
  byteSize += 2; // for the eventId and the l1Id

  byteSize += result.nhlt_info() + 1;

  byteSize += result.nstream_tag() + 1;

  byteSize += result.nstatus() + 1;

  byteSize += 1; // for nbrRobs

  std::vector<eformat::read::ROBFragment> robs;
  result.robs(robs);
  for (auto hltFragment : robs) {
    byteSize += hltFragment.fragment_size_word();
  }
  byteSize *= sizeof(std::uint32_t); // Convert to byte size

  // Make sure the buffer is big enough
  adjustBufferSize( byteSize );

  // Serialize the message in the buffer
  std::uint32_t *p = m_buffer.get();
  // Write the message header
  *p++ = std::uint32_t(MsgTypeId::EVENT_ACCEPTED);
  *p++ = 0;
  *p++ = byteSize - 3*sizeof(std::uint32_t); // don't count header in payload size
  // write the event Identifier
  *p++ = dcmEvent->eventId();
  // write the level 1 Id
  *p++ = dcmEvent->l1Id();

  // write trigger info
  const uint32_t *p_ti = result.hlt_info();
  *p++ = result.nhlt_info();
  p = std::copy( p_ti, p_ti + result.nhlt_info() , p );

  // write streamTags
  const uint32_t *p_st = result.stream_tag();
  *p++ = result.nstream_tag();
  p = std::copy( p_st, p_st + result.nstream_tag() , p );

  // write pscErrors
  const uint32_t *p_se = result.status();
  *p++ = result.nstatus();
  p = std::copy( p_se, p_se + result.nstatus() , p );

  // write HLT Fragments
  *p++ = robs.size();
  for (auto r : robs)
    p = std::copy( r.start(), r.end(), p );

  // Size check
  if( (p-m_buffer.get())*sizeof(std::uint32_t) != byteSize )
  {
      std::stringstream str;
      str << "Message size mismatch: " << (p-m_buffer.get())*sizeof(std::uint32_t) << " instead of " << byteSize << " bytes";
      throw dfinterface::CommunicationError( ERS_HERE, str.str() );
  }

  // Write the message to the socket
  write( m_buffer.get(), byteSize );

  // Clear the event data
  dcmEvent->clear();

  m_state = DCMSession::IDLE;

}

// Marks the event as rejected by the High-Level Trigger.
void DCMSession::reject(std::unique_ptr<Event> event)
{
  ERS_DEBUG(3,"Starting reject function: " << std::hex << this);
  if( m_state != DCMSession::PROCESSING_EVENT )
  {
    close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }

  ERS_ASSERT_MSG( event, "event pointer must not be nullptr" );
  DCMEvent* dcmEvent = dynamic_cast<DCMEvent*>(event.release());
  ERS_ASSERT_MSG( dcmEvent, "event pointer must point to a DCMEvent" );
  if( m_event )
      throw dfinterface::CommunicationError( ERS_HERE, "reject() called while not processing an event" );
  m_event.reset(dcmEvent);

  // Send the event reject message with the level 1 ID as data
  std::uint32_t hdr[5] = { std::uint32_t(MsgTypeId::EVENT_REJECTED), 0, 2*sizeof (std::uint32_t), 0, 0 };
  hdr[3] = dcmEvent->eventId();
  hdr[4] = dcmEvent->l1Id();
  write( hdr, sizeof(hdr) );

  // Clear the event data
  dcmEvent->clear();

  m_state = DCMSession::IDLE;
  ERS_DEBUG(3,"Reject function done.");
}


// Return the list of robIds expected to be requested to the DCM
std::vector<uint32_t> DCMSession::getRobIds()
{
  if( m_state != DCMSession::IDLE )
  {
    close();
    throw dfinterface::CommunicationError( ERS_HERE, "dfinterface is not open" );
  }

  // Send request for list of robIds
  std::uint32_t hdr[3] = { std::uint32_t(MsgTypeId::ROBIDS_REQ), 0, 0};
  write( hdr, sizeof(hdr) );

  // Receive response header
  read( hdr, sizeof(hdr) );
  if( hdr[0] != std::uint32_t(MsgTypeId::ROBIDS_RSP) )
  {
    close(); // Close the connection
    std::stringstream s;
    s << "Received unexpected Message type id : 0x" << std::hex << hdr[0] << std::dec <<
         " instead of MsgTypeId::ROBIDS_RSP (0xEFDF09)";
    throw dfinterface::CommunicationError( ERS_HERE, s.str() );
  }

  // Receive robIds vector (will initialize robIds to array of value 0!)
  std::vector<uint32_t> robIds(hdr[2]/sizeof (std::uint32_t));
  read( &robIds[0], hdr[2] );

  // Return received vector
  return robIds;
}


// Read size bytes of data from the socket and write them into buf
// throws boost::system::system_error
void DCMSession::read( void* buf, size_t size )
{
  boost::asio::read(m_socket, boost::asio::buffer(buf, size));
}


// Write the size bytes of data to the socket
// throws boost::system::system_error
void DCMSession::write( const void* data, size_t size )
{
  boost::asio::write( m_socket, boost::asio::buffer(data, size) );
}

// ---------------------------------------------------------------------------------

const uint32_t NMINUTES_TO_WAIT = 1;

// Constructor
DCMEvent::DCMEvent(DCMSession &session)
  : m_session(session)
  , m_l1Id(0)
  , m_fullEvent()
  , m_fetchedAllRobs(false)
{
}


// Destructor
DCMEvent::~DCMEvent() {
  // clear();
}


// Initialize the event with new data (l1Id + l1Result)
void DCMEvent::initialize( const std::uint32_t* data, std::uint32_t byteSize )
{
  if( byteSize != 7*sizeof(std::uint32_t) )
  {
    m_session.close(); // Close the connection
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid amount of data received" );
  }

  m_eventId = data[0];
  std::uint32_t blockId = data[1], newDataWordSize = data[2];
  m_globalId = data[3];
  m_globalId <<= 32;
  m_globalId |= data[4];
  m_l1Id = data[5];
  m_lumiBlock = (std::uint16_t)data[6];
  m_l1Result.clear();
  m_fetchedAllRobs = false;

  // Get the event block index out of data and map the corresponding Shared Block Array Block
  m_block = m_session.sba()->block(blockId);

  // Insert in robInfoIndex all robInfo structures found in dataWordSize
  // and adjust block data word size to newDataWordSize
  m_block.appendAndInsertRobInfos(newDataWordSize);

  // Assume all robs in the block are part of the L1R
  const dcm::SharedBlockArray::RobInfoIndex& robInfoIndex = m_block.robInfoIndex();
  m_l1Result.reserve(robInfoIndex.size());
  for (dcm::SharedBlockArray::RobInfoIndex::const_iterator it = robInfoIndex.begin();
       it != robInfoIndex.end(); ++it)
     m_l1Result.emplace_back(it->second->robFragment());
}


// Remove the transient event data
void DCMEvent::clear()
{
  m_block = dcm::SharedBlockArray::Readable::Block(); // unmaps the shared array block
  m_l1Result.clear();
  m_l1Id = 0;
  m_eventId = 0;
}


// Returns the Level-1 ID of the event.
std::uint32_t DCMEvent::l1Id()
{
  if( m_session.m_state != DCMSession::PROCESSING_EVENT )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }
  return m_l1Id;
}

// Return the GID of the event
std::uint64_t DCMEvent::gid()
{
  return m_globalId;
}

// Return the GID of the event
std::uint16_t DCMEvent::lumiBlock()
{
  return m_lumiBlock;
}

// Stores the Level-1 Result fragments for this event into the provided vector.
void DCMEvent::getL1Result(std::unique_ptr<uint32_t[]>& l1Result)
{
  ERS_DEBUG(3,"getL1Result entered.");
  if( m_session.m_state != DCMSession::PROCESSING_EVENT )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }

  //
  // TODO move CTP decoding into the DCM
  //

  eformat::write::FullEventFragment l1Result_w;

  // Look for CTP fragment with the smallest value and append all of them to the event
  const uint32_t *ctpRobPtr = 0;
  uint32_t sid=0xFFFFFFFF;
  for(auto r : m_l1Result)
  {
    if ( eformat::helper::SourceIdentifier(r.source_id()).subdetector_id() == eformat::TDAQ_CTP)
    {
      if(!ctpRobPtr or sid > r.source_id())
      {
        ctpRobPtr = r.start();
        sid = r.source_id();
      }
    }
    l1Result_w.append_unchecked(r.start());
  }
  /// Check if CTP fragment has been found, otherwise send a fragmment to HLT with missing information.
  /// This has been changed in order to make the HLT take the decision on what to do with the event.
  if(!ctpRobPtr){
    //throw dfinterface::CommunicationError( ERS_HERE, "Missing CTP fragment" );
    ers::warning(dfinterface::CommunicationError( ERS_HERE, "Missing CTP fragment"));
  }
  else{
    eformat::ROBFragment<const std::uint32_t*> ctp(ctpRobPtr);
    if(m_l1Id != ctp.rod_lvl1_id()){
      ERS_LOG("Wrong L1ID " << m_l1Id << " vs " << ctp.rod_lvl1_id() );
    }
    /// Access CTPfragment payload to read times
    /// NB: lumi block already read in DCM
    uint32_t bc_time_secs = 0;
    uint32_t bc_time_nsec = 0;
    CTPfragment::time(&ctp, bc_time_secs, bc_time_nsec);
    l1Result_w.bc_time_seconds(bc_time_secs);
    l1Result_w.bc_time_nanoseconds(bc_time_nsec);
    l1Result_w.run_no(ctp.rod_run_no());
    l1Result_w.bc_id(ctp.rod_bc_id());
    l1Result_w.lvl1_trigger_type(ctp.rod_lvl1_trigger_type());
  }
  /// Fill  l1Result header
  l1Result_w.source_id(eformat::helper::SourceIdentifier(eformat::TDAQ_L2SV, 0).code());
  l1Result_w.global_id(m_globalId);
  //l1Result_w.run_type(u32);  // TODO: find runtype somewhere
  l1Result_w.lumi_block(m_lumiBlock);
  l1Result_w.lvl1_id(m_l1Id);
  /// Seriealize the FullEventFragment
  const eformat::write::node_t* top = l1Result_w.bind();
  auto l1r_size = l1Result_w.size_word();
  uint32_t* l1r_read = new uint32_t[l1r_size];
  auto res = eformat::write::copy(*top, l1r_read, l1r_size);
  if (res != l1r_size) {
    std::string msg = "dfinterfaceDCM Caught exception: Event serialization failed!!";
    throw daq::dfinterface::CommunicationError(ERS_HERE, msg);
  }
  l1Result.reset(l1r_read);
  ERS_DEBUG(3,"getL1Result done.");
}


// Informs the dfinterface implementation that the specified ROBs may be requested later
void DCMEvent::mayGetRobs(const std::vector<uint32_t>& robIds)
{
  ERS_DEBUG(3,"Starting mayGetRobs function.");
  std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
  if ( !attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    ERS_LOG("Unable to lock the mayGetRobs mutex after the " << NMINUTES_TO_WAIT << " time interval.");
    throw dfinterface::CommunicationError( ERS_HERE, "Unable to lock dfinterfaceDCM mutex." );
  }
  if ( m_session.m_robPreFetching )
  {
    std::vector<hltinterface::DCM_ROBInfo> ignoredRobInfos;
    //getRobs(robIds, ignoredRobInfos);
    getRobs_unprotected(robIds, ignoredRobInfos);
    return;
  }

  if( m_session.m_state != DCMSession::PROCESSING_EVENT )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }

  // Select robs not yet in cache
  std::vector<uint32_t> missingRobs;
  for ( auto robId : robIds )
    if (!m_block[robId])
      missingRobs.push_back(robId);

  if (!missingRobs.empty())
  {
    // Send the request message with the list of missing robIds we may receive
    std::uint32_t hdr[4] = { std::uint32_t(MsgTypeId::MAY_GET_ROBS), 0, 0, 0};
    hdr[2] = sizeof (std::uint32_t)+missingRobs.size()*sizeof (std::uint32_t);
    hdr[3] = eventId();
    m_session.write(hdr, sizeof(hdr));
    m_session.write(&missingRobs[0], missingRobs.size()*sizeof(std::uint32_t));
  }
}

// Get the robs and return the new data end
void DCMEvent::getTheRobs(const std::vector<uint32_t>& robIds)
{
  ERS_DEBUG(3,"getTheRobs entered");
  // Build and send the request
  std::uint32_t hdr[4] = { std::uint32_t(MsgTypeId::GET_ROBS_REQ), 0, sizeof(std::uint32_t), 0};
  hdr[2] += robIds.size()*sizeof(std::uint32_t);
  hdr[3] = eventId();
  m_session.write(hdr, sizeof(hdr));
  m_session.write(&robIds[0], robIds.size()*sizeof (std::uint32_t));

  // Received the response and update block's robInfoIndex accordingly
  m_session.read( hdr, 3*sizeof(std::uint32_t) );
  if( hdr[0] != std::uint32_t(MsgTypeId::GET_ROBS_RSP) )
  {
    m_session.close();
    std::stringstream s;
    s << "Received unexpected Message type id : 0x" << std::hex << hdr[0] << std::dec <<
         " instead of MsgTypeId::GET_ROBS_RSP (0xEFDF0D)";
    throw dfinterface::CommunicationError( ERS_HERE, s.str() );
  }
  std::uint32_t byteSize = hdr[2];
  if (byteSize != 2*sizeof(std::uint32_t))
  {
    m_session.close(); // Close the connection
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid number of data bytes received" );
  }
  m_session.adjustBufferSize( byteSize );
  m_session.read( m_session.buffer(), byteSize );
  auto data = m_session.buffer();
  if( data[0] != eventId() )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Received invalid eventId" );
  }
  m_block.appendAndInsertRobInfos(data[1]);
  ERS_DEBUG(3,"getTheRobs done");
}


// Retrieves the specified ROB fragments.
void DCMEvent::getRobs(const std::vector<uint32_t>& robIds,
    std::vector<hltinterface::DCM_ROBInfo>& robInfos )
{
  ERS_DEBUG(3,"Starting getRobs function.");
  std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
  if ( !attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    ERS_LOG("Unable to lock the getRobs mutex after the " << NMINUTES_TO_WAIT << " time interval.");
    throw dfinterface::CommunicationError( ERS_HERE, "Unable to lock dfinterfaceDCM mutex." );
  }
  if( m_session.m_state != DCMSession::PROCESSING_EVENT )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }
  getRobs_unprotected(robIds, robInfos);
}

// Actually performing the functions of getRobs while not locking the mutex. Must be called by a protected function (mayGetRobs, getRobs)
void DCMEvent::getRobs_unprotected(const std::vector<uint32_t>& robIds,
    std::vector<hltinterface::DCM_ROBInfo>& robInfos )
{
  // Build the list of missing robs
  std::vector<uint32_t> missingRobs;
  for (auto robId : robIds)
    if (!m_block[robId])
      missingRobs.push_back(robId);
  // if not empty request the missing robs
  if (!missingRobs.empty())
    getTheRobs(missingRobs);

  // Fill the hltinterface robInfo vector with the request robs information
  std::sort(missingRobs.begin(), missingRobs.end());
  for (auto robId : robIds)
  {
    const dcm::SharedBlockArray::RobInfo* robInfo = m_block[robId];
    // Ignore robId without associated ROB data
    if (robInfo)
    {
      bool cached = !std::binary_search(missingRobs.begin(), missingRobs.end(), robId);
      robInfos.emplace_back(robInfo->robFragment(), cached,
                            robInfo->reqTime, robInfo->rspTime);
    }
  }
}

// Retrieves all the available ROB fragments.
void DCMEvent::getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robInfos)
{
  ERS_DEBUG(3,"Starting getAllRobs function.");
  std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
  if ( !attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    ERS_LOG("Unable to lock the getAllRobs mutex after the " << NMINUTES_TO_WAIT << " time interval.");
    throw dfinterface::CommunicationError( ERS_HERE, "Unable to lock dfinterfaceDCM mutex." );
  }

  // Send the request message containing an empty list of robIds
  if( m_session.m_state != DCMSession::PROCESSING_EVENT )
  {
    m_session.close();
    throw dfinterface::CommunicationError( ERS_HERE, "Invalid dfinterface state" );
  }
  std::vector<uint32_t> robIds; // empty robIds vector
  std::chrono::steady_clock::time_point reqTime = std::chrono::steady_clock::now();
  if (!m_fetchedAllRobs)
  {
    getTheRobs(robIds);
    m_fetchedAllRobs = true;
  }

  // Fill the hltinterface robInfo vector
  const dcm::SharedBlockArray::RobInfoIndex& robInfoIndex = m_block.robInfoIndex();
  for (dcm::SharedBlockArray::RobInfoIndex::const_iterator it = robInfoIndex.begin();
       it != robInfoIndex.end(); ++it )
  {
    const dcm::SharedBlockArray::RobInfo* robInfo = it->second;
    robInfos.emplace_back(robInfo->robFragment(), robInfo->rspTime < reqTime /*true if was cached*/,
                          robInfo->reqTime, robInfo->rspTime);
  }
}

// Tries to lock the mutex for n minutes
bool DCMEvent::attempt_lock_for_n_minutes( std::unique_lock<std::timed_mutex>& lck, const uint32_t nMinutesToWait ) {
  uint32_t nMinutesWaited = 0;
  bool gotlock = false;
  while ( !( gotlock = lck.try_lock_for(std::chrono::seconds(60)) ) ) {
    ++nMinutesWaited;
    if ( nMinutesWaited >= nMinutesToWait ) {
      break;
    }
  }
  return gotlock;
}


} // namespace dfinterface
} // namespace daq

