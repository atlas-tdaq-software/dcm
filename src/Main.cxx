#include "dcm/Main.h"

#include "dcm/DataCollector.h"
#include "dcm/DummyDataCollector.h"
#include "dcm/DummyL1Source.h"
#include "dcm/DummyProcessor.h"
#include "dcm/HltpuProcessor.h"
#include "dcm/EventBuilder.h"
#include "dcm/HltsvL1Source.h"
#include "dcm/Issues.h"
#include "dcm/L1Source.h"
#include "dcm/Processor.h"
#include "dcm/RosDataCollector.h"
#include "dcm/SfoOutput.h"
#include "dcm/EventBuilderMultiStripping.h"
#include "dcm/Utilities.h"
#include "dcm/dal/DcmDataCollector.h"
#include "dcm/dal/DcmDummyDataCollector.h"
#include "dcm/dal/DcmDummyL1Source.h"
#include "dcm/dal/DcmDummyProcessor.h"
#include "dcm/dal/DcmFileOutput.h"
#include "dcm/dal/DcmHltpuProcessor.h"
#include "dcm/dal/DcmHltsvL1Source.h"
#include "dcm/dal/DcmL1Source.h"
#include "dcm/dal/DcmOutput.h"
#include "dcm/dal/DcmProcessor.h"
#include "dcm/dal/DcmRosDataCollector.h"
#include "dcm/dal/DcmSfoOutput.h"
#include "dcm/SharedBlockArray/Writable.h"
#include "dcm/Event.h"
#include "dcm/FileOutput.h"
#include "dcm/PortFileName.h"

#include "asyncmsg/NameService.h"
#include "ers/ers.h"
#include "is/infodictionary.h"
#include "is/inforeceiver.h"
#include "is/infoT.h"
#include "monsvc/MonitoringService.h"
#include "oh/OHRootMutex.h"
#include "rc/RunParams.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMStates.h"

#include <boost/algorithm/string.hpp>
#include <cstdlib> // For std::srand(), std::exit()
#include <random>
#include <set>
#include <vector>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#include <sys/types.h>
#include <pwd.h>

namespace bpt = boost::posix_time;

namespace daq
{
namespace dcm
{

EventHistograms::EventHistograms(boost::asio::io_service& ioService,
    std::chrono::steady_clock::duration updateInterval,
    float maxSize) :
    m_timer(ioService),
    m_updateInterval(updateInterval)
{
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    m_EventSize_kB = monsvc::MonitoringService::instance().register_object("/DEBUG/EventSize_kB",
        new TH1I("EventSize_kB", "Event size (kB)", 300, 0, maxSize),
        true);
    m_EventSize_kB_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/EventSize_kB_recent",
        new TH1I("EventSize_kB_recent", "Event size (kB) [CURR]", 300, 0, maxSize),
        true);
    m_EventSize_kB_last.reset(
        new TH1I("EventSize_kB_last", "Event size (kB) [cache for current]", 300, 0, maxSize));

    m_EventLocation = monsvc::MonitoringService::instance().register_object("/DEBUG/EventLocation",
        new TH1I("EventLocation", "Location of events inside DCM", 5, 0, 5),
        true);

    m_PUsStatus = monsvc::MonitoringService::instance().register_object("/DEBUG/PUsStatus",
        new TH1I("PUsStatus", "Status of Processing Units", 3, 0, 3),
        true);

    {
      std::lock_guard<monsvc::ptr<TH1I>> l1(m_EventLocation);
      m_EventLocation.get()->GetXaxis()->SetBinLabel(1, "Pending L1 requests");
      m_EventLocation.get()->GetXaxis()->SetBinLabel(2, "Processing pre-EB");
      m_EventLocation.get()->GetXaxis()->SetBinLabel(3, "Processing post-EB");
      m_EventLocation.get()->GetXaxis()->SetBinLabel(4, "Output queue");
      m_EventLocation.get()->GetXaxis()->SetBinLabel(5, "Sampling queue");
    }

    {
      std::lock_guard<monsvc::ptr<TH1I>> l1(m_PUsStatus);
      m_PUsStatus.get()->GetXaxis()->SetBinLabel(1, "FreePUs");
      m_PUsStatus.get()->GetXaxis()->SetBinLabel(2, "BusyPUs");
      m_PUsStatus.get()->GetXaxis()->SetBinLabel(3, "IdlePUs");
    }
  }

  m_timer.expires_at(std::chrono::steady_clock::now());
  m_timer.async_wait(std::bind(&EventHistograms::timer, this, std::placeholders::_1));
}

EventHistograms::~EventHistograms()
{
  m_timer.cancel();

  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
    monsvc::MonitoringService::instance().remove_object(m_EventSize_kB.get());
    monsvc::MonitoringService::instance().remove_object(m_EventSize_kB_recent.get());
    monsvc::MonitoringService::instance().remove_object(m_EventLocation.get());
    monsvc::MonitoringService::instance().remove_object(m_PUsStatus.get());
  }

}

void EventHistograms::clear()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  m_EventSize_kB.get()->Reset();
  m_EventSize_kB_recent->Reset();
  m_EventSize_kB_last->Reset();
  m_EventLocation->Reset();
  m_PUsStatus->Reset();
}

void EventHistograms::updateRecents()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_EventSize_kB);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_EventSize_kB_recent);
    m_EventSize_kB_recent.get()->Add(m_EventSize_kB.get(), m_EventSize_kB_last.get(), 1, -1);
    m_EventSize_kB.get()->Copy(*m_EventSize_kB_last.get());
  }
}

void EventHistograms::timer(const boost::system::error_code& error)
{
  if (error)
  {
    if (error != boost::asio::error::operation_aborted)
    {
      ers::warning(dcm::GenericIssue(ERS_HERE,"EventHistograms diff for recents timer error",
          makeSystemError(ERS_HERE, error)));
    }
  }
  else
  {
    updateRecents();
    m_timer.expires_at(m_timer.expires_at() + m_updateInterval);
    m_timer.async_wait(std::bind(&EventHistograms::timer, this, std::placeholders::_1));
  }
}

void EventHistograms::setEventLocationBin(int bin, double value)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_EventLocation->SetBinContent(bin, value);
}

void EventHistograms::setPUsStatusBin(int bin, double value)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_PUsStatus->SetBinContent(bin, value);
}

void EventHistograms::fillEventSize_kB(double value)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_EventSize_kB->Fill(value);
}


Main::Main() :
    daq::rc::Controllable(),
    m_ioService(1),
    m_dfConfigServer("DFConfig"),
    m_fakeRunNumber(0),
    m_maxOutEvts(0),
    m_oldInfos(20)
{
  if (const char* dfConfigServer = getenv("DF_CONFIG_IS_SERVER"))
  {
    m_dfConfigServer = dfConfigServer;
  }

  m_userName = daq::dcm::username_r();
 
  // Initialize the C random number generator once and for all.
  std::random_device rd;
  std::srand(rd() ^ std::chrono::steady_clock::now().time_since_epoch().count());
}

Main::~Main() noexcept
{
}

void Main::configure(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  // Dump the configuration object
  ERS_LOG("Global configuration:\n" << applicationConf());

  // Configuration sanity check
  {
    auto conf = confDatabase().cast<dal::DcmProcessor>(applicationConf().get_processor());
    if(conf)
    {
      // Check processingTimeOut vs actionTimeout (only for DcmHltpuProcessor)
      if( confDatabase().cast<dal::DcmHltpuProcessor>( applicationConf().get_processor() ) )
      {
        uint32_t pTimeOut = conf->get_processingTimeOut_ms() / 1000;
        uint32_t aTimeOut = applicationConf().get_ActionTimeout();
        if( pTimeOut > aTimeOut )
        {
          std::ostringstream o;
          o << "ProcessingTimeOut(" << pTimeOut
            << "s) greater than ActionTimeOut(" << aTimeOut
            << "s), this may entail STOP transition failures";
          ers::warning( ConfigurationIssue( ERS_HERE, o.str() )); 
        }
      }  
      // Check SBA configuration
      m_maxOutEvts       = conf->get_maxOutputEvents();
      uint32_t maxBlocks = applicationConf().get_sbaNumOfBlocks();
      if ( maxBlocks < m_maxOutEvts )
      {
        std::ostringstream o;
        o << "DcmApplication.sbaNumOfBlocks(" << maxBlocks    << ") smaller than "
          << "DcmProcessor.maxOutputEvents("  << m_maxOutEvts << "); "
          << "NB: DcmApplication.sbaNumOfBlocks must be bigger than DcmProcessor.maxOutputEvents "
          << "plus the number of PUs (HLTRCApplication.numForks)";
        ers::warning( ConfigurationIssue( ERS_HERE, o.str() )); 
      }
    }
  } // Sanity check

  // Prevent m_ioService.run() to return when there are no pending asynchronous operations
  m_ioServiceWork.reset(new boost::asio::io_service::work(m_ioService));
  // Run m_ioService
  m_ioServiceThread.reset(new std::thread([&] () {
    try
    {
      m_ioService.run();
    }
    catch (std::exception& ex)
    {
      ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled exception in IO service thread", ex));
      sleep(2); std::abort();
    }
    catch (...)
    {
      ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled unknown exception in IO service thread"));
      sleep(2); std::abort();
    }
  }));

  // Setup the Monitoring infrastructure:
  // create and register Info objects and Histograms and configure PublishingController
  initializeMonitoring();

  // Access the SBA configuration and instantiate it
  std::string fileName = applicationConf().get_sbaDirectory();
  fileName += "/";
  fileName += applicationConf().get_sbaBaseName(); // Can be empty
  fileName += applicationName();
  fileName += ".";
  fileName += partitionConf().UID();
  fileName += ".";
  fileName += userName();
  fileName += ".sba";
  ERS_LOG("SBA file: '" << fileName << "'");
  ::unlink(fileName.c_str());

  // Trigger MAP_ANONYMOUS if there are no HLTPUs in the configuration,
  // i.e. the processor is dummy and the dcm works in bypass mode 
  bool mapAnonymous = false;
  if (confDatabase().cast<dal::DcmDummyProcessor>(applicationConf().get_processor())) 
    mapAnonymous = true;

  m_sba.reset(new SharedBlockArray::Writable(
                fileName,
                applicationConf().get_sbaNumOfBlocks(),
                applicationConf().get_sbaBlockSize_MiB(),
                mapAnonymous));
  if(mapAnonymous) m_dcmInfo->SbaFile = "No file (MAP_ANONYMOUS)";
  else             m_dcmInfo->SbaFile = fileName;


  // Access the object implementing the L1Source and instantiate it
  if (auto conf = confDatabase().cast<dal::DcmHltsvL1Source>(applicationConf().get_l1Source())) {
    ERS_LOG("L1Source configuration:\n" << *conf);
    m_l1Source.reset(new HltsvL1Source(*this, *conf));
  }
  else if (auto conf = confDatabase().cast<dal::DcmDummyL1Source>(applicationConf().get_l1Source())) {
    ERS_LOG("L1Source configuration:\n" << *conf);
    m_l1Source.reset(new DummyL1Source(*this, *conf));
  }
  else {
    ers::fatal(GenericIssue(ERS_HERE, "Unexpected OKS class derived from DcmL1Source"));
  }
  // Access the object implementing the Processor and instantiate it
  if (auto conf = confDatabase().cast<dal::DcmHltpuProcessor>(applicationConf().get_processor())) {
    ERS_LOG("Processor configuration:\n" << *conf);
    m_processor.reset(new HltpuProcessor(*this, *conf));
  }
  else if (auto conf = confDatabase().cast<dal::DcmDummyProcessor>(applicationConf().get_processor())) {
    ERS_LOG("Processor configuration:\n" << *conf);
    m_processor.reset(new DummyProcessor(*this, *conf));
  }
  else {
    ers::fatal(GenericIssue(ERS_HERE, "Unexpected OKS class derived from DcmTrigger"));
  }

  // Access the object implementing the DataCollector and instantiate it
  if (auto conf = confDatabase().cast<dal::DcmRosDataCollector>(applicationConf().get_dataCollector())) {
    ERS_LOG("DataCollector configuration:\n" << *conf);
    m_dataCollector.reset(new RosDataCollector(*this, *conf));
  }
  else if (auto conf = confDatabase().cast<dal::DcmDummyDataCollector>(applicationConf().get_dataCollector())) {
    ERS_LOG("DataCollector configuration:\n" << *conf);
    m_dataCollector.reset(new DummyDataCollector(*this, *conf));
  }
  else {
    ers::fatal(GenericIssue(ERS_HERE, "Unexpected OKS class derived from DcmDataCollector"));
  }


  // Instantiate the object implementing the EventBuilder
  // TODO: get runType value from IS (read it from Main!!!!)
  uint32_t runType = 0;
  m_eventBuilder.reset(new EventBuilderMultiStripping(*this, runType, m_dataCollector->robIds()));

  // Access the object implementing the Output and instantiate it
  if (auto conf = confDatabase().cast<dal::DcmFileOutput>(applicationConf().get_output())) {
    ERS_LOG("Output configuration:\n" << *conf);
    m_output.reset(new FileOutput( *this, *conf));
  }
  else if (auto conf = confDatabase().cast<dal::DcmSfoOutput>(applicationConf().get_output())) {
    ERS_LOG("Output configuration:\n" << *conf);
    m_output.reset(new SfoOutput(*this, *conf));
  }
  else {
    ers::fatal(GenericIssue(ERS_HERE, "Unexpected OKS class derived from DcmOutput"));
  }

  m_l1Source->initialize(m_processor.get());
  m_dataCollector->initialize(m_processor.get());
  m_processor->initialize(m_l1Source.get(), m_dataCollector.get(), m_eventBuilder.get() );
  m_eventBuilder->initialize(m_processor.get(), m_output.get());
  m_output->initialize(m_eventBuilder.get());

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::connect(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  // Random sleep to spread the system load during the connect transition
  if (applicationConf().get_maxConnectDelay_s() != 0) {
    std::uint32_t sleep_ms = applicationConf().get_maxConnectDelay_s() * (std::rand() % 1001);
    ERS_LOG("CONNECT transition ... going to sleep for " << sleep_ms << " ms.");
    std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
    ERS_LOG("CONNECT transition ... just woke up.");
  }

  // IS
  if (m_publisher)
    m_publisher->start_publishing();

  doTransition(CONNECT);

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::prepareForRun(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  // Clean up counters and and reset circular buffer 
  m_eventHistograms->clear();
  {
    std::lock_guard<monsvc::ptr<info::DCM>> lock(m_dcmInfo);
    m_oldInfos.clear();
  }
  resetIsInfo();

  // Get run number from IS
  try {
    ISInfoDictionary infoDict(partition());
    infoDict.findValue("RunParams.RunParams", m_runParams);
  } catch (daq::is::Exception & ex) {
    // TODO: use better exception
    ers::error(GenericIssue(ERS_HERE,
        "Could not access IS information RunParams.RunParams. Using bogus run number and project tag."));
    m_runParams = RunParams();
    m_runParams.run_number = ++m_fakeRunNumber;
    m_runParams.T0_project_tag = "data_test";
  }
  ERS_LOG("Run number: " << m_runParams.run_number);

  //   Start requesting events to the HLTSV interface
  doTransition(START);

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::stopHLT(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  // Execute end of run operations:
  //   1. Stop requesting events to HLTSV
  //   2. Wait until the DCM is empty
  //      1. If the shortTimeout expires, force accept the events without processing
  //      2. If the actionTimeout expires, we are in truble, raise a FATAL
  //   3. Dump end of run statistics to ERS_LOG
  //      Commit the latest counters and histograms in IS and OH

  doTransition(STOP);

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::stopGathering(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  doTransition(RESET);

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::disconnect(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  doTransition(DISCONNECT);

  // IS
  if (m_publisher)
    m_publisher->stop_publishing();

  // asyncmsg name service
  m_serverToEndpoint.clear();

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::unconfigure(const daq::rc::TransitionCmd& cmd)
{
  ERS_LOG(cmd.toString() << " [TimeOut = " << applicationConf().get_ActionTimeout() << " s]");
  auto timestamp = std::chrono::steady_clock::now();

  // Destroy the components
  m_l1Source.reset();
  m_dataCollector.reset();
  m_processor.reset();
  m_eventBuilder.reset();
  m_output.reset();

  // Clean-up monsvc
  monsvc::MonitoringService::instance().remove_object(m_dcmInfo.get());
  m_eventHistograms.reset();

  // Destroy the SBA
  m_sba.reset();

  // Allow m_ioService.run() to return when there are no pending asynchronous operations
  m_ioServiceWork.reset();
  // Wait for m_ioService.run() to return (i.e. wait for all asynchronous operations to finish)
  m_ioServiceThread->join();
  // Reset m_ioService so it can run again.
  m_ioService.reset();

  // Clear the asyncmsg name service cache
  m_serverToEndpoint.clear();

  // Done
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - timestamp);
  ERS_LOG(cmd.toString() << " done [ " << duration.count() << " s ]");
}

void Main::user(const daq::rc::UserCmd& cmd)
{
  if (rc::FSMStates::stringToState(cmd.currentFSMState()) < rc::FSM_STATE::CONFIGURED)
  {
    DCM_WARNING(dcm::GenericIssue, "Received user command: " << cmd.commandName() <<
                ", but the application is not yet configured. Ignoring." );
  }
  else if (cmd.commandName() == "DCM_DATACOLLECTOR_SET_PARAMETERS" ||
      cmd.commandName() == "DCM_PROCESSOR_SET_PARAMETERS")
  {
    if (!applicationConf().get_dynamicUpdate())
    {
      DCM_WARNING(dcm::GenericIssue, "Received dynamic parameters command: " << cmd.commandName() <<
                  ", but dynamic update is disabled. Ignoring." );
    }
    else
    {
      std::map<std::string, std::string> parameters;
      for (auto& parameter : cmd.commandParameters())
      {
        std::vector<std::string> kv;
        boost::split(kv, parameter, boost::is_any_of("="));
        if (kv.size() != 2)
        {
          ERS_LOG("Invalid dynamic parameter " << parameter << ". Ignored.");
          continue;
        }
        parameters[kv[0]] = kv[1];
      }
      if (cmd.commandName() == "DCM_DATACOLLECTOR_SET_PARAMETERS")
      {
        ioService().post(std::bind(&DataCollector::setDynamicParameters, m_dataCollector.get(), parameters));
      }
      else if (cmd.commandName() == "DCM_PROCESSOR_SET_PARAMETERS")
      {
        ioService().post(std::bind(&Processor::setDynamicParameters, m_processor.get(), parameters));
      }
    }
  }
  else if (cmd.commandName() == "ROS_DOWN")
  {
    auto rosNames = cmd.commandParameters();
    ioService().post([this, rosNames] ()
    {
      for (const auto& ros : rosNames)
      {
        m_dataCollector->disableSource(ros);
      }
    });
  }
  else if (cmd.commandName() == "ROS_UP")
  {
    auto rosNames = cmd.commandParameters();
    ioService().post([this, rosNames] ()
    {
      for (const auto& ros : rosNames)
      {
        m_dataCollector->enableSource(ros);
      }
    });
  }
  else
  {
    ERS_LOG("Received unknown user command: " << cmd.commandName() << ". Ignoring.");
  }
}

void Main::onExit(daq::rc::FSM_STATE state) noexcept
{
  // onExit() is called when the DCM is asked to exit, regardless of the state.
  // After onExit returns, the daq::rc::ItemCtrl::run() method, called in the main(), returns, so
  // the main() returns as well. This means that this object is destroyed. Many destructors for the
  // objects owned by this, do not behave well when the state is not INITIAL, leading to crashes.
  // To avoid mixing those spurious crashes with more serious ones, we call sys::exit() here, which
  // exits the application without trying to run destructors.
  if (state > daq::rc::FSM_STATE::INITIAL)
    std::exit(EXIT_FAILURE);
}

boost::asio::ip::tcp::endpoint Main::resolveServer(const std::string& serverName)
{
  // The keys in the m_serverToEndpoint map are the names of the servers. The values are pairs of
  // an endpoint and a boolean. The boolean is true if resolveServer() has already been called for
  // that server.

  asyncmsg::NameService nameService(partition(), {});

  if (m_serverToEndpoint.empty()) {
    // On the first request, lookup the endpoints of *all* servers.
    try {
      auto serverToEndpoint = nameService.lookup_names("");
      for (const auto& serverEndpoint : serverToEndpoint) {
        m_serverToEndpoint[serverEndpoint.first] = std::make_pair(serverEndpoint.second, false);
      }
    }
    catch (const is::NotFound& ex) {
      throw GenericIssue(ERS_HERE, "Could not lookup asyncmsg servers.", ex);
    }
  }

  if (m_serverToEndpoint.find(serverName) != m_serverToEndpoint.end() &&
      m_serverToEndpoint[serverName].second == false) {
    // If the server was already resolved and this is the first time it is asked for, we return
    // the stored endpoint.
    m_serverToEndpoint[serverName].second = true;
  }
  else {
    // If the server was not resolved or this is not the first time it is asked for (i.e. the caller
    // is probably trying to reconnect to a server that changed endpoint, we resolve it again.
    try {
      m_serverToEndpoint[serverName] = std::make_pair(nameService.resolve(serverName), true);
    }
    catch (const is::NotFound& ex) {
      throw GenericIssue(ERS_HERE, "Could not lookup asyncmsg server " + serverName + ".", ex);
    }
  }
  return m_serverToEndpoint[serverName].first;
}

void Main::onL1SourceConnect()
{
  ERS_LOG("L1Source connected.");
  updateTransition(L1_SOURCE_DONE);
}

void Main::onL1SourceConnectError(const boost::system::error_code& error)
{
  ERS_LOG("L1Source connect error: " << error.message() << " (" << error << ").");
  updateTransition(L1_SOURCE_DONE | COMPONENT_FAILED);
}

void Main::onL1SourceStart()
{
  ERS_LOG("L1Source started.");
  updateTransition(L1_SOURCE_DONE);
}

void Main::onL1SourceStartError(const boost::system::error_code& error)
{
  ERS_LOG("L1Source start error: " << error.message() << " (" << error << ").");
  updateTransition(L1_SOURCE_DONE | COMPONENT_FAILED);
}

void Main::onL1SourceStop()
{
  ERS_LOG("L1Source stopped.");
  updateTransition(L1_SOURCE_DONE);
}

void Main::onL1SourceStopError(const boost::system::error_code& error)
{
  ERS_LOG("L1Source stop error: " << error.message() << " (" << error << ").");
  updateTransition(L1_SOURCE_DONE | COMPONENT_FAILED);
}

void Main::onL1SourceDisconnect()
{
  ERS_LOG("L1Source disconnected.");
  updateTransition(L1_SOURCE_DONE);
}

void Main::onL1SourceDisconnectError(const boost::system::error_code& error)
{
  ERS_LOG("L1Source disconnect error: " << error.message() << " (" << error << ").");
  updateTransition(L1_SOURCE_DONE | COMPONENT_FAILED);
}

void Main::onDataCollectorConnect()
{
  ERS_LOG("DataCollector connected.");
  updateTransition(DATA_COLLECTOR_DONE);
}

void Main::onDataCollectorConnectError(const boost::system::error_code& error)
{
  ERS_LOG("DataCollector connect error: " << error.message() << " (" << error << ").");
  updateTransition(DATA_COLLECTOR_DONE | COMPONENT_FAILED);
}

void Main::onDataCollectorStart()
{
  ERS_LOG("DataCollector started.");
  updateTransition(DATA_COLLECTOR_DONE);
}

void Main::onDataCollectorStartError(const boost::system::error_code& error)
{
  ERS_LOG("DataCollector start error: " << error.message() << " (" << error << ").");
  updateTransition(DATA_COLLECTOR_DONE | COMPONENT_FAILED);
}

void Main::onDataCollectorStop()
{
  ERS_LOG("DataCollector stopped.");
  updateTransition(DATA_COLLECTOR_DONE);
}

void Main::onDataCollectorStopError(const boost::system::error_code& error)
{
  ERS_LOG("DataCollector stop error: " << error.message() << " (" << error << ").");
  updateTransition(DATA_COLLECTOR_DONE | COMPONENT_FAILED);
}

void Main::onDataCollectorDisconnect()
{
  ERS_LOG("DataCollector disconnected.");
  updateTransition(DATA_COLLECTOR_DONE);
}

void Main::onDataCollectorDisconnectError(const boost::system::error_code& error)
{
  ERS_LOG("DataCollector disconnect error: " << error.message() << " (" << error << ").");
  updateTransition(DATA_COLLECTOR_DONE | COMPONENT_FAILED);
}

void Main::onOutputConnect()
{
  ERS_LOG("Output connected.");
  updateTransition(OUTPUT_DONE);
}

void Main::onOutputConnectError(const boost::system::error_code& error)
{
  ERS_LOG("Output connect error: " << error.message() << " (" << error << ").");
  updateTransition(OUTPUT_DONE | COMPONENT_FAILED);
}

void Main::onOutputStart()
{
  ERS_LOG("Output started.");
  updateTransition(OUTPUT_DONE);
}

void Main::onOutputStartError(const boost::system::error_code& error)
{
  ERS_LOG("Output start error: " << error.message() << " (" << error << ").");
  updateTransition(OUTPUT_DONE | COMPONENT_FAILED);
}

void Main::onOutputStop()
{
  ERS_LOG("Output stopped.");
  updateTransition(OUTPUT_DONE);
}

void Main::onOutputStopError(const boost::system::error_code& error)
{
  ERS_LOG("Output stop error: " << error.message() << " (" << error << ").");
  updateTransition(OUTPUT_DONE | COMPONENT_FAILED);
}

void Main::onOutputDisconnect()
{
  ERS_LOG("Output disconnected.");
  updateTransition(OUTPUT_DONE);
}

void Main::onOutputDisconnectError(const boost::system::error_code& error)
{
  ERS_LOG("Output disconnect error: " << error.message() << " (" << error << ").");
  updateTransition(OUTPUT_DONE | COMPONENT_FAILED);
}

void Main::onProcessorConnect()
{
  ERS_LOG("Processor connected.");
  updateTransition(PROCESSOR_DONE);
}

void Main::onProcessorConnectError(const boost::system::error_code& error)
{
  ERS_LOG("Processor connect error: " << error.message() << " (" << error << ").");
  updateTransition(PROCESSOR_DONE | COMPONENT_FAILED);
}

void Main::onProcessorStart()
{
  ERS_LOG("Processor started.");
  updateTransition(PROCESSOR_DONE);
}

void Main::onProcessorStartError(const boost::system::error_code& error)
{
  ERS_LOG("Processor start error: " << error.message() << " (" << error << ").");
  updateTransition(PROCESSOR_DONE | COMPONENT_FAILED);
}

void Main::onProcessorStop()
{
  ERS_LOG("Processor stopped.");
  updateTransition(PROCESSOR_DONE);
}

void Main::onProcessorStopError(const boost::system::error_code& error)
{
  ERS_LOG("Processor stop error: " << error.message() << " (" << error << ").");
  updateTransition(PROCESSOR_DONE | COMPONENT_FAILED);
}

void Main::onProcessorReset()
{
  ERS_LOG("Processor reset.");
  updateTransition(PROCESSOR_DONE);
}

void Main::onProcessorResetError(const boost::system::error_code& error)
{
  ERS_LOG("Processor reset error: " << error.message() << " (" << error << ").");
  updateTransition(PROCESSOR_DONE | COMPONENT_FAILED);
}

void Main::onProcessorDisconnect()
{
  ERS_LOG("Processor disconnected.");
  updateTransition(PROCESSOR_DONE);
}

void Main::onProcessorDisconnectError(const boost::system::error_code& error)
{
  ERS_LOG("Processor disconnect error: " << error.message() << " (" << error << ").");
  updateTransition(PROCESSOR_DONE | COMPONENT_FAILED);
}

void Main::resetIsInfo()
{  
  m_dcmInfo->L1Rate                     = 0;
  m_dcmInfo->L1RateAvg                  = 0;
  m_dcmInfo->EbRate                     = 0;
  m_dcmInfo->EbRateAvg                  = 0;
  m_dcmInfo->OutRate                    = 0;
  m_dcmInfo->OutRateAvg                 = 0;
  m_dcmInfo->DcReqRate                  = 0;
  m_dcmInfo->DcReqRateAvg               = 0;
  m_dcmInfo->L1SourceRequestedEvents    = 0;
  m_dcmInfo->L1SourceReceivedEvents     = 0;
  m_dcmInfo->L1SourceDoneEvents         = 0;
  m_dcmInfo->L1SourceConnectionTimeouts = 0;
  m_dcmInfo->L1SourceConnectionErrors   = 0;
  m_dcmInfo->ProxReqEvents              = 0;
  m_dcmInfo->ProxL1Events               = 0;
  m_dcmInfo->ProxRejEvents              = 0;
  m_dcmInfo->ProxAccEvents              = 0;
  //m_dcmInfo->ProxBusyPUs                = 0;
  //m_dcmInfo->ProxFreePUs                = 0;
  m_dcmInfo->ProxDoneEvents             = 0;
  m_dcmInfo->DcROBRequests              = 0;
  //m_dcmInfo->DcPendingRequests          = 0;
  //m_dcmInfo->DcTrafficShapingCredits    = 0;
  m_dcmInfo->DcReceivedData             = 0;
  m_dcmInfo->DcDataBW                   = 0;
  //m_dcmInfo->DcActiveConnections        = 0;
  m_dcmInfo->DcConnectionTimeouts       = 0;
  m_dcmInfo->DcConnectionErrors         = 0;
  m_dcmInfo->EbEvents                   = 0;
  m_dcmInfo->SampledEvents              = 0;
  m_dcmInfo->OutputEvents               = 0;
  m_dcmInfo->OutputDeliveredEvents      = 0;
  m_dcmInfo->OutputDeliveredData        = 0;
  //m_dcmInfo->OutputActiveConnections    = 0;
  m_dcmInfo->OutputConnectionTimeouts   = 0;
  m_dcmInfo->OutputConnectionErrors     = 0;
  m_dcmInfo->DiskWrBytes                = 0;
  m_dcmInfo->DiskWrBytesRate            = 0;
  m_dcmInfo->EventsInside               = 0;  
  m_dcmInfo->EventsOnOutputQueue        = 0;  
  m_dcmInfo->BusyProcessing             = 0.;
  m_dcmInfo->BusyFromOutput             = 0.;
  m_dcmInfo->BlockOccupancy             = 0.;
  m_dcmInfo->ProxHltpuTimeouts          = 0;
  m_dcmInfo->ProxHltpuCrashes           = 0;
}

void Main::initializeMonitoring()
{
  // 1. Register the object;
  m_dcmInfo = monsvc::MonitoringService::instance().register_object("info", new info::DCM(), true,
      std::bind(&Main::isInfoCallBack, this, std::placeholders::_1, std::placeholders::_2));

  // TODO: use a proper configuration parameter instead of abusing infoAverageInterval_s
  m_eventHistograms.reset(new EventHistograms(m_ioService,
      std::chrono::seconds(applicationConf().get_infoAverageInterval_s()),
      applicationConf().get_UpperEdgeSizeHistos()
      ));

  // 2. Create the publisher
  try
  {
    m_publisher.reset(new monsvc::PublishingController(partition(), applicationName()));
    m_publisher->add_configuration_rules(confDatabase(), &applicationConf());
    // Dump the configuration
    std::stringstream o;
    for (auto s : m_publisher->get_rules())
      o << "\t" << s << "\n";
    ERS_LOG("Publishing rules for'" << applicationName() << ": \n" << o.str());
  }
  catch (std::exception& ex)
  {
    ers::error(daq::dcm::GenericIssue(ERS_HERE, "Cannot configure Monitoring Service", ex));
  }
}

void Main::isInfoCallBack(const std::string& /* name */, info::DCM *dcm)
{
  auto now  = std::chrono::steady_clock::now();

  // Update the rates if there is at least 1 archived info
  if(!m_oldInfos.empty())
  {
    // Find the entry with an age of ~ applicationConf().get_infoAverageInterval_s()
    auto last = m_oldInfos.begin();
    for (; last != m_oldInfos.end(); ++last) {
      if ((now - last->first) >=
          std::chrono::seconds(applicationConf().get_infoAverageInterval_s())) {
        break;
      }
    }
    if (last == m_oldInfos.end()) {
      if (m_oldInfos.capacity() < 100) {  // Cut off at 100
        // Increase the buffer capacity to accommodate the requested interval
        m_oldInfos.set_capacity(m_oldInfos.capacity() * 2);
      }
      last = m_oldInfos.end() - 1;
    }

    auto previous = m_oldInfos.begin();

    std::chrono::duration<double> interval;

    // Update instantaneous rates
    interval = now - previous->first;
    if (interval < std::chrono::seconds(1)) {
      //ERS_LOG("Received two monsvc callbacks in " << interval.count() << " seconds!");
      return;
    }

    //FIXME
    if ( dcm->EbEvents < previous->second.EbEvents ||
         dcm->L1SourceReceivedEvents < previous->second.L1SourceReceivedEvents ||
         dcm->OutputDeliveredEvents < previous->second.OutputDeliveredEvents   ||
         dcm->DcROBRequests < previous->second.DcROBRequests                   ||
         dcm->DcReceivedData < previous->second.DcReceivedData                 ||
         dcm->OutputDeliveredData < previous->second.OutputDeliveredData )
    {
      ERS_INFO("Wrong values in circular buffer for one of the counters. Example:"
        << "dcm->EbEvents (" << dcm->EbEvents << ") < "
        << "previous->second.EbEvents (" << previous->second.EbEvents << "). "  
        << "Reset the buffer and skip the publication."
      );
      m_oldInfos.clear();
      return;
    }

    dcm->L1Rate    = (dcm->L1SourceReceivedEvents - previous->second.L1SourceReceivedEvents) / interval.count();
    dcm->OutRate   = (dcm->OutputDeliveredEvents - previous->second.OutputDeliveredEvents) / interval.count();
    dcm->EbRate    = (dcm->EbEvents - previous->second.EbEvents) / interval.count();
    dcm->DcReqRate = (dcm->DcROBRequests - previous->second.DcROBRequests) / interval.count();
    dcm->DcDataBW     = (dcm->DcReceivedData - previous->second.DcReceivedData) / interval.count();
    dcm->OutputDataBW = (dcm->OutputDeliveredData - previous->second.OutputDeliveredData) / interval.count();

    // Update average rates (last applicationConf().get_infoAverageInterval_s() seconds)
    interval = now - last->first;
    dcm->L1RateAvg    = (dcm->L1SourceReceivedEvents  - last->second.L1SourceReceivedEvents)  / interval.count();
    dcm->OutRateAvg   = (dcm->OutputDeliveredEvents - last->second.OutputDeliveredEvents) / interval.count();
    dcm->EbRateAvg    = (dcm->EbEvents - last->second.EbEvents) / interval.count();
    dcm->DcReqRateAvg = (dcm->DcROBRequests - last->second.DcROBRequests) / interval.count();
    dcm->DiskWrBytesRate = (dcm->DiskWrBytes - last->second.DiskWrBytes) / interval.count();
  }
  
  // Update CPU usage and diskIO
  dcm->cpuUsageProc = cpuUsageByProcess();
  dcm->cpuUsageNode = cpuUsageByNode();
  dcm->DiskWrBytes  = procSelfIo("write_bytes:");
 
  dcm->EventsInside        = dcm->ProxL1Events - dcm->ProxDoneEvents;
  dcm->EventsOnOutputQueue = dcm->ProxRejEvents + dcm->ProxAccEvents - dcm->ProxDoneEvents;
  dcm->BusyProcessing      = (dcm->ProxFreePUs) ? 0. : 1.;
  dcm->BusyFromOutput      = ( dcm->EventsOnOutputQueue >= m_maxOutEvts ) ? 1. : 0.;

  // Feed the circular buffer
  m_oldInfos.push_front(std::make_pair(now, *dcm));
  
  // Fill the event location histogram (NB: updated with IS)
  uint32_t l1rPending  = dcm->L1SourceRequestedEvents - dcm->L1SourceReceivedEvents;
  uint64_t processed   = dcm->ProxRejEvents + dcm->ProxAccEvents;
  uint64_t onProc      = dcm->ProxL1Events - processed;
  uint64_t onProcPreEB = dcm->L1SourceReceivedEvents - dcm->L1SourceDoneEvents;
  uint64_t onProcPostEB= onProc - onProcPreEB;
  uint64_t outputQueue = processed - dcm->ProxDoneEvents;  //dcm->OutputEvents - dcm->OutputDeliveredEvents;
  uint64_t idlePUs     = dcm->ProxReqEvents - dcm->ProxL1Events ;
  
  dcm->ProxIdlePUs         = idlePUs;

  /// SBA occupancy
  dcm->BlockOccupancy      = m_sba->maxBlockOccupancy()*100;

  eventHistograms().setEventLocationBin(1, l1rPending);   //
  eventHistograms().setEventLocationBin(2, onProcPreEB);  //
  eventHistograms().setEventLocationBin(3, onProcPostEB); //
  eventHistograms().setEventLocationBin(4, outputQueue);  //
  eventHistograms().setEventLocationBin(5, 0);            // FIXME Sampling Queue
  eventHistograms().setPUsStatusBin(1, dcm->ProxFreePUs);
  eventHistograms().setPUsStatusBin(2, dcm->ProxBusyPUs);
  eventHistograms().setPUsStatusBin(3, idlePUs);

}

void Main::doTransition(Transition transition)
{
  m_transition = transition;
  m_transitionDone = std::promise<void>();
  m_transitionStatus = 0;

  m_ioService.post([this] () {
    updateTransition(0);
  });

  m_transitionDone.get_future().get();
}

void Main::updateTransition(std::uint32_t status)
{
  const unsigned int LEAFS_DONE = L1_SOURCE_DONE | DATA_COLLECTOR_DONE | OUTPUT_DONE;

  m_transitionStatus |= status;
  bool failed = m_transitionStatus & COMPONENT_FAILED;
  unsigned int done = m_transitionStatus & ~COMPONENT_FAILED;
  switch (m_transition) {
  case CONNECT:
    if (done == 0) {
      m_l1Source->asyncConnect();
      m_dataCollector->asyncConnect();
      m_output->asyncConnect();
    }
    else if (done == LEAFS_DONE) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_processor->asyncConnect();
      }
    }
    else if (done == (LEAFS_DONE | PROCESSOR_DONE)) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_transitionDone.set_value();
      }
    }
    break;
  case START:
    if (done == 0) {
      m_l1Source->asyncStart();
      m_dataCollector->asyncStart();
      m_output->asyncStart();
    }
    else if (done == LEAFS_DONE) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_processor->asyncStart();
      }
    }
    else if (done == (LEAFS_DONE | PROCESSOR_DONE)) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_transitionDone.set_value();
      }
    }
    break;
  case STOP:
    if (done == 0) {
      m_processor->asyncStop();
    }
    else if (done == PROCESSOR_DONE ) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_l1Source->asyncStop();
        m_dataCollector->asyncStop();
        m_output->asyncStop();
      }
    }
    else if (done == (LEAFS_DONE | PROCESSOR_DONE)) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_transitionDone.set_value();
      }
    }
    break;
  case RESET:
    if (done == 0) {
      m_processor->asyncReset();
    }
    else if (done == PROCESSOR_DONE) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_transitionDone.set_value();
      }
    }
    break;
  case DISCONNECT:
    if (done == 0) {
      m_processor->asyncDisconnect();
    }
    else if (done == PROCESSOR_DONE) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_l1Source->asyncDisconnect();
        m_dataCollector->asyncDisconnect();
        m_output->asyncDisconnect();
      }
    }
    else if (done == (LEAFS_DONE | PROCESSOR_DONE)) {
      if (failed) {
        m_transitionDone.set_exception(std::make_exception_ptr(
            GenericIssue(ERS_HERE, "One or more components failed the transition.")));
      }
      else {
        m_transitionDone.set_value();
      }
    }
    break;
  }
}

} // namespace dcm
} // namespace daq
