#include "dcm/SamplerSingleStripping.h"
#include "dcm/EventBuilderMultiStripping.h"
#include "eformat/StreamTag.h"
#include "eformat/FullEventFragmentNoTemplates.h"
#include "ers/ers.h"


namespace daq
{
namespace dcm
{

// brief Construct a sampler for single stripping events
SamplerSingleStripping::SamplerSingleStripping(const IPCPartition &partition,
                                               std::uint32_t maxChannels,
                                               std::uint32_t maxEvents,
                                               EventBuilder *builder ) :
  m_builder(builder), m_maxEvents(maxEvents)
{
  try
  {
    m_sampler.reset(
          new emon::EventSampler(partition,
                                 emon::SamplingAddress("dcm", getenv("TDAQ_APPLICATION_NAME")),
                                 new SamplerSingleStripping::ChannelFactory(*this, maxEvents),
                                 maxChannels));
  }
  catch(ers::Issue &ex)
  {
    ers::warning(daq::dcm::GenericIssue(ERS_HERE, "Instantiation of emon::EventSampler failed. Disabling event sampling.", ex));
  }
  catch(std::exception &ex)
  {
    ers::warning(daq::dcm::GenericIssue(ERS_HERE, "Instantiation on emon::EventSampler failed. Disabling event sampling.", ex));
  }
  catch(...)
  {
    ers::warning(daq::dcm::GenericIssue(ERS_HERE, "Instantiation on emon::EventSampler failed due to unknown exception. Disabling event sampling." ));
  }
  ERS_LOG("SamplerSingleStripping instantiated as 'dcm:'" << getenv("TDAQ_APPLICATION_NAME") << "'");
}


// Add a free buffer to the free buffer recylcing storage
void SamplerSingleStripping::addFreeBuffer(Buffer* buffer)
{
  std::lock_guard<std::mutex> lock(m_buffersMtx);
  m_buffers.emplace_back(buffer);
}


// Get a free buffer able to hold the specified amount of data
std::shared_ptr<SamplerSingleStripping::Buffer> SamplerSingleStripping::getFreeBuffer()
{
  std::lock_guard<std::mutex> lock(m_buffersMtx);
  Buffer* buffer;
  if (m_buffers.empty())
    buffer = new Buffer();
  else
  {
    buffer = m_buffers.back().release();
    m_buffers.pop_back();
  }
  // Set the
  return std::shared_ptr<Buffer>(buffer, std::bind(&SamplerSingleStripping::addFreeBuffer, this, std::placeholders::_1));
}


/*! @brief Return true if the two streamTags have exactly the same set of robs and dets
    @param tag1 The streamTag to compare with tag2
    @param tag2 The streamTag to compare with tag1
    @return true if the two streamTags have exactly the same set of robs and dets
 */
static inline bool hasSameSetOfRobs(const eformat::helper::StreamTag& tag1, const eformat::helper::StreamTag& tag2)
{
  return tag1.dets == tag2.dets && tag1.robs == tag2.robs;
}


// Return event as singleStripped to send to emon
std::shared_ptr<SamplerSingleStripping::Buffer> SamplerSingleStripping::getSingleStrippedEvent(const Event& event)
{
  // -- First: build union of ROBs
  // Get a handy reference on the vector of streams
  const std::vector<Event::Stream>& streams(event.streams);
  // Get a handy reference on the vector of streamTags
  const std::vector<eformat::helper::StreamTag>& streamTags(event.streamTags);

  ERS_ASSERT(streamTags.size() > 0);
  // Build a streamTag with union of dets and robs (singleStripped)
  eformat::helper::StreamTag unionTag("robUnionTag", eformat::MONITORING_TAG, false);
  for (const auto& streamTag: streamTags)
  {
    // not robs and no dets => all robs
    if (streamTag.dets.empty() && streamTag.robs.empty())
    {
      unionTag.dets.clear();
      unionTag.robs.clear();
      break;
    }
    // Insert streamTag dets in unionTag
    unionTag.dets.insert(streamTag.dets.begin(), streamTag.dets.end());
    // Insert streamTag robs in unionTag
    unionTag.robs.insert(streamTag.robs.begin(), streamTag.robs.end());
  }

  // -- Second: Try locating preserialized event matching the criteria
  if (event.streamTags.size() == event.outputStreamTags.size())
  {
    // there is no monitoring streamTag
    size_t streamIdx = 0;
    while (streamIdx < streams.size() &&
           !hasSameSetOfRobs(streamTags[streams[streamIdx].tags[0].idx], unionTag))
      ++streamIdx;
    if (streamIdx < streams.size())
    {
      // Found a matching preserialized event: copy it into a buffer and return it
      const std::uint32_t *data = reinterpret_cast<const std::uint32_t *>(streams[streamIdx].data);
      size_t byte_size = streams[streamIdx].size;
      std::shared_ptr<Buffer> buffer = getFreeBuffer();
      buffer->data.assign(data, data + (byte_size+sizeof(std::uint32_t)-1)/sizeof(std::uint32_t));
      return buffer;
    }
  }

  // -- Third: If no matching event was found, serialize the event containing the union of ROBs
  const std::uint32_t *templateHeader;
  std::uint32_t *data, nbrBytes;

  // if there was no monitoring streamTags,
  if (event.streamTags.size() == event.outputStreamTags.size()) {
    // Use preserialized Header
    templateHeader = event.eventHeader;
  } else {
    // Serialize a new eventHeader without Monitoring StreamTag
    std::uint32_t *data, nbrWords;

    // There must be at least one streamTag
    ERS_ASSERT(event.streamTags.size() > 0);

    // Construct the new event header using the event header for output as template
    eformat::write::FullEventFragment header(event.eventHeader);

    // Store the full list of streamTags (including monitoring streamTags)
    nbrWords = eformat::helper::size_word(event.streamTags);
    data = event.allocate(nbrWords);
    eformat::helper::encode(event.streamTags, nbrWords, data);
    header.stream_tag(nbrWords, data);

    // Bind the fragments of the modified event header
    const eformat::write::node_t *node = header.bind();

    // Serialize the event header in the event block
    nbrWords = header.size_word();
    data = event.allocate(nbrWords);
    std::uint32_t res = eformat::write::copy(*node, data, nbrWords);
    ERS_ASSERT(res == nbrWords);
    templateHeader = data;
  }

  // Serialize the event (uses templateHeader as header)
  m_builder->serializeEvent (event, unionTag, templateHeader, data, nbrBytes);

  // Copy it into a buffer and return it
  std::shared_ptr<Buffer> buffer = getFreeBuffer();
  buffer->data.assign(data, data + (nbrBytes+sizeof(std::uint32_t)-1)/sizeof(std::uint32_t));
  return buffer;
}


// Submit a DCM event for sampling
void SamplerSingleStripping::sample(const Event& event)
{
  // -- First: remove closed channels
  bool channelsUpdated = false;
  for (auto& channel : m_channels)
  {
    if (channel->is_closed())
    {
      ERS_LOG("Remove closed sampling channel " << emon::construct_name(channel->criteria()));
      channel.reset();
      channelsUpdated = true;
    }
  }
  auto remove = std::remove(m_channels.begin(), m_channels.end(), nullptr);
  m_channels.erase(remove, m_channels.end());

  // -- Second: Activate new pending channels
  {
    std::lock_guard<std::mutex> lock(m_newChannelsMtx);
    for (auto& channel : m_newChannels) {
      m_channels.push_back(channel);
      ERS_LOG("Activate sampling channel " << emon::construct_name(channel->criteria()));
      channelsUpdated = true;
    }
    m_newChannels.clear();
  }
  if (channelsUpdated) {
    ERS_LOG("Number of active (open) channels: " << m_channels.size());
  }

  // -- Third: Perform event sampling
  std::shared_ptr<Buffer> buffer;
  for (auto& channel : m_channels) {
    if (channel->is_selected(event)) {
      if (!buffer) {
        buffer = getSingleStrippedEvent(event);
      }
      channel->queue_event(buffer);
    }
  }
}


//! Return true if the status criteria is valid
inline bool isStatusCriteriaValid(const eformat::read::FullEventFragment& fe, std::uint32_t statusWord)
{
  const std::uint32_t *status = fe.status();
  for (std::uint32_t i = fe.nstatus(); i != 0; --i, ++status)
    if (*status == statusWord)
      return true;
  return false;
}


//! Return true if the stream tag type is present
static inline bool isStreamTagTypePresent(const std::vector<eformat::helper::StreamTag>& tags,
                                          const std::string& type)
{
  for (const auto& tag : tags)
    if (tag.type == type)
      return true;
  return false;
}


//! Return true if at least one criteria's stream tag is present
static inline bool atLeastOneStreamTagPresent(const std::vector<eformat::helper::StreamTag>& tags,
                                              const std::string& type, const std::vector<std::string>& names)
{
  for (const auto& tag : tags)
    if (tag.type == type)
      for (const auto& name : names)
        if (tag.name == name)
          return true;
  return false;
}


//! Return true if the criteria's stream tags is present
static inline bool isStreamTagPresent(const std::vector<eformat::helper::StreamTag>& tags,
                                      const std::string& type, const std::string& name)
{
  for (const auto& tag : tags)
    if (tag.type == type && tag.name == name)
      return true;
  return false;
}


//! Return true if all criteria's stream tags are present
static inline bool allStreamTagPresent(const std::vector<eformat::helper::StreamTag>& tags,
                                       const std::string& type, const std::vector<std::string>& names)
{
  for (const auto& name : names)
    if (!isStreamTagPresent(tags, type, name))
      return false;
  return true;
}


// To make atLeastOneBitSet() and allBitsSet() undependent from the type of 
// emon::SmartBitValue::m_bit_positions that changed after release tdaq-05-05-00
typedef decltype(emon::SmartBitValue().m_bit_positions) BitPositions;

//! Return true if has at least one of the bits set
static inline bool atLeastOneBitSet(const std::uint32_t* base, const std::uint32_t nWords,
                                    const BitPositions& bit_positions)
{
  for (auto bit : bit_positions)
    if ((bit/32 < nWords) && ((base[bit/32]>>(bit%32))&0x1))
      return true;
  return false;
}


//! Return true if has all the bits set
static inline bool allBitsSet(const std::uint32_t* base, const std::uint32_t nWords,
                              const BitPositions& bit_positions)
{
  for (auto bit : bit_positions)
    if ((bit/32 >= nWords) || !((base[bit/32]>>(bit%32))&0x1))
      return false;
  return true;
}


// Return true if the event is selected based on the selection criteria
bool SamplerSingleStripping::Channel::is_selected(const Event& event)
{
  eformat::read::FullEventFragment fe(event.eventHeader);

  // Return false if not ignore and lvl1 trigger type value doesn't match
  if (!m_criteria.m_lvl1_trigger_type.m_ignore &&
      m_criteria.m_lvl1_trigger_type.m_value != fe.lvl1_trigger_type())
    return false;

  // Return false if not ignore and status_word doesn't match any in event's status
  if (!m_criteria.m_status_word.m_ignore &&
      !isStatusCriteriaValid(fe, m_criteria.m_status_word.m_value))
    return false;

  // Ignore streamTag tests if logic ignore or type string is empty
  if (m_criteria.m_stream_tags.m_logic != emon::logic::IGNORE &&
      !m_criteria.m_stream_tags.m_type.empty())
  {
    // Return false if the criteria's tag type is found in no event's stream tags
    if (m_criteria.m_stream_tags.m_names.empty())
    {
      if (!isStreamTagTypePresent(event.streamTags, m_criteria.m_stream_tags.m_type))
        return false;
    }
    // Return false if none of the criteria's streamTags is found in the event's streamTag list
    else if (m_criteria.m_stream_tags.m_logic == emon::logic::OR)
    {
      if (!atLeastOneStreamTagPresent(event.streamTags, m_criteria.m_stream_tags.m_type, m_criteria.m_stream_tags.m_names))
        return false;
    }
    // Return false if at least one criteria's streamTags is not found in the event's streamTag list
    else if (m_criteria.m_stream_tags.m_logic == emon::logic::AND)
    {
      if (!allStreamTagPresent(event.streamTags, m_criteria.m_stream_tags.m_type, m_criteria.m_stream_tags.m_names))
        return false;
    }
  }

  // Ignore if logic ignore or the bit_positions is empty
  if (m_criteria.m_lvl1_trigger_info.m_logic != emon::logic::IGNORE && !m_criteria.m_lvl1_trigger_info.m_bit_positions.empty())
  {
    // The L1 trigger info field is divided in three subfields of equal size
    std::uint32_t nWords = fe.nlvl1_trigger_info() / 3;
    std::uint32_t offset;
    switch (m_criteria.m_lvl1_trigger_info.m_origin)
    {
    default: // fall through
    case emon::origin::BEFORE_PRESCALE:
      offset = 0;
      break;
    case emon::origin::AFTER_PRESCALE:
      offset = nWords;
      break;
    case emon::origin::AFTER_VETO:
      offset = 2 * nWords;
      break;
    }
    // Return false if all bit at criteria's bit positions are set to 0 in the event
    if (m_criteria.m_lvl1_trigger_info.m_logic == emon::logic::OR)
    {
      if (!atLeastOneBitSet(fe.lvl1_trigger_info() + offset, nWords, m_criteria.m_lvl1_trigger_info.m_bit_positions))
        return false;
    }
    // Return false if at least one of the criteria's bit positions is set to 0 in the event
    else if (m_criteria.m_lvl1_trigger_info.m_logic == emon::logic::AND)
    {
      if (!allBitsSet(fe.lvl1_trigger_info() + offset, nWords, m_criteria.m_lvl1_trigger_info.m_bit_positions))
        return false;
    }
  }

  // Event is selected based on criteria
  // Return true if Channel is open and eventQueue is not full
  std::lock_guard<std::mutex> lock(m_channelMtx);
  return m_eventQueue && !m_eventQueue->is_full();
}


}
}
