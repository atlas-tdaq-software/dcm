#include "dcm/FatalHandler.h"
#include "dcm/Issues.h"
#include "dcm/Main.h"

#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

#include <TSystem.h> // for gSystem
#include <boost/program_options.hpp>
#include <cstdlib> // for EXIT_SUCCESS, EXIT_FAILURE
#include <exception>
#include <iostream>


int main(int argc, char** argv)
{
  // Initialize IPC
  IPCCore::init(argc, argv);

  // Disable the officious ROOT signal handlers
  gSystem->ResetSignals();

#if 0
#include "version.ixx"

  // Print build info
  std::cout << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl;
  std::cout << version;
#endif

  // Print command line
  std::cout << "Command line: ";
  for (int i = 0; i < argc; ++i)
    std::cout << argv[i] << " ";
  std::cout << std::endl;

  namespace po = boost::program_options;
  po::options_description desc("Data Collection Manager");
  try
  {
    // Parse DCM-specific command line arguments
    std::string dummy;
    desc.add_options()
    ("disable-fatal-handler", "Do not print stack trace and memory map when a fatal signal is caught");
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    po::notify(vm);

    // Parse RC command line arguments
    daq::rc::CmdLineParser cmdParser(argc, argv, true);

    // Set handler for fatal signals, which dump a stack trace and the memory
    // map before exiting
    if (!vm.count("disable-fatal-handler"))
      daq::dcm::FatalHandler::install();
    // Create the main controllable object
    auto main = std::make_shared<daq::dcm::Main>();

    // Create, initialize and start the ItemCtrl
    daq::rc::ItemCtrl itemCtrl(cmdParser, main);
    itemCtrl.init();
    itemCtrl.run();
  }
  catch (daq::rc::CmdLineHelp& ex)
  {
    std::cout << desc << std::endl << ex.message() << std::endl;
    return EXIT_SUCCESS;
  }
  catch (daq::rc::CmdLineError& ex)
  {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch (boost::program_options::error& ex)
  {
    ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
    return EXIT_FAILURE;
  }
  catch (std::exception& ex)
  {
    ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled exception in main thread", ex));
    return EXIT_FAILURE;
  }
  catch (...)
  {
    ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled unknown exception in main thread"));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
