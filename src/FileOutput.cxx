#include "dcm/FileOutput.h"

#include "dcm/Main.h"
#include "dcm/EventBuilder.h"
#include "dcm/dal/DcmFileOutput.h"

#include "eformat/DetectorMask.h"
#include "EventStorage/RawFileName.h"
#include "is/infodictionary.h"
#include "is/infoT.h"
#include "rc/RunParams.h"

#include <boost/make_shared.hpp>

namespace daq
{
namespace dcm
{

FileOutput::FileOutput(Main& main, const dal::DcmFileOutput& config) :
    m_main(main),
    m_config(config),
    m_acceptanceRemainder(0)
{
}

FileOutput::~FileOutput()
{
}

void FileOutput::initialize(EventBuilder* eventBuilder)
{
  m_eventBuilder = eventBuilder;
}

void FileOutput::asyncConnect()
{
  // Nothing to do in this implementation
  m_main.ioService().post(std::bind(&Main::onOutputConnect, &m_main));
}

void FileOutput::asyncStart()
{
  // Warn the user of likely misconfiguration
  bool iguiRecordingEnabled = bool(m_main.runParams().recording_enabled);
  bool confRecordingEnabled = (m_config.get_storageAcceptance() > 0.0);
  if (m_main.runParams().recording_enabled != confRecordingEnabled)
  {
    std::string iguiDesc = iguiRecordingEnabled ? "enabled" : "disabled";
    std::string confDesc = confRecordingEnabled ? "enabled" : "disabled";
    DCM_WARNING(dcm::OutputIssue, "Data recording is " << iguiDesc << " in the IGUI and " << confDesc
                << " in the configuration data base. It will be disabled.");
  }

  // Prepare run parameters record with data from IS
  using eformat::helper::DetectorMask;
  DetectorMask mask(m_main.runParams().det_mask);
  m_runParameters.run_number = m_main.runParams().run_number;
  m_runParameters.max_events = m_main.runParams().max_events;
  m_runParameters.rec_enable = m_main.runParams().recording_enabled;
  m_runParameters.trigger_type = m_main.runParams().trigger_type;
  m_runParameters.detector_mask_LS = mask.serialize().second;
  m_runParameters.detector_mask_MS = mask.serialize().first;
  m_runParameters.beam_type = m_main.runParams().beam_type;
  m_runParameters.beam_energy = m_main.runParams().beam_energy;

  // Get UserMetaData strings from IS
  try
  {
    ISInfoDictionary infoDict(m_main.partition());
    ISInfoT<std::vector<std::string>> info;
    infoDict.getValue("RunParams.UserMetaData", info);
    m_userMetaData = info.getValue();
  }
  catch (daq::is::Exception & ex)
  {
    ERS_LOG("RunParams.UserMetaData strings not found in IS.");
  }

  m_main.ioService().post(std::bind(&Main::onOutputStart, &m_main));
}

void FileOutput::asyncStop()
{
  // Close the data writers
  m_dataWriters.clear();

  // Reset UserMetaData strings
  m_userMetaData.clear();

  m_main.ioService().post(std::bind(&Main::onOutputStop, &m_main));
}

void FileOutput::asyncDisconnect()
{
  // Nothing to do in this implementation
  m_main.ioService().post(std::bind(&Main::onOutputDisconnect, &m_main));
}

// Writes the received events into output file
void FileOutput::asyncEventSend(std::unique_ptr<Event> event)
{
  m_main.isInfo()->OutputEvents++;

  if (m_main.runParams().recording_enabled)
  {
    // We only write a fraction of the total number of events, as specified by the
    // storageAcceptance parameter
    m_acceptanceRemainder += m_config.get_storageAcceptance();
    if (m_acceptanceRemainder >= 1.)
    {
      m_acceptanceRemainder -= 1.;

      bool success = true;
      for (auto& stream : event->streams)
      {
        for (auto& tag : stream.tags)
        {
          auto& writer = getWriter(tag.type, tag.name);
          auto err = writer.putData(stream.size, stream.data);
          if (err)
          {
            ers::error(dcm::FileOutputWriteIssue(ERS_HERE, tag.type, tag.name, event->globalId, event->l1Id));
            success = false;
          }
          else
          {
            m_main.isInfo()->OutputDeliveredData += stream.size;
          }
        }
      }
      if (success)
      {
        m_main.isInfo()->OutputDeliveredEvents++;
      }
    }
  }

  // Notify the EventBuilder that the event was processed
  auto eventBuilder = m_eventBuilder;
  auto event_ = event.release();
  m_main.ioService().post(
      [eventBuilder, event_]
      {
        eventBuilder->onEventSent(std::unique_ptr<Event>(event_));
      });
}

EventStorage::DataWriter& FileOutput::getWriter(const std::string& type, const std::string& name)
{
  auto fullTag = type + "_" + name;
  auto it = m_dataWriters.find(fullTag);
  if (it == m_dataWriters.end())
  {
    ERS_LOG("Opening file for stream " << fullTag);

    it = m_dataWriters.emplace(
        std::piecewise_construct,
        std::forward_as_tuple(fullTag),
        std::forward_as_tuple(
            m_config.get_directory(),
            boost::make_shared<daq::RawFileName>(
                m_main.runParams().T0_project_tag,
                m_runParameters.run_number,
                type,
                name,
                0, // Lumiblock
                m_main.applicationName()),
            m_runParameters,
            m_main.runParams().T0_project_tag,
            type,
            name,
            fullTag,
            0, // Lumiblock
            m_main.applicationName(),
            m_userMetaData
        )).first;

    if (!it->second.good()) {
      ers::error(dcm::FileOutputOpenIssue(ERS_HERE, fullTag, type, name));
    }
  }
  return it->second;
}

} // namespace dcm
} // namespace daq
