#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "dcm/SharedBlockArray/Readable.h"
#include "dcm/Issues.h"

#include <iostream>

namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{


// Open the SharedBlockArray file for read only access
Readable::Readable( const std::string& fileName ) :
  m_fd(0), m_minBlockByteOffset(0), m_maxBlockByteOffset(0)
{
  // Perform some basic sanity checks
  if( fileName.empty() )
    throw dcm::SBAInvalidArgument(ERS_HERE, "empty file name parameter");

  // Try open file for read only access
  int file_fd = ::open( fileName.c_str(), O_RDONLY );
  if( file_fd < 0 )
  {
    std::stringstream s;
    s << "Failed to open the SharedBlockArray file '"
      << fileName << "' : " << strerror( errno );
    throw dcm::SBARunTimeError(ERS_HERE, s.str());
  }

  // Read header
  std::uint32_t header[6];
  if( read( file_fd, &header, sizeof(header) ) != sizeof(header) )
  {
    std::stringstream s;
    s << "Failed reading header in SharedBlockArray file '"
      << fileName << "' : " << strerror( errno );
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }

  // Check header check word
  if( ::memcmp( header, "SBA\x01", 4 ) || header[1] < 6 )
  {
    std::stringstream s;
    s << "file header check word mismatch in '"
      << fileName << "'";
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }

  std::uint32_t blockByteSize = header[2];
  std::uint32_t numberOfBlocks = header[3];
  std::uint32_t pageByteSize = header[4];
  std::uint32_t headerAndIndexByteSize = header[5];

  // Perform some integrity checks
  if( pageByteSize != std::uint32_t( ::sysconf(_SC_PAGESIZE)) )
  {
    std::stringstream s;
    s << "Incompatible system page size in '"
      << fileName << "' : found " << pageByteSize << " while expecting " << ::sysconf( _SC_PAGESIZE );
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }
  if( headerAndIndexByteSize % pageByteSize )
  {
    std::stringstream s;
    s << "Offset to first block is not a multiple of a page byte size in '"
      << fileName << "' : offset is " << headerAndIndexByteSize << " while page size is " << pageByteSize;
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }
  if( blockByteSize % pageByteSize )
  {
    std::stringstream s;
    s << "Block byte size is not a multiple of a page byte size in '"
      << fileName << "' : block size is " << blockByteSize << " while page size is " << pageByteSize;
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }

  // Set member variable values
  m_fd = file_fd;
  m_blockByteSize = blockByteSize;
  m_minBlockByteOffset = headerAndIndexByteSize;
  m_maxBlockByteOffset = headerAndIndexByteSize + size_t(blockByteSize)*numberOfBlocks;
  m_fileName = fileName;
  m_nbrBlocks = numberOfBlocks;
}


// Close the mapped file, leaving recoverable data recoverable
Readable::~Readable() {
  ::close( m_fd );
}

// Returns a pointer on the non modifiable block with the specified block identifier
Readable::Block Readable::block(std::uint32_t blockId)
{
  size_t offset = m_minBlockByteOffset;
  offset += size_t(blockId) * m_blockByteSize;
  if (offset > m_maxBlockByteOffset)
  {
    std::stringstream s;
    s << "BlockId " << blockId << " out of range";
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }
  return Readable::Block::create( m_fd, offset, m_blockByteSize );
}

// Unmap the memory zone used by block and deletes the block
static void block_deleter( SharedBlockArray::Block* block )
{
  if (block)
  {
    ::munmap(const_cast<std::uint32_t*>(block->dataStart()), block->byteSize());
    delete block;
  }
}

// Readable block factory
Readable::Block Readable::Block::create( int fd, size_t blockByteOffset, size_t blockByteSize )
{
  ERS_ASSERT(blockByteSize < 0xEFFFFFFF);

  void * mmapPtr = ::mmap(NULL, blockByteSize, PROT_READ, MAP_SHARED, fd, blockByteOffset);
  if (mmapPtr == nullptr)
  {
    std::stringstream s;
    s << "Failed mapping the block at offset " <<
         blockByteOffset << " with size " << blockByteSize << " : " << strerror( errno );
    throw dcm::SBAInvalidArgument(ERS_HERE, s.str());
  }

  // Create shared pointer with specific deleter
  std::shared_ptr<SharedBlockArray::Block> block(
        new SharedBlockArray::Block(reinterpret_cast<std::uint32_t*>(mmapPtr),
                                    static_cast<std::uint32_t>(blockByteSize)),
        block_deleter);
  return Readable::Block(block);
}


// Return a vector of blockId and number of recoverable bytes pair
std::vector<Readable::RecoverableInfo> Readable::recoverInfos() const
{
  std::vector<RecoverableInfo> res;
  std::uint32_t* header = reinterpret_cast<std::uint32_t*>(::mmap(NULL, m_minBlockByteOffset,
                                                           PROT_READ, MAP_SHARED, m_fd, 0 ));
  const RecoverableWordsIndex recoverableWords(header);
  for (std::uint32_t blockId = 0, n = recoverableWords.numberOfBlocks(); blockId < n; ++blockId )
    res.push_back(RecoverableInfo{blockId, recoverableWords.get(blockId)});
  ::munmap(header, m_minBlockByteOffset);
  return res;
}


}
}
}

