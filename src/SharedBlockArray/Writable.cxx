#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "dcm/SharedBlockArray/Writable.h"


namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

/*
  The SharedBlockArray file format is the following.

  |<------ 32bit ------>|
  |---------------------|
  | CheckWord:"SBA\x01" |
  |  HeaderSize (words) |
  |  BlockSize (bytes)  |
  |    NumberOfBlock    |
  |  PageSize (bytes)   |
  | Block0Start (bytes) |
  |---------------------|
  | RecoverableInBlock0 |
  | RecoverableInBlock1 |
  | RecoverableInBlock2 |
  | RecoverableInBlock3 |
  =       . . .         =
  |---------------------|
  =    padding space    =
  |---------------------|
  |                     |
  |   Block 0 starting  |
  |   at page boundary  |
  |                     |
  |---------------------|
  |                     |
  |   Block 1 starting  |
  |   at page boundary  |
  |                     |
  |---------------------|
  =        . . .        =
  |_____________________|


  The first header word is a check word that contains the 4 byte
  string "SBA\x01" in that order. BlockSize codes the size of a
  block in bytes. Block0Start codes the offset in bytes to the
  first byte of the block 0 relative to the first byte of the file.

  The RecoverableInBlockX are the number of 32bit words of data
  flagged as recoverable in the block X. See Block.cxx for a
  description of the block data content.

  The padding space ensures that a block starts at a system
  memory page boundary.

  Note that the Read class requires that system page matches the
  one specified in SharedBlockArray file because it maps single
  blocks. This should normally be the case when used in the HTLPU
  with a SharedBlockArray constructed by the DCM running on the same
  host. The recovery class doesn't have this constrain because the
  whole file is mapped in memory as a single block of memory.

  When initialized all the memory mapped zone in protected as
  unreadable and unwritable. When a block is allocated, the full block
  memory space protection is removed and the memory space is writable
  and readable. The header and index zone is kept protected all the
  time because it is a critical information for recovery.
*/

// Creates a writable SharedBlockArray with its memory mapped file
Writable::Writable(const std::string& fileName, std::uint32_t numberOfBlocks, std::uint32_t blockMiBSize, bool MapAnonymous ) :
  m_header(nullptr), m_mmapByteSize(0)
{

  // Perform some basic sanity checks
  if (fileName.empty())
    throw dcm::SBAInvalidArgument(ERS_HERE, "empty file name parameter");
  if (blockMiBSize == 0)
    throw dcm::SBAInvalidArgument(ERS_HERE, "block size is 0 MiB");
  if (blockMiBSize == 0)
    throw dcm::SBAInvalidArgument(ERS_HERE, "number of blocks is 0");

  // get system memory page size
  const long tmp = ::sysconf(_SC_PAGESIZE);
  if (tmp < 2)
    throw dcm::SBAInvalidArgument(ERS_HERE, "Failed to get a valid system page size" );
  const std::uint32_t pageByteSize = static_cast<std::uint32_t>(tmp);

  // Set the number of words in the header
  const std::uint32_t headerWordSize = 6;

  // Block byte size
  const std::uint32_t blockByteSize = blockMiBSize*1024*1024;
  ERS_ASSERT((blockByteSize % pageByteSize) == 0);

  // Compute the header and index byte size
  std::uint32_t headerAndIndexByteSize = (headerWordSize + numberOfBlocks)*sizeof(uint32_t);

  // Make sure the header and index byte size is a multiple of the page size
  std::uint32_t delta = headerAndIndexByteSize % pageByteSize;
  if (delta != 0)
    headerAndIndexByteSize += pageByteSize - delta;

  // Compute the total file byte size to map
  size_t mmapByteSize = size_t(numberOfBlocks) * blockByteSize + headerAndIndexByteSize;

  // Map the file (no file is need if the map is MAP_ANONYMOUS) 
  std::uint32_t* header;
  if(MapAnonymous)
  {
    header = reinterpret_cast<std::uint32_t*>(
       ::mmap(NULL, mmapByteSize, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, 0, 0 ));
    ERS_LOG("mmap flags: MAP_SHARED | MAP_ANONYMOUS");
  }
  else
  {
    ERS_LOG("mmap flags: MAP_SHARED");
  
    // Try creating the file, fails if the file already exist
    int fd = ::open( fileName.c_str(), O_CREAT|O_RDWR|O_DIRECT|O_EXCL,
                   S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if (fd < 0)
    {
      std::stringstream s;
      s << "Failed to open or create the file '" << fileName << "' as writable : " << strerror( errno );
      throw dcm::SBARunTimeError(ERS_HERE, s.str());
    }

    // Set the file size
    if (::ftruncate( fd, mmapByteSize ) < 0)
    {
      std::stringstream s;
      s << "Failed setting the size of the file '" << fileName << "' to "
        << mmapByteSize << " bytes : " << strerror( errno );
      ::close( fd );
      throw dcm::SBARunTimeError(ERS_HERE, s.str());
    }
    header = reinterpret_cast<std::uint32_t*>(
       ::mmap(NULL, mmapByteSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0 ));
    ::close( fd );
  }
  if (!header)
  {
    std::stringstream s;
    s << "Failed mapping the file '" << fileName << "' with size "
      << mmapByteSize << " in memory : " << strerror( errno );
    throw dcm::SBARunTimeError(ERS_HERE, s.str());
  }

  // Clear the file header and index zone, and initialize the header
  ::memset( header, 0, headerAndIndexByteSize );
  ::memcpy( header, "SBA\x01", 4 );   // Set the check word
  header[1] = headerWordSize;         // Number of words in the header
  header[2] = blockByteSize;          // Byte size of a block
  header[3] = numberOfBlocks;         // Number of blocks in the array
  header[4] = pageByteSize;           // Byte size of a page
  header[5] = headerAndIndexByteSize; // Offset to first block

  // Set the memory mapped file zone as not readable and writable
  ::mprotect( header, mmapByteSize, PROT_NONE );
  // Set header and recoverable byte index as readable
  ::mprotect( header, headerAndIndexByteSize, PROT_READ );

  // Set the instance attributes value
  m_header = header;
  m_mmapByteSize = mmapByteSize;
  m_fileName = fileName;

  m_recoverableWords = std::unique_ptr<RecoverableWordsIndex>(new RecoverableWordsIndex(header));

  // Fill in the block vector and the free block index
  std::uint32_t* blockPtr = m_header + headerAndIndexByteSize/sizeof(std::uint32_t);
  const std::uint32_t blockWordSize = blockByteSize/sizeof(std::uint32_t);
  m_blocks.reserve(numberOfBlocks);
  for( std::uint32_t blockId = 0; blockId < numberOfBlocks; ++blockId, blockPtr += blockWordSize )
  {
    m_blocks.emplace_back(blockPtr, blockByteSize, blockId, m_recoverableWords.get());
    m_freeBlocksIndex.push(blockId);
  }
}


// Destructor closes the memory mapped file
Writable::~Writable()
{
  if (m_header)
    ::munmap(m_header, m_mmapByteSize);
}


// Return a newly allocated block or throws SBARunTimeError failed to allocate a block
Block& Writable::allocate()
{
  if (m_freeBlocksIndex.empty())
    throw SBARunTimeError(ERS_HERE, "No more free blocks to allocate");
  Block& block = m_blocks[m_freeBlocksIndex.top()];
  m_freeBlocksIndex.pop();
  block.init();
  return block;
}


// Deallocate the specified block
void Writable::deallocate(Block &block )
{
  if (block.deallocated())
    throw SBAInvalidArgument(ERS_HERE, "Request to deallocate a free block");
  block.clear();
  m_freeBlocksIndex.push(block.blockId());
}

// Level of occupancy of the most used block
float Writable::maxBlockOccupancy() const
{
  if (m_blocks.size() == 0) {
    return 0.;
  }
  std::uint32_t byteSize     = m_blocks[0].byteSize();
  std::uint32_t minFreeSpace = byteSize;
  for (auto const &b : m_blocks)
  {
    std::uint32_t freeSpace = b.freeSpace();
    if (minFreeSpace > freeSpace) {
      minFreeSpace = freeSpace;
    }
  }
  return (byteSize - minFreeSpace) / static_cast<float>(byteSize);
}



}
}
}
