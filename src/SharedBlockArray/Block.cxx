#include <cstring>

#include "eformat/HeaderMarker.h"
#include "eformat/ROBFragment.h"
#include "dcm/SharedBlockArray/Block.h"
#include "dcm/Issues.h"


namespace daq
{
namespace dcm
{
namespace SharedBlockArray
{

// Functions to be executed at compile time

// convert byteSize into wordSize
constexpr std::uint32_t toWordSize (std::uint32_t byteSize)
{
  return byteSize/sizeof(std::uint32_t);
}

// convert wordSize into byteSize
constexpr std::uint32_t toByteSize (std::uint32_t wordSize)
{
  return wordSize*sizeof(std::uint32_t);
}

// convert wordSize into byteSize
constexpr bool isInWordUnits (std::uint32_t byteSize)
{
  return (byteSize%sizeof(std::uint32_t)) == 0;
}


// Construct Block
Block::Block( std::uint32_t* start,
       std::uint32_t  blockByteSize,
       std::uint32_t  blockId,
       RecoverableWordsIndex* recoverableWordsIndex ) :
  m_dataStart(start), m_byteSize(blockByteSize), m_blockId(blockId),
  m_recoverableWords(recoverableWordsIndex), m_state(Block::CLEARED)
{
  ERS_ASSERT(blockByteSize > 0);
  long tmp = ::sysconf(_SC_PAGESIZE);

  if (tmp < 2 )
    throw dcm::SBAInvalidArgument(ERS_HERE, "Failed to get a valid system page size" );
  m_pageSize = static_cast<std::uint32_t>(tmp);
  if (m_byteSize < m_pageSize || (m_byteSize%m_pageSize) != 0)
    throw dcm::SBAInvalidArgument(ERS_HERE, "Block byte size must be a multiple of the system page size");
  m_dataEnd = m_dataStart;
  m_freeSpace = m_byteSize - m_pageSize;
  ::mprotect(m_dataStart, m_byteSize, PROT_READ|PROT_WRITE );
  if (m_recoverableWords)
    m_recoverableWords->set(blockId, 0);
}


// Clear the block after use
void Block::clear()
{
  ERS_ASSERT( m_state == Block::IDLE );
  m_dataEnd = m_dataStart;
  m_freeSpace = m_byteSize - m_pageSize;
  ::mprotect(m_dataStart, m_byteSize, PROT_READ );
  if (m_recoverableWords)
    m_recoverableWords->set(m_blockId, 0);
  m_robInfoIndex.clear();
  m_state = Block::CLEARED;
}


// Initialize the block to be used
void Block::init()
{
  ERS_ASSERT( m_state == Block::CLEARED );
  // Leave last memory page protected as security fence
  ::mprotect(m_dataStart, m_freeSpace, PROT_READ|PROT_WRITE );
  m_state = Block::IDLE;
}

/*
// Append the raw data bytes to the block leaving RobInfoIndex unmodified
std::uint8_t* Block::appendRawData(const void* data, std::uint32_t byteSize)
{
  ERS_ASSERT( m_state == Block::IDLE );
  ERS_ASSERT( data != nullptr && byteSize > 0 );
  // Ensure 64bit alignement in block assuming dataEnd is already 32bit aligned and dataStart is 64bit aligned
  if (reinterpret_cast<std::uint64_t>(m_dataEnd) & (sizeof(std::uint64_t)-1))
  {
    *m_dataEnd++ = Block::NULL_MARKER;
    m_freeSpace -= sizeof(std::uint32_t);
  }
  ERS_ASSERT(!(reinterpret_cast<std::uint64_t>(m_dataEnd) & (sizeof(std::uint64_t)-1)));
  // Compute data block size including marker and size and padding to 32bit words
  const std::uint32_t paddedDataBlockWordSize = toWordSize(byteSize+3*sizeof(std::uint32_t)-1);
  const std::uint32_t paddedDataBlockByteSize = toByteSize(paddedDataBlockWordSize);
  if (paddedDataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append raw data" );
  std::uint32_t* ptr = m_dataEnd;
  *ptr++ = Block::RAW_DATA_MARKER;
  *ptr++ = paddedDataBlockWordSize;
  ::memcpy( ptr, data, byteSize );
  m_dataEnd += paddedDataBlockWordSize;
  m_freeSpace -= paddedDataBlockByteSize;
  return reinterpret_cast<std::uint8_t*>(ptr);
}
*/


// Return a pointer in the block storage where raw data may be written in place
std::uint8_t* Block::startAppendRawData(std::uint32_t maxByteSize, std::uint32_t marker)
{
  ERS_ASSERT( m_state == Block::IDLE );
  ERS_ASSERT( maxByteSize > 0 );
  ERS_ASSERT( marker == Block::RAW_DATA_MARKER ||
              marker == Block::ROB_INFO_MARKER );
  // Ensure 64bit alignement in block assuming dataEnd is already 32bit aligned and dataStart is 64bit aligned
  if (reinterpret_cast<std::uint64_t>(m_dataEnd) & (sizeof(std::uint64_t)-1))
  {
    *m_dataEnd++ = Block::NULL_MARKER;
    m_freeSpace -= sizeof(std::uint32_t);
  }
  ERS_ASSERT(!(reinterpret_cast<std::uint64_t>(m_dataEnd) & (sizeof(std::uint64_t)-1)));
  // Compute data block size including marker and size and padding to 32bit words
  const std::uint32_t paddedDataBlockWordSize = toWordSize(maxByteSize+3*sizeof(std::uint32_t)-1);
  const std::uint32_t paddedDataBlockByteSize = toByteSize(paddedDataBlockWordSize);
  if (paddedDataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append raw data" );
  m_dataEnd[0] = marker;
  m_dataEnd[1] = 0;
  m_state = Block::APPENDING_RAW_DATA;
  return reinterpret_cast<std::uint8_t*>(m_dataEnd+2);
}


// completes appending rob data in place leaving RobInfoIndex unmodified
void Block::endAppendRawData( std::uint32_t actualByteSize )
{
  ERS_ASSERT( m_state == Block::APPENDING_RAW_DATA );
  ERS_ASSERT( actualByteSize > 0 );
  // Compute data block size including marker and size and padding
  const std::uint32_t paddedDataBlockWordSize = toWordSize(actualByteSize+3*sizeof(std::uint32_t)-1);
  const std::uint32_t paddedDataBlockByteSize = toByteSize(paddedDataBlockWordSize);
  if (paddedDataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append raw data" );
  m_dataEnd[1] = paddedDataBlockWordSize;
  m_dataEnd += paddedDataBlockWordSize;
  m_freeSpace -= paddedDataBlockByteSize;
  m_state = Block::IDLE;
}


// Append one or more ROBs data bytes to the block
std::vector<RobInfo*> Block::appendRobData( const std::uint32_t* data, std::uint32_t byteSize )
{
  ERS_ASSERT( m_state == Block::IDLE );
  ERS_ASSERT( data != nullptr && byteSize > 0 );
  ERS_ASSERT_MSG( isInWordUnits(byteSize), "byteSize is not a multiple of 32bit words" );
  std::uint32_t dataBlockByteSize = byteSize + 2*sizeof(std::uint32_t);
  if (dataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append ROB data" );
  std::uint32_t* ptr = m_dataEnd;
  *ptr++ = Block::ROB_DATA_MARKER;
  *ptr++ = toWordSize(dataBlockByteSize);
  ::memcpy( ptr, data, byteSize );
  return parseAndInsertROBs();
}


// Return a pointer in the block storage where ROB data may be written in place
std::uint32_t* Block::startAppendRobData( std::uint32_t maxByteSize )
{
  ERS_ASSERT( m_state == Block::IDLE );
  ERS_ASSERT( maxByteSize > 0 );
  std::uint32_t dataBlockByteSize = maxByteSize + 2*sizeof(std::uint32_t);
  if (dataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append ROB data" );
  m_dataEnd[0] = Block::ROB_DATA_MARKER;
  m_dataEnd[1] = 0;
  m_state = Block::APPENDING_ROB_DATA;
  return m_dataEnd+2;
}


// completes appending rob data in place and updates the RobInfoIndex
std::vector<RobInfo*> Block::endAppendRobData( std::uint32_t actualByteSize )
{
  ERS_ASSERT( m_state == Block::APPENDING_ROB_DATA );
  if (actualByteSize ==  0) {
    m_state = Block::IDLE;
    ERS_LOG("endAppendRobData(O) called on block " << static_cast<void*>(m_dataStart)
            << " containing " << toByteSize(m_dataEnd - m_dataStart) << " bytes");
    return std::vector<RobInfo*>();
  }
  ERS_ASSERT_MSG( isInWordUnits(actualByteSize), "actualByteSize is not a multiple of 32bit words" );
  std::uint32_t dataBlockByteSize = actualByteSize + 2*sizeof(std::uint32_t);
  if (dataBlockByteSize > m_freeSpace)
    throw dcm::SBAInvalidArgument(ERS_HERE, "not enough free space in block to append ROB data" );
  m_dataEnd[1] = toWordSize(dataBlockByteSize);
  m_state = Block::IDLE;
  return parseAndInsertROBs();
}


// Parse ROBs found in the data block at dataEnd, append the data block and
// a RobInfo for each found ROB and insert it in the robInfoIndex
std::vector<RobInfo*> Block::parseAndInsertROBs()
{
  // First pass to check ROB validity and include it in the block data
  std::uint32_t *robDataPtr = m_dataEnd+2, robDataWordSize = m_dataEnd[1]-2;
  std::uint32_t *robPtr = robDataPtr, wordSize = robDataWordSize;
  while (wordSize && robPtr[0] == eformat::HeaderMarker::ROB && robPtr[1] <= wordSize )
  {
    wordSize -= robPtr[1];
    robPtr += robPtr[1];
  }
  if (wordSize)
    throw dcm::SBARunTimeError(ERS_HERE, "Received invalid or incomplete ROB to parse" );
  m_freeSpace -= toByteSize(m_dataEnd[1]);
  m_dataEnd = robPtr;
  // Create a RobInfo record for each ROB and insert it in the robInfoIndex and in the result vector
  std::vector<RobInfo*> res;
  for (std::uint32_t *robPtr = robDataPtr, wordSize = robDataWordSize;
       wordSize > 0; wordSize -= robPtr[1], robPtr += robPtr[1])
  {
    eformat::read::ROBFragment robFragment(robPtr);
    std::uint32_t robId = eformat::helper::SourceIdentifier(robFragment.source_id()).simple_code();
    std::uint8_t* dataPtr = startAppendRobInfo(sizeof(RobInfo));
    RobInfo* robInfoPtr = new(dataPtr) RobInfo(robId, robPtr);
    endAppendRawData(sizeof(RobInfo));
    m_robInfoIndex[robId] = robInfoPtr;
    res.push_back(robInfoPtr);
  }
  return res;
}


// locate RobInfo structure in the extended data block size if any
void Block::appendAndInsertRobInfos(std::uint32_t newDataWordSize )
{
  ERS_ASSERT(toByteSize(newDataWordSize) <= m_byteSize);

  // Do nothing if there is no new data appended to the block
  if (newDataWordSize <= dataWordSize())
    return;

  RobInfo* robInfoPtr;
  std::uint32_t wordSize = newDataWordSize - dataWordSize();
  std::uint32_t* ptr = m_dataEnd, byteSize = toByteSize(wordSize);
  while (wordSize)
  {
    switch (ptr[0])
    {
    case Block::NULL_MARKER:
      ++ptr;
      --wordSize;
      break;

    case Block::ROB_INFO_MARKER:
      if (ptr[1] > wordSize)
        throw dcm::SBARunTimeError(ERS_HERE, "Invalid SBA data block size" );
      robInfoPtr = reinterpret_cast<RobInfo*>(ptr+2);
      m_robInfoIndex[robInfoPtr->robId] = robInfoPtr;
      wordSize -= ptr[1];
      ptr += ptr[1];
      break;

    case Block::RAW_DATA_MARKER:
    case Block::ROB_DATA_MARKER:
      if (ptr[1] > wordSize)
        throw dcm::SBARunTimeError(ERS_HERE, "Invalid SBA data block size" );
      wordSize -= ptr[1];
      ptr += ptr[1];
      break;

    default:
      throw dcm::SBARunTimeError(ERS_HERE, "Unknow SBA data block marker" );
    }
  }
  // Append dataWordSize to the block
  m_dataEnd = ptr;
  m_freeSpace -= byteSize;
}

// Fill the vector of RobFragments found between the start and end data pointer in the block
std::vector<eformat::read::ROBFragment>& Block::getRobFragments(
    const std::uint32_t *begin, const std::uint32_t *end,
    std::vector<eformat::read::ROBFragment>& robs ) const
{
  ERS_ASSERT(begin == nullptr||begin == m_dataEnd||*begin == NULL_MARKER||
             *begin == RAW_DATA_MARKER||*begin == ROB_INFO_MARKER||*begin == ROB_DATA_MARKER);
  ERS_ASSERT(end == nullptr||end == m_dataEnd||*end == NULL_MARKER||
             *end == RAW_DATA_MARKER||*end == ROB_INFO_MARKER||*end == ROB_DATA_MARKER);
  ERS_ASSERT(begin != m_dataEnd||end == m_dataEnd);
  ERS_ASSERT(begin != nullptr||end == nullptr);

  robs.clear();
  if (begin != nullptr && begin != m_dataEnd)
  {
    // Scan all data blocks
    while (begin < end)
    {
      ERS_ASSERT(*begin==NULL_MARKER||*begin==RAW_DATA_MARKER||*begin==ROB_INFO_MARKER||*begin==ROB_DATA_MARKER);
      // If the data block contains a ROB fragment sequence
      if (*begin == ROB_DATA_MARKER)
      {
        const std::uint32_t* robBegin = begin+2, *robEnd = begin+begin[1];
        // scan all ROB fragments
        while (robBegin < robEnd)
        {
          ERS_ASSERT(robBegin[0] == eformat::HeaderMarker::ROB && robBegin[1] > 0);

          // append ROB fragment to robs
          robs.emplace_back(robBegin);

          // jump to next Rob fragment
          robBegin += robBegin[1];
        }
      }

      // jump to next data block
      begin += begin[1];
    }
  }
  return robs;
}

}
}
}
