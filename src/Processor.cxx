
#include "dcm/Processor.h"

#include "dcm/DataCollector.h"
#include "dcm/Event.h"
#include "dcm/EventBuilder.h"
#include "dcm/Issues.h"
#include "dcm/L1Source.h"
#include "dcm/Main.h"
#include "dcm/Utilities.h"
#include "dcm/dal/DcmProcessor.h"

#include "ers/ers.h"
#include "monsvc/MonitoringService.h"
#include "oh/OHRootMutex.h"
#include "eformat/StreamTag.h"
#include "CTPfragment/CTPfragment.h"
#include "dal/Detector.h"

#include <boost/algorithm/string.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/system/error_code.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <cstdlib> // for std::rand()
#include <functional>
#include <locale> // for std::isalnum()

namespace daq
{
namespace dcm
{

Processor::Processor(Main& main,
                           std::uint32_t maxHLTPUs,
                           std::unique_ptr<Processor::Histograms> histograms,
                           const dal::DcmProcessor &config) :
    m_main(main),
    m_config(config),
    m_l1Source(nullptr),
    m_dataCollector(nullptr),
    m_eventBuilder(nullptr),
    m_state(State::UNCONNECTED),
    m_nEventsInDCM(0),
    m_nOutputEvents(0),
    m_maxOutputEvents(config.get_maxOutputEvents()),
    m_maxProcessingTime_ms(config.get_processingTimeOut_ms()),
    m_maxIdleTime_ms(50000),
    m_maxHLTPUs(maxHLTPUs),
    m_histograms(std::move(histograms)),
    m_timer(main.ioService()),
    m_markDoneAfterEB(false)
{
  ERS_ASSERT_MSG(m_maxHLTPUs+m_maxOutputEvents <= m_main.sba().numberOfBlocks(),
                 "Invalid configuration parameter values");

  for (const auto subdetector : config.get_bcidCheckIgnoreSubdetectors())
    m_bcidCheckIgnoreSubdetectors.insert(subdetector->get_LogicalId());
  for (const auto subdetector : config.get_bcidCheckSilentSubdetectors())
    m_bcidCheckSilentSubdetectors.insert(subdetector->get_LogicalId());
  m_timer.expires_from_now(std::chrono::seconds(1));
  m_timer.async_wait(std::bind(&Processor::timer, this, std::placeholders::_1));
  m_markDoneAfterEB = config.get_markDoneAfterEventBuilding();
}

Processor::~Processor() {
  m_histograms->finalize();
}


void Processor::initialize(L1Source* l1Source, DataCollector* dataCollector, EventBuilder* eventBuilder)
{
  ERS_LOG("Initialize processor");
  m_l1Source = l1Source;
  m_dataCollector = dataCollector;
  m_eventBuilder = eventBuilder;
  m_robIds = dataCollector->robIds();
  std::sort(m_robIds.begin(), m_robIds.end());
  m_histograms->initialize();
}


static void setBcidCheckSubdetectors(const std::string& key,
                                            std::set<std::uint8_t>& subdetectors,
                                            const std::vector<const daq::core::Detector*>& config,
                                            const std::string& value)
{
  try
  {
    subdetectors.clear();
    if (value.empty())
    {
      for (const auto subdetector : config)
      {
        subdetectors.insert(subdetector->get_LogicalId());
      }
    }
    else if (value != "NULL")
    {
      std::vector<std::string> values;
      std::vector<eformat::SubDetector> ids;
      boost::split(values, value, boost::is_any_of(","));
      for (const auto& v : values)
      {
        // Translate subdetector name string into the numeric ID
        std::uint8_t id = eformat::helper::SubDetectorDictionary[v];
        // If the translation failed, maybe the user specified a numeric ID
        if (id == eformat::helper::SubDetectorDictionary.unknown())
        {
          id = boost::numeric_cast<std::uint8_t>(boost::lexical_cast<int>(v));
        }
        subdetectors.insert(id);
      }
    }
  }
  catch (std::bad_cast& ex)
  {
    ers::warning(dcm::ConfigInvalidValue(ERS_HERE, key, value, ex));
  }
}

bool Processor::setDynamicParameter(const std::string& key, const std::string& value)
{
  if (key == "bcidCheckIgnoreSubdetectors")
  {
    setBcidCheckSubdetectors(key,
                             m_bcidCheckIgnoreSubdetectors,
                             m_config.get_bcidCheckIgnoreSubdetectors(),
                             value);
    return true;
  }
  if (key == "bcidCheckSilentSubdetectors")
  {
    setBcidCheckSubdetectors(key,
                             m_bcidCheckSilentSubdetectors,
                             m_config.get_bcidCheckSilentSubdetectors(),
                             value);
    return true;
  }
  return false;
}


void Processor::setDynamicParameters(const std::map<std::string, std::string>& parameters)
{
  for (const auto& kv : parameters)
  {
    try
    {
      if (setDynamicParameter(kv.first, kv.second))
        ERS_INFO("Dynamic parameter " << kv.first << " changed to: " << kv.second << ".");

      else
        ers::warning(dcm::ConfigUnknownParameter(ERS_HERE, kv.first));
    }
    catch (boost::bad_lexical_cast& ex)
    {
      ers::warning(dcm::ConfigInvalidValue(ERS_HERE, kv.first, kv.second, ex));
    }
    catch (DC::DistributionIssue& ex)
    {
      ers::warning(dcm::ConfigInvalidValue(ERS_HERE, kv.first, kv.second, ex));
    }
  }
}


// Try register a new hltpu. Returns false if attachment is rejected
bool Processor::attachHLTPU(std::unique_ptr<HLTPU>& hltpu)
{
  // Assert that HLTPU is not already attached
  for (auto it = m_hltpus.begin(); it != m_hltpus.end(); ++it) {
      ERS_ASSERT_MSG(hltpu != *it, "Duplicate HLTPU attachment");
  }

  // If HLTPU has no name, construct one
  if (hltpu->name.empty())
  {
    std::stringstream str;
    str << "HLTPU_" << m_hltpus.size();
    hltpu->name = str.str();
  }

  if (m_state != State::RUNNING && m_state != State::CONNECTED)
  {
    ers::warning(dcm::RejectHLTPUConnection(ERS_HERE, hltpu->name));
    return false;
  }

  if( m_hltpus.size() >= m_maxHLTPUs )
  {
    ers::warning(dcm::MaxHLTPUConnections(ERS_HERE, m_maxHLTPUs, hltpu->name));
    return false;
  }

  // Attach the hltpu
  auto h = hltpu.get();
  m_hltpus.push_back(std::move(hltpu));
  ERS_LOG("Attach " << h->name << " (now " << m_hltpus.size() << " HLTPUs)");
  m_main.isInfo()->ProxFreePUs++;
  setStateIdle(h);
  return true;
}


// Detach the given hltpu from the vector of active session objects
void Processor::detachHLTPU(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);

  auto it = m_hltpus.begin();
  while (it != m_hltpus.end())
    if (it->get() == hltpu)
      break;
    else
      ++it;
  bool isExisting = it != m_hltpus.end();
  ERS_ASSERT_MSG(isExisting, "Inexitant HLTPU detachment");

  ERS_LOG("Detach " << hltpu->name << " (" << m_hltpus.size()-1 << " HLTPUs left)");

  if ((*it)->state == HLTPU::State::PROCESSING)
    m_main.isInfo()->ProxBusyPUs--;
  else
    m_main.isInfo()->ProxFreePUs--;

  it->swap(m_hltpus.back());
  m_hltpus.pop_back();
  tryFinalizeReset();
}


// --------------------------------- Transitions ---------------------------------


void Processor::asyncConnect()
{
  ERS_ASSERT(m_state == State::UNCONNECTED);
  m_state = State::CONNECTED;
  m_main.ioService().post(std::bind(&Main::onProcessorConnect, &m_main));
}


void Processor::asyncStart()
{
  ERS_ASSERT(m_state == State::CONNECTED);
  m_state = State::RUNNING;
  m_histograms->clear();
  m_lastL1RAssignmentTime = std::chrono::steady_clock::now();
  unsuspendAllHtpu();
  m_main.ioService().post(std::bind(&Main::onProcessorStart, &m_main));
}


void Processor::asyncStop()
{
  ERS_DEBUG(3,"Strting asyncStop function.");
  ERS_ASSERT(m_state == State::RUNNING);
  m_state = State::STOPPING;
  unsuspendAllHtpu();
  tryFinalizeStop();
  ERS_DEBUG(3, "asyncStop completed.");
}


void Processor::tryFinalizeStop()
{
  if (m_state == State::STOPPING && m_nEventsInDCM == 0)
  {
    m_state = State::STOPPED;
    m_main.ioService().post(std::bind(&Main::onProcessorStop, &m_main));
  }
}


void Processor::asyncReset()
{
  ERS_ASSERT(m_state == State::STOPPED);
  m_state = State::RESETTING;
  tryFinalizeReset();
}


void Processor::tryFinalizeReset()
{
  if (m_state == State::RESETTING && m_hltpus.empty())
  {
    m_state = State::CONNECTED;
    m_main.ioService().post(std::bind(&Main::onProcessorReset, &m_main));
  }
}


void Processor::asyncDisconnect()
{
  ERS_ASSERT(m_state == State::CONNECTED);
  m_state = State::UNCONNECTED;
  m_main.ioService().post(std::bind(&Main::onProcessorDisconnect, &m_main));
}


// --------------------------------- Data collection  ---------------------------------


void Processor::tryFetchL1R(HLTPU* hltpu)
{
  ERS_DEBUG(3, "Starting tryFetchL1R for PU " << hltpu->name );
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT(hltpu->state == HLTPU::State::IDLE);

  switch(m_state)
  {
  case State::CONNECTED:
    hltpu->state = HLTPU::State::SUSPENDED;
    break;

  case State::RUNNING:
    // if backpressure active
    if (m_nOutputEvents >= m_maxOutputEvents)
    {
      ERS_DEBUG(3, "TryFetch will not be called for " << hltpu->name << " because of backpressure.\n Going to suspend the pu." );
      hltpu->state = HLTPU::State::SUSPENDED;
    }
    else
      fetchL1R(hltpu);
    break;

  case State::STOPPING:
  case State::STOPPED:
    noMoreEvents(hltpu);
    break;

  case State::UNCONNECTED:
  case State::RESETTING:
    // do nothing
    break;
  }
}


void Processor::unsuspendAllHtpu() {
  for (auto &hltpu : m_hltpus) {
    ERS_DEBUG(3,"Unsuspend HLTPU " << hltpu->name << " is in state " << hltpu->stateStr());
    if (hltpu->state == HLTPU::State::SUSPENDED) {
      hltpu->state = HLTPU::State::IDLE;
      tryFetchL1R(hltpu.get());
    }
    else if (hltpu->state == HLTPU::State::FETCHING_L1R && m_state == State::STOPPING) {
      ERS_DEBUG(3,"Found a FETCHING_L1R HLTPU: going to call onFetchL1RStop");
      onFetchL1RStop(hltpu.get());
    }
  }
}


void Processor::fetchL1R(HLTPU* hltpu)
{
  ERS_DEBUG(3, "Starting fetchL1R for PU " << hltpu->name );
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT(hltpu->state == HLTPU::State::IDLE);

  hltpu->state = HLTPU::State::FETCHING_L1R;
  auto event = make_unique<Event>(m_main.sba());
  event->lastDataEnd = event->block.dataEnd();
  event->lifeStartTime = std::chrono::steady_clock::now();
  m_main.isInfo()->ProxReqEvents++;
  m_l1Source->asyncFetchL1R(std::move(event), hltpu);
  ERS_DEBUG(3, "fetchL1R for PU " << hltpu->name << " finished.");
}


void Processor::onFetchL1R(std::unique_ptr<Event> event, void* context)
{
  ERS_DEBUG(3, "Fetched new L1 result (GID: " << event->globalId << ").");

  auto hltpu = static_cast<HLTPU*>(context);
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT(hltpu->state == HLTPU::State::FETCHING_L1R);
  ERS_ASSERT(event.get() != nullptr);

  // initialize Hltpu state for a new event processing
  hltpu->state = HLTPU::State::PROCESSING;
  hltpu->deadline = std::chrono::steady_clock::now() + m_maxProcessingTime_ms;
  hltpu->event.swap(event);
  hltpu->fetchedAllRobs = hltpu->fetchingROBs = false;
  readCTPFragment(hltpu->event.get());

  // Update stats and set some time points for delay computation
  m_main.isInfo()->ProxFreePUs--;
  m_main.isInfo()->ProxBusyPUs++;
  m_main.isInfo()->ProxL1Events++;
  m_nEventsInDCM++;

  auto now = std::chrono::steady_clock::now();
  auto interAssignmentInterval = std::chrono::duration_cast<std::chrono::microseconds>(
      now - m_lastL1RAssignmentTime);
  m_histograms->fillInterAssignmentTime_us(interAssignmentInterval.count());
  m_lastL1RAssignmentTime = now;
  auto l1SourceWaitInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      now - hltpu->event->lifeStartTime);
  m_histograms->fillWaitTime_ms(l1SourceWaitInterval.count());
  hltpu->event->processingStartTime = now;

  // if the event is reassigned, force accept it
  if (hltpu->event->forceAccept == Event::ForceAccept::HLTSV_REASSIGN)
  {
    hltpu->event->forceAccept = Event::ForceAccept::NONE;
    forceAccept(hltpu, Event::ForceAccept::HLTSV_REASSIGN);
    return;
  }

  // check event data validity
  checkBcIdAndL1IdMismatch(hltpu);
  if(hltpu->event->hasL1IdMismatch)
  {
    forceAccept(hltpu, Event::ForceAccept::L1ID_MISMATCH);
    return;
  }

  // call specific DummyProcessor and HltpuProcessor L1R handling method
  processL1R(hltpu);
}

void Processor::onFetchL1RStop(HLTPU* hltpu)
{
  ERS_DEBUG(3,"Entered onFetchL1RStop for PU " << hltpu->name << " in state " << hltpu->stateStr());
  std::unique_ptr<Event> event=std::move(hltpu->event);
  event.reset();
  noMoreEvents(hltpu);
  return;
}

void Processor::onFetchL1RError(const ers::Issue& issue, std::unique_ptr<Event> event, void* context)
{
  auto hltpu = static_cast<HLTPU*>(context);
  ERS_ASSERT(hltpu != nullptr);
  event.reset();

  // If L1RSource::asyncFetchL1R has been canceled because L1RSource is stopping
  if (dynamic_cast<const dcm::L1RRequestCanceled*>(&issue) /*&& m_state == State::STOPPED*/)
  {
    noMoreEvents(hltpu);
    return;
  }
  ers::warning(dcm::FetchLvl1Error(ERS_HERE, issue));
  tryFetchL1R(hltpu);
}


void Processor::fetchROBs(HLTPU* hltpu, const std::vector<std::uint32_t>& robIds)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING ||
             hltpu->state == HLTPU::State::ACCEPTING ||
             hltpu->state == HLTPU::State::FORCE_ACCEPTING,
             "unexpected HLTPU state " << hltpu->stateStr());
  ERS_ASSERT(hltpu->fetchingROBs == false);

  hltpu->fetchingROBs = true;
  hltpu->event->lastDataEnd = hltpu->event->block.dataEnd();
  hltpu->event->lastDataCollectionStartTime = std::chrono::steady_clock::now();
  m_dataCollector->asyncFetchRobs(hltpu->event.get(), robIds, hltpu);
}


void Processor::fetchAllROBs(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING ||
             hltpu->state == HLTPU::State::ACCEPTING ||
             hltpu->state == HLTPU::State::FORCE_ACCEPTING,
             "unexpected HLTPU state " << hltpu->stateStr());
  ERS_ASSERT(hltpu->fetchedAllRobs == false);

  hltpu->fetchedAllRobs = true;
  fetchROBs(hltpu, m_robIds);
}


void Processor::mayGetROBs(HLTPU* hltpu, const std::vector<std::uint32_t> &robIds)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  m_dataCollector->mayGetRobs(hltpu->event.get(), robIds);
}


void Processor::onFetchRobs(Event* event, void* context)
{
  auto hltpu = static_cast<HLTPU*>(context);
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING ||
                 hltpu->state == HLTPU::State::ACCEPTING ||
                 hltpu->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  ERS_ASSERT(event && event == hltpu->event.get());

  hltpu->fetchingROBs = false;

  event->dataCollectionDuration += std::chrono::steady_clock::now() - event->lastDataCollectionStartTime;

  if (hltpu->state == HLTPU::State::FORCE_ACCEPTING) {
    ERS_ASSERT(event->forceAccept != Event::ForceAccept::NONE);
    ERS_ASSERT(hltpu->fetchedAllRobs);
    event->block.setRecoverable();
    markDoneAfterEB(event);
    finalizeForceAccept(hltpu);
    return;
  }

  if (hltpu->state == HLTPU::State::ACCEPTING)
  {
    event->block.setRecoverable();
    markDoneAfterEB(event);
    checkBcIdAndL1IdMismatch(hltpu);
    finalizeAccept(hltpu);
    setStateIdle(hltpu);
    return;
  }

  ERS_ASSERT(hltpu->state == HLTPU::State::PROCESSING);
  if (hltpu->fetchedAllRobs)
  {
    event->block.setRecoverable();
    markDoneAfterEB(event);
  }
  checkBcIdAndL1IdMismatch(hltpu);
  processROBs(hltpu);
}


void Processor::onFetchRobsError(const ers::Issue& issue, Event* event, void* context)
{
  auto hltpu = static_cast<HLTPU*>(context);
  ERS_ASSERT(hltpu != nullptr);

  if (hltpu->state == HLTPU::State::FORCE_ACCEPTING) {
    // ignore error and proceed with normal force accept handling
    onFetchRobs(event, context);
    return;
  }

  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING ||
                 hltpu->state == HLTPU::State::ACCEPTING ||
                 hltpu->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  ERS_ASSERT(event && event == hltpu->event.get());

  ers::warning(dcm::FetchROBsHLTPUError(ERS_HERE, hltpu->name, event->globalId, event->l1Id, issue));

  hltpu->fetchingROBs = false;
  forceAccept(hltpu, Event::ForceAccept::FETCHROBS_ERROR);
}


// --------------------------------- Event Processing  ---------------------------------


void Processor::markDone(Event* event)
{
  ERS_ASSERT(event != nullptr);
  ERS_ASSERT(!event->markedDone);
  ERS_DEBUG(3, "Event marked as done (GID: " << event->globalId << ").");
  event->markedDone = true;
  event->ackTime = std::chrono::steady_clock::now();
  m_l1Source->markDone(event);
}


void Processor::reject(HLTPU *hltpu)
{
  ERS_DEBUG(3, "Rejected event for PU " << hltpu->name );
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  if (!hltpu->event->markedDone)
    markDone(hltpu->event.get());
  m_main.isInfo()->ProxRejEvents++;
  finalizeProcessing(hltpu);
  finalizeEvent(std::move(hltpu->event));
  setStateIdle(hltpu);
  ERS_DEBUG(3, "Pu " << hltpu->name << " set in IDLE state.");
}


void Processor::accept(HLTPU *hltpu)
{
  ERS_DEBUG(3, "Accepted event for PU " << hltpu->name );
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  hltpu->state = HLTPU::State::ACCEPTING;

  // Make sure there is at least one stream tag
  if (hltpu->event->streamTags.empty())
  {
    ers::warning(dcm::ActiveHLTPUSessionError(ERS_HERE, "Accepted event has no stream tags",
                                              hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                              "Add debug tag 'missingStreamTag'"));
    hltpu->event->streamTags.emplace_back("missingStreamTag", eformat::DEBUG_TAG, false);
  }

  // Build list of robs left to collect
  bool hasInvalidRobIds = false;
  bool hasToFetchAllROBs = false;
  std::vector<uint32_t> robIds;

  std::vector<uint32_t> trigRobIds;
  for (const auto robInfo : hltpu->event->l1Result)
    trigRobIds.push_back(robInfo->robId);
  for (const auto robInfo : hltpu->event->hltResult)
    trigRobIds.push_back(robInfo->robId);
  std::sort(trigRobIds.begin(), trigRobIds.end());

  for (const auto& streamTag : hltpu->event->streamTags)
  {
    if (streamTag.dets.empty() && streamTag.robs.empty())
      hasToFetchAllROBs = true;
    else
    {
      std::vector<std::uint32_t> invalidRobIds;
      std::vector<eformat::SubDetector> invalidDetIds;
      for (auto detId : streamTag.dets)
      {
        std::uint32_t loId = eformat::helper::SourceIdentifier(detId, 0).simple_code();
        std::uint32_t hiId = eformat::helper::SourceIdentifier(detId, UINT16_MAX).simple_code();

        // Check if at least some ROBs belonging to Detector detId can be collected from the DataCollector
        auto it = std::lower_bound(m_robIds.begin(), m_robIds.end(), loId);
        auto end = std::upper_bound(m_robIds.begin(), m_robIds.end(), hiId);
        if (it == end)
        {
          // Check if at least some ROBs belonging to Detector detId are part of the Level-1 or HLT results
          auto it = std::lower_bound(trigRobIds.begin(), trigRobIds.end(), loId);
          auto end = std::upper_bound(trigRobIds.begin(), trigRobIds.end(), hiId);
          if (it == end)
            invalidDetIds.push_back(detId);
        }
        else
          for (; it != end; ++it)
            robIds.push_back(*it);
      }
      for (auto robId : streamTag.robs)
      {
        if (!std::binary_search(m_robIds.begin(), m_robIds.end(), robId))
        {
          if (!std::binary_search(trigRobIds.begin(), trigRobIds.end(), robId))
            invalidRobIds.push_back(robId);
        }
        else
          robIds.push_back(robId);
      }
      if (!invalidRobIds.empty() || !invalidDetIds.empty())
      {
        hasInvalidRobIds = true;
        ers::warning(dcm::InvalidRobIdOrDetInStreamTag(ERS_HERE, "Found invalid ROB IDs or Detector IDs in a streamTag",
                                                       hltpu->name, hltpu->event->globalId, hltpu->event->l1Id,
                                                       streamTag.type + "_" + streamTag.name,
                                                       robIdsAsString(invalidRobIds), detsAsString(invalidDetIds),
                                                       "ignoring"));
      }
    }
  }

  if (hasInvalidRobIds)
  {
    forceAccept(hltpu, Event::ForceAccept::INVALID_ROBID_ERROR);
    return;
  }

  // If has not yet fetched all robs
  if (!hltpu->fetchedAllRobs)
  {
    if (hasToFetchAllROBs)
    {
      fetchAllROBs(hltpu);
      return;
    }

    if (!robIds.empty())
    {
      std::sort(robIds.begin(), robIds.end());
      robIds.erase(std::unique(robIds.begin(), robIds.end()), robIds.end());
      fetchROBs(hltpu, robIds);
      return;
    }
  }

  if (!hltpu->event->markedDone)
     markDoneAfterEB(hltpu->event.get());
  finalizeAccept(hltpu);
  setStateIdle(hltpu);
  ERS_DEBUG(3, "Pu " << hltpu->name << " set in IDLE state.");
}


void Processor::finalizeAccept(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::ACCEPTING ||
                 hltpu->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  m_main.isInfo()->ProxAccEvents++;
  finalizeProcessing(hltpu);
  ++m_nOutputEvents;
  m_eventBuilder->asyncBuildAndSend(std::move(hltpu->event));
}


void Processor::forceAccept(HLTPU* hltpu, Event::ForceAccept type)
{
  ERS_DEBUG(3, "Pu " << hltpu->name << " undergoes forceAccept.");
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT(hltpu->event != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING ||
                 hltpu->state == HLTPU::State::ACCEPTING,
                 "invalid hltpuState " << hltpu->stateStr() << "; " <<
                 "event->forceAccept=" << hltpu->event->forceAccept << "; " <<
                 "type=" << type << "; " <<
                 "hltpuName=" << hltpu->name);
  ERS_ASSERT_MSG(hltpu->event->forceAccept == Event::ForceAccept::NONE,
                 "invalid event forceAccept " << hltpu->event->forceAccept <<
                 "hltpuState=" << hltpu->stateStr() << "; " <<
                 "type=" << type << "; " <<
                 "hltpuName=" << hltpu->name);

  hltpu->state = HLTPU::State::FORCE_ACCEPTING;
  hltpu->event->forceAccept = type;
  hltpu->event->streamTags.clear();

  if (type == Event::ForceAccept::HLTSV_REASSIGN)
  {
    ERS_LOG( "Force accept HLTSVForceAccept (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("HLTSVForceAccept", eformat::DEBUG_TAG, false);
  }
  else if (type == Event::ForceAccept::HLTPU_CRASH)
  {
    ERS_LOG( "Force accept PUCrash (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("PUCrash", eformat::DEBUG_TAG, false);
    m_main.isInfo()->ProxHltpuCrashes++;
  }
  else if (type == Event::ForceAccept::HLTPU_TIMEOUT)
  {
    ERS_LOG( "Force accept PUTimeout (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("PUTimeout", eformat::DEBUG_TAG, false);
    m_main.isInfo()->ProxHltpuTimeouts++;
  }
  else if (type == Event::ForceAccept::INVALID_ROBID_ERROR)
  {
    ERS_LOG( "Force accept DcmFetchRobsError (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("InvalidRobIdError", eformat::DEBUG_TAG, false);
  }
  else if (type == Event::ForceAccept::FETCHROBS_ERROR)
  {
    ERS_LOG( "Force accept DcmFetchRobsError (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("DcmFetchRobsError", eformat::DEBUG_TAG, false);
  }
  else if (type == Event::ForceAccept::L1ID_MISMATCH)
  {
    ERS_LOG( "Force accept DcmL1IdMismatchError (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("DcmL1IdMismatchError", eformat::DEBUG_TAG, false);
  }
  else if (type == Event::ForceAccept::DFINTERFACE_ERROR)
  {
    ERS_LOG( "Force accept DFInterfaceError (" << type << ") for " << hltpu->name);
    hltpu->event->streamTags.emplace_back("DFInterfaceError", eformat::DEBUG_TAG, false);
  }
  else
    ERS_ASSERT_MSG( false, "Unrecognized force accept type " << type << " for " << hltpu->name );

  if (type.fetchAllRobs() && !hltpu->fetchedAllRobs)
    fetchAllROBs(hltpu);
  else
  {
    if (!hltpu->event->markedDone)
      markDoneAfterEB(hltpu->event.get());
    finalizeForceAccept(hltpu);
  }
}


void Processor::finalizeForceAccept(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  Event::ForceAccept type = hltpu->event->forceAccept;
  finalizeAccept(hltpu);
  if (type == Event::ForceAccept::HLTSV_REASSIGN || type == Event::ForceAccept::L1ID_MISMATCH)
  {
    // Don't wait for a new event request from the HLTPU,
    // Resume prior event request as if we came from IDLE state
    hltpu->state = HLTPU::State::IDLE;
    tryFetchL1R(hltpu);
    return;
  }
  if (type.closeSession())
  {
    terminate(hltpu);
    return;
  }
  setStateIdle(hltpu);
}


void Processor::onBuildAndSend(std::unique_ptr<Event> event)
{
  ERS_DEBUG(3, "Event built and sent (GID: " << event->globalId << ").");
  markDoneAfterES(event.get());
  --m_nOutputEvents;
  finalizeEvent(std::move(event));
}


void Processor::onBuildAndSendError(const ers::Issue& issue, std::unique_ptr<Event> event)
{
  if (event->nbrRetries < 3)
  {
    if (m_state != State::RUNNING)
      ++event->nbrRetries;

    // if must force accept the event as possible dupliate
    if (!event->possibleDuplicate && dynamic_cast<const dcm::EventMaybeDelivered*>(&issue))
    {
      ers::warning(dcm::PossibleDuplicateError(ERS_HERE, event->globalId, event->l1Id, issue));

      // flag event as possible duplicate, rebuild and send it
      event->possibleDuplicate = true;
      m_eventBuilder->asyncBuildAndSend(std::move(event));
    }
    else // resubmit the event to the output instance (no rebuild)
      m_eventBuilder->asyncSend(std::move(event));
  }
  else // drop the event
  {
    ers::error(dcm::BuildAndSendError(ERS_HERE, event->globalId, event->l1Id, issue));
    // foward call to onBuildAndSend to update stats and call markDone()
    onBuildAndSend(std::move(event));
  }
}


void Processor::finalizeProcessing(HLTPU* hltpu)
{
  ERS_DEBUG(3, "Finalize Processing for HLTPU " << hltpu->name);
  ERS_ASSERT(hltpu != nullptr);
  m_main.isInfo()->ProxBusyPUs--;
  m_main.isInfo()->ProxFreePUs++;
  hltpu->deadline = std::chrono::steady_clock::now() + m_maxIdleTime_ms;
  auto dataCollectionInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      hltpu->event->dataCollectionDuration);
  m_histograms->fillDataCollectionTime_ms(dataCollectionInterval.count());
  auto processingInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::steady_clock::now() - hltpu->event->processingStartTime);
  m_histograms->fillProcessingTime_ms(processingInterval.count());
}


void Processor::finalizeEvent(std::unique_ptr<Event> event)
{
  ERS_DEBUG(3, "On finalizeEvent");
  m_dataCollector->clearEvent(event.get());

  m_main.isInfo()->ProxDoneEvents++;
  auto lifeInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::steady_clock::now() - event->processingStartTime);
      //std::chrono::steady_clock::now() - event->lifeStartTime);
  m_histograms->fillLifeTime_ms(lifeInterval.count());
  
  auto ownedInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      event->ackTime - event->processingStartTime);
  m_histograms->fillOwnTime_ms(ownedInterval.count());

  --m_nEventsInDCM;
  event.reset();
  // If we are RUNNING and backpressure has just been released
  if (m_state == State::RUNNING && m_nOutputEvents + 1 == m_maxOutputEvents) {
      unsuspendAllHtpu();
  }
  tryFinalizeStop();
}


void Processor::terminate(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_LOG("Terminate " << hltpu->name);
  hltpu->state = HLTPU::State::TERMINATED;
  m_main.ioService().post(std::bind(&Processor::detachHLTPU, this, hltpu));
}


void Processor::noMoreEvents(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu != nullptr);
  ERS_LOG("NoMoreEvents " << hltpu->name);
  terminate(hltpu);
}


// --------------------------------- Utilities  ---------------------------------


void Processor::readCTPFragment(Event *event)
{
  // Locate a CTP fragment (pick the one with the smallest robId)
  const auto robInfoIndex = event->block.robInfoIndex();
  std::uint32_t ctpRobId = eformat::helper::SourceIdentifier(eformat::TDAQ_CTP, 0).simple_code();
  auto it = robInfoIndex.lower_bound(ctpRobId);
  if (it != robInfoIndex.end() &&
      eformat::helper::SourceIdentifier(it->first).subdetector_id() == eformat::TDAQ_CTP)
  {
    // Set ctpROB to the rob fragment
    event->ctpROB = it->second->robFragment();
    // Set the event BcId to the BcId found in the CTP fragment
    event->bcId = event->ctpROB.rod_bc_id();
    // Get the lumiBlockNumber
    event->lumiBlock = CTPfragment::lumiBlockNumber(&event->ctpROB);
  }
  else
  {
    // If the L1R has no CTP fragment pick the BcId of the first fragment of the L1R
    auto it = robInfoIndex.begin();
    if (it != robInfoIndex.end())
      event->bcId = it->second->robFragment().rod_bc_id();
  }
}


// Checks for a l1id mismatch if needed and issue error if required
void Processor::checkL1IdMismatch(Event* event, std::uint32_t robId, std::uint64_t l1Id)
{
  if (event->l1Id != l1Id)
  {
    // Get subdetector ID
    auto sourceId = eformat::helper::SourceIdentifier(robId);

    // get the ROS name
    const char* rosName = "HLTSV";
    std::map<std::uint32_t, std::string>::const_iterator it = m_dataCollector->robIdToSource().find(robId);
    if (it != m_dataCollector->robIdToSource().end())
      rosName = it->second.c_str();

    event->hasL1IdMismatch = true;
    dcm::L1IDMismatchIssue myIssue(ERS_HERE,
                                   robId,
                                   l1Id,
                                   event->l1Id,
                                   sourceId.human_detector().c_str(),
                                   rosName);
    ers::error(myIssue);
  }
}


// Return true if the BcId check on the given rob should be ignored.
bool Processor::ignoreBcIdCheck(Event* event, std::uint32_t robId) const
{
  bool ignore = false;
  SharedBlockArray::RobInfo *robInfo = event->block[robId];
  if (robInfo != nullptr)
  {
    const eformat::read::ROBFragment rob(robInfo->robFragment());
    if (rob.nstatus() > 0)
    {
      using namespace eformat;
      auto status = helper::Status(rob.status()[0]);
      if (status.generic() & DUMMY_FRAGMENT)
        ignore = true;
      else if (status.specific() & (FRAGMENT_LOST | FRAGMENT_PENDING | ROBIN_DISCARD_MODE))
        ignore = true;
    }
  }
  return ignore;
}


// Check for a bcId mismatch
void Processor::checkBcIdMismatch(Event* event, std::uint32_t robId, std::uint32_t bcId)
{
  // Get subdetector ID
  auto sourceId = eformat::helper::SourceIdentifier(robId);
  auto subdetectorId = sourceId.subdetector_id();

  // Check if BCID mismatch checks are disabled for this subdetector
  if (m_bcidCheckIgnoreSubdetectors.find(subdetectorId) == m_bcidCheckIgnoreSubdetectors.end())
  {
    // Check for BCID mismatch
    if (bcId != event->bcId && !ignoreBcIdCheck(event, robId))
    {
      event->hasBcIdMismatch = true;

      // Check if BCID mismatch reports are disabled for this subdetector
      if (m_bcidCheckSilentSubdetectors.find(subdetectorId) == m_bcidCheckSilentSubdetectors.end())
      {
        // get the ROS name
        const char* rosName = "HLTSV";
        std::map<std::uint32_t, std::string>::const_iterator it = m_dataCollector->robIdToSource().find(robId);
        if (it != m_dataCollector->robIdToSource().end())
          rosName = it->second.c_str();

        dcm::BCIDMismatchIssue myIssue(ERS_HERE,
                                       robId,
                                       static_cast<std::int32_t>(bcId),
                                       static_cast<std::int32_t>(event->bcId),
                                       event->l1Id,
                                       sourceId.human_detector().c_str(),
                                       rosName);
        ers::warning(myIssue);
      }
    }
  }
}


// Checks for BCId and L1Id mismatch of all received ROBs
void Processor::checkBcIdAndL1IdMismatch(HLTPU* hltpu)
{
  // Get all robs to check
  std::vector<eformat::read::ROBFragment> robs;
  hltpu->event->block.getRobFragments(hltpu->event->lastDataEnd, robs);
  for (const auto& rob : robs )
  {
    checkBcIdMismatch(hltpu->event.get(), rob.source_id(), rob.rod_bc_id());
    checkL1IdMismatch(hltpu->event.get(), rob.source_id(), rob.rod_lvl1_id());
  }
}


// Return list of robIds as string
std::string Processor::robIdsAsString( const std::vector<std::uint32_t>& robIds)
{
  if (robIds.empty())
    return std::string("{}");
  std::stringstream str;
  str << "{ 0x" << std::hex << robIds[0] << std::dec << " " << eformat::helper::SourceIdentifier(robIds[0]).human();
  for (size_t i = 1; i < robIds.size(); ++i)
    str << ", 0x" << std::hex << robIds[i] << std::dec << " " << eformat::helper::SourceIdentifier(robIds[i]).human();
  str << " }";
  return str.str();
}


// Return list of dets as string
std::string Processor::detsAsString( const std::vector<eformat::SubDetector>& dets)
{
  if (dets.empty())
    return std::string("{}");
  std::stringstream str;
  str << "{ 0x" << std::hex << (std::uint32_t)dets[0] << std::dec << " "
      << eformat::helper::SourceIdentifier(dets[0],0).human_detector();
  for (size_t i = 1; i < dets.size(); ++i)
    str << ", 0x" << std::hex << (std::uint32_t)dets[i] << std::dec << " "
           << eformat::helper::SourceIdentifier(dets[i],0).human_detector();
  str << " }";
  return str.str();
}


void Processor::timer(const boost::system::error_code& error)
{
  if(error == boost::asio::error::operation_aborted)
    return;

  if (error)
  {
    ers::error(dcm::ProcessorWatchDogError(ERS_HERE, makeSystemError(ERS_HERE, error)));
  }
  else
  {
    auto now = std::chrono::steady_clock::now();
    for (auto& hltpu : m_hltpus)
    {
      if (hltpu->state == HLTPU::State::IDLE && now > hltpu->deadline && m_state == State::STOPPING)
      {
        ers::warning(dcm::Level1RequestTimedOut(ERS_HERE, hltpu->name));
        terminate(hltpu.get());
      }
      else if (hltpu->state == HLTPU::State::PROCESSING && now > hltpu->deadline && !hltpu->fetchingROBs &&
               hltpu->event && hltpu->event->forceAccept == Event::ForceAccept::NONE)
      {
        ers::warning(dcm::HltpuTimeoutIssue(ERS_HERE, "Event processing timed out. Force accept event", hltpu->name));
        forceAccept(hltpu.get(), Event::ForceAccept::HLTPU_TIMEOUT);
      }
    }
  }

  m_timer.expires_at(m_timer.expires_at() + std::chrono::seconds(1));
  m_timer.async_wait(std::bind(&Processor::timer, this, std::placeholders::_1));
}

const char *Processor::HLTPU::stateStr()
{
    const char *stateStrings[]{"INITIALIZED", "IDLE", "SUSPENDED", "FETCHING_L1R",
                                      "PROCESSING", "FORCE_ACCEPTING", "ACCEPTING","TERMINATED"};
    return stateStrings[(int)state];
}

// --------------------------------- Histogram  ---------------------------------


Processor::Histograms::Histograms(boost::asio::io_service& ioService,
    std::chrono::steady_clock::duration updateInterval, float maxTime, float procTimeOut) :
    m_timer(ioService),
    m_updateInterval(updateInterval),
    m_maxTime(maxTime),
    m_procTimeOut(procTimeOut)
{
}


void Processor::Histograms::initialize()
{
  ERS_LOG("Initialize Processor::Histograms");
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    m_InterAssignmentTime_us = monsvc::MonitoringService::instance().register_object("/DEBUG/InterAssignmentTime_us",
        new TH1I("InterAssignmentTime_us",
            "Interval between two consecutive event assignments from the L1Source (us)",
            10000, 0, 1000000),
        true);
    m_InterAssignmentTime_us_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/InterAssignmentTime_us_recent",
        new TH1I("InterAssignmentTime_us_recent",
            "Interval between two consecutive event assignments from the L1Source (us) [recent]",
            10000, 0, 1000000),
        true);
    m_InterAssignmentTime_us_last.reset(new TH1I("InterAssignmentTime_us_last",
        "Interval between two consecutive event assignments from the L1Source (us) [cache for recent]",
        10000, 0, 1000000));

    m_LifeTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/EventLifeTime_ms",
        new TH1I("EventLifeTime_ms",
            "Lifetime of an Atlas event inside the dcm (ms)",
            200, 0, m_maxTime),
        true);
    m_LifeTime_ms_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/EventLifeTime_ms_recent",
        new TH1I("EventLifeTime_ms_recent",
            "Lifetime of an Atlas event inside the dcm (ms)",
            200, 0, m_maxTime),
        true);
    m_LifeTime_ms_last.reset(new TH1I("EventLifeTime_ms_last",
            "Lifetime of an Atlas event inside the dcm (ms)",
             200, 0, m_maxTime));

    m_OwnedTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/EventOwnershipTime_ms",
        new TH1I("EventOwnershipTime_ms",
            "Interval between L1R arrival and ack message sent back to hltsv (ms)",
            200, 0, m_maxTime),
        true);
    m_OwnedTime_ms_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/EventOwnershipTime_ms_recent",
        new TH1I("EventOwnershipTime_ms_recent",
            "Interval between L1R arrival and ack message sent back to hltsv (ms)",
            200, 0, m_maxTime),
        true);
    m_OwnedTime_ms_last.reset(new TH1I("EventOwnershipTime_ms_last",
            "Interval between L1R arrival and ack message sent back to hltsv (ms)",
        200, 0, m_maxTime));

    m_WaitTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/WaitTime_ms",
        new TH1I("WaitTime_ms",
            "Interval between the request of an event from the L1Source and its reply (ms)",
            200, 0, 2000),
        true);
    m_WaitTime_ms_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/WaitTime_ms_recent",
        new TH1I("WaitTime_ms_recent",
            "Interval between the request of an event from the L1Source and its reply (ms) [recent]",
            200, 0, 2000),
        true);
    m_WaitTime_ms_last.reset(new TH1I("WaitTime_ms_last",
        "Interval between the request of an event from the L1Source and its reply (ms) [cache for recent]",
        200, 0, 2000));

    m_ProcessingTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/ProcessingTime_ms",
        new TH1I("ProcessingTime_ms",
            "Time spent processing an event (ms)",
            200, 0, m_maxTime),
        true);
    m_ProcessingTime_ms_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/ProcessingTime_ms_recent",
        new TH1I("ProcessingTime_ms_recent",
            "Time spent processing an event (ms)",
            200, 0, m_maxTime),
        true);
    m_ProcessingTime_ms_last.reset(new TH1I("ProcessingTime_ms_last",
        "Time spent processing an event (ms) [cache for recent]",
        200, 0, m_maxTime));
    
    m_ProcessingTimeWideView_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/ProcessingTimeWideView_ms",
        new TH1I("ProcessingTimeWideView_ms",
            "Time spent processing an event (ms)",
            200, 0, m_procTimeOut),
        true);
    m_ProcessingTimeWideView_ms_recent = monsvc::MonitoringService::instance().register_object("/DEBUG/ProcessingTimeWideView_ms_recent",
        new TH1I("ProcessingTimeWideView_ms_recent",
            "Time spent processing an event (ms)",
            200, 0, m_procTimeOut),
        true);
    m_ProcessingTimeWideView_ms_last.reset(new TH1I("ProcessingTimeWideView_ms_last",
        "Time spent processing an event (ms) [cache for recent]",
        200, 0, m_procTimeOut));

    m_DataCollectionTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/DataCollectionTime_ms",
        new TH1I("DataCollectionTime_ms",
            "Time spent retrieving data for an event (ms)",
            1000, 0, 1000),
        true);
    m_DataCollectionTime_ms_recent = monsvc::MonitoringService::instance().register_object(
        "/DEBUG/DataCollectionTime_ms_recent",
        new TH1I("DataCollectionTime_ms_recent",
            "Time spent retrieving data for an event (ms) [recent]",
            1000, 0, 1000),
        true);
    m_DataCollectionTime_ms_last.reset(new TH1I("DataCollectionTime_ms_last",
        "Time spent retrieving data for an event (ms) [cache for recent]",
        1000, 0, 1000));
  }
  m_timer.expires_at(std::chrono::steady_clock::now());
  m_timer.async_wait(std::bind(&Processor::Histograms::timer, this, std::placeholders::_1));
  ERS_LOG("Initialized Processor::Histograms");
}


Processor::Histograms::~Histograms() {}


void Processor::Histograms::finalize()
{
  ERS_LOG("Finalize Processor::Histograms");
  m_timer.cancel();
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    monsvc::MonitoringService::instance().remove_object(m_InterAssignmentTime_us.get());
    monsvc::MonitoringService::instance().remove_object(m_InterAssignmentTime_us_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_LifeTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_LifeTime_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_OwnedTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_OwnedTime_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_WaitTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_WaitTime_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_ProcessingTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_ProcessingTime_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_ProcessingTimeWideView_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_ProcessingTimeWideView_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_DataCollectionTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_DataCollectionTime_ms_recent.get());
  }
  ERS_LOG("Finalized Processor::Histograms");
}


void Processor::Histograms::clear()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  m_InterAssignmentTime_us->Reset();
  m_InterAssignmentTime_us_recent->Reset();
  m_InterAssignmentTime_us_last->Reset();

  m_LifeTime_ms->Reset();
  m_LifeTime_ms_recent->Reset();
  m_LifeTime_ms_last->Reset();

  m_OwnedTime_ms->Reset();
  m_OwnedTime_ms_recent->Reset();
  m_OwnedTime_ms_last->Reset();

  m_WaitTime_ms->Reset();
  m_WaitTime_ms_recent->Reset();
  m_WaitTime_ms_last->Reset();

  m_ProcessingTime_ms->Reset();
  m_ProcessingTime_ms_recent->Reset();
  m_ProcessingTime_ms_last->Reset();

  m_ProcessingTimeWideView_ms->Reset();
  m_ProcessingTimeWideView_ms_recent->Reset();
  m_ProcessingTimeWideView_ms_last->Reset();

  m_DataCollectionTime_ms->Reset();
  m_DataCollectionTime_ms_recent->Reset();
  m_DataCollectionTime_ms_last->Reset();
}


void Processor::Histograms::updateRecents()
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_InterAssignmentTime_us);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_InterAssignmentTime_us_recent);
    m_InterAssignmentTime_us_recent.get()->Add(m_InterAssignmentTime_us.get(), m_InterAssignmentTime_us_last.get(), 1, -1);
    m_InterAssignmentTime_us.get()->Copy(*m_InterAssignmentTime_us_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_LifeTime_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_LifeTime_ms_recent);
    m_LifeTime_ms_recent.get()->Add(m_LifeTime_ms.get(), m_LifeTime_ms_last.get(), 1, -1);
    m_LifeTime_ms.get()->Copy(*m_LifeTime_ms_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_OwnedTime_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_OwnedTime_ms_recent);
    m_OwnedTime_ms_recent.get()->Add(m_OwnedTime_ms.get(), m_OwnedTime_ms_last.get(), 1, -1);
    m_OwnedTime_ms.get()->Copy(*m_OwnedTime_ms_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_WaitTime_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_WaitTime_ms_recent);
    m_WaitTime_ms_recent.get()->Add(m_WaitTime_ms.get(), m_WaitTime_ms_last.get(), 1, -1);
    m_WaitTime_ms.get()->Copy(*m_WaitTime_ms_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_ProcessingTime_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_ProcessingTime_ms_recent);
    m_ProcessingTime_ms_recent.get()->Add(m_ProcessingTime_ms.get(), m_ProcessingTime_ms_last.get(), 1, -1);
    m_ProcessingTime_ms.get()->Copy(*m_ProcessingTime_ms_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_ProcessingTimeWideView_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_ProcessingTimeWideView_ms_recent);
    m_ProcessingTimeWideView_ms_recent.get()->Add(m_ProcessingTimeWideView_ms.get(), m_ProcessingTimeWideView_ms_last.get(), 1, -1);
    m_ProcessingTimeWideView_ms.get()->Copy(*m_ProcessingTimeWideView_ms_last.get());
  }

  {
    std::lock_guard<monsvc::ptr<TH1I>> l1(m_DataCollectionTime_ms);
    std::lock_guard<monsvc::ptr<TH1I>> l2(m_DataCollectionTime_ms_recent);
    m_DataCollectionTime_ms_recent.get()->Add(m_DataCollectionTime_ms.get(), m_DataCollectionTime_ms_last.get(), 1, -1);
    m_DataCollectionTime_ms.get()->Copy(*m_DataCollectionTime_ms_last.get());
  }
}


void Processor::Histograms::timer(const boost::system::error_code& error)
{
  if (error)
  {
    if (error != boost::asio::error::operation_aborted)
    {
      ers::warning(dcm::GenericIssue(ERS_HERE,"DummyProcessor::Histograms diff for recents timer error",
      makeSystemError(ERS_HERE, error)));
    }
  }
  else
  {
    updateRecents();
    m_timer.expires_at(m_timer.expires_at() + m_updateInterval);
    m_timer.async_wait(std::bind(&Processor::Histograms::timer, this, std::placeholders::_1));
  }
}


void Processor::Histograms::fillInterAssignmentTime_us(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_InterAssignmentTime_us->Fill(x);
}


void Processor::Histograms::fillLifeTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_LifeTime_ms->Fill(x);
}

void Processor::Histograms::fillOwnTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_OwnedTime_ms->Fill(x);
}

void Processor::Histograms::fillWaitTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_WaitTime_ms->Fill(x);
}


void Processor::Histograms::fillProcessingTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_ProcessingTime_ms->Fill(x);
  m_ProcessingTimeWideView_ms->Fill(x);
}


void Processor::Histograms::fillDataCollectionTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_DataCollectionTime_ms->Fill(x);
}




} // namespace dcm
} // namespace daq
