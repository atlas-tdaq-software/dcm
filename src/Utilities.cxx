#include "dcm/Utilities.h"
#include "ers/ers.h"
#include <fstream>
#include <sstream>
#include <vector>
#include "sys/times.h"

namespace daq
{
namespace dcm
{


/*
 * Read diskIO write_bytes (from /proc/self/io)
 */
uint64_t procSelfIo(std::string label)
{
  std::ifstream f("/proc/self/io"); 
  if( !f ) {
    ERS_LOG("Error opening file '/proc/self/io'");
    return 0; 
  }

  std::string s;
  uint64_t n = 0; 
  while (!f.eof() ) 
  {
    getline( f, s );
    if ( s.find(label) == 0 )  
    { 
      std::istringstream is(s.substr(label.size()));
      is >> n;
      break;
    }  
  } 
  f.close();
  return n;
}



/*
 *  Format of the "cpu " line of /proc/stat:
 *  userTicks   niceTicks  systemTicks  idleTicks
 *    => cpuUsage = (user+nice+system)/total
 */
float cpuUsageByNode()
{
  static uint64_t cpu_usage_buffer[4] = {0,0,0,0};
  std::string statfile("/proc/stat");
  char tmp[20];
  std::ifstream f(statfile.c_str());
  if (!f) {
    ERS_LOG("Cannot open '"<< statfile << "'");
    std::perror("Opening file"); 
    return -1;
  }
  std::string s;
  while (!f.eof() ) {
    getline( f, s );
    if ( s.find("cpu ") == 0 )  break;
  }
  f.close();

  std::istringstream is(s);
  uint64_t v[4];
  is >> tmp;
  for(int i=0; i<4; i++) is >> v[i];

  bool first = (cpu_usage_buffer[2]) ? false : true;
  int usage = -1;
  uint64_t den = 0;
  uint64_t num = 0;
  for (int i=0; i<4; i++) {
    den += v[i] - cpu_usage_buffer[i];
    cpu_usage_buffer[i] = v[i];
    if(i==2) num=den;
  }
  if(!first)
    usage = (100*num)/float(den);
  
  return float( int(usage*10) )/10 ;
}


// 
float cpuUsageByProcess()
{
  static clock_t lastCPU, lastSysCPU, lastUserCPU;
  static bool first = true;

  struct tms timeSample;
  if(first)
  {
     lastCPU     = times(&timeSample);
     lastSysCPU  = timeSample.tms_stime;
     lastUserCPU = timeSample.tms_utime;
     first = false;
     return -1;
  }   

  float percent = 1.0;
  clock_t now = times(&timeSample);
  if (!(now <= lastCPU || timeSample.tms_stime < lastSysCPU || timeSample.tms_utime < lastUserCPU))
  {
    percent = (timeSample.tms_stime - lastSysCPU) + (timeSample.tms_utime - lastUserCPU);
    percent /= (now - lastCPU);
    percent *= 100;
  }
  lastCPU     = now;
  lastSysCPU  = timeSample.tms_stime;
  lastUserCPU = timeSample.tms_utime;

  return float( int(percent*10) )/10 ;
}


/*
//   It takes about 0.2 ms
int cpuUsageByProcess()
{
  static SystemResource sysres;
  static std::string s_cpu("Estimated CPU usage since last update (%)");

  int cpu=-99;
  StringResourceStream rs;
  sysres.get_resource(rs);
  std::string sres = rs.str();

  std::string sub  = sres.substr(sres.find(s_cpu) + s_cpu.size());
  sscanf(sub.c_str(), "%d", &cpu);

  return cpu;
}
*/

// Utility to monitor the traffic on network interface
bool getEthTraffic(std::string &interface, uint64_t &recv, uint64_t &sent)
{
  std::ifstream f("/proc/net/dev");
  if( !f ) {
    ERS_LOG( "Error opening file '/proc/net/dev'");
    return false;
  }
  static char *buffer = new char[4096];
  f.read(buffer, 4096);
  f.close();

  char *match = strstr (buffer, interface.c_str());
  if (match == NULL) {
    ERS_LOG("Interface '" << interface << "' not found");
    return false;
  }

  std::stringstream o;
  o << match+interface.size()+1;

  int x;
  o >> recv;
  for (int i=0; i<7; i++) o >> x;
  o >> sent;
  return true;
}

} // namespace dcm
} // namespace daq



