#include "dcm/DummyProcessor.h"
#include "dcm/dal/DcmDummyProcessor.h"
#include "dcm/Main.h"
#include "dcm/Utilities.h"
#include "dcm/DataCollector.h"

#include "ers/ers.h"
#include "monsvc/MonitoringService.h"
#include "oh/OHRootMutex.h"


namespace daq
{
namespace dcm
{

DummyProcessor::DummyProcessor(Main& main, const dal::DcmDummyProcessor& config) :
    Processor(main,
                 config.get_nUnits(),
                 std::move(make_unique<DummyProcessor::Histograms>(main.ioService(),
                          std::chrono::seconds(main.applicationConf().get_infoAverageInterval_s()),
                          m_main.applicationConf().get_UpperEdgeTimeHistos(),
                          config.get_processingTimeOut_ms()
                          )),
                 config),
    m_config(config),
    m_streamTag("DcmDummyProcessor", eformat::CALIBRATION_TAG, false)
{
  ERS_LOG("Construct DummyProcessor");
  m_histograms = dynamic_cast<DummyProcessor::Histograms*>(Processor::m_histograms.get());
  ERS_ASSERT(m_histograms != nullptr);
  m_l2ProcessingTimeDistribution_ms.reset(
      new DC::Distribution(config.get_l2ProcessingTimeDistribution_ms(), std::rand()));
  m_l2RequestsDistribution.reset(
      new DC::Distribution(config.get_l2RequestsDistribution(), std::rand()));
  m_l2RequestsPerSourceDistribution.reset(
      config.get_l2RequestsPerSourceDistribution() == "NULL" ?
          nullptr : new DC::Distribution(config.get_l2RequestsPerSourceDistribution(), std::rand()));
  m_efProcessingTimeDistribution_ms.reset(
      new DC::Distribution(config.get_efProcessingTimeDistribution_ms(), std::rand()));
  m_l2Acceptance = m_config.get_l2Acceptance();
  m_efAcceptance = m_config.get_efAcceptance();

  if (m_maxHLTPUs + m_maxOutputEvents > m_main.applicationConf().get_sbaNumOfBlocks()) {
    // TODO: use proper issue
    throw ers::Message(ERS_HERE, "Not enough Shared Block Array blocks configured.");
  }
}


void DummyProcessor::initialize(L1Source* l1Source, DataCollector* dataCollector, EventBuilder* eventBuilder)
{
  Processor::initialize(l1Source, dataCollector, eventBuilder);
  m_robSources = dataCollector->sources();
}


bool DummyProcessor::setDynamicParameter(const std::string& key, const std::string& value)
{
  if (key == "l2Acceptance")
  {
    m_l2Acceptance = (value != "") ? boost::lexical_cast<float>(value) : m_config.get_l2Acceptance();
    return true;
  }
  if (key == "l2ProcessingTimeDistribution_ms")
  {
    auto v = (value != "") ? value : m_config.get_l2ProcessingTimeDistribution_ms();
    m_l2ProcessingTimeDistribution_ms.reset(new DC::Distribution(v, std::rand()));
    return true;
  }
  if (key == "l2RequestsDistribution")
  {
    auto v = (value != "") ? value : m_config.get_l2RequestsDistribution();
    m_l2RequestsDistribution.reset(new DC::Distribution(v, std::rand()));
    return true;
  }
  if (key == "l2RequestsPerSourceDistribution")
  {
    auto v = (value != "") ? value : m_config.get_l2RequestsPerSourceDistribution();
    m_l2RequestsPerSourceDistribution.reset(
        v == "NULL" ? nullptr : new DC::Distribution(v, std::rand()));
    return true;
  }
  if (key == "efAcceptance")
  {
    m_efAcceptance = (value != "") ? boost::lexical_cast<float>(value) : m_config.get_efAcceptance();
    return true;
  }
  if (key == "efProcessingTimeDistribution_ms")
  {
    auto v = (value != "") ? value : m_config.get_efProcessingTimeDistribution_ms();
    m_efProcessingTimeDistribution_ms.reset(new DC::Distribution(v, std::rand()));
    return true;
  }
  return Processor::setDynamicParameter(key, value);
}


void DummyProcessor::asyncStart()
{
  ERS_ASSERT(m_state == State::CONNECTED);

  if (!m_main.runParams().filename_tag.empty()) {
    m_streamTag.name = m_main.runParams().filename_tag;
    // Sanitize the string
    std::replace_if(m_streamTag.name.begin(), m_streamTag.name.end(),
        [] (const char& c) { return !std::isalnum(c, std::locale::classic()); }, '-');
    if (m_streamTag.name != m_main.runParams().filename_tag) {
      DCM_WARNING(dcm::ConfigurationIssue, "The file name tag " << m_main.runParams().filename_tag <<
          " specified in IS information RunParams.RunParams contains invalid characters. " <<
          m_streamTag.name << " will be used instead.");
    }
  } else {
    m_streamTag.name = "DcmDummyProcessor";
  }

  while (m_hltpus.size() < m_maxHLTPUs)
    startHLTPU();
  Processor::asyncStart();
}


void DummyProcessor::startHLTPU()
{
  std::unique_ptr<Processor::HLTPU> hltpu(new DummyProcessor::HLTPU(m_main.ioService()));
  // Assign unique name to HLTPU
  static std::uint32_t hltpuId = 0;
  std::stringstream str;
  str << "HLTPU_" << hltpuId++;
  hltpu->name = str.str();
  if (!attachHLTPU(hltpu)) // will call recylce or set as suspended if not running
  {
    std::stringstream str;
    str << "Failed attaching HLTPU " << hltpu->name;
    ers::warning(ers::Message(ERS_HERE, str.str()));
  }
}


void DummyProcessor::processL1R(Processor::HLTPU* hltpu)
{
  ERS_ASSERT_MSG(hltpu->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << hltpu->stateStr());
  l2DataCollection(static_cast<HLTPU*>(hltpu));
}


void DummyProcessor::processROBs(Processor::HLTPU* h)
{
  ERS_ASSERT_MSG(h->state == HLTPU::State::PROCESSING,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);
  ERS_ASSERT(hltpu->operation == HLTPU::Operation::L2_DATA_COLLECTION ||
             hltpu->operation == HLTPU::Operation::EF_DATA_COLLECTION);

  if (hltpu->operation == HLTPU::Operation::L2_DATA_COLLECTION)
    l2Processing(hltpu);
  else
    efProcessing(hltpu);
}


void DummyProcessor::setStateIdle(Processor::HLTPU* h)
{
  ERS_ASSERT(h != nullptr);
  ERS_ASSERT_MSG(h->state == HLTPU::State::INITIALIZED ||
                 h->state == HLTPU::State::PROCESSING ||
                 h->state == HLTPU::State::ACCEPTING ||
                 h->state == HLTPU::State::FORCE_ACCEPTING,
                 "unexpected HLTPU state " << h->stateStr());
  auto hltpu = static_cast<HLTPU*>(h);
  hltpu->state = HLTPU::State::IDLE;
  hltpu->operation = HLTPU::Operation::L1R_FETCHING;
  tryFetchL1R(hltpu);
}


void DummyProcessor::terminate(Processor::HLTPU* hltpu)
{
  // Call overriden method to post detach and hltpu object destruction
  Processor::terminate(hltpu);

  // if we are running, try restart the hltpu
  if (m_state == State::RUNNING)
    m_main.ioService().post(std::bind(&DummyProcessor::startHLTPU, this));
}


// --------------------------------- Processing  ---------------------------------


void DummyProcessor::l2DataCollection(HLTPU* hltpu)
{
  ERS_ASSERT(hltpu->operation == HLTPU::Operation::L1R_FETCHING);
  hltpu->operation = HLTPU::Operation::L2_DATA_COLLECTION;

  std::vector<std::uint32_t> selectedRobIds;
  std::uint32_t nRobs = m_l2RequestsDistribution->next_value();
  if (m_l2RequestsPerSourceDistribution) {
    // Request multiple ROBs per ROS
    std::random_shuffle(m_robSources.begin(), m_robSources.end());
    for (auto& robSource : m_robSources) {
      std::vector<std::uint32_t> rosRobIds;
      for (auto it = m_dataCollector->sourceToRobIds().lower_bound(robSource);
          it != m_dataCollector->sourceToRobIds().upper_bound(robSource); ++it) {
        rosRobIds.push_back(it->second);
      }
      auto nRosRobs = std::min(m_l2RequestsPerSourceDistribution->next_value(),
          std::uint32_t(std::min(rosRobIds.size(), nRobs - selectedRobIds.size())));
      auto shuffleEnd = random_shuffle_n(rosRobIds.begin(), rosRobIds.end(), nRosRobs);
      selectedRobIds.insert(selectedRobIds.end(), rosRobIds.begin(), shuffleEnd);
      if (selectedRobIds.size() == nRobs) {
        break;
      }
    }
  }
  else {
    // Request completely random ROBs
    auto shuffleEnd = random_shuffle_n(m_robIds.begin(), m_robIds.end(),
        std::min(nRobs, std::uint32_t(m_robIds.size())));
    selectedRobIds.assign(m_robIds.begin(), shuffleEnd);
  }
  if (selectedRobIds.size() < nRobs) {
    DCM_WARNING(dcm::DataCollectingError,
        "The dummy L2 data collection wanted to request " << nRobs <<
        " fragments in total, but only " << selectedRobIds.size() << " could be selected.");
  }

  fetchROBs(hltpu, selectedRobIds);
}


void DummyProcessor::l2Processing(HLTPU* hltpu)
{
  // Monitoring
  auto l2DataCollectionInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::steady_clock::now() - hltpu->event->lastDataCollectionStartTime);
  m_histograms->fillL2DataCollectionTime_ms(l2DataCollectionInterval.count());

  // Emulate L2 processing
  hltpu->operation = HLTPU::Operation::L2_PROCESSING;
  hltpu->timer.expires_from_now(
      std::chrono::milliseconds(m_l2ProcessingTimeDistribution_ms->next_value()));
  hltpu->timer.async_wait([this, hltpu] (const boost::system::error_code& error) {
    if (error)
    {
      if(error == boost::asio::error::operation_aborted)
        return;
      ers::error(dcm::ProcessingSimulationError(ERS_HERE, "L2", hltpu->event->globalId, hltpu->event->l1Id,
                                                makeSystemError(ERS_HERE, error)));
    }
    if (std::rand() > m_l2Acceptance * RAND_MAX)
    {
      ERS_DEBUG(3, "Event rejected at L2 (GID: " << hltpu->event->globalId << ").");
      reject(hltpu);
    }
    else {
      // L2 accept: build the event
      ERS_DEBUG(3, "Event accepted at L2 (GID: " << hltpu->event->globalId << ").");
      efDataCollection(hltpu);
    }
  });
}


void DummyProcessor::efDataCollection(HLTPU* hltpu)
{
  // Emulate EF data collection
  hltpu->operation = HLTPU::Operation::EF_DATA_COLLECTION;
  fetchAllROBs(hltpu);
}


void DummyProcessor::efProcessing(HLTPU* hltpu)
{
  // Monitoring
  auto efDataCollectionInterval = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::steady_clock::now() - hltpu->event->lastDataCollectionStartTime);
  m_histograms->fillEbDataCollectionTime_ms(efDataCollectionInterval.count());

  // Emulate EF processing
  hltpu->operation = HLTPU::Operation::EF_PROCESSING;
  hltpu->timer.expires_from_now(
      std::chrono::milliseconds(m_efProcessingTimeDistribution_ms->next_value()));
  hltpu->timer.async_wait([this, hltpu] (const boost::system::error_code& error) {
    if (error) {
      if(error == boost::asio::error::operation_aborted)
        return;
      ers::error(dcm::ProcessingSimulationError(ERS_HERE, "EF", hltpu->event->globalId, hltpu->event->l1Id,
                                                makeSystemError(ERS_HERE, error)));
    }
    if (std::rand() > m_efAcceptance * RAND_MAX) {
      // EF reject: processing is finished
      ERS_DEBUG(3, "Event rejected at EF (GID: " << hltpu->event->globalId << ").");
      reject(hltpu);
    }
    else {
      // EF accept: send the event
      ERS_DEBUG(3, "Event accepted at EF (GID: " << hltpu->event->globalId << ").");
      hltpu->event->streamTags.push_back(m_streamTag);
      accept(hltpu);
    }
  });
}


// --------------------------------- Histogram  ---------------------------------


DummyProcessor::Histograms::Histograms(boost::asio::io_service& ioService,
    std::chrono::steady_clock::duration updateInterval, float maxTime, float procTimeOut) :
    Processor::Histograms(ioService, updateInterval, maxTime, procTimeOut)
{
}


void DummyProcessor::Histograms::initialize()
{
  ERS_LOG("Initialize DummyProcessor::Histograms");
  {
    std::lock_guard<std::mutex> lock(OHRootMutex::getMutex());

    m_L2DataCollectionTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/L2DataCollectionTime_ms",
        new TH1I("L2DataCollectionTime_ms",
            "Time spent retrieving data for L2 processing of an event (ms)",
            1000, 0, 1000),
        true);
    m_L2DataCollectionTime_ms_recent = monsvc::MonitoringService::instance().register_object(
        "/DEBUG/L2DataCollectionTime_ms_recent",
        new TH1I("L2DataCollectionTime_ms_recent",
            "Time spent retrieving data for L2 processing of an event (ms) [recent]",
            1000, 0, 1000),
        true);
    m_L2DataCollectionTime_ms_last.reset(
        new TH1I("L2DataCollectionTime_ms_last",
            "Time spent retrieving data for L2 processing of an event (ms) [cache for recent]",
            1000, 0, 1000));

    m_EbDataCollectionTime_ms = monsvc::MonitoringService::instance().register_object("/DEBUG/EbDataCollectionTime_ms",
        new TH1I("EbDataCollectionTime_ms",
            "Time spent retrieving data for EB processing of an event (ms)",
            1000, 0, 1000),
        true);
    m_EbDataCollectionTime_ms_recent = monsvc::MonitoringService::instance().register_object(
        "/DEBUG/EbDataCollectionTime_ms_recent",
        new TH1I("EbDataCollectionTime_ms_recent",
            "Time spent retrieving data for EB processing of an event (ms) [recent]",
            1000, 0, 1000),
        true);
    m_EbDataCollectionTime_ms_last.reset(
        new TH1I("EbDataCollectionTime_ms_last",
            "Time spent retrieving data for EB processing of an event (ms) [cache for recent]",
            1000, 0, 1000));
  }
  Processor::Histograms::initialize();
  ERS_LOG("Initialized Processor::Histograms");
}


void DummyProcessor::Histograms::finalize()
{
  ERS_LOG("Finalize Processor::Histograms");
  Processor::Histograms::finalize();
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    monsvc::MonitoringService::instance().remove_object(m_L2DataCollectionTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_L2DataCollectionTime_ms_recent.get());

    monsvc::MonitoringService::instance().remove_object(m_EbDataCollectionTime_ms.get());
    monsvc::MonitoringService::instance().remove_object(m_EbDataCollectionTime_ms_recent.get());
  }
  ERS_LOG("Finalized Processor::Histograms");
}


void DummyProcessor::Histograms::clear()
{
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    m_L2DataCollectionTime_ms->Reset();
    m_L2DataCollectionTime_ms_recent->Reset();
    m_L2DataCollectionTime_ms_last->Reset();

    m_EbDataCollectionTime_ms->Reset();
    m_EbDataCollectionTime_ms_recent->Reset();
    m_EbDataCollectionTime_ms_last->Reset();
  }
  Processor::Histograms::clear();
}


void DummyProcessor::Histograms::updateRecents()
{
  {
    std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());

    {
      std::lock_guard<monsvc::ptr<TH1I>> l1(m_L2DataCollectionTime_ms);
      std::lock_guard<monsvc::ptr<TH1I>> l2(m_L2DataCollectionTime_ms_recent);
      m_L2DataCollectionTime_ms_recent.get()->Add(m_L2DataCollectionTime_ms.get(), m_L2DataCollectionTime_ms_last.get(),
          1, -1);
      m_L2DataCollectionTime_ms.get()->Copy(*m_L2DataCollectionTime_ms_last.get());
    }

    {
      std::lock_guard<monsvc::ptr<TH1I>> l1(m_EbDataCollectionTime_ms);
      std::lock_guard<monsvc::ptr<TH1I>> l2(m_EbDataCollectionTime_ms_recent);
      m_EbDataCollectionTime_ms_recent.get()->Add(m_EbDataCollectionTime_ms.get(), m_EbDataCollectionTime_ms_last.get(),
          1, -1);
      m_EbDataCollectionTime_ms.get()->Copy(*m_EbDataCollectionTime_ms_last.get());
    }
  }
  Processor::Histograms::updateRecents();
}


void DummyProcessor::Histograms::fillL2DataCollectionTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_L2DataCollectionTime_ms->Fill(x);
}


void DummyProcessor::Histograms::fillEbDataCollectionTime_ms(double x)
{
  std::lock_guard<std::mutex> rootLock(OHRootMutex::getMutex());
  m_EbDataCollectionTime_ms->Fill(x);
}

} // namespace dcm
} // namespace daq
