#include "dcm/dfinterfaceDcm.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
#include <boost/dll.hpp>

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <unordered_map>
#include "dcm/Issues.h"
#include "ers/ers.h"

#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/infoT.h"
#include "is/infodictionary.h"

#include <cstdlib> // for std::rand()
#include "eformat/write/ROBFragment.h"
#include "eformat/ROBFragmentNoTemplates.h"
#include "dccommon/Distribution.h"

#include <random>
#include <signal.h>

#include "dal/Computer.h"
#include "dal/Detector.h"
#include "dal/util.h"
#include "DFdal/DFParameters.h"
#include "DFdal/InputChannel.h"
#include "DFdal/ROS.h"


//#include "dcm/Main.h" //TODO
#include "config/Configuration.h"
#include "dal/Partition.h"
#include "dal/Segment.h"
#include "ipc/partition.h"
#include "RunControl/RunControl.h"
#include "monsvc/PublishingController.h"
#include "monsvc/ptr.h"
#include "rc/RunParams.h"
#include <set>
#include "eformat/SourceIdentifier.h"

static bool     s_stalled = false;
static uint32_t s_l1id = 0;

// SIGNAL CATCHER
static void sig_handler(int signr)
{
  s_stalled = !s_stalled;
  ERS_LOG("Caught signal " << signr << "; switched stalled variable to " << s_stalled );
}



namespace daq
{
  ERS_DECLARE_ISSUE( dcm, HltpuEmuIssue, "Hltpu Emulator Issue: " << reason , ((const char*) reason) )
}

// TODO:
//  - Implement rate control via IS
//  - Implement signal catcher to trigger an infinite loop (for processing timeout testing purpose)
//  - Fix port file naming convention


/// This class emulates the HLT layer:
///  - burn/sleep at L2/EF according to a given distribution
///  - provide list of ROBs to be retrieved in a RoI (according to a given distribution)
///  - provide L2/EB acceptances (according to given distributions)
class Hlt
{
  friend daq::rc::Controllable;

  public:
    enum Level {L2, EF};

    Hlt(std::string l2BurnDist,
        std::string l2RedPerRosDist,
        std::string efBurnDist,
        std::string roiDist,
        std::string l2Steps,
        float l2Acc,
        float efAcc,
        std::vector<uint32_t> robIds,
        IPCPartition ipcPartition,
        bool rosFetching,
        bool sleep=false);

    ~Hlt() { delete m_l2BurnDist; delete m_l2ReqPerRosDist; delete m_efBurnDist; delete m_RoiDist; delete m_stepsDist; }

    void setL2Burn(std::string str) { if(m_l2BurnDist) delete m_l2BurnDist; m_l2BurnDist = new DC::Distribution(str, m_seed); }
    void setL2ReqPerRos(std::string str) { if(m_l2ReqPerRosDist) delete m_l2ReqPerRosDist; m_l2ReqPerRosDist = new DC::Distribution(str, m_seed); }
    void setEFBurn(std::string str) { if(m_efBurnDist) delete m_efBurnDist; m_efBurnDist = new DC::Distribution(str, m_seed); }
    void setRoi(std::string str)    { if(m_RoiDist)    delete m_RoiDist;    m_RoiDist    = new DC::Distribution(str, m_seed); }
    void setSteps(std::string str)  { if(m_stepsDist)  delete m_stepsDist;  m_stepsDist  = new DC::Distribution(str, m_seed); }
    void setL2Acc(float val)        { m_l2Acc = val; }
    void setEFAcc(float val)        { m_efAcc = val; }

    std::string getL2BurnD() { return m_l2BurnDist->get_DistributionString(); }
    std::string getL2ReqPerRos() { return m_l2ReqPerRosDist->get_DistributionString(); }
    std::string getEFBurnD() { return m_efBurnDist->get_DistributionString(); }
    std::string getRoiD()    { return m_RoiDist->get_DistributionString(); }
    std::string getStepsD()  { return m_stepsDist->get_DistributionString(); }
    float getL2Acc()        { return m_l2Acc; }
    float getEFAcc()        { return m_efAcc; }
    uint32_t getSteps()     { return m_stepsDist->next_value(); }

    void reset();   /// Reshuffle the m_robIds vector

    std::vector<std::uint32_t> robsInRoi();
    uint32_t burn(Level lvl);
    bool     isAccepted(Level lvl);
    DC::Distribution *getRoiDist() { return m_RoiDist;}

  private:
    double   burner(double val);
    void     calibrate();
    void     buildRos2RobMap();
    std::vector<std::uint32_t> random_robsInRoi();
    std::vector<std::uint32_t> rosaware_robsInRoi();

  private:
    bool                  m_sleep;
    float                 m_l2Acc;
    float                 m_efAcc;
    DC::Distribution     *m_l2BurnDist;
    DC::Distribution     *m_l2ReqPerRosDist;
    DC::Distribution     *m_efBurnDist;
    DC::Distribution     *m_RoiDist;
    DC::Distribution     *m_stepsDist;
    uint32_t              m_loopsPerUsec;
    uint32_t              m_seed;
    std::vector<uint32_t> m_robIds; /// List of rob ids
    uint32_t              m_offset; /// Offset in the m_robIds

    IPCPartition             m_ipcPartition;
    bool                     m_rosAware;
    std::unordered_map<std::string, std::vector<std::uint32_t> > m_ros2RobMap;
    std::vector<std::string> m_rosUIds;

};


int main(int argc, char** argv)
{
  // initialize random number with processor pid value
  srand(::getpid());
   
  // Install signal handler for SIGHUP
  signal(SIGHUP, sig_handler);
  ERS_LOG("Installed signal handler [SIGHUP]: the signal will block/unblock the processing");


  try
  {
    ERS_LOG("Starting...");

    // Parse command line argument
    std::string defLib("libdfinterfaceDcm");
    std::string library;// Name of the DLL library
    std::string l2B;    // string describing the L2 burning time distribution
    std::string l2RpR;   // string describing the L2 n. of requests per ros distribution
    std::string efB;    // string describing the EF burning time distribution
    std::string roi;    // string describing the RoI request distribution
    std::string steps;  // string describing the number of L2-like steps
    float l2A;          // l2Acceptance
    float efA;          // efAcceptance
    uint32_t hltRobSize;// Size of HLT result
    uint32_t deltaT;    //
    bool  sleepNotBurn; // Use usleep instead of burn cpu
    uint32_t isInterval;// Interval to check in IS
    bool rosAwareFetching; // Enable L2 fetching Ros aware
    bool noEB;
    bool andCalib;
    uint32_t monOpt;
    uint32_t detId;
    uint32_t nRobs;
    uint32_t prefetchProb;
    bool     prefetchHack;
    std::ostringstream os;
    try
    {
      namespace po = boost::program_options;
      po::options_description desc("HLTPU Emulator");
      desc.add_options()
        ("l2Burn,b", po::value<std::string>(&l2B)->default_value("C|50"),    "L2 processing time distribution (ms)")
        ("l2RpR,P",  po::value<std::string>(&l2RpR)->default_value("C|3"),   "L2 n. of requests per ros distribution")
        ("efBurn,B", po::value<std::string>(&efB)->default_value("C|1000"),  "EF processing time distribution (ms)")
        ("l2Req,r",  po::value<std::string>(&roi)->default_value("C|20"),    "L2 request distribution")
        ("steps,n",  po::value<std::string>(&steps)->default_value("C|1"),   "Number of L2-like steps")
        ("l2Acc,a",  po::value<float>(&l2A)->default_value(0.1),             "L2 acceptance (0.<->1.)")
        ("efAcc,A",  po::value<float>(&efA)->default_value(0.1),             "EF acceptance (0.<->1.)")
        ("hltSize,S",po::value<uint32_t>(&hltRobSize)->default_value(100),   "HLT result size in words")
        ("deltaT,t", po::value<uint32_t>(&deltaT)->default_value(100),       "Dump interval in seconds")
        ("sleep,s",  po::value<bool>(&sleepNotBurn)->default_value(false),   "Sleep instead of burning cpu")
        ("isInt,i",  po::value<uint32_t>(&isInterval)->default_value(10),    "Interval to check in IS (0=disable)")
        ("rosAw,w",  po::value<bool>(&rosAwareFetching)->default_value(false),"Fetching per Ros")
        ("noEB,N",   po::value<bool>(&noEB)->default_value(false),           "Do not call getAllRobs()")
        ("andCal,C", po::value<bool>(&andCalib)->default_value(false),       "Add a 2nd streamtag (cal) w/ rob list")
        ("detId,D",  po::value<uint32_t>(&detId)->default_value(0),          "request detector ID in calibration stream") 
        ("nRobs,R",  po::value<uint32_t>(&nRobs)->default_value(2),          "Number of ROBs in calibration stream")
        ("preFet,F", po::value<uint32_t>(&prefetchProb)->default_value(0),   "Probability(%) to call MayGetRobs()")
        ("preHack,f",po::value<bool>(&prefetchHack)->default_value(false),   "Use HLTPU extra param to map MayGetRobs() -> GetRobs()")
        ("lib,l",    po::value<std::string>(&library)->default_value(defLib),"dfinterface library to load")
        ("monOpt,m", po::value<uint32_t>(&monOpt)->default_value(0),         "1 = mon-stream, 2 = mon-stream and phys-stream")
        ("help,h",                                                           "Print help message");

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);
      if (vm.count("help"))
      {
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
      }
      po::notify(vm);
 
      // Ugly trick to dump variables_map (aka <string, boost::any>)
      for (auto var : vm)
      {
        os << "\n\t" << var.first << " = ";
        auto v = var.second.value();
        if      (v.type() == typeid(uint32_t))    os << boost::any_cast<uint32_t>(v);  
        else if (v.type() == typeid(bool))        os << boost::any_cast<bool>(v);
        else if (v.type() == typeid(float))       os << boost::any_cast<float>(v);
        else if (v.type() == typeid(std::string)) os << boost::any_cast<std::string>(v);
      }

    }
    catch (std::exception& ex) {
      ERS_LOG( "Command line parsing error: " << ex.what() );
    }


    ERS_LOG( "Configuration: " << os.str());
/*    
             << "\n\tlibrary          = " << library
             << "\n\tl2_acceptance    = " << l2A
             << "\n\tef_acceptance    = " << efA
             << "\n\tl2_burn          = " << l2B
             << "\n\tef_burn          = " << efB
             << "\n\tl2_req_distr     = " << roi
             << "\n\tl2_req_ros_distr = " << l2RpR
             << "\n\tl2 steps         = " << steps
             << "\n\tsleepNotBurn     = " << sleepNotBurn
             << "\n\thltRobSize       = " << hltRobSize
             << "\n\tisInterval_s     = " << isInterval
             << "\n\tnoEB             = " << noEB
             );
*/


   if(monOpt and andCalib)
    { DCM_FATAL(daq::dcm::HltpuEmuIssue, "Options 'andCal' and 'monOpt' are mutually exclusive "); }
      

    // Initialize IPC
    try   { IPCCore::init(argc, argv); }
    catch (daq::ipc::AlreadyInitialized&) { ; } // ignore

    // Get the username
    std::string username("unknown");
    struct passwd * pw = ::getpwuid(geteuid());
    if( pw ) { username = pw->pw_name; }

    // Read TDAQ_PARTITION
    const char* partitionName = ::getenv( "TDAQ_PARTITION" );
    if(!partitionName)
      { DCM_FATAL(daq::dcm::HltpuEmuIssue, "Missing $TDAQ_PARTITION in the enviroment"); }
    else
      { ERS_LOG("PartitionName = " << partitionName); }

    // Calculate the fileName containing the dcm listening port number
    // FIXME: Currently uses a hard code file name, should use the file name specified in config
    std::string portNumberFileName = "/tmp/dcmport";
    portNumberFileName += ".";
    portNumberFileName += std::string(partitionName);
    portNumberFileName += ".";
    portNumberFileName += username;
    portNumberFileName += ".txt";

    // Create empty property tree and set the parameters
    boost::property_tree::ptree pt;
    pt.put( "dfinterfaceDCM.portNumFile", portNumberFileName );
    if(prefetchHack)
      pt.put( "Configuration.HLTMPPUApplication.DataSource.HLTDFDCMBackend.extraParams.parameter", "robPreFetching=1");

    // Load the DLL
    using builder_t = std::unique_ptr<daq::dfinterface::Session> (const boost::property_tree::ptree& configuration);
    std::function<std::unique_ptr<daq::dfinterface::Session> (const boost::property_tree::ptree& configuration)> build;

    // the shared library has to stay in scope
    boost::dll::shared_library dynLib;
    try
    {
      dynLib.load(library + ".so",
                  boost::dll::load_mode::type::search_system_folders | boost::dll::load_mode::type::rtld_now);
      build = dynLib.get<builder_t>("createSession");
    }
    catch(...)
    {
      ERS_LOG("DLL failure: cannot load " << library << ".so");
      exit(EXIT_FAILURE);
    }
    ERS_LOG("dynamic library dfinterface loaded ...");

    // Create the Session using the "createSession" function from the DLL
    std::unique_ptr<daq::dfinterface::Session> session;
    try
    {
      session = build(pt);
      if( session == nullptr )
      {
        DCM_FATAL(daq::dcm::HltpuEmuIssue, "dcmSession is a nullptr");
      }
      else
      {
        ERS_LOG( "dcmSession instantiated" );
      }
    }
    catch (std::exception& ex)
    {
      DCM_FATAL(daq::dcm::HltpuEmuIssue, "Failed instantiating dcmSession: " << ex.what() );
    }

    // Build a client name
    std::string dcmClientName;
    const char* appName = ::getenv( "TDAQ_APPLICATION_NAME" );
    if(appName != nullptr)
      dcmClientName=appName;
    else
    {
      dcmClientName="hltpuEmu_pid";
      pid_t pid = ::getpid();
      std::stringstream s;
      s << pid;
      dcmClientName += s.str();
    }

    ERS_LOG( "Using DCM client name " << dcmClientName );
    ERS_LOG( "Try opening connection to DCM ..." );
    session->open( dcmClientName );

    // Use the getRobIds method to get the list of robIds
    auto ptr = dynamic_cast<daq::dfinterface::DCMSession*>(session.get());
    ERS_ASSERT_MSG( ptr, "hltpuEmu works only with dfinterfaceDcm.so" );
    std::vector<std::uint32_t> robIds = ptr->getRobIds();

    std::ostringstream o;
    for(auto r: robIds)
          o << "0x" << std::hex << r << std::dec << " ";
    ERS_LOG("Got a list of " << robIds.size() << " ROBs from DCM \n" << std::hex << o.str() << std::dec);


    // Create the HLT emulator
    IPCPartition ipcPartition(partitionName);
    Hlt hlt(l2B, l2RpR, efB, roi, steps, l2A, efA, robIds, ipcPartition, rosAwareFetching, sleepNotBurn);
   
    // Can be used to add detector list to streamTag
    std::set<eformat::SubDetector> dets;
    if(detId)
    {
      dets.insert( eformat::SubDetector(detId) );
      //dets.insert(eformat::TDAQ_HLT);
      //dets.insert(eformat::TDAQ_CTP);
      //dets.insert(eformat::LAR_EM_BARREL_A_SIDE); 
    }

    // Initialize dummy event accept information
    std::vector<uint32_t> triggerInfo = { 1, 2, 3, 4 }, pscErrors, pscErrors2 = { 1, 2};
    std::vector<eformat::helper::StreamTag> streamTags;
    if ( nRobs > robIds.size() ) nRobs =  robIds.size();  
    std::set<uint32_t> robSet;
    //for(uint32_t i=0; i< nRobs; i++)
    //  robSet.insert(  robIds[i]  );
    robSet.insert(  0x7c0005  );

    std::set<uint32_t> robSetC;
    robSetC.insert(  0x111707  );
    robSetC.insert(  0x111708  );
    // Event Monitoring case
    if (monOpt)
    {
      streamTags.emplace_back(eformat::helper::StreamTag("hltpuEmuM", eformat::MONITORING_TAG, false, robSet ));
      if (monOpt>1)
        streamTags.emplace_back(eformat::helper::StreamTag("hltpuEmuP", eformat::PHYSICS_TAG, false));
    }
    // Usual case
    else
    {
      // TDAQ_HLT
      streamTags.emplace_back(eformat::helper::StreamTag("Background", eformat::CALIBRATION_TAG, false ));
      streamTags.emplace_back(eformat::helper::StreamTag("CosmicCalo", eformat::PHYSICS_TAG, false ));
      streamTags.emplace_back(eformat::helper::StreamTag("DataScouting_05_Jets", eformat::CALIBRATION_TAG, false, {0x7c0005} ));
      streamTags.emplace_back(eformat::helper::StreamTag("hltpuCal", eformat::CALIBRATION_TAG, false, {0x111707, 0x111708}));
      streamTags.emplace_back(eformat::helper::StreamTag("hltpuEmuP", eformat::PHYSICS_TAG, false));
      streamTags.emplace_back(eformat::helper::StreamTag("Monitoring", eformat::MONITORING_TAG, false, {0x111707, 0x111708, 0x7c0000, 0x7c0005}));
      streamTags.emplace_back(eformat::helper::StreamTag("hltpuEmuQ", eformat::PHYSICS_TAG, false));

      if(andCalib)
      {
        if (dets.empty())
          streamTags.emplace_back(eformat::helper::StreamTag("hltpuEmuC", eformat::CALIBRATION_TAG, false, robSet ));
        else
          streamTags.emplace_back(eformat::helper::StreamTag("DataScouting_05_Jets", eformat::CALIBRATION_TAG, false, robSet , dets));
      }
    }  


    // Loop forever - Settings
    std::string lastParameters;
    std::string ISInfoName = "DcmRateCtrl";
    ISInfoString cmdString;
    ISInfoDictionary id(ipcPartition);
    char * ISServerName;
    ISServerName = ::getenv("TDAQ_IS_SERVER");
    if (isInterval && ISServerName == NULL){
      isInterval = 0;
      DCM_WARNING(daq::dcm::HltpuEmuIssue, "Variable \"TDAQ_IS_SERVER\" not found: disabling Auto IS Params Update.");
    }
    else
      ERS_LOG("$TDAQ_IS_SERVER=" << ISServerName );

    uint32_t l1cnt=0;
    uint32_t l2cnt=0;
    uint32_t efcnt=0;
    auto     dumpTimeStamp = std::chrono::steady_clock::now();
    auto     isTimeStamp   = dumpTimeStamp;

    // Loop forever
    for( uint32_t i = 1; true; ++i )
    {
      // Provide some logging hart beat
      auto now   = std::chrono::steady_clock::now();
      std::chrono::duration<float> dt = now - dumpTimeStamp;
      if(i%1000==0 or dt > std::chrono::seconds(deltaT))
      {
        ERS_LOG("Cnts = "  << l1cnt << ", " << l2cnt << ", " << efcnt << "; "
             << "Rates = " << float(l1cnt)/dt.count()
             << ", "       << float(l2cnt)/dt.count()
             << ", "       << float(efcnt)/dt.count()
        );
        l1cnt=l2cnt=efcnt=0;
        dumpTimeStamp = now;
      }


      // Auto IS Params Update
      if ( isInterval  &&  (now - isTimeStamp) > std::chrono::seconds(isInterval))
      {
        isTimeStamp = dumpTimeStamp;
        try
        {
          id.getValue(std::string(ISServerName) + "." + ISInfoName, cmdString);
          if (cmdString.getValue() != lastParameters)
          {
            lastParameters = cmdString.getValue();
            boost::tokenizer<boost::escaped_list_separator<char> > tokens(cmdString.getValue());
            boost::char_separator<char> eq_sep("=");
            std::vector<std::string> splitVec;
            for(boost::tokenizer<boost::escaped_list_separator<char> >::iterator beg=tokens.begin(); beg!=tokens.end();++beg){
              boost::split(splitVec, *beg, boost::is_any_of("="), boost::token_compress_on);
              if (splitVec[0] == "l2_a") {
                hlt.setL2Acc(atof(splitVec[1].c_str()));
              } else if (splitVec[0] == "l2_ptd") {
                hlt.setL2Burn(splitVec[1]);
              } else if (splitVec[0] == "l2_rd") {
                hlt.setRoi(splitVec[1]);
              } else if (splitVec[0] == "l2_rpr") {
                hlt.setL2ReqPerRos(splitVec[1]);
              } else if (splitVec[0] == "ef_a") {
                hlt.setEFAcc(atof(splitVec[1].c_str()));
              } else if (splitVec[0] == "ef_ptd") {
                hlt.setEFBurn(splitVec[1]);
              } else if (splitVec[0] == "l2_steps") {
                hlt.setSteps(splitVec[1]);
              } else {
                ERS_LOG(splitVec[0] << " " << splitVec[1] << " Ignored!!\n");
              }
            }
            ERS_LOG("HLT params updated: " << cmdString.getValue() << " / "
              << "l2_a: "    << hlt.getL2Acc()       << ", "
              << "l2_ptd: "  << hlt.getL2BurnD()     << ", "
              << "l2_rd: "   << hlt.getRoiD()        << ", "
              << "l2_rpr: "  << hlt.getL2ReqPerRos() << ", "
              << "l2_steps: "<< hlt.getStepsD()      << ", "
              << "ef_a: "    << hlt.getEFAcc()       << ", "
              << "ef_ptd: "  << hlt.getEFBurnD()     << ", "
            );
          }
        }
        catch (const daq::is::InfoNotFound& ex) {
        }
        catch (const daq::is::InvalidName& ex) {
          DCM_WARNING(daq::dcm::HltpuEmuIssue, "InvalidName " << ex << ". Ignoring");
        }
      }


      // Request the next event
      std::unique_ptr<daq::dfinterface::Event> event = session->getNext();
      std::unique_ptr<uint32_t[]> fullev_write;
      event->getL1Result(fullev_write);
      eformat::read::FullEventFragment l1Result(fullev_write.get());
      std::vector<eformat::read::ROBFragment> robs_frag;
      l1Result.robs(robs_frag);

      ERS_LOG("Received event..." << event->gid() 
            << ", gid = "         << l1Result.global_id() 
            << ", #robs = "  << robs_frag.size()
      );
      l1cnt++;

      if(!robs_frag.size())
      {
        DCM_FATAL(daq::dcm::HltpuEmuIssue, "Empty L1Result");
        exit(77);
      }

      // Reset the HLT steering
      hlt.reset();
      s_l1id = event->l1Id();

      // Ask a number of random robs for L2 processing (n-times)
      std::vector<hltinterface::DCM_ROBInfo> robInfos;
      for(uint32_t i=0; i< hlt.getSteps(); i++)
      {
         auto v = hlt.robsInRoi();
         if(!v.empty())
            event->getRobs(v, robInfos);
         //std::ostringstream o;
         //for(auto r : v)
         //  o << std::hex << r << ",";
         //ERS_LOG("LIST: \n" << o.str());
      }

      // L2 processing
      uint32_t ms = hlt.burn(Hlt::L2);
      //ERS_LOG("L2 processing: " << ms << " ms");
      ms++; // to suppress warning that ms is never used

      // Check L2 decision
      if(!hlt.isAccepted(Hlt::L2))
      {
        session->reject( std::move(event) );
        continue;
      }
      l2cnt++;

      // Prefetch robs
      if( (uint32_t)(std::rand()%100) < prefetchProb ) //(prefetch)
      {
        auto v = hlt.robsInRoi();
        if(!v.empty()) {
          ERS_LOG("Call mayGetRobs()");  
            event->mayGetRobs(v);
         }   
      }

      // Ask all the remaining robs
      if(!noEB)
        event->getAllRobs(robInfos);

      // EF processing
      ms = hlt.burn(Hlt::EF);
      //ERS_LOG("EF processing: " << ms << " ms");

      // Check EF decision
      if(!hlt.isAccepted(Hlt::EF))
      {
        //ERS_LOG("Rejected event..." << event->l1Id() );
        session->reject( std::move(event) );
        continue;
      }



      // Create one HLT result ROB (NB: they can be more than one!)
      uint32_t source_id   = eformat::helper::SourceIdentifier(eformat::TDAQ_EVENT_FILTER, 5).code();
      uint32_t *data       = new uint32_t[hltRobSize];
      const uint32_t ndata = hltRobSize;
      for(uint32_t i=0; i< hltRobSize; i++) data[i] = 0xEFDC0000+i;
      eformat::write::ROBFragment hltrobw(
         source_id,
         source_id,
         l1Result.run_no(),
         l1Result.lvl1_id(),
         l1Result.bc_id(),
         l1Result.lvl1_trigger_type(),
         0, // Detector event type
         ndata,
         data,
         1);
      
      uint32_t source_id1   = eformat::helper::SourceIdentifier(eformat::TDAQ_EVENT_FILTER, 0).code();
      uint32_t *data1       = new uint32_t[hltRobSize];
      const uint32_t ndata1 = hltRobSize;
      for(uint32_t i=0; i< hltRobSize; i++) data[i] = 0xF0CA0000+i;
      eformat::write::ROBFragment hltrobw1(
         source_id1,
         source_id1,
         l1Result.run_no(),
         l1Result.lvl1_id(),
         l1Result.bc_id(),
         l1Result.lvl1_trigger_type(),
         0, // Detector event type
         ndata1,
         data1,
         1);
       
      /// Create the FullEventFragment to be serialized
      eformat::write::FullEventFragment hltResult(
          eformat::helper::SourceIdentifier(eformat::TDAQ_EVENT_FILTER, 5).code(), 
          l1Result.bc_time_seconds(), 
          l1Result.bc_time_nanoseconds(),
          l1Result.global_id(), 
          l1Result.run_type(),
          l1Result.run_no(), 
          l1Result.lumi_block(), 
          l1Result.lvl1_id(),
          l1Result.bc_id(), 
          l1Result.lvl1_trigger_type()
        );


      hltResult.append(&hltrobw);
      hltResult.append(&hltrobw1);

      // Add trigger info
      hltResult.lvl1_trigger_info(triggerInfo.size(), &triggerInfo[0]);

      // Add psc errors in the status words
      hltResult.status(pscErrors.size(), &pscErrors[0]);

      // Add streamTags
      uint32_t stSize = eformat::helper::size_word(streamTags);
      uint32_t *stBuf = new uint32_t[stSize];
      eformat::helper::encode(streamTags, stSize, stBuf);
      hltResult.stream_tag(stSize, stBuf);

      // Serialize
      auto node = hltResult.bind();
      auto event_size_w = eformat::write::count_words(*node);
      auto buf = std::unique_ptr<uint32_t[]>(new uint32_t[event_size_w]);
      eformat::write::copy(*node, buf.get(), event_size_w);

      ERS_LOG("Accept: " <<  hltResult.global_id());  
      session->accept( std::move(event), std::move(buf));

      delete[] data;
      delete[] data1;
      delete[] stBuf;

      efcnt++;
    }
  }
  catch( daq::dfinterface::NoMoreEvents & ){ ERS_LOG("No more events to process, exiting" ); sleep(10);}
  catch( ers::Issue &ex)     { ers::fatal(daq::dcm::HltpuEmuIssue(ERS_HERE, "Caught ers::Issue",     ex )); }
  catch( std::exception &ex) { ers::fatal(daq::dcm::HltpuEmuIssue(ERS_HERE, "Caught std::exception", ex )); }
  catch( ... )               { ers::fatal(daq::dcm::HltpuEmuIssue(ERS_HERE, "Caught unknown exception"  )); }

  return 0;
}



Hlt::Hlt(std::string l2BurnDist,
         std::string l2ReqPerRos,
         std::string efBurnDist,
         std::string roiDist,
         std::string l2Steps,
         float l2Acc,
         float efAcc,
         std::vector<uint32_t> robIds,
         IPCPartition ipcPartition,
         bool rosFetching,
         bool sleep)
  : m_sleep(sleep),
    m_l2Acc(l2Acc),
    m_efAcc(efAcc),
    m_offset(0),
    m_ipcPartition(ipcPartition),
    m_rosAware(rosFetching)
{
  m_robIds = robIds;
  m_l2BurnDist = new DC::Distribution(l2BurnDist, m_seed);
  m_l2ReqPerRosDist = new DC::Distribution(l2ReqPerRos, m_seed);
  m_efBurnDist = new DC::Distribution(efBurnDist, m_seed);
  m_RoiDist    = new DC::Distribution(roiDist,    m_seed);
  m_stepsDist  = new DC::Distribution(l2Steps,    m_seed);
  m_seed       = getpid();
  calibrate();

  if (m_rosAware) {
    buildRos2RobMap();
  }

  ERS_LOG("Hlt constructed");
}

bool Hlt::isAccepted(Level lvl)
{
  uint32_t val = std::rand();
  return (lvl==L2) ? val < m_l2Acc*RAND_MAX : val < m_efAcc*RAND_MAX;
}

uint32_t Hlt::burn(Level lvl)
{
  uint32_t ms = (lvl == L2) ? m_l2BurnDist->next_value() : m_efBurnDist->next_value();
  if(m_sleep)
    usleep(ms*1000);
  else
  {
    uint32_t total_loops = m_loopsPerUsec * ms * 1000;
    volatile double value = 0.5;
    for (uint32_t i=0; i<total_loops; i++)
      value = burner(value);
  }
  if (s_stalled)
  {
    ERS_LOG("Request to stall: sleep forever (L1id = " << s_l1id << ")");
    sleep(99999);
  }
  return ms;
}

double Hlt::burner(double value)
{
  value = (1 - value)/(1 + value);
  if (value < 0.001) value = value * 100.0;
  else value = value * 2.001;
  return value;
}

void Hlt::reset()
{
  m_offset = 0;
  std::random_device rd;
  std::mt19937 g(rd());
  std::shuffle(m_robIds.begin(), m_robIds.end(),g);
}

std::vector<std::uint32_t> Hlt::robsInRoi()
{
  if (m_rosAware){
    return rosaware_robsInRoi();
  } else {
    return random_robsInRoi();
  }
}

std::vector<std::uint32_t> Hlt::random_robsInRoi()
{
  uint32_t n = m_RoiDist->next_value();
  if( (m_offset+n) > m_robIds.size() )
    return std::vector<std::uint32_t>();
  auto beg   = m_robIds.begin() + m_offset;
  m_offset  += n;
  return std::vector<std::uint32_t>(beg, beg+n);
}

std::vector<std::uint32_t> Hlt::rosaware_robsInRoi()
{
  // Multiple Requests per ROS
  std::uint32_t nRequest = m_RoiDist->next_value();
  std::vector<std::uint32_t> robVector;

  std::random_shuffle(m_rosUIds.begin(), m_rosUIds.end());
  for (std::vector<std::string>::iterator it = m_rosUIds.begin();
      it != m_rosUIds.end(); ++it)
  {
    if (robVector.size() >= nRequest)
      break;
    //ERS_LOG(*it);
    std::vector<std::uint32_t> rosRobs = m_ros2RobMap[*it];
    std::random_shuffle(rosRobs.begin(), rosRobs.end());
    std::vector<std::uint32_t>::iterator r_it = rosRobs.begin();
    int c = m_l2ReqPerRosDist->next_value();
    while (c && r_it != rosRobs.end() && (robVector.size() < nRequest)) {
      robVector.push_back(*r_it);
      ++r_it;
      c--;
    }
  }
  return robVector;
}



void Hlt::calibrate()
{
  // Determines how many loops we need to execute microsecond.
  ERS_LOG("Calibrating cpu burner");
  namespace bpt = boost::posix_time;
  uint32_t  loops = 10000000;
  volatile double value = 0.5;
  m_loopsPerUsec = 0;
  for(size_t j=0; j<10; j++)
  {
    bpt::ptime timestamp = bpt::microsec_clock::universal_time();
    for (unsigned int i=0; i<loops; i++) value = burner(value);
    bpt::time_duration dt = bpt::microsec_clock::universal_time() - timestamp;
    if (m_loopsPerUsec == 0) //first assignment
      m_loopsPerUsec = loops/dt.total_microseconds();
    else { //other assignments, migrate towards a mean value
      m_loopsPerUsec = (m_loopsPerUsec+loops/dt.total_microseconds())/2;
    }
  }
  ERS_LOG("CPU burner calibrated to " << m_loopsPerUsec << " cycles/usec for this computer.");
}

void Hlt::buildRos2RobMap()
{
  ERS_LOG("Building ROS to ROB Map");
  std::set<std::string> types = { "ROS" };
  ::Configuration* confDatabase = new Configuration("");

  const daq::core::Partition& partitionConf = *daq::core::get_partition(*confDatabase, m_ipcPartition.name());
  std::vector<const daq::core::BaseApplication *> apps;
  apps = partitionConf.get_all_applications(&types);
  for (auto app : apps) {
    if (app->get_host()->get_State() != 0) {
      //auto ros = confDatabase->cast<daq::df::ROS>(&app.get_base_app());
      auto ros = confDatabase->cast<daq::df::ROS>(app);
      if (ros != nullptr && !ros->disabled(partitionConf)) {
        auto subDetectorId = ros->get_Detector()->get_LogicalId();
        for (auto rosResource : ros->get_Contains()) {
          auto module = confDatabase->cast<daq::core::ResourceSet>(rosResource);
          if (module != nullptr && !module->disabled(partitionConf)) {
            for (auto moduleResource : module->get_Contains()) {
              auto channel = confDatabase->cast<daq::df::InputChannel>(moduleResource);
              if (channel != nullptr && !channel->disabled(partitionConf)) {
                // XXX: the following logical OR works in the following cases:
                // - The ROS sub-detector ID is 0 and the channel ID contains the full ROB ID
                // - The ROS sub-detector ID is set and the channel ID contains just the module ID
                // - The ROS sub-detector ID is set and the channel ID contains the full ROB ID,
                //   with its sub-detector ID part equal to the sub-detector ID.
                std::uint32_t robId = channel->get_Id() | ((std::uint32_t) subDetectorId << 16);
                m_ros2RobMap[ros->UID()].push_back(robId);
              }
            }
          }
        }
      }
    }
  }
  for(auto kv : m_ros2RobMap)
    m_rosUIds.push_back(kv.first);
}
