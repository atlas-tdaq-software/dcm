#include "dcm/SfoOutput.h"

#include "dcm/Event.h"
#include "dcm/EventBuilder.h"
#include "dcm/Main.h"
#include "dcm/Utilities.h"
#include "dcm/Issues.h"
#include "dcm/dal/DcmSfoOutput.h"

#include "asyncmsg/Error.h"
#include "asyncmsg/Session.h"
#include "dal/app-config.h"
#include "dal/Component.h"
#include "dal/Computer.h"

#include "RunControl/Common/OnlineServices.h"

#include <boost/asio/deadline_timer.hpp>

#include <memory>
#include <string>

namespace daq
{
namespace dcm
{

struct SfoOutput::Transaction
{
  enum class State
  {
    NONE,
    ASSIGNED,
    SENT,
    ACKNOWLEDGED
  };

  std::unique_ptr<Event> event;
  void* context = 0;
  State state = State::NONE;
  std::chrono::steady_clock::time_point timestamp;
};

class SfoOutput::Session: public asyncmsg::Session
{

private:

  struct AssignMessage: public asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF40;

    AssignMessage(std::uint32_t nReadyEvents_) :
        nReadyEvents(nReadyEvents_)
    {
    }

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      // XXX: this assumes we are on a little-endian arch
      buffers.emplace_back(static_cast<const void*>(&nReadyEvents), sizeof(nReadyEvents));
    }

    std::uint32_t nReadyEvents;

  };

  struct UpdateMessage: public daq::asyncmsg::InputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF41;

    UpdateMessage(std::uint32_t nGIDs) :
        nRequestedEvents(0),
        acknowledgedGIds(nGIDs)
    {
    }

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      // XXX: this assumes we are on a little-endian arch
      buffers.emplace_back(&nRequestedEvents, sizeof(nRequestedEvents));
      buffers.emplace_back(acknowledgedGIds.data(), acknowledgedGIds.size() * sizeof(std::uint64_t));
    }

    std::uint32_t nRequestedEvents;
    std::vector<std::uint64_t> acknowledgedGIds;

  };

  struct TransferMessage: public daq::asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF42;

    TransferMessage(const Transaction* transaction_) :
        transaction(transaction_)
    {
      for (const auto& stream : transaction->event->streams)
      {
        streamIndexes.push_back(stream.tags.size());
        for (const auto& tag : stream.tags)
        {
          streamIndexes.push_back(tag.idx);
        }
      }
    }

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      // XXX: this assumes we are on a little-endian arch
      buffers.emplace_back(&transaction->event->globalId, sizeof(transaction->event->globalId));
      std::uint32_t i = 0;
      for (const auto& stream : transaction->event->streams)
      {
        buffers.emplace_back(stream.data, stream.size);
        buffers.emplace_back(&streamIndexes[i], (1 + streamIndexes[i]) * sizeof(std::uint32_t));
        i += (1 + streamIndexes[i]);
      }
    }

    const Transaction* transaction;
    std::vector<std::uint32_t> streamIndexes;

  };

public:

  Session(SfoOutput& output, boost::asio::io_service& ioService, const std::string& localName,
      const std::string& remoteName) :
      asyncmsg::Session(ioService),
      m_output(output),
      m_ioService(ioService),
      m_state(State::CLOSED),
      m_localName(localName),
      m_remoteName(remoteName),
      m_openAttemptCount(0),
      m_openAttemptTimer(ioService)
  {
  }

  State state() const
  {
    return m_state;
  }

  const std::string& remoteName() const
  {
    return m_remoteName;
  }

  const std::string& localName() const
  {
    return m_localName;
  }

  const boost::asio::ip::tcp::endpoint& remoteEndpoint() const
  {
    return m_remoteEndpoint;
  }

  std::string remoteEndPointString() const
  {
      std::ostringstream out;
      out << m_remoteEndpoint;
      return out.str();
  }

  void asyncConnect()
  {
    ERS_ASSERT(m_transactions.empty());
    m_openAttemptCount = 0;
    m_state = State::OPEN_PENDING;
    try {
      m_remoteEndpoint = m_output.m_main.resolveServer(m_remoteName);
    }
    catch (ers::Issue& ex) {
      ers::error(daq::dcm::SfoHostNotFoundIssue(ERS_HERE, "Resolving remote name failed",
                                                localName(), remoteName(), ex));
      auto self = shared_from_this();
      getStrand().post([this, self] ()
      {
        onOpenError(make_error_code(boost::asio::error::host_not_found));
      });
      return;
    }
    asyncOpen(m_localName, m_remoteEndpoint);
  }

private:

  void onOpen() override
  {
    m_state = asyncmsg::Session::state();
    m_output.onSessionConnect(this);
  }

  void onOpenError(const boost::system::error_code& error) override
  {
    // retry open after OpenAttemptIntervalDuration_sec seconds at most MaxOpenAttemptCount times
    if (m_openAttemptCount < MaxOpenAttemptCount) {
      ++m_openAttemptCount;
      m_openAttemptTimer.expires_from_now(boost::posix_time::seconds(OpenAttemptIntervalDuration_sec));
      m_openAttemptTimer.async_wait([this](const boost::system::error_code& error){
        if (!error) {
          this->asyncOpen(this->m_localName, this->m_remoteEndpoint);
        } else { // wait terminated with an error (could have been aborted)
          // make sure it falls through error handling
          this->m_openAttemptCount = MaxOpenAttemptCount;
          onOpenError(error);
        }
      });
    } else {
      m_state = asyncmsg::Session::state();
      m_output.onSessionConnectError(this, error);
    }
  }

public:

  void asyncDisconnect()
  {
    m_state = State::CLOSE_PENDING;
    asyncClose();
  }

private:

  void onClose() override
  {
    m_state = asyncmsg::Session::state();
    abort(boost::asio::error::operation_aborted);
    m_output.onSessionDisconnect(this);
  }

  void onCloseError(const boost::system::error_code& error) override
  {
    m_state = asyncmsg::Session::state();
    abort(boost::asio::error::operation_aborted);
    m_output.onSessionDisconnectError(this, error);
  }

public:

  void asyncTransfer(std::unique_ptr<Transaction> transaction)
  {
    if (state() != Session::State::OPEN)
    {
      boost::asio::post(m_ioService, [this, tr = std::move(transaction), output = &m_output] () mutable
          {
            output->onSessionTransferError(this, asyncmsg::Error::SESSION_NOT_OPEN,
                std::move(tr));
          });
    } else {
      transaction->state = Transaction::State::ASSIGNED;
      transaction->timestamp = std::chrono::steady_clock::now();
      m_transactions.emplace_back(std::move(transaction));
      asyncSend(make_unique<AssignMessage>(1));
      if (m_transactions.size() == 1)
      {
        asyncReceive();
      }
    }
  }

  std::size_t nTransactions() const
  {
    return m_transactions.size();
  }

private:

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) override
  {
  }

  void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override
  {
    if (message->typeId() == AssignMessage::TYPE_ID)
    {
        ers::error(daq::dcm::SfoAssignMessageIssue(ERS_HERE, "SFO Assign Event issue",
                                                  localName(), remoteName(),
                                                  remoteEndPointString(),
                                                  error.message(), error.value()));
    }
    else if (message->typeId() == TransferMessage::TYPE_ID)
    {
        ers::error(daq::dcm::SfoTransferMessageIssue(ERS_HERE, "SFO Transfer Event issue",
                                                  localName(), remoteName(),
                                                  remoteEndPointString(),
                                                  error.message(), error.value()));
    }
    abort(error);
  }

  std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t /* transactionId */, std::uint32_t size) override
  {
    if (typeId == UpdateMessage::TYPE_ID)
    {
      auto nGIDs = (size - 4) / 8;
      if (size != 4 + nGIDs * 8)
      {
        ers::error(daq::dcm::SfoUpdateMessageSizeIssue(ERS_HERE, "SFO Update Message size issue",
                                                    localName(), remoteName(),
                                                    remoteEndPointString(), size));
        abort(make_error_code(boost::system::errc::protocol_error));
        return std::unique_ptr<asyncmsg::InputMessage>();
      }
      return make_unique<UpdateMessage>(nGIDs);
    }
    else
    {
      ers::error(daq::dcm::SfoMessageTypeIssue(ERS_HERE, "SFO Update Message size issue",
                                               localName(), remoteName(),
                                               remoteEndPointString(), typeId));
      abort(make_error_code(boost::system::errc::protocol_error));
      return std::unique_ptr<asyncmsg::InputMessage>();
    }
  }

  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    auto update = static_pointer_cast<UpdateMessage>(std::move(message));

    // Handle the event requests
    std::vector<Transaction*> transferTransactions;
    for (auto& transaction : m_transactions) {
      if (transferTransactions.size() == update->nRequestedEvents) {
        break;
      }
      if (transaction->state == Transaction::State::ASSIGNED) {
        transferTransactions.push_back(transaction.get());
      }
    }
    if (transferTransactions.size() < update->nRequestedEvents)
    {
      ers::error(daq::dcm::SfoRequestEventIssue(ERS_HERE, "SFO requested more events that were assigned to it",
                                                 localName(), remoteName(), remoteEndPointString()));
      abort(make_error_code(boost::system::errc::protocol_error));
      return;
    }

    // Handle the acknowledgments
    std::vector<std::unique_ptr<Transaction>> acknowledgedTransactions;
    for (auto globalId : update->acknowledgedGIds)
    {
      auto it = std::find_if(m_transactions.begin(), m_transactions.end(),
          [&] (const std::unique_ptr<Transaction>& t)
          {
            return t->event->globalId == globalId;
          });
      if (it == m_transactions.end() || (*it)->state != Transaction::State::SENT)
      {
        ers::error(daq::dcm::SfoReportEventIssue(ERS_HERE, "SFO reports that it recorded an event that was not sent",
                                                  localName(), remoteName(), remoteEndPointString()));
        abort(make_error_code(boost::system::errc::protocol_error));
        return;
      }
      acknowledgedTransactions.push_back(std::move(*it));
      m_transactions.erase(it);
    }

    if (!m_transactions.empty())
    {
      asyncReceive();
    }
    for (auto transaction : transferTransactions) {
      transaction->state = Transaction::State::SENT;
      transaction->timestamp = std::chrono::steady_clock::now();
      asyncSend(make_unique<TransferMessage>(transaction));
    }
    for (auto& transaction: acknowledgedTransactions)
    {
      transaction->state = Transaction::State::ACKNOWLEDGED;
      m_output.onSessionTransfer(this, std::move(transaction));
    }
  }

  void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<daq::asyncmsg::InputMessage> /* message */) override
  {
    ers::error(daq::dcm::SfoUpdateMessageIssue(ERS_HERE, "Error receiving Update message",
                                               localName(), remoteName(),
                                               remoteEndPointString(),
                                               error.message(), error.value()));
    abort(error);
  }

private:

  void abort(const boost::system::error_code& error)
  {
    // Clear all pending transactions
    decltype(m_transactions) transactions;
    std::swap(transactions, m_transactions);
    for (auto& transaction : transactions) {
      m_output.onSessionTransferError(this, error, std::move(transaction));
    }
  }

public:

  void checkTimeouts(const std::chrono::steady_clock::time_point& now)
  {
    for (const auto& transaction : m_transactions)
    {
      if (transaction->state == Transaction::State::ASSIGNED &&
          now > transaction->timestamp + m_output.m_requestTimeout)
      {
        ers::error(daq::dcm::SfoEventRequestTimeoutIssue(ERS_HERE, "Timeout while waiting for event request",
                                                         localName(), remoteName(), remoteEndPointString()));
        abort(make_error_code(boost::system::errc::timed_out));
        return;
      }
      else if (transaction->state == Transaction::State::SENT &&
          now > transaction->timestamp + m_output.m_ackTimeout)
      {
        ers::error(daq::dcm::SfoAcknowledgmentTimeoutIssue(ERS_HERE, "Timeout while waiting for acknowledgment",
                                                           localName(), remoteName(), remoteEndPointString()));
        abort(make_error_code(boost::system::errc::timed_out));
        return;
      }
    }
  }

private:

  SfoOutput& m_output;
  boost::asio::io_service& m_ioService;

  State m_state;
  std::string m_localName;
  std::string m_remoteName;
  boost::asio::ip::tcp::endpoint m_remoteEndpoint;

  std::deque<std::unique_ptr<Transaction>> m_transactions;

  int m_openAttemptCount;
  const int MaxOpenAttemptCount = 3;
  const int OpenAttemptIntervalDuration_sec = 1;
  boost::asio::deadline_timer m_openAttemptTimer;
};

SfoOutput::SfoOutput(Main& main, const dal::DcmSfoOutput& config) :
    m_main(main),
    m_config(config),
    m_eventBuilder(nullptr),
    m_state(NONE),
    m_timer(main.ioService()),
    m_requestTimeout(std::chrono::seconds(config.get_requestTimeout_s())),
    m_ackTimeout(std::chrono::seconds(config.get_acknowledgmentTimeout_s()))
{
  std::set<std::string> types = { "SFOngApplication" };
  std::vector<const daq::core::BaseApplication *> apps = m_main.partitionConf().get_all_applications(&types);
  for (auto& app : apps)
  {
    auto component = m_main.confDatabase().cast<daq::core::Component>(app);
    if (component == nullptr || !component->disabled(m_main.partitionConf()))
    {
      if (m_config.get_onlyLocalSfo()) {
	// Ignore SFOs not running of this node
	std::string this_fqhn =
	  daq::rc::OnlineServices::instance().getHostName();
	std::string sfo_host = app->get_host()->UID();
	if (this_fqhn != sfo_host) continue;
      }
      m_sinks.push_back(app->UID());
    }
  }
  if (m_sinks.empty())
  {
    // TODO: use a better exception and throw, so that we fail the CONFIGURE transition
    ers::fatal(dcm::NoSFOFound(ERS_HERE));
    return;
  }
  std::sort(m_sinks.begin(), m_sinks.end());
}

void SfoOutput::SfoOutput::initialize(daq::dcm::EventBuilder* eventBuilder)
{
  m_eventBuilder = eventBuilder;
}

void SfoOutput::asyncConnect()
{
  ERS_ASSERT(m_state == NONE);

  m_state = CONNECTING;
  for (auto& sink : m_sinks)
  {
    m_sessions.push_back(std::make_shared<Session>(*this, m_main.ioService(),
        m_main.applicationName(), sink));
  }
  for (auto& session : m_sessions)
  {
    session->asyncConnect();
  }
  m_main.ioService().post(std::bind(&SfoOutput::checkConnect, this));
}

void SfoOutput::asyncStart()
{
  ERS_ASSERT(m_state == CONNECTED);
  ERS_ASSERT(m_queuedTransactions.empty());

  m_state = STARTING;
  m_main.ioService().post(std::bind(&SfoOutput::checkStart, this));
}

void SfoOutput::asyncStop()
{
  ERS_ASSERT(m_state == RUNNING);

  m_state = STOPPING;
  m_main.ioService().post(std::bind(&SfoOutput::checkStop, this));
}

void SfoOutput::asyncDisconnect()
{
  ERS_ASSERT(m_state == CONNECTED);

  m_state = DISCONNECTING;
  // Stop watchdog timer
  m_timer.cancel();
  // Disconnect open sessions
  for (auto& session : m_sessions)
  {
    if (session->state() == Session::State::OPEN ||
        session->state() == Session::State::OPEN_PENDING)
    {
      session->asyncDisconnect();
    }
  }
  // Check if we are already done
  m_main.ioService().post(std::bind(&SfoOutput::checkDisconnect, this));
}

void SfoOutput::asyncEventSend(std::unique_ptr<Event> event)
{
  // TODO: enable context pointers

  ERS_ASSERT(m_state == RUNNING);

  ++m_main.isInfo()->OutputEvents;

  m_queuedTransactions.emplace_back(new Transaction);
  m_queuedTransactions.back()->event = std::move(event);
  m_queuedTransactions.back()->context = nullptr;
  startTransfers();
}

void SfoOutput::startTransfers()
{
  std::vector<Session*> idleSessions;
  for (auto& session : m_sessions)
  {
    if (session->state() == Session::State::OPEN && session->nTransactions() == 0)
    {
      idleSessions.push_back(session.get());
    }
  }
  std::random_shuffle(idleSessions.begin(), idleSessions.end());
  for (auto session : idleSessions)
  {
    if (m_queuedTransactions.empty())
    {
      break;
    }
    session->asyncTransfer(std::move(m_queuedTransactions.front()));
    m_queuedTransactions.pop_front();
  }
}

void SfoOutput::onSessionConnect(Session* session)
{
  ERS_LOG("Connection to "
      << session->remoteName() << " (" << session->remoteEndpoint() << ") open.");

  ++m_main.isInfo()->OutputActiveConnections;

  checkConnect();
}

void SfoOutput::onSessionConnectError(Session* session, const boost::system::error_code& error)
{
  ers::error(daq::dcm::SfoConnectIssue(ERS_HERE, "Error could not connect",
                                               session->localName(), session->remoteName(),
                                               session->remoteEndPointString(),
                                               error.message(), error.value()));
  checkConnect();
}

void SfoOutput::onSessionDisconnect(Session* session)
{
  ERS_LOG("Connection to "
      << session->remoteName() << " (" << session->remoteEndpoint() << ") closed.");

  --m_main.isInfo()->OutputActiveConnections;

  checkDisconnect();
}

void SfoOutput::onSessionDisconnectError(Session* session,
    const boost::system::error_code& error)
{
  DCM_ERROR(dcm::GenericIssue, "session (to " << session->remoteName() << ") disconnect failed: " << error.message());
}

void SfoOutput::onSessionTransfer(Session* /* session */, std::unique_ptr<Transaction> transaction)
{
  ++m_main.isInfo()->OutputDeliveredEvents;
  for (const auto& stream : transaction->event->streams)
  {
    m_main.isInfo()->OutputDeliveredData += stream.size;
  }
  m_eventBuilder->onEventSent(std::move(transaction->event));
  startTransfers();
  checkStop();
}

void SfoOutput::onSessionTransferError(Session* session, const boost::system::error_code& error,
    std::unique_ptr<Transaction> transaction)
{
    ers::error(daq::dcm::SfoEventTransferIssue(ERS_HERE, "Error could not transfer event",
                                               session->localName(), session->remoteName(),
                                               session->remoteEndPointString(),
                                               error.message(), error.value(),
                                               transaction->event->globalId,
                                               transaction->event->l1Id));
  if (session->state() == Session::State::OPEN)
  {
    if (error == boost::system::errc::timed_out) {
      ++m_main.isInfo()->OutputConnectionTimeouts;
    }
    else {
      ++m_main.isInfo()->OutputConnectionErrors;
    }
    session->asyncDisconnect();
  }

  if (transaction->state == Transaction::State::SENT)
  {
    // We are not sure if the event reached the SFO or not. Use a special error
    // code, so that the EventBuilder can mark it as a possible duplicate
    dcm::EventMaybeDelivered issue(ERS_HERE, transaction->event->globalId, transaction->event->l1Id);
    m_eventBuilder->onEventSentError(issue, std::move(transaction->event));
  }
  else
  {
    dcm::EventNotDelivered issue(ERS_HERE, transaction->event->globalId, transaction->event->l1Id);
    m_eventBuilder->onEventSentError(issue, std::move(transaction->event));
  }

  startTransfers();
  checkStop();
}

void SfoOutput::onTimer(const boost::system::error_code& error)
{
  if (error)
  {
    if (!(m_state == DISCONNECTING && error == boost::asio::error::operation_aborted))
    {
      ers::error(daq::dcm::SfoWatchdogTimerIssue(ERS_HERE, "SFO connection watchdog timer error",
                                                 error.message(), error.value()));
    }
    return;
  }

  auto now = std::chrono::steady_clock::now();
  for (auto& session : m_sessions)
  {
    if (session->state() == Session::State::OPEN)
    {
      // Timeout detection
      session->checkTimeouts(now);
    }
    else if (session->state() == Session::State::CLOSED)
    {
      // Automatic reconnection
      session->asyncConnect();
    }
  }

  // Start transfers that may be hanging after a session was forcibly closed
  if (m_state == RUNNING || m_state == STOPPING) {
    startTransfers();
  }

  m_timer.expires_at(m_timer.expires_at() + std::chrono::seconds(1));
  m_timer.async_wait(std::bind(&SfoOutput::onTimer, this, std::placeholders::_1));
}

void SfoOutput::checkConnect()
{
  if (m_state == CONNECTING)
  {
    // Wait for all sessions to settle: if at least one connected successfully we succeed
    bool hasPendingSession = false;
    bool hasOpenSession = false;
    for (auto& session : m_sessions)
    {
      if (session->state() == Session::State::OPEN_PENDING ||
          session->state() == Session::State::CLOSE_PENDING)
      {
        hasPendingSession = true;
      }
      else if (session->state() == Session::State::OPEN)
      {
        hasOpenSession = true;
      }
    }
    if (!hasPendingSession) {
      if (hasOpenSession)
      {
        m_state = CONNECTED;
        // Start watchdog timer
        m_timer.expires_at(std::chrono::steady_clock::now());
        m_timer.async_wait(std::bind(&SfoOutput::onTimer, this, std::placeholders::_1));
        // Notify the Main
        m_main.onOutputConnect();
      }
      else
      {
        m_state = NONE;
        // Notify the Main
        m_main.onOutputConnectError(make_error_code(boost::system::errc::connection_refused));
      }
    }
  }
}

void SfoOutput::checkStart()
{
  if (m_state == STARTING)
  {
    m_state = RUNNING;
    // Notify the Main
    m_main.onOutputStart();
  }
}

void SfoOutput::checkStop()
{
  if (m_state == STOPPING && m_queuedTransactions.empty())
  {
    for (const auto& session : m_sessions)
    {
      if (session->nTransactions() > 0)
      {
        return;
      }
    }
    m_state = CONNECTED;
    // Notify the Main
    m_main.onOutputStop();
  }
}

void SfoOutput::checkDisconnect()
{
  if (m_state == DISCONNECTING)
  {
    // Wait for all sessions to settle: if all disconnected successfully we succeed
    bool allSessionsClosed = true;
    for (auto& session : m_sessions)
    {
      if (session->state() != Session::State::CLOSED)
      {
        allSessionsClosed = false;
        break;
      }
    }
    if (allSessionsClosed) {
      m_state = NONE;
      // Delete the sessions
      m_sessions.clear();
      // Notify the Main
      m_main.onOutputDisconnect();
    }
  }
}

} // namespace dcm
} // namespace daq
