#include "dcm/EventBuilderMultiStripping.h"

#include "CTPfragment/CTPfragment.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/compression.h"

// Max number of concurrent emon channels
#define MAX_EMON_CHANNELS 128
// Event queue size of each channel
#define MAX_EMON_EVENTS 10

namespace daq
{
namespace dcm
{

// Constructor initializes EventBuilder with NULL processor and output
EventBuilderMultiStripping::EventBuilderMultiStripping( Main& main, std::uint32_t runType,
                                                        const std::vector<std::uint32_t>& robIds ) :
  m_main(main), m_processor(nullptr), m_output(nullptr), m_builderStop(false),
  m_builderThread(&EventBuilderMultiStripping::builderTask, this),
  m_runType(runType), m_robIds(robIds), m_compAlg(eformat::UNCOMPRESSED), m_ctpFromRoI(false)
  {}


// The destructor (terminates the event builder and sampler threads)
EventBuilderMultiStripping::~EventBuilderMultiStripping()
{
  // Tell the thread to stop and wait for it to stop
  m_builderStop = true;
  m_builderCond.notify_all();
  m_builderThread.join();
  // Terminate and destruct the sampling
  m_sampler.reset();
  ERS_LOG("EventBuilderMultiStripping destructed");
}


// Task executed by event builder thread
void EventBuilderMultiStripping::builderTask()
{
  try
  {
    ERS_LOG("Event builder thread starting");
    while(true)
    {
      std::unique_ptr<Event> event;
      { // wait for an event and extract it, or stop the thread if requested to
        std::unique_lock<std::mutex> lock(m_builderMtx);
        while (!m_builderStop && m_builderQueue.empty())
          m_builderCond.wait(lock);
        if (m_builderStop)
          break;
        event.swap (m_builderQueue.front());
        m_builderQueue.pop_front();
      }
      buildEvents(std::move(event));
    }
    ERS_LOG("Event builder thread terminating");
  }
  catch (std::exception& ex)
  {
    ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled exception in Event Builder thread", ex));
    sleep(2); std::abort();
  }
  catch (...)
  {
    ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Unhandled unknown exception in Event Builder thread"));
    sleep(2); std::abort();
  }
}


// Configures the EventBuilder
void EventBuilderMultiStripping::initialize (Processor* processor, Output* output)
{
  ERS_ASSERT (processor != nullptr);
  ERS_ASSERT (output != nullptr);
  m_processor = processor;
  m_output = output;
  m_sampler.reset(new SamplerSingleStripping(m_main.partition(), MAX_EMON_CHANNELS, MAX_EMON_EVENTS, this));
  if (m_main.applicationConf().get_compressionAlgorithm() == dal::DcmApplication::CompressionAlgorithm::ZLIB)
    m_compAlg = eformat::ZLIB;
  
  // Parse hiddenParams for parameter used for forcing "CTP data from RoI"
  std::vector<std::string> vs = m_main.applicationConf().get_hiddenParameters();
  for (auto s : vs)
  {
    if(s.find("ctpFromRoI") != std::string::npos )
    {
      m_ctpFromRoI=true;
      ers::warning( ConfigurationIssue( ERS_HERE, 
        "NB: found 'ctpFromRoI' in hiddenParameters oks field, CTP data will be retrived from RoI instead of from CTP ROB") );
    }
  }  
}


// Build the submitted event: queue it for the builder thread to process it
void EventBuilderMultiStripping::asyncBuildAndSend( std::unique_ptr<Event> event )
{
  ERS_ASSERT (m_processor != nullptr);
  ERS_ASSERT (m_output != nullptr);
  ERS_ASSERT (event.get() != nullptr);

  // Queue the event to process by the event builder thread and notify it that an event has been queued
  {
    std::unique_lock<std::mutex> lock(m_builderMtx);
    m_builderQueue.push_back(std::move(event));
  }
  m_builderCond.notify_all();
}


// Send the already built event directly to the output
void EventBuilderMultiStripping::asyncSend( std::unique_ptr<Event> event )
{
  // Submit the event directly to the output
  Event *tmpEvent = event.release();
  Output *output = m_output;
  m_main.ioService().post ([output, tmpEvent] {
      output->asyncEventSend( std::unique_ptr<Event>(tmpEvent) );
  });
}


// Informs the EventBuilder that the given event was successfully sent by the Output
void EventBuilderMultiStripping::onEventSent( std::unique_ptr<Event> event )
{
  // forwards the callback to the processor
  m_processor->onBuildAndSend( std::move(event) );
}


// Informs the EventBuilder that the given event failed to be sent by the Output
void EventBuilderMultiStripping::onEventSentError(const ers::Issue& issue,
                    std::unique_ptr<Event> event )
{
  // forwards the callback to the processor
  m_processor->onBuildAndSendError( issue, std::move(event) );
}


// Return true if all stream tags are of type monitoring
static inline bool allStreamTagsAreMonitoring( const std::vector<eformat::helper::StreamTag>& streamTags )
{
  for (const auto& streamTag : streamTags )
    if (streamTag.type != "monitoring")
      return false;
  return true;
}


// Send the built event to output or drop it if its a monitoring event
void EventBuilderMultiStripping::eventBuilt( std::unique_ptr<Event> event )
{
  ERS_ASSERT(event.get() != nullptr);

  // Update stats
  m_main.isInfo()->EbEvents++;
  ERS_DEBUG(3, "Event built (GID: " << event->globalId << ").");

  // If there are no events to send to SFO, return callback to processor, else send events to SFO
  if (event->streams.empty())
  {
    Event *tmpEvent = event.release();
    Processor *processor = m_processor;
    m_main.ioService().post ([processor, tmpEvent] {
        processor->onBuildAndSend( std::unique_ptr<Event>(tmpEvent) );
    });
  }
  else
  {
    // FIXME: what definition of "event size" should we use?
    m_main.eventHistograms().fillEventSize_kB(event->streams[0].size / 1000.);

    // Output the event to SFO
    Event *tmpEvent = event.release();
    Output *output = m_output;
    m_main.ioService().post ([output, tmpEvent] {
        output->asyncEventSend( std::unique_ptr<Event>(tmpEvent) );
    });
  }
}


// Build the serialized event header used as template for all events
void EventBuilderMultiStripping::buildEventHeader(Event* event)
{
  // Check parameter integrity
  if (!event)
    throw EventBuilderIssue(ERS_HERE, "EventBuilderMultiStripping::buildEventHeader() "
                                      "received a null event");

  // Locate a CTP fragment (pick the one with the smallest robId)
  const auto robInfoIndex = event->block.robInfoIndex();
  
  eformat::ROBFragment<const std::uint32_t*> ctpDAQ;
  eformat::ROBFragment<const std::uint32_t*> ctpRoI;
  std::uint32_t ctpDAQRobId = 
    eformat::helper::SourceIdentifier(eformat::TDAQ_CTP, 0).simple_code();
  std::uint32_t ctpRoIRobId = 
    eformat::helper::SourceIdentifier(eformat::TDAQ_CTP, 1).simple_code();
  
  auto itDAQ = robInfoIndex.find(ctpDAQRobId);
  if (itDAQ != robInfoIndex.end())
  {
    ctpDAQ = itDAQ->second->robFragment();
  }

  auto itRoI = robInfoIndex.find(ctpRoIRobId);
  if (itRoI != robInfoIndex.end())
  {
    ctpRoI = itRoI->second->robFragment();
  }
  
  // Get all the FullEventFragment parameters required to instantiate it
  std::uint32_t src_id = eformat::helper::SourceIdentifier(eformat::TDAQ_EVENT_FILTER, 0).code();
  std::uint32_t bc_time_secs = 0, bc_time_nsec = 0;
  std::uint64_t global_id = event->globalId;
  std::uint32_t run_type = m_runType;
  std::uint32_t run_no = 0;
  std::uint16_t lumi_block = event->lumiBlock;
  std::uint32_t lvl1_id = event->l1Id;
  std::uint16_t bc_id = event->bcId;
  std::uint8_t  lvl1_type = 0;

  eformat::ROBFragment<const std::uint32_t*> ctp;
  
  // Check if the CTP DAQ fragment is available and valid
  // otherwise use the CTP RoI fragment, if available
  if (ctpDAQ.start() && ctpDAQ.nstatus() && ctpDAQ.status()[0] == 0) 
  {
    // This is a good one, as the first status word is zero
    ctp = ctpDAQ;
  } 
  else if(ctpRoI.start())
  {
    ctp = ctpRoI;
  }

  // Forced "CTP from RoI" from OKS
  if(m_ctpFromRoI)
  {
    ctp = ctpRoI;
    if(!ctpRoI.start())
    {
      ers::warning( GenericIssue( ERS_HERE, 
        "Missing CTP info in RoI fragment (NB: forced CTP data from RoI, check configuration )" ));
    }
  }

  // Use parameters extracted from CTP ROB
  if (ctp.start())
  {
    CTPfragment::time(&ctp, bc_time_secs, bc_time_nsec);
    run_no = ctp.rod_run_no();
    lvl1_type = ctp.rod_lvl1_trigger_type();
  }

  std::uint32_t main_run_no = m_main.runParams().run_number;
  if (main_run_no != run_no && event->forceAccept == Event::ForceAccept::NONE) {
    ers::warning(dcm::RunNoMismatchIssue(ERS_HERE, main_run_no, run_no,
                                         global_id, lvl1_id));
  }
  // ERS_LOG("Event: " << global_id << " main_run_no: " << main_run_no
  //        << " run_no: " << run_no
  //        << " forceAccepted: 0x" << std::hex << (int)event->forceAccept << std::dec );

  // Instantiate the event used for header template
  eformat::write::FullEventFragment header(src_id, bc_time_secs, bc_time_nsec, 
					   global_id, run_type,
					   run_no, lumi_block, lvl1_id, 
					   bc_id, lvl1_type);

  std::uint32_t nbrWords, *data;

  // Build Status word flags
  /*
    GenericStatus :
      UNCLASSIFIED = 0x0,
      BCID_CHECK_FAIL = 0x1,
      LVL1ID_CHECK_FAIL = 0x2,
      TIMEOUT = 0x4,
      DATA_CORRUPTION = 0x8,
      INTERNAL_OVERFLOW = 0x10,
      DUMMY_FRAGMENT = 0x20

    FullEventStatus :
      L2_PROCESSING_TIMEOUT = 0x1,
      L2PU_PROCESSING_TIMEOUT = 0x2,
      SFI_DUPLICATION_WARNING = 0x4,
      HLTSV_DUPLICATION_WARNING = 0x8,
      L2PSC_PROBLEM = 0x10,
      DCM_PROCESSING_TIMEOUT = 0x100,
      HLTPU_PROCESSING_TIMEOUT = 0x200,
      DCM_DUPLICATION_WARNING = 0x400,
      DCM_RECOVERED_EVENT = 0x800,
      PSC_PROBLEM = 0x1000,
      DCM_FORCED_ACCEPT = 0x2000,
      PARTIAL_EVENT = 0x8000
   */
  std::uint32_t firstStatusWord = 0;
  if (event->forceAccept != Event::ForceAccept::NONE )
  {
    if (event->forceAccept == Event::ForceAccept::HLTSV_REASSIGN )
      firstStatusWord = eformat::helper::Status( eformat::DATA_CORRUPTION,
                                                 eformat::HLTSV_DUPLICATION_WARNING).code();

    else if (event->forceAccept == Event::ForceAccept::HLTPU_TIMEOUT )
      firstStatusWord = eformat::helper::Status( eformat::TIMEOUT,
                                                 eformat::DCM_PROCESSING_TIMEOUT).code();
    else if (event->forceAccept == Event::ForceAccept::L1ID_MISMATCH )
    {
      // do nothing. The flag is set below
    }
    else
    {
      // Make sure all ForceAccept conditions are met
      ERS_ASSERT( event->forceAccept == Event::ForceAccept::HLTPU_CRASH ||
                  event->forceAccept == Event::ForceAccept::FETCHROBS_ERROR ||
                  event->forceAccept == Event::ForceAccept::INVALID_ROBID_ERROR );
      firstStatusWord = eformat::helper::Status( eformat::DATA_CORRUPTION,
                                                 eformat::DCM_FORCED_ACCEPT).code();
    }
  }
  if (event->possibleDuplicate)
    firstStatusWord |= eformat::helper::Status( eformat::DATA_CORRUPTION,
                                                eformat::DCM_DUPLICATION_WARNING).code();

  //We don't add the firstStatusWord in case of pscErrors, we just add the pscErrors vector 
  //See https://its.cern.ch/jira/browse/ADHI-4934
  //if (!event->pscErrors.empty())
  //  firstStatusWord |= eformat::helper::Status( eformat::DATA_CORRUPTION,
                                                //eformat::PSC_PROBLEM).code();

  // eformat::PARTIAL_EVENT is set by serializeEvent()
  if (event->hasBcIdMismatch)
    firstStatusWord |= eformat::helper::Status( eformat::BCID_CHECK_FAIL, 0).code();
  if (event->hasL1IdMismatch)
    firstStatusWord |= eformat::helper::Status( eformat::LVL1ID_CHECK_FAIL, 0).code();

  // Add first status word or pscError vector content if not empty
  if(event->pscErrors.empty())
    nbrWords = 1;
  else
    nbrWords = event->pscErrors.size();
  data = event->allocate(nbrWords);
  *data = firstStatusWord;
  if( !event->pscErrors.empty() )
    std::copy(event->pscErrors.begin(), event->pscErrors.end(), data);
  header.status(nbrWords, data);

  if(ctp.start())
  {
    // Add lvl1 Trigger info : concatenation of these vectors in that order
    std::vector<std::uint32_t> vec1, vec2, vec3;
    vec1 = CTPfragment::triggerDecisionBeforePrescales(&ctp);
    vec2 = CTPfragment::triggerDecisionAfterPrescales(&ctp);
    vec3 = CTPfragment::triggerDecisionAfterVeto(&ctp);
    
    /*** HACK MODE ON ***/
    // Protect for (RoI) CTP fragment without before and after prescale
    // bits
    if (vec1.empty()) 
    {
      vec1.resize(vec3.size(), 0);
    }

    if (vec2.empty()) 
    {
      vec2.resize(vec3.size(), 0);
    }
    /*** HACK MODE OFF ***/

    nbrWords = vec1.size() + vec2.size() + vec3.size();
    if (nbrWords)
    {
      data = event->allocate(nbrWords);
      {
        std::uint32_t *ptr = data;
        ::memcpy(ptr, vec1.data(), vec1.size()*sizeof(std::uint32_t));
        ptr += vec1.size();
        ::memcpy(ptr, vec2.data(), vec2.size()*sizeof(std::uint32_t));
        ptr += vec2.size();
        ::memcpy(ptr, vec3.data(), vec3.size()*sizeof(std::uint32_t));
      }
      header.lvl1_trigger_info(nbrWords, data);
    }
  }

  // Add hltpu (Event Filter) Trigger info
  nbrWords = event->hltpuTriggerInfo.size();
  if (nbrWords)
  {
    data = event->allocate(nbrWords);
    ::memcpy( data, event->hltpuTriggerInfo.data(), nbrWords * sizeof(std::uint32_t) );
    header.event_filter_info(nbrWords, data);
  }

  // Add output streamTags to header (stripped of monitoring streamTags)
  nbrWords = eformat::helper::size_word(event->outputStreamTags);
  if (nbrWords)
  {
    data = event->allocate(nbrWords);
    eformat::helper::encode(event->outputStreamTags, nbrWords, data);
    header.stream_tag(nbrWords, data);
  }

  // Set compression flags
  header.compression_type(m_compAlg);

  // Bind the fragments of the constructed event
  const eformat::write::node_t *node = header.bind();

  // Serialize the event in the event block
  nbrWords = header.size_word();
  data = event->allocate(nbrWords);
  std::uint32_t res = eformat::write::copy(*node, data, nbrWords);
  if (res != nbrWords)
  {
    EBFailedSerializingEventHeader issue(ERS_HERE, global_id, lvl1_id);
    ers::error(issue);
    throw issue;
  }
  event->eventHeader = data;
}


// Serialize the event using the header found in the given event, and set of robIds
void EventBuilderMultiStripping::serializeEvent( const Event& event,
                                                 const eformat::helper::StreamTag& streamTag,
                                                 const std::uint32_t *templateHeader,
                                                 std::uint32_t*& data,
                                                 std::uint32_t& nbrBytes)
{
  ERS_ASSERT(event.eventHeader != nullptr);

  // Construct the event using the header as template
  eformat::write::FullEventFragment outEvent(templateHeader);

  // Build vector of robIds
  std::vector<std::uint32_t> robIds;

  // If there is no specific robs and no specific dets, append all ROBs
  if (streamTag.robs.empty() && streamTag.dets.empty())
  {
    // collect all robs in the SharedBlockArray block (in memory)
    for (auto it = event.block.robInfoIndex().begin(); it != event.block.robInfoIndex().end(); ++it)
      outEvent.append_unchecked(it->second->robPtr());
  }
  else
  {
    // Set the partial event flag in the first status word
    std::uint32_t statusByteSize = outEvent.nstatus()*sizeof(std::uint32_t);
    std::uint32_t *statusData = event.allocate(outEvent.nstatus());
    ::memcpy(statusData, outEvent.status(), statusByteSize);
    *statusData |= eformat::helper::Status(eformat::UNCLASSIFIED, eformat::PARTIAL_EVENT).code();
    outEvent.status(outEvent.nstatus(), statusData);

    // Get robIds from rob set
    for( auto robId : streamTag.robs )
      robIds.push_back(robId);

    // Get robIds from det set (considering only ROBs in memory)
    for( auto det : streamTag.dets )
    {
      const std::uint32_t loId = eformat::helper::SourceIdentifier( det, 0 ).simple_code();
      const std::uint32_t hiId = eformat::helper::SourceIdentifier( det, ~0 ).simple_code();
      auto it = event.block.robInfoIndex().lower_bound(loId);
      auto end = event.block.robInfoIndex().upper_bound(hiId);
      for( ;it != end; ++it )
        robIds.push_back(it->first);
    }

    // sort robIds and remove duplicates
    std::sort(robIds.begin(), robIds.end());
    robIds.erase(std::unique(robIds.begin(), robIds.end()), robIds.end());

    // append robs specified in the stream tag and in memory
    for( auto robId : robIds )
    {
      SharedBlockArray::RobInfo* robInfo = event.block[robId];
      if (robInfo)
        outEvent.append_unchecked(robInfo->robPtr());
    }
  }

  // Bind the fragments of the constructed event
  const eformat::write::node_t *node = outEvent.bind();

  // Serialize the event in the event block storage
  std::uint32_t nbrWords = outEvent.size_word();
  nbrBytes = nbrWords * sizeof(std::uint32_t);
  data = event.allocate(nbrWords);
  std::uint32_t res = eformat::write::copy(*node, data, nbrWords);
  if (res != nbrWords)
  {
    EBFailedSerializingEvent issue(ERS_HERE, event.globalId, event.l1Id);
    ers::error(issue);
    throw issue;
  }
}


// Build the event
void EventBuilderMultiStripping::buildEvents (std::unique_ptr<Event> event)
{
  ERS_ASSERT(event.get() != nullptr);
  if (event->block.robInfoIndex().empty())
  {
    dcm::EBEventWithoutROBs issue(ERS_HERE, event->globalId, event->l1Id);
    ers::error(issue);
    throw issue;
  }

  // If the event is to be built as possible duplicate
  if (event->possibleDuplicate)
  {
    event->streamTags.clear();
    event->streamTags.emplace_back("possibleDuplicate", eformat::DEBUG_TAG, false);
  }

  // Build list of streamTags stripped of monitoring tag
  // Note: outputStreamTags may be empty
  event->outputStreamTags.clear();
  for (const auto& tag : event->streamTags)
  {
    if (tag.type != "monitoring")
      event->outputStreamTags.emplace_back(tag);
  }

  // Build the event header
  buildEventHeader(event.get());

  // Get a handy reference on the vector of output streamTags (stripped of monitoring streamTag)
  std::vector<eformat::helper::StreamTag>& streamTags(event->outputStreamTags);

  // Get a handy reference on the vector of streams and clear it
  std::vector<Event::Stream>& streams(event->streams);
  streams.clear();

  // Iterate over all streamTags (Note: output list may be empty)
  for (size_t tagIdx = 0; tagIdx < streamTags.size(); ++tagIdx)
  {
    // Get a handy reference on the currently processed streamTag
    eformat::helper::StreamTag& streamTag(streamTags[tagIdx]);

    // Try locating a stream with same set of dets and robs
    size_t streamIdx = 0;
    while (streamIdx < streams.size() &&
           !hasSameSetOfRobs(streamTags[streams[streamIdx].tags[0].idx], streamTag))
      ++streamIdx;;

    // If a stream with the same set of dets and robs is found
    if (streamIdx < streams.size())
      // Add the streamTag in the list
      streams[streamIdx].addTag(streamTag.name, streamTag.type, tagIdx);
    else
    {
      // Serialize the event with the specified set of robs
      std::uint32_t *data, nbrBytes;
      serializeEvent (*event, streamTag, event->eventHeader, data, nbrBytes );

      // Append a new stream with the serialized event
      event->streams.emplace_back(streamTag.name, streamTag.type, tagIdx, data, nbrBytes);
    }
  }

  // Sample the event (Must be done here because it may have to serialize the singleStrippedEvent)
  if (m_sampler)
    m_sampler->sample(*event);

  // call eventBuilt by ioService
  EventBuilderMultiStripping* self(this);
  Event *tmpEvent = event.release();
  m_main.ioService().post ([self, tmpEvent] {
    self->eventBuilt( std::unique_ptr<Event>(tmpEvent) );
  });
}


} // namespace dcm
} // namespace daq

