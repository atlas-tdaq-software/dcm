# dcm

## tdaq-09-03-00
Fix for Jira ADHI-4503, added ERS_DEBUG messages for easing debugging.
Moved all python scripts (dcm/test) to python3.

## tdaq-09-02-01
New features include:
dfinterfaceDCM for Jira ADHI-4792: changed the missing CTP fragment management. Instead of throwing an error, a empty fragment is generated and passed to the HLT.
dfinterfaceDCM for Jira ADHI-4795: Added the function getRobs_unprotected, for the requests of the ROBs. This function requires to be called from a protected function (getRobs, mayGetRobs. Both functions now lock a mutex at the function very beginning.)

## tdaq-09-00-00
New features include:
dfinterfaceDCM: added an operationTimeOut issue when the tryGetNextUntil function is not able to retrieve an event.
This is required to fix the problem of Jira ADHI-4728.
Processor.cxx: increased the IDLE timeout. Required for testing for the Jira ADHI-4770. 

## tdaq-08-03-01 – Intermediate release 2019


## tdaq-07-01-00 - Production release for 2017-2018

