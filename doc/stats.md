Event Journey through DCM
-------------------------
Understanding precisely the meaning of statistics published by DCM requires
knowledge on its internal functioning.

Outline of event journey through DCM for ATLAS partition (`Processor` is
`HltpuProcessor`, `EventBuilder` is `EventBuilderMultiStripping`, `Output` is
`SfoOutput`)

    Processor::tryFetchL1R()
      if (SFO backpressure) do not request, set hltpu state to SUSPENDED
      otherwise:
    Processor::fetchL1R()
      Event life in DCM starts here
      PU becomes Idle
    L1Source asynchronously requests L1R to HLTSV, and eventually calls:
    Processor::onFetchL1R()
      PU Free -> Busy, and no longer Idle
      The event is now considered "in" DCM
    Interactions between HLTPU and DCM to get ROBs:
      fetchRobs()
      onFetchRobs()

    Reject:
    Processor::markDone()
      Event is marked done: this is propagated to L1Source which asynchronously
        informs HLTSV
    Processor::finalizeProcessing()
      PU Busy -> Free
      Processing is now completed
    Processor::finalizeEvent()
      Event is counted as Done
      Event life in DCM stops here, no longer "in" DCM
      Event is destroyed
    Processor::setStateIdle()
      Gets ready to receive messages from HLTPU
      At this point upon HLTPU new event request the event "loop" starts over at
      tryFetchL1R()

    Accept:
    Processor::finalizeAccept()
      Processor::finalizeProcessing()
        PU Busy -> Free
        Processing is now completed
      Event is now on output
      Event is forwarded to EventBuiler which processes asynchronously (see below)
    Processor::setStateIdle()
      Gets ready to receive messages from HLTPU
      At this point upon HLTPU new event request the event "loop" starts over at
      tryFetchL1R()

    EventBuilder::asyncBuildAndSend()
      Event is added to a queue
    EventBuilder::builderTask() pops event from queue and calls
    EventBuilder::buildEvents()
      Build events
      If (sampler) feed event to sampler
    EventBuilder::eventBuilt()
      Output::asyncEventSend() add event to a queue
    Output asynchronously transfers queued event
      All steps are asynchronous
      Sends assign message
      Receives assignment ack
      Sends event
      Receives event ack, triggers callbacks:
        EventBuilder::onEventSent()
        Processor::onBuildAndSend()
          Processor::markDone()
            Event is marked done: this is propagated to L1Source which asynchronously
            informs HLTSV
          Event is no longer on output
          Processor::finalizeEvent()
            Event is counted as Done
            Event life in DCM stops here, no longer "in" DCM
            Event is destroyed

![DCM Stats](dcmstats.png)

Statistics
----------

`ProxFreePUs`
: Number of PUs that are not processing an event, i.e. between end of processing
(`finalizeProcessing()`) and reception of L1R (`onFetchL1R()`). A PU is Free xor
Busy.

`ProxBusyPUs`
: Number of PUs that are processing an event, i.e. between reception of L1R
(`onFetchL1R()`) and end of processing (`finalizeProcessing()`). A PU is Free xor
Busy.

`ProxL1Events`
: Incremented at reception of L1R (`onFetchL1R()`)

`ProxReqEvents`
: Incremented at request of L1R (`fetchL1R()`)

`ProxIdlePUs`
: Number of PUs waiting for a requested L1R, i.e. `ProxReqEvents`-`ProxL1Events`,
i.e. between `fetchL1R()` and `onFetchL1R()`. Idle PUs are a subset of Free PUs.
See Processor documentation on discrepancy between Idle and Free.

`ProxDoneEvents`
: Incremented at event destruction (`finalizeEvent()`). Note that
`ProxDoneEvents` is not exactly synchronized with `markDone()` which triggers
the ack to HLTSV.

`EventsInside`
: Numbers of events between reception of L1R and destruction, i.e. `ProxL1Events`-`ProxDoneEvents`, i.e. between `onFetchL1R()` and  `finalizeEvent()`.
This excludes the period between event object creation (`onFetchL1R()`) and
reception of L1R, when no data is associated with the event object.

`EventsOnOutputQueue`
: Number of events between accept/reject and event destruction, i.e.
`= ProxRejEvents + ProxAccEvents - ProxDoneEvents`. It matches the internal `Processor::m_nOutputEvents` used for backpressure definition.

Histograms
----------

`InterAssignmentTime_us`
: Duration between consecutive L1R receptions (`onFetchL1R()`)

`WaitTime_ms`
: Duration between event creation and reception of L1R, i.e. between
`fetchL1R()` and `onFetchL1R()`

`DataCollectionTime_ms`
: Sum of all `fetchRobs()`--`onFetchRobs()` durations for an event

`ProcessingTime_ms`
: Duration between reception of L1R and accept/reject, i.e. between
`onFetchL1R()` and `finalizeProcessing()`

`EventLifeTime_ms`
: Duration bewtween reception of L1R and event destruction, i.e. between
`onFetchL1R()` and `finalizeEvent()`

`EventOwnershipTime_ms`
: Duration between reception of L1R and event marked Done, i.e. between
`onFetchL1R()` and `markDone()`

Remarks:
  - For rejected events: life time ≈ processing time ≈ ownership time
  - For accepted events: life time ≈ ownership time
  - Event life time does not start at event creation but at reception of first data (L1R)
