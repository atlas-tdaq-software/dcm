The `Processor` Interface
=========================

The `Processor` implemented in the `Processor.h` and `Processor.cxx` files is an 
interface. Two implementations exist: `DummyProcessor` and `HltpuProcessor`
classes. The former simulates processing. The later handles connections with
the real HLTPU processes. 

The `Processor` class has been designed so that it implements the common business 
logic. The derived classes need only to implement the specific actions.

Data structure architecture
---------------------------

     ___________           _______
    |           | 1     N |       |
    | Processor |<------->| HLTPU |
    |___________|         |_______|
    
`Processor` is a class and `HLTPU` is a struct defined inside the Processor 
class. The `Processor` class contains:

- a state information
- references to the L1Source, DataCollector and EventBuilder instances
- some time points, statistics and histograms
- a vector of pointers to `HLTPU` strucs

There is one `HLTPU` struct instance per HLTPU instance. The `HLTPU` struct 
contains:

- the HLTPU name
- a time point (end of IDLE or PROCESSING time)
- the currently processed event
- some state information
- a pointer to the Processor instance


Execution logic
---------------

### Processor state transition diagram

The `Processor` states match the run control states relatively closely. 
When the run control requests a stop, the `Processor` states goes
from `Running` to `Connected`. The intermediate `Reset` transition allows faster 
stopping since the `EventBuilder` can be stopped once the `Processor` is stopped.
\newpage
     
              |   Processor instantiation
              |
         _____v_______
        |             |
        | Unconnected |
        |_____________|
              |  ^
       Config |  | Unconfig
        ______v__|_____      
       |               |
       |   Connected   |
       |_______________|
          |         ^
          |         |  (when all HLTPU are disconnected)
          |   ______|______
          |  |             |
    Start |  |  Resetting  |
          |  |_____________|
          |         ^
          |         |  Reset
          |   ______|______
          |  |             |
          |  |   Stopped   |
          |  |_____________|
          |         ^
          |         |  (when no more events in the DCM)
          |     ____| ____
          |    |          |
          |    | Stopping |
          |    |__________|
          |         ^
          |         |  Stop
        __v_________|__
       |               |
       |    Running    |
       |_______________|
       
The state transitions is driven by the main routine following directives of the 
run controller.
       
\newpage
### HLTPU state transition diagram

                              _______________ 
                             |               |
                             |  Initialized  |
                             |_______________|
                                     |  
                        Attach HLTPU |  
                              _______v_______
                             |               |
                             |     Idle      |<-----------------+
                             |_______________|                  |
                                     |                          |
                      event request  |                          |
             ___________      _______v_______     ____________  |
            |           |    |               |   |            | |
            | Suspended |<-->|  TryFetchL1R  |-->| Terminated | | 
            |___________|    |_______________|   |____________| |
                                |         ^             ^       |
                     fetch L1R  |         |  error      |       |
                              __v_________|__           |       |
                             |               | Stop     |       |
                             |  FetchingL1R  |----------+       |
                             |_______________|                  |
          ______________             |                          |
         |              |            |  L1R fetched             |
         | FetchingRobs |     _______v_______                   |
         |______________|<-->|               |  Reject          |
       _________________     |  Processing   |----------------->|
      |                 |<-->|_______________|                  |
      | FetchingAllRobs |            |                          |
      |_________________|            |  Accept or               |
          ______________             |  force accept            |
         |              |            |                          |
         | FetchingRobs |     _______v_______                   |
         |______________|<-->|               |                  |
       _________________     |   Accepting   | Forward event to |
      |                 |<-->|_______________| event assembler  |
      | FetchingAllRobs |            |__________________________|
      |_________________| 


HLTPU attachment will be rejected if the `Processor` is not in the `Connected` 
or `Running` state, or if the maximum connected HLTPU is reached. 

When an event is requested, the HLTPU may get into the `Suspended` state when 
the `Processor` is not in the `Running` state, or if backpressure kicked 
in (`m_nOutputEvents >= m_maxOutputEvents`). The HLTPU leaves the `Suspended`
state when the `Processor` is started, or the backpressure is released. 

When the `Processor` is requested to stop and the HLTPU is in the `TryFetchL1R` 
or `FetchingL1R` states, the HLTPU is detached from the processor and goes into 
the `Terminated` state. 

### Code architechture

The code is designed so that all the common business logic may be contained in
the `Processor` class. `DummyProcessor` and `HltpuProcessor` contains only
their specific tasks. 


#### HLTPU struct creation

When an `HLTPU` struct is created, it is in the `Initialized` state. 
`DummyProcessor` will automatically create the required number of HLTPU structs
when the `Processor` starts. `HltpuProcessor` will create the structs when a 
HLTPU process connects to the DCM.

An attempt to attach the HLTPU is then performed. It will be rejected if the 
`Processor` is not in the `Connected` or `Running` state, or if the maximum 
number of connected HLTPU has already been reached. 

If it succeeds, the method `setStateIdle()` is called. 

#### setStateIdle()

This method moves the HLTPU into the `Idle` state. The method is abstract
because `DummyProcessor` and `HltpuProcessor` do different operations.

`DummyProcessor` does nothing beside changing the state and directly calling
`tryFetchL1R()`. `HltpuProcessor` starts waiting for an event request 
message from the HLTPU processor. When this message is received, 
`tryFetchL1R()` is called. 

#### tryFetchL1R()

The action of this method will depend on the state of the `Processor`. 

If the `Processor` state is `Connected`, the HLTPU process connected and issued
an event request before the `Processor` was started. The HLTPU is then put into
`Suspended` state. 

If the `Processor` state is `Stopping` or `Stopped`, the method `noMoreEvents()`
is called. It notifies the HLTPU process that it must terminate. 

If the state is `Running`, the action will depend on the backpressure. If
backpressure active, the HLTPU will be put into `Suspended` state. Otherwise
the `fetchL1R()` is called.

This method is not allowed to be called in any other `Processor` state. An
assert failure will be triggered in that case. 

#### unsuspendAllHltpu()

This method calls `tryFetchL1R()` for all suspended HLTPUs. It is called when
the `Processor` is started or stopped, or when the deletion of an event
(`finalizeEvent()`) releases the backpressure. 

#### fetchL1R()

This method sets the HLTPU state into `FetchingL1R`, creates the `event` 
instance and forwards the call to the `L1Source`. On completion, the callback 
method `onFetchL1R()` or `onFetchL1RError()` is called. 

#### onFetchL1R()

Callback called when a L1R is received from the HLTSV. The HLTPU state changes
to `Processing` and the HLTPU is initialized for the event processing. Some 
stats is updated and time is recorded to compute processing time.

If the event was reassigned by the HLTSV, or some invalid data is detected it is 
force-accepted, otherwise the method `processL1R()` is called.

#### processL1R()

This method is abstract because `DummyProcessor` and `HltpuProcessor` have a
specific implementation. At this stage, the HLTPU is in the `Processing` 
state depicted in the state diagram above. 

`DummyProcessor` calls a method that will simulate a level 2 processing (prior
to gathering all ROBs). It will requests some ROBs by calling `fetchRobs()`. 

`HltpuProcessor` forwards the L1R to the HLTPU process and waits for a 
message from the HLTPU. When the `fetchRobs` message is received, the method 
`fetchRobs()` is called. When the `accept` message is received, some checking is
performed. If something bogus is detected, `forceAccept()` is called. Otherwise
`accept()` is called. When the `reject` message is received, the `reject()`
method is called. 
 
#### onFetchRobs()
 
Callback method called when fetching ROBs is completed. This method may be 
called in multiple conditions. 

- while a force accept is in progress that requested to fetch all Robs. In this
  case we call `finalizeForecAccept()`. 
- while an event is accepted and requires to fetch some Robs. The method 
  `finalizeAccept()` is called and `setStateIdle()` after it to close the state
  transision loop. 
- while an event is processed and ROBs where requested. The overriden method
  `processRobs()` is then called. 

#### processRobs()

This method is called when the event is processed. It is overriden by 
`DummyProcessor` and `HltpuProcessor` to implement specific behavior.

`DummyProcessor` will simulate processing of a random duration and randomly
chose between requesting more ROBs, requesting all ROBs, accepting or rejecting 
the event. It will call the corresponding `fetchRobs()`, `accept()` or `reject()`
method. 

#### reject()

This method will call in sequence the `finalizeProcessing()`, the 
`finalizeEvent()`, and the `setStateIdle()` methods. The later will close the 
loop of state transitions.

#### finalizeProcessing()

This method simply update stats and histograms. It is also called when accepting
an event is completed. 

#### finalizeEvent()

This method updates histograms and stats. It will call `unsuspedAllHltpu()` when
the backpressure is released. It deletes the event object and calls 
`tryFinalizeStop()` to update the stopping satus if required.

#### tryFinalizeStop()

If the Processor is `Stopping` and we just deleted that last processed even,
we change he state of the Processor into the `Stopped` state. 

#### accept()

This method performs some checking on the event and calls `forceAccept()` if a 
problem is detected. It then checks if ROBs are missing and it should fetch all
ROBs in witch case it calls `fetchAllRobs()`, or only a subset of ROBs need to
be fetched in which case it calls `fetchRobs()`. If no ROB need to be fetched,
it calls in sequence `finalizeAccept()` and `setStateIdle()`. 

HLTPU States
------------
DCM classifies processing units in three non-separated states:

- busy: the PU is processing an event;
- free: the PU is not processing;
- idle: the PU has requested an L1R and is waiting for it; all idle PUs and also
  free (i.e. idle is a subset of free).

DCM publishes the following corresponding counters:

- `ProxBusyPUs`: number of PUs currently processing;
- `ProxFreePUs`: number of PUs currently not processing;
- `ProxIdlePUs`: number of PUs currently waiting for a request L1R.

Internally, `ProxBusyPUs` and `ProxFreePUs` are directly set in the main
statistics objects, while `ProxIdlePUs` is computed on publication
(`Main::isInfoCallBack`) from:

    uint64_t idlePUs = dcm->ProxReqEvents - dcm->ProxL1Events;

- `ProxReqEvents` is the number of request L1R;
- `ProxL1Events` is the number of received L1R;

Here's the sequence of updates of these counters:

                              Busy Free (Req L1E => Idle)
        init value               0    0    0   0       0
        attachPU()               0    1    0   0       0
    /-> fetchL1R()               0    1    1   0       1
    |   receivedL1R()            1    0    1   1       0
    \-- finalizeProcessing()     0    1    1   1       0

Thus, by design, a PU is either free *xor* busy.

Differences between free and idle can be observed for events between
`finalizeProcessing()` and `fetchL1R()`. Here's an a summary of the sequence of
events between these
calls:

    finalizeProcessing()
    finalizeEvent()
      data_collector.clearEvent()
      destroy event
      tryFinalizeStop()
        checks if stop was requested
    setStateIdle()
      DummyProcessor directly calls tryFetchL1R()
      HltpuProcessor calls asyncReceive() and waits
        for an event request from the PU to call tryFetchL1R()

Therefore:

- There should be no significant difference between free and idle for
  `DummyProcessor`.
- For `HltpuProcessor` any significant difference between free and idle is
  likely due to the PU logic between accept/reject of events and
  request of next event.

Note on `HLTSV::AvailableCores`: HLTSV counts the number of available cores as
the number of requested events. Then a PU is "available" between the reception
of the event request and the sending of the assigned L1R. This matches the
computation of `ProxIdlePUs` in DCM, modulo the network transfers.

SFO Backpressure Impact
-----------------------
Backpressure from SFO is defined by: number of output events greater
than configuration parameter `maxOutputEvents`:

    m_nOutputEvents >= m_maxOutputEvents

Only `Processor` has the concept of backpressure. Accepted events are
asynchronously built, then asynchronously sent to SFO, and asynchronously
ackowledged by SFO. Events are "output" between `accept` (`finalizeAccept()`)
and acknowledgement by SFO (`onBuildAndSend()`).

The only impact of backpressure condition being true is to *not* request a L1R
to HLTSV when an HLTPU requests a new event. An HLTPU requesting a new event 
while backpressure is true sets the HLTPU in SUSPENDED state. SUSPENDED HLTPUs
are unsuspended when an event is destroyed if backpressure is no longer true.

Example:
    
    maxOutputEvents = 10
    Output events: 9
    Processing PU: 32
    SFOs suddently do not accept any events
    A PU accepts an event: output events -> 10
    HLTPU requests new event: not forwareded to HLTSV, HLTPU state -> SUSPENDED
    Eventually all PUs accept events: output events -> 41
      There are actually 41 events stored between EventBuilder and Output
