#!/usr/bin/env tdaq_python

import sys
import time
import re

from ispy import InfoReader, IPCPartition

part_name = sys.argv[1]
partition = IPCPartition(part_name)

areader = InfoReader(partition, 'DFConfig', 'MSG.*')
areader.update()


for k, obj in areader.objects.items():
  print("%-40s %-30s %-40s %6d " % (k, obj.Hostname, obj.Addresses[0], obj.Port))


