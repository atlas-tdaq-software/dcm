#!/bin/bash

ISSRV="DF.DcmRateCtrl"
_phelp()
{
  echo "Control the rate of the dcm.DummyProcessor via IS ($ISSRV)"
  echo "Usage: $0 -p partition -a L2Acc -A EFAcc -b L2Burn_ms -B EFBurn_ms"
  echo "   eg: $0 -p part_dcm -a 0.2 -A 0.1 -b 100 -B 1000"
  exit
}

PART=
L2A=
L2B=
EFB=
EFA=
while getopts ":p:a:A:b:B:h" Option
do
case $Option in
  p ) PART=$OPTARG;;
  a ) L2A=$OPTARG ;;
  A ) EFA=$OPTARG ;;
  b ) L2B=$OPTARG ;;
  B ) EFB=$OPTARG ;;
  h ) _phelp      ;;
  * ) _phelp      ;;
esac
done

if [ -z $PART ]; then echo "Missing -p argument"; exit; fi
if [ -z $L2A  ]; then echo "Missing -a argument"; exit; fi
if [ -z $EFA  ]; then echo "Missing -A argument"; exit; fi
if [ -z $L2B  ]; then echo "Missing -b argument"; exit; fi
if [ -z $EFB  ]; then echo "Missing -B argument"; exit; fi


which is_write 1>/dev/null 2>/dev/null 
if [ $? -ne 0  ]; 
   then echo "Source the release please";
   exit
fi

CMD="$L2B,$L2A,$EFB,$EFA"
is_write -p $PART -t String -n $ISSRV -a value -v $CMD

EFFB=`echo "$L2B + $EFB*($L2A/100.)" | bc -l`
CRATE=`echo "1000./$EFFB" | bc -l`
NRATE=`echo "$CRATE * 16." | bc -l`

echo "Set  $ISSRV = $CMD"
echo "Effective burn time = $EFFB ms"
echo "Per core rate       = $CRATE Hz"
echo "Per node rate (16)  = $NRATE Hz"
