#!/usr/bin/env tdaq_python

import sys
import time
from ispy import IPCPartition, Summer, InfoReader
from ispy import ISInfoIterator, ISInfoAny, ISCriteria, ISServerIterator

#some default definitions
col_red = "\033[31m"
col_def = "\033[0m"
col_gre = "\033[32m"
col_bol = "\033[1m"
col_bla = "\033[30m"
col_blu = "\033[34m"
col_yel = "\033[33m"


# parse command line
if len(sys.argv) == 1:
  print('Read dcm avg (default) or sum dcm objects provided by DefMIG')
  print('Usage:', sys.argv[0], ' partitionName [-s]')
  print('  -s  Use sum instead of avg')
  sys.exit(1)
part_name = sys.argv[1]
sum = False
if len(sys.argv) == 3 and sys.argv[2] == "-s":
  sum = True

# Partition object
partition = IPCPartition(part_name)

regExp = 'DefMIG.*info_avg'
if sum:
  regExp = 'DefMIG.*info_sum'

# Reader
reader = InfoReader(partition, 'DF_IS:HLT', regExp)
reader.update()
if len(reader.objects) != 1:
  print ("WARN: found", len(reader.objects), " DCM objects instead of one")
  sys.exit(1)

# Get the object (the 1st one in the dictionary)
dcm = reader.objects.itervalues().next()

# Calculate derived values
L1WaitingForSrc = dcm.L1SourceRequestedEvents - dcm.L1SourceReceivedEvents     
L1WaitingForProx= dcm.ProxReqEvents - dcm.ProxL1Events
OnL2Proc        = dcm.L1SourceReceivedEvents  - dcm.L1SourceDoneEvents
Processed       = dcm.ProxRejEvents +  dcm.ProxAccEvents
Inside          = dcm.ProxL1Events  -  dcm.ProxDoneEvents
OnProc          = dcm.ProxL1Events  - (dcm.ProxRejEvents + dcm.ProxAccEvents)
OnEFProc        = OnProc - OnL2Proc
               #= dcm.ProxRejEvents +  dcm.ProxAccEvents - (dcm.L1SourceReceivedEvents  - dcm.L1SourceDoneEvents)
AfterProc       = dcm.ProxRejEvents + dcm.ProxAccEvents - dcm.ProxDoneEvents
TotPUs          = dcm.ProxBusyPUs   + dcm.ProxFreePUs
OutQueue        = dcm.OutputEvents  -  dcm.OutputDeliveredEvents - dcm.OutputConnectionTimeouts - dcm.OutputConnectionErrors


# Dump data
print ("L1SourceRequestedEvents = %8d" % (dcm.L1SourceRequestedEvents))
print ("L1SourceReceivedEvents  = %8d" % (dcm.L1SourceReceivedEvents))
print ("L1SourceDoneEvents      = %8d" % (dcm.L1SourceDoneEvents)     )
print ("L1SourceConnectionTime..= %8d" % (dcm.L1SourceConnectionTimeouts))
print ("L1SourceConnectionErrors= %8d" % (dcm.L1SourceConnectionErrors))
print ("  --> WaitingForL1      = %8d" % (L1WaitingForSrc),    " [L1SourceRequestedEvents - L1SourceReceivedEvents]")
print ("  --> OnL2Process       = %8d" % (OnL2Proc),           " [L1SourceReceivedEvents  - L1SourceDoneEvents]")
print ()
print ("ProxL1Events            = %8d" % (dcm.ProxL1Events),   " [== L1SourceReceivedEvents]")               
print ("ProxRejEvents           = %8d" % (dcm.ProxRejEvents)   )           
print ("ProxAccEvents           = %8d" % (dcm.ProxAccEvents)  )        
print ("ProxDoneEvents          = %8d" % (dcm.ProxDoneEvents))
print ("  --> Processed         = %8d" % (Processed),          " [ProxRejEvents + ProxAccEvents]")
print ("  --> InsideDCM         = %8d" % (Inside),             " [ProxL1Events  - ProxDoneEvents]")
print ("  --> OnProcess         = %8d" % (OnProc),             " [ProxL1Events  - (ProxRejEvents + ProxAccEvents)]")
print ("  --> OnEFProcess       = %8d" % (OnEFProc),           " [_OnProcess - _OnL2Process]")
print ("  --> AfterProces       = %8d" % (AfterProc),          " [ProxRejEvents + ProxAccEvents - ProxDoneEvents]")
print ("  --> WaitingForL1      = %8d" % (L1WaitingForProx),   " [ProxReqEvents - ProxL1Events]")
print ()
print ("EventsInside            = %8d" % (dcm.EventsInside))
print ("EventsOnOutputQueue     = %8d" % (dcm.EventsOnOutputQueue))
print ("BusyProcessing          = %8d" % (dcm.BusyProcessing))
print ("BusyFromOutput          = %8d" % (dcm.BusyFromOutput))
print ("ProxBusyPUs             = %8d" % (dcm.ProxBusyPUs),    " [== _OnProcess]")
print ("ProxFreePUs             = %8d" % (dcm.ProxFreePUs) )           
print ("ProxIdlePUs             = %8d" % (dcm.ProxIdlePUs))
print ("  --> TotalPUs          = %8d" % (TotPUs),             " [ProxBusyPUs + ProxFreePUs]")
print ()
print ("EbEvents                = %8s" % (dcm.EbEvents),       " [== ProxAccEvents]")
print ("SampledEvents           = %8s" % (dcm.SampledEvents))
print () 
print ("OutputEvents            = %8s" % (dcm.OutputEvents),   " [== EbEvents + OutputConnectionTimeouts + OutputConnectionError ]")
print ("OutputDeliveredEvents   = %8s" % (dcm.OutputDeliveredEvents))
print ("OutputConnectionTimeouts= %8s" % (dcm.OutputConnectionTimeouts))
print ("OutputConnectionErrors  = %8s" % (dcm.OutputConnectionErrors))
print ("  --> OutputQueue       = %8d" % (OutQueue),           " [OutputEvents - (OutputConnectionTimeouts + OutputConnectionErrors) - OutputDeliveredEvents ]")
print ("                                ==  [ProxRejEvents + ProxAccEvents - ProxDoneEvents]")

print
if dcm.ProxBusyPUs != OnProc:
  print (col_red, "[WARN] ProxBusyPUs != OnProc", col_def)
if dcm.EbEvents != dcm.ProxAccEvents:
  print (col_red, "[WARN] EbEvents != ProxAccEvents", col_def)
if AfterProc != OutQueue:
  print (col_red, "[WARN] AfterProc != OutQueue", col_def)
if dcm.EbEvents + dcm.OutputConnectionTimeouts + dcm.OutputConnectionErrors != dcm.OutputEvents:
  print (col_red, "[WARN] EbEvents + OutputConnectionTimeouts + OutputConnectionErrors", col_def)
