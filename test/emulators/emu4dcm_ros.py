#!/usr/bin/env tdaq_python
###############################################################################
# ROS emulator, server side: open a socket on a port, listen for a message 
# from a client, and send an echo reply; forks a process to handle each client 
# connection; child processes share parent's socket descriptors; fork is less 
# portable than threads--not yet on Windows, unless Cygwin or similar installed;
###############################################################################

import os, time, sys
import eformat                            # Event format 
import optparse
from optparse import OptionParser
import socket
import string
import struct                             # This module performs conversions between Python values and C structs represented as Python strings
from socket import *                      # get socket constructor and constants
import random
import datetime            
from ispy import *

# ------------- Data Format ------------------
#         ----------
#        | TypeID   | Header (Message Type)
#        | X ID     | Header (Transaction ID)
#        | Msg size | Header (Payload size)
#         ----------
#        | RobD[0]  | ROB Data-0 (event format)
#        | ......   |
#          ......
#          ......
#        | RobD[n]  | ROB Data-n (event format)
#        | ......   |
#          ......
#          ......
#         ----------
# --------------------------------------------

# Protocols type IDs
reqMessage  = 0x00DCDF20  # DCM -> ROS
respMessage = 0x00DCDF21  # ROS -> DCM
headerSize     = 12


def publishMsgInfo(partition, isServer, port, nMask, rosid):
  part = IPCPartition(options.partition)
  name = "%s.MSG_%s" % (isServer, rosid);
  print("Publishing in ISServer '%s@%s'" % (name, partition))
  try:
    msgInfo = ISObject(part, name, 'MsgInfo')
    msgInfo.Hostname = gethostname()
    msgInfo.Port     = port
    msgInfo.Addresses= [gethostbyname(msgInfo.Hostname) + "/" + nMask ]
    msgInfo.checkin(False)
    print("msgInfo = ", msgInfo)
  except UserWarning as details:
    print("WARNING: Unable to publish in IS:", details)
    pass


##############################################################
# ROS Session Class
class RosSession:
  def __init__(self, connection, timeout_ms, verbose):
    self.reqCounter    = 0
    self.lvl1_id       = 0
    self.exchangeId    = 0
    self.pid           = os.getpid()
    self.verbose       = verbose
    self.connection    = connection
    self.RobIds        = []             
    self.timeout_ms    = timeout_ms
    self.timeout       = datetime.timedelta(microseconds= self.timeout_ms * 1000)

  def dump(self): 
    print("DUMP pid, req counter: ", self.pid," ", self.reqCounter)
    l = len(self.RobIds)
    for k in self.RobIds:
      print("DUMP L1Id, RobIds ",  self.lvl1_id, hex(k))
      
  def checkId(self, my_id):
    check = False
    for x in eformat.helper.SubDetector.values.values():
      hex_id = eformat.helper.SourceIdentifier(x<<16) 
      if hex(my_id) == hex(hex_id.code()): 
        check = True
        #print "Found ", hex(hex_id.code())  
    if check: 
      return 1
    else:    
      print ("Warning: no source_id matching")  
      return 0

  # make sure we read exactly size bytes
  def recvAll(self, size):
    data = self.connection.recv(size)
    size -= len(data)
    while size > 0:
      m = self.connection.recv(size)
      if not m:
        return ""
      data += m
      size -= len(m)
    return data

  def readHeader(self, typeId):
    data = self.recvAll(12)
    if not data:
      print("Failed reading message header. Connection closed ?")
      return -1
    header = struct.unpack('III', data)
    if self.verbose: print("<<<--- Header ")
    if self.verbose: print("<<<---[%5d] %s" % (self.pid, listHexDump(header)))
    if header[0] != typeId:
      print("Invalid message typeID 0X%08x, expected 0X%08x" %  (header[0], typeId))
      return -1
    if self.verbose > 100:
      print("<<< Received asyncmeg header: 0X%08x 0X%08x 0X%08x" % (header[0], header[1], header[2]))
    return header[2] # Return payload size

  def handshake(self):
    # Get the client name
    size = self.readHeader(0x80000001)
    if size < 0: return False
    data = self.connection.recv(size * 4)
    print("<<< Received connection request from", data)

    # Send the server name
    serverName = "sfoEmu-1"  # Name aligned to 4 bytes ;-)
    outData  = ''
    outData += struct.pack('I', 0x80000001)
    outData += struct.pack('I', 0x0)
    outData += struct.pack('I', len(serverName))
    outData += serverName
    self.connection.send(outData)
    print('>>> Sent server name', serverName)
    print('--------------------------------\n')
    return True
 
  def receiveData(self):
    # Parse the header and return the message payload size
    size = self.readHeader(reqMessage)
    if size < 0:
      return False
    if size == 0:
      print('Wrong event size (%d) ' % (rSize))
      return False

    # Read the payload
    data    = self.recvAll(size)
    fmt     = "%dI" % (size/4)
    payload = struct.unpack(fmt, data)
    if self.verbose: print("Exchange ID: ", self.exchangeId)
    if self.verbose: print("<<<--- Payload ") 
    if self.verbose: print("<<<---[%5d] %s" % (self.pid, listHexDump(payload)))
    self.lvl1_id = payload[0]
    self.RobIds = payload[1:]
    self.reqCounter +=1
    
    # Check the RobIds
    for i in range(0, len(self.RobIds)):
      self.checkId(self.RobIds[i])
    
    return True

  def sendEvents(self):
    # TODO:
    #  - Support multiple ROB per L1Result
    #  - Support CTPFragment as payload! NB: there is no CTPFragment write interface,
    #    therefore the data has to be hardplugged in the code
    lvl1_id      = self.lvl1_id
    bc_id        = lvl1_id%1000
    outData      = ''
    status       = [0x0]
    robArraySz   = 0
    for x in self.RobIds:
      source_id    = eformat.helper.SourceIdentifier(x)
      dummyPayload = options.robSize
      robArray     = eformat.dummy.make_rob(source_id, dummyPayload, lvl1_id, bc_id, status)    # Make random rob data (see dummy.py)
      robArraySz   += len(robArray)*4                                                           # Increment ROBs array size  
      outData += ''.join((struct.pack('I', w) for w in robArray.__raw__()))
      
    # - header
    outData_header  = ''
    outData_header += struct.pack('I', respMessage)
    outData_header += struct.pack('I', self.exchangeId) 
    outData_header += struct.pack('I', robArraySz)              # send the rob array size

    # - merge header and the events
    outData = outData_header + outData

    # Send
    self.connection.send(outData)
    
    if self.verbose: 
      s = listHexDump(struct.unpack('%dI' % (len(outData)/4), outData)[:8])
      print("--->>>[%5d] %s" % (self.pid, s))
   

##############################################################


# Helper function to dump in hexadecimal 
def listHexDump(l):
  return ''.join('0x%08x ' % x for x in l)


# ----------------------------------------------------------------
# --- HLTSV emulator: TCP server implementation
def server(nbrConnReject):
  # - Set running options from argv 
  readFromFile = options.file
  if readFromFile: 
    inputFileName = options.filename
  myHost = ''
  myPort = options.port                        # listen on a non-reserved port number  
  
  sockobj = socket(AF_INET, SOCK_STREAM)       # make a TCP socket object
  sockobj.bind((myHost, myPort))               # bind it to server port number
  sockobj.listen(5)                            # allow 5 pending connects

  if myPort == 0:
    myPort = sockobj.getsockname()[1]
    print("Random port assigned:", myPort)

  # Try to publish in IS
  if options.partition:
    publishMsgInfo(options.partition, options.isServer, myPort, options.nMask, options.rosid )


  def now( ):                                  # current time on server
    return time.ctime(time.time( ))

  activeChildren = []
  def reapChildren( ):                         # reap any dead child processes
    while activeChildren:                      # else may fill up system table
      pid,stat = os.waitpid(0, os.WNOHANG)     # don't hang if no child exited
      if not pid: break
      activeChildren.remove(pid)

  # Worker
  def handleClient(connection):           # child process: reply, exit
    mySession = RosSession(connection, options.timeout_ms, options.verbose)

    # Connection handshake
    if not mySession.handshake():
      connection.close( )
      print("Connection closed")
      sys.exit(2)

    cnt = 0
    while True:
      cnt += 1
      
      # Dump stats
      if cnt%options.dumpStep == 0:
        mySession.dump()

      # Receive a request (blocking)
      if not mySession.receiveData():
        break
      

      # Sends some events: random number
      #number = random.randint(1,mySession.reqCounter)
      mySession.sendEvents()

    connection.close( )
    os._exit(0)

  def dispatcher(nbrConnReject):              # listen until process killed
    actualNbrConnReject = 0
    while True:                               # wait for next connection,
      connection, address = sockobj.accept( ) # pass to process for service
      if actualNbrConnReject < nbrConnReject:
        connection.close()
        actualNbrConnReject += 1
        print("Reject connection from %s, %d/%d" %(address, actualNbrRejectConn, nbrRejectConn))
      else:
        print('Server connected by', address)
        print('at', now( ))
        actualNbrConnReject = 0
        reapChildren( )                         # clean up exited children now
        childPid = os.fork( )                   # copy this process
        if childPid == 0:                       # if in child process: handle
          handleClient(connection)
        else:                                   # else: go accept next connect
          activeChildren.append(childPid)       # add to active child pid list

  dispatcher(nbrConnReject)
# ----------------------------------------------------------------


# --- Main ---
if __name__ == "__main__":
  
  parser = OptionParser()
  parser.add_option('-V', '--verbose', dest='verbose', type='int', default=0, help='Verbose mode')
  parser.add_option('-F', '--file', dest='file', help='Read from file')
  parser.add_option('-N', '--filename', dest='filename', type='string', help='Input file name, if read from file selected')
  parser.add_option('-E', '--robSize', dest='robSize', type='int', help='ROB fragment size')
  parser.add_option('-P', '--port', dest='port', type='int', default=0, help='TCP server port')
  parser.add_option('-p', '--partition', dest='partition', type='string', default='', help='partition name' )
  parser.add_option('-s', '--isServer',  dest='isServer', type='string', default='DF', help='IS server name' )
  parser.add_option('-m', '--nMask',  dest='nMask', type='string', default='255.255.255.0', help='Network mask' )
  parser.add_option('-d', '--dumpStep', dest='dumpStep',   type='int', default=100,  help='Dump stats every n events')
  parser.add_option('-t', '--timeout',  dest='timeout_ms', type='int', default=5000, help='Timeout is ms')
  parser.add_option('-i', '--rosid', dest='rosid',  type='string', default='ROS-1', help='The ROS UID')
  parser.add_option('-r', '--nbrConnReject', dest='nbrConnReject',  type='int', default=0, help='Number systematic connection rejection')

  (options, args) = parser.parse_args()  
  if (not options.robSize):
    print('')
    print(' --- ROS emulator ---')
    print('')
    parser.print_help()
    print('')
    print('-> Please insert at least robSize and port number')
    print('')
    sys.exit(0)
  
  if options.verbose :
    print('')
    print(' --- ROS emulator ---')
    print('')
    if options.file: 
      print('Read from file: ', options.file)
  
  print("===== Configuration ============")
  for k, v in vars(options).items():
    print("%-10s = %s" % (k, v))
  print("================================")

  server(options.nbrConnReject)

