#!/usr/bin/env tdaq_python
###############################################################################
# SFO emulator, server side: open a socket on a port, listen for a message 
# from a client, and send an echo reply; forks a process to handle each client 
# connection; child processes share parent's socket descriptors; fork is less 
# portable than threads--not yet on Windows, unless Cygwin or similar installed;
###############################################################################

import os, time, sys
import eformat                            # Event format 
import libpyeformat
import optparse
from optparse import OptionParser
import socket
import string
import struct                             # This module performs conversions between Python values and C structs represented as Python strings
from socket import *                      # get socket constructor and constants
import random
import datetime            
import signal
from ispy import *




def publishMsgInfo(partition, isServer, port, nMask, sfoid):
  part = IPCPartition(options.partition)
  name = "%s.MSG_%s" % (isServer, sfoid);
  print("Publishing in ISServer '%s@%s'" % (name, partition))
  try:
    msgInfo = ISObject(part, name, 'MsgInfo')
    msgInfo.Hostname = gethostname()
    msgInfo.Port     = port
    msgInfo.Addresses= [gethostbyname(msgInfo.Hostname) + "/" + nMask ]
    msgInfo.checkin(False)
    print("msgInfo = ", msgInfo)
  except UserWarning as details:
    print("WARNING: Unable to publish in IS:", details)
    pass


##############################################################
# SFO Session Class
class SfoSession:
  def __init__(self, connection, opt):
    self.counter       = 0
    self.lvl1_id       = 0
    self.exchangeId    = 0
    self.pid           = os.getpid()
    self.verbose       = opt.verbose
    self.connection    = connection
    self.timeout_ms    = opt.timeout_ms
    self.timeout       = datetime.timedelta(microseconds= self.timeout_ms * 1000)
    self.opt           = opt


  def dump(self): 
    print("...")
      
  def checkId(self, my_id):
    check = False
    for x in eformat.helper.SubDetector.values.values():
      hex_id = eformat.helper.SourceIdentifier(x<<16) 
      if hex(my_id) == hex(hex_id.code()): 
        check = True
        #print "Found ", hex(hex_id.code())  
    if check: 
      return 1
    else:    
      print("Warning: no source_id matching")  
      return 0   

  def readHeader(self, typeId):
    data = self.connection.recv(12)
    header = struct.unpack('III', data)
    if header[0] != typeId:
      print("Invalid message typeID 0X%08x, expected 0X%08x" %  (header[0], typeId))
      return -1
    if self.verbose > 100:
      print("<<< Received asyncmeg header: 0X%08x 0X%08x 0X%08x" % (header[0], header[1], header[2]))
    return header[2] # Return payload size

  def handshake(self):
    # Get the client name
    size = self.readHeader(0x80000001)
    if size < 0: return False
    data = self.connection.recv(size * 4)
    print("<<< Received connection request from", data)

    # Send the server name
    serverName = "sfoEmu-1"  # Name aligned to 4 bytes ;-)
    outData  = ''
    outData += struct.pack('I', 0x80000001)
    outData += struct.pack('I', 0x0)
    outData += struct.pack('I', len(serverName))
    outData += serverName
    self.connection.send(outData)
    print('>>> Sent server name', serverName)
    print('--------------------------------\n')
    return True


  def boom(self, where):
    if self.opt.stall == where:
      print("\033[31m \n\n I'm going to stall before %s \n\n \033[0m" % ( locmap[where]))
      time.sleep(9999999)
    elif self.opt.die == where:
      print("\033[31m \n\n I'm going to die before %s \n\n \033[0m" % ( locmap[where]))
      os.kill( os.getppid() , signal.SIGKILL)  # Send SIGKILL to the mother
      sys.exit(2)                              # Exit
    else:
      return 

  def receiveData(self):
    # Debug
    if self.counter == self.opt.events:
      self.boom(1)

    # Receive space request
    size = self.readHeader(0x00DCDF40)
    if size < 0: return False
    data   = self.connection.recv(size*4)
    numEv  = struct.unpack('I', data)
    if self.verbose > 100: print('<<< Received space request for %d events' % numEv)
    
    # Debug
    if self.counter == self.opt.events:
      self.boom(2)
    
    # Request just one event
    outData  = ''
    outData += struct.pack('I', 0x00DCDF41)
    outData += struct.pack('I', 0x0)
    outData += struct.pack('I', 4)
    outData += struct.pack('I', 1)
    self.connection.send(outData)
    if self.verbose > 100: print('>>> Requested one event (%d)' % len(outData)) 
   
    # Debug
    if self.counter == self.opt.events:
      self.boom(3)
    
    # Receive the event 
    size = self.readHeader(0x00DCDF42)
    if size < 0: return False

    # Debug
    if self.counter == self.opt.events:
      self.boom(4)

    # Receive the data
    buffer = []
    received = 0
    while received < size:
      data = self.connection.recv(size)
      received = received + len(data)
      fmt = "%dI" % (len(data)/4)
      tmp = struct.unpack(fmt , data)
      for w in tmp:
        buffer.append(w)
    
    # read the GID: words 0 and 1 (only 32bits for the time being)
    gid = buffer[0]
    if self.verbose > 100: print('>>> Received event %d of size %d B' % (gid, size)) 

    # Look for full event header (quink and dirty)
    if self.verbose >= 1: print(gid)
    off = 0
    while off < size/4:
      if buffer[off] != 0xaa1234aa:
        off = off + 1
        continue

      # Create full event
      ulist = libpyeformat.u32list(buffer[off:])
      fe = libpyeformat.FullEventFragment(ulist)
      stags = fe.stream_tag()
      evtSz = fe.fragment_size_word()

      # Dump verbosity 1
      if self.verbose >= 1:
        print("\tgid=%8d l1id=%8d sz=%6dB #robs=%3d #streamTags=%2d #status=%2d" % (
          fe.global_id(), fe.lvl1_id(), evtSz, fe.nchildren(), len(stags), len(fe.status()) ))

      # Dump verbosity 2
      if self.verbose >= 2:
        str="\t Streams: "
        for s in stags:
          sR =  "".join('x%08x, ' % i for i in s.robs)
          sD =  "".join('x%08x, ' % i for i in s.dets)
          str+="%s@%s {%s} {%s}" % (s.name, s.type, sR, sD)
        print(str)
        str="\t Status: "
        for s in fe.status():
          str += "0x%08x, " % s
        print(str)

      # Dump verbosity 3
      if self.verbose >= 3:
        str="\t Robs in events: "
        for r in fe.children():
          str += "0x%08x, " % r.source_id()
        print(str)
      
      off = off + evtSz

    # Debug
    if self.counter == self.opt.events:
      self.boom(5)
      
    #Send the ack
    outData  = ''
    outData += struct.pack('I', 0x00DCDF41)
    outData += struct.pack('I', 0x0)
    outData += struct.pack('I', 12)
    outData += struct.pack('I', 0)
    outData += struct.pack('I', gid)
    outData += struct.pack('I', 0)
    self.connection.send(outData)
    if self.verbose > 100: print('>>> Send ack (%d) for event %d' % (len(outData), gid))
 
    self.counter += 1

    return True



##############################################################


# Helper function to dump in hexadecimal 
def listHexDump(l):
  return ''.join('0x%08x ' % x for x in l)


# ----------------------------------------------------------------
# --- SFO emulator: TCP server implementation
def server(nbrConnReject):
  # - Set running options from argv 
  myHost = ''
  myPort = options.port                        # listen on a non-reserved port number  
  sockobj = socket(AF_INET, SOCK_STREAM)       # make a TCP socket object
  sockobj.bind((myHost, myPort))               # bind it to server port number
  sockobj.listen(5)                            # allow 5 pending connects

  if myPort == 0:
    myPort = sockobj.getsockname()[1]
    print("Listen on random port:", myPort)
  else:  
    print("Listen on port:", myPort)

  # Try to publish in IS
  if options.partition:
    publishMsgInfo(options.partition, options.isServer, myPort, options.nMask, options.sfoid )


  def now( ):                                  # current time on server
    return time.ctime(time.time( ))

  activeChildren = []
  def reapChildren( ):                         # reap any dead child processes
    while activeChildren:                      # else may fill up system table
      pid,stat = os.waitpid(0, os.WNOHANG)     # don't hang if no child exited
      if not pid: break
      activeChildren.remove(pid)

  # Worker
  def handleClient(connection):           # child process: reply, exit
    mySession = SfoSession(connection, options)
    cnt = 0

    # Connetion handshake
    if not mySession.handshake():
      connection.close( )
      print("Connection closed")
      sys.exit(2)

    while True:
      cnt += 1
     
      # Dump stats
      if cnt%options.dumpStep == 0:
        print("Receiving event %d" % cnt)
     
      # Receive a request (blocking)
      if not mySession.receiveData():
        break
  
      # Number of events
      if cnt == options.events + 1:
        print("Received %d events, exiting" % cnt)
        break

    connection.close( )
    print("Connection closed")
    sys.exit(0)

  def dispatcher(nbrConnReject):              # listen until process killed
    actualNbrConnReject = 0
    while True:                               # wait for next connection,
      connection, address = sockobj.accept( ) # pass to process for service
      if actualNbrConnReject < nbrConnReject:
        connection.close()
        actualNbrConnReject += 1
        print("Reject connection from %s, %d/%d" %(address, actualNbrConnReject, nbrConnReject))
      else:
        print('Server connected by', address)
        print('at', now( ))
        actualNbrConnReject = 0
        if len(activeChildren) < options.maxConns:
          reapChildren( )                         # clean up exited children now
          childPid = os.fork( )                   # copy this process
          if childPid == 0:                       # if in child process: handle
            handleClient(connection)
          else:                                   # else: go accept next connect
            activeChildren.append(childPid)       # add to active child pid list
        else:
          print("Maximum number of connection reached:",  options.maxConns)

  dispatcher(nbrConnReject)
# ----------------------------------------------------------------




locmap = { 1 : "space request rcv",
           2 : "event request send",
           3 : "event header rcv",
           4 : "event data rcv",
           5 : "ack send" }

# --- Main ---
if __name__ == "__main__":
  
  hh = "Before: %s" % locmap
  parser = OptionParser()
  parser.add_option('-V', '--verbose', dest='verbose', type='int', default=0, help='Verbosity level (stream 2, robs 3)')
  parser.add_option('-P', '--port', dest='port', type='int', default=0, help='TCP server port')
  parser.add_option('-p', '--partition', dest='partition', type='string', default='', help='partition name' )
  parser.add_option('-s', '--isServer',  dest='isServer', type='string', default='DFConfig', help='IS server name' )
  parser.add_option('-m', '--nMask',  dest='nMask', type='string', default='255.255.255.0', help='Network mask' )
  parser.add_option('-d', '--dumpStep', dest='dumpStep',   type='int', default=100,  help='Dump stats every n events')
  parser.add_option('-t', '--timeout',  dest='timeout_ms', type='int', default=5000, help='Timeout is ms')
  parser.add_option('-i', '--sfoid', dest='sfoid',  type='string', default='SFO-1', help='The SFO UID')
  parser.add_option('-n', '--events', dest='events',  type='int', default='99999999', help='Number of events to be received')
  parser.add_option('-c', '--mConns', dest='maxConns',type='int', default='100',  help='Max number of connections')
  parser.add_option('-S', '--stall', dest='stall', type='int', default='0', help="Stall " + hh) 
  parser.add_option('-D', '--die'  , dest='die',   type='int', default='0', help="Die " + hh) 
  parser.add_option('-r', '--nbrConnReject', dest='nbrConnReject',  type='int', default=0, help='Number systematic connection rejection')


  (options, args) = parser.parse_args()  
  if (not options.port):
    print('')
    print(' --- SFO emulator ---')
    print('')
    parser.print_help()
    print('')
    print('-> Please insert at least a port number')
    print('')
    sys.exit(0)
  
  if options.verbose :
    print('')
    print(' --- SFO emulator ---')
    print('')
  
  print("===== Configuration ============")
  for k, v in vars(options).items():
    print("%-10s = %s" % (k, v))
  print("================================")

  server(options.nbrConnReject)

