#!/usr/bin/env tdaq_python
#############################################################################
# Client side: connect to the server and ask for data
# Example of usage: 
#    ./dcm.py --verbose=true --numberEv=10 --ip=127.0.0.1 --port=9999
#############################################################################

import os
# import SocketServer
import socketserver 
import socket
import threading
import time
import eformat
import random
import struct
import sys
import optparse
from optparse import OptionParser
from datetime import timedelta, datetime

# -------------- Data Format ------------------------
#         ----------
#        | TypeID   | Header (Message Type)
#        | X ID     | Header (Transaction ID)
#        | Msg size | Header (Payload size)
#         ----------
#        | nReqRoIs | At 1st call it will be equal to the number of PUs
#        | #OfL1IDs | Size of the L1ID list
#        | L1ID-0   | List of L1ID of "done" events. Can be empty
#          ........
#          ........
#        | L1ID-n   |
#         ----------
# --------------------------------------------------

# Protocols type IDs
typeIdUpdate   = 0x00DCDF00
typeIdAssign   = 0x00DCDF01
typeIdReAssign = 0x00DCDF0F
headerSize     = 12

# Global counters
transIdCnt = 0


def listHexDump(l):
  return ''.join('0x%08x ' % x for x in l)

# Create the Update Message
def makePacket(numOfReq, done = []):
  global typeIdUpdate
  global transIdCnt
  transIdCnt += 1            # Increment the transId at any new request
  size = ( 1 + len(done))*4  # ReqWord + list of L1Ids done
  data  = ''
  data += struct.pack('I', typeIdUpdate)
  data += struct.pack('I', transIdCnt)
  data += struct.pack('I', size)
  data += struct.pack('I', numOfReq)
  for l1id in done:
   data += struct.pack('I', l1id)
  return data

# Ugly class to describe HLTPU. ToDo: redesign! 
class HltPus:
  def __init__(self, size, l2ProcTime, efProcTime, l2Acc):
    self.l2Proc = l2ProcTime 
    self.efProc = efProcTime 
    self.l2Acc  = l2Acc
    self.l1id   = []
    self.stat   = []
    self.t0     = []
    self.tout   = []
    self.size   = size
    self.Free = 0
    self.OnL1 = 1
    self.OnL2 = 2
    self.OnEf = 3
    self.Done = 4
    t0 = datetime.now()
    for i in range(0, size):
      self.l1id.append(0)
      self.stat.append(self.Free)
      self.t0.append(t0)
      self.tout.append(t0)

  def popAvailable(self):
    cnt = 0
    for i in range(0, self.size):
      if self.stat[i] == self.Free: 
        self.stat[i] = self.OnL1
        cnt += 1
    return cnt
  
  def popExpired(self):
    exp = []
    for i in range(0, self.size):
      if self.stat[i] == self.Done: 
        exp.append(self.l1id[i])
        self.stat[i] = self.Free
    return exp

  def set(self, l1id):
    for i in range(0, self.size):
      if self.stat[i] == self.OnL1: 
         self.stat[i] = self.OnL2
         half = self.l2Proc/2
         ms   = random.randint(half, 3*half)
         self.t0[i]   = datetime.now()
         self.tout[i] = self.t0[i] + timedelta(microseconds=ms*1000)
         self.l1id[i] = l1id
         return True
    return False  

  def smear(self):
    r = random.random()

  def process(self, verb=0):
    t = datetime.now()
    for i in range(0, self.size):
      # Process L2
      if self.stat[i] == self.OnL2:    
        if t > self.tout[i]: 
          ef_ms = 0
          r = random.random()
          if r < self.l2Acc :         # L2 accepted: mark and set EF timeout
            self.stat[i] = self.OnEf
            half = self.efProc/2
            ef_ms  = random.randint(half, 3*half)
            self.tout[i] = datetime.now() + timedelta(microseconds=ef_ms*1000)
          else:                       # L2 rejected
            self.stat[i] = self.Done
          if verb: 
            pt = t - self.t0[i]
            pt_ms = pt.seconds*1000 + pt.microseconds/1000
            print("L2 = %d [l1id=%8d] --> %d ms" % (pt_ms, self.l1id[i], ef_ms))
      # Process EF    
      elif self.stat[i] == self.OnEf:         
        if t > self.tout[i]: 
          self.stat[i] = self.Done
          if verb:
            pt = t - self.t0[i]
            pt_ms = pt.seconds*1000 + pt.microseconds/1000
            print("EF = %d (l1id=%d)" % (pt_ms, self.l1id[i]))
  
  def stats(self):
    stats = [0,0,0,0,0]
    for i in range(0, self.size):
      stats[self.stat[i]] = stats[self.stat[i]] +  1
    return stats  



def client():
 ip         = options.ip
 port       = options.port
 verbose    = options.verbose
 doneEv     = options.numberEv
 l2ProcTime = options.L2Time
 efProcTime = options.EFTime 
 l2Acc      = options.L2Acc
 hltpusMax  = options.HltPUs

 # Initialize processing hltpus
 hltpus     = HltPus(hltpusMax, l2ProcTime, efProcTime, l2Acc)


 # Connect to the server
 s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 s.connect((ip, port))

 # Useful variables
 num       = 0                       # ExchangeId counter
 while True:
   
   hltpus.process(verbose>1)
   #print "STATUS: ", hltpus.stats()

   # -------- Send the data request --------
   done  = hltpus.popExpired()
   avail = hltpus.popAvailable()
   if avail or len(done) > 0:
     dataReqs = makePacket(avail, done)
     len_sent = s.send(dataReqs)  
     num = num + 1
     if verbose:
       print("--->>>", listHexDump( struct.unpack('%dI' % (len(dataReqs)/4), dataReqs) ))
   
 
   # Receive header, if any
   data = ''
   try: data = s.recv(headerSize, socket.MSG_DONTWAIT)
   except socket.error: pass
   if len(data):
     header = struct.unpack('III', data)
     if verbose: print("<<<---", listHexDump(header))
     rTypeId     = header[0]  
     rExchangeId = header[1]
     rSize       = header[2]
     if rTypeId != typeIdAssign :
       print('Data request from wrong client (%x / %x) ' % (rTypeId, typeIdAssign))
       os._exit(7)
     if rSize <= 0: 
       print('Wrong event size (%d) ' % (rSize))
       os._exit(7)
    
     # --------- Receive Payload  ----------
     data = s.recv(rSize)
     if len(data) != rSize:
       print("Data size mismatch  %d / %d" % (len(data), rSize))
       os._exit(7)
     fmt = '%dI' % (len(data)/4)
     payload = struct.unpack( fmt , data)
     if verbose: print("<<<---", listHexDump(payload[:8]))
     gidLo = payload[0]
     gidUp = payload[1]
    
     # Access the list of ROBs  (only one at the moment)
     if len(payload) > 2:
       l1Id  = payload[2]
       hltpus.set(l1Id)
       bindata = list(payload[3:])
       rob = eformat.ROBFragment(eformat.u32list(bindata)) 
       if verbose > 3:
         print("ROB %x, %x" % (rob.source_id(), rob.rod_lvl1_id()))

  


# --- Main ---
if __name__ == "__main__":
 
  parser = OptionParser()
  parser.add_option('-V', '--verbose', dest='verbose', type='int', default=0, help='Verbose level')
  parser.add_option('-I', '--ip', dest='ip', type='string', help='IP number or host name')
  parser.add_option('-E', '--numberEv', dest='numberEv', type='int', help='Number of dummy events')
  parser.add_option('-P', '--port', dest='port', type='int', help='TCP server port')
  parser.add_option('-a', '--L2Acc', dest='L2Acc',  type='int', default=0.05, help='L2 acceptance (0.-1.)')
  parser.add_option('-2', '--L2Time',dest='L2Time', type='int', default=50,   help='L2 burn time in ms')
  parser.add_option('-3', '--EFTime',dest='EFTime', type='int', default=2000, help='EF burn time in ms')
  parser.add_option('-p', '--PUs'   ,dest='HltPUs', type='int', default=8,    help='Number of HLTPUs')

  (options, args) = parser.parse_args()  
  if (not options.numberEv):
    print('')
    print(' --- HLTSV client emulator ---')
    print('')
    parser.print_help()
    print('')
    print('-> Please insert at least number of events, ip and port number')
    print('')
    sys.exit(0)
  
  if options.verbose :
    print('')
    print(' ------ HLTSV client emulator ------')
    print(' --- Connect to: ', options.ip)
    print(' --- Port: ', options.port)    
    print(' --- Number of events: ', options.numberEv)
    print('------------------------------------')
    print('')

  print("===== Configuration ============")
  for k, v in vars(options).items():
    print("%-10s = %s" % (k, v))
  print("================================")


  # - Start TCP client
  client()

