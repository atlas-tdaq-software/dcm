#!/usr/bin/env tdaq_python

import sys
import getopt
from ispy import *
import pm.project
import socket

pName = ""
isSrv = "DF"
aName = "HLTSV"
port  = 7777
nMask = "255.255.255.0"

def Usage():
  print("Usage:", sys.argv[0], "-p partition_name ")
  print("       [-s IS_server_name]" )
  print("       [-a app_name]")
  print("       [-P port {def=7777}]")
  print("       [-m netmask {def=255.255.255.0}]")
  sys.exit(2)

#Read command line parameteras
try:
  opts, args = getopt.getopt(sys.argv[1:], "p:s:P:h")
except getopt.GetoptError:
  print(str(err))
  Usage()
  sys.exit(2)  


for o,a in opts:
    if    o == "-h": Usage()
    elif  o == "-p": pName=a
    elif  o == "-s": isSrv=a
    elif  o == "-a": aName=a
    elif  o == "-P": port = int(a)
    elif  o == "-m": nMask=a

if not len(pName): Usage()

name = isSrv+".MSG_"+aName
print ("Publishing in ISServer '%s@%s'" % (name, pName))

part = IPCPartition(pName);

msgInfo           = ISObject(part, name, 'MsgInfo')
msgInfo.Hostname  = socket.gethostname()
msgInfo.Port      = port
msgInfo.Addresses = [socket.gethostbyname(msgInfo.Hostname)+"/"+nMask]
msgInfo.checkin(False)
print(msgInfo)
print("MsgInfo published!")






