#!/usr/bin/env tdaq_python
#############################################################################
# Client side: ask ROS for ROB data
# Example of usage: 
#    ./dcm.py --verbose=true --numberEv=10 --ip=127.0.0.1 --port=9999
#############################################################################

import os
import socketserver
import socket
import threading
import time
import eformat
import random
import struct
import sys
import optparse
from optparse import OptionParser
from datetime import timedelta, datetime

# -------------- Data Format ------------------------
#         ----------
#        | TypeID   | Header (Message Type)
#        | X ID     | Header (Transaction ID)
#        | Msg size | Header (Payload size)
#         ----------
#        | L1Id     | 
#        | ROBId[0] | 
#        | ........ | 
#          ........
#          ........
#        | ROBId[n] |
#         ----------
# --------------------------------------------------

# Protocols type IDs
reqMessage  = 0x00DCDF20  # DCM -> ROS
respMessage = 0x00DCDF21  # ROS -> DCM
headerSize     = 12

typeIdAssign = 0x00DCDF21

# Create the Update Message
def makePacket(transIdCnt, L1Id, RobIds = []):
  size = ( 1 + len(RobIds))*4  # 
  data  = ''
  # - Header
  data += struct.pack('I', reqMessage)
  data += struct.pack('I', transIdCnt)
  data += struct.pack('I', size)
  # - Payload
  data += struct.pack('I', L1Id)
  for x in RobIds:
   data += struct.pack('I', x)
  return data


def listHexDump(l):
  return ''.join('0x%08x ' % x for x in l)

def client():
 ip         = options.ip
 port       = options.port
 verbose    = options.verbose
 numEv      = options.numberEv    # Number of Robs to request
 L1Id       = options.l1Id

 # Connect to the server
 s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 s.connect((ip, port))

 # Useful variables
 transIdCnt = 0                       # ExchangeId counter
 
 transIdCnt = 0
 
 while True:
   #print "STATUS: ", hltpus.stats()

   # -------- Send the data request --------
   if transIdCnt == 0:
     if L1Id == -1:
       L1Id = random.randint(1,10)
   else:
     L1Id = (L1Id+1) % 1000000
   # Source_id -> select random from the list
   RobIds = []
   for k in range(0, numEv): 
     source_id = eformat.helper.SourceIdentifier(random.choice(eformat.helper.SubDetector.values.values())<<16)
     RobIds.append(source_id.code())
   dataReqs = makePacket(transIdCnt, L1Id, RobIds)   
   len_sent = s.send(dataReqs) 
   if verbose:
     print('Exchange ID: ', transIdCnt)
     print("--->>>", listHexDump( struct.unpack('%dI' % (len(dataReqs)/4), dataReqs) ))    
   transIdCnt = transIdCnt + 1

   
 
   # Receive header, if any
   data = ''
   try: data = s.recv(headerSize, socket.MSG_DONTWAIT)
   except socket.error: pass
   if len(data):
     header = struct.unpack('III', data)
     if verbose: print("<<<---", listHexDump(header))
     rTypeId     = header[0]  
     rExchangeId = header[1]
     rSize       = header[2]
     if rTypeId != typeIdAssign :
       print('Data request from wrong client (%x / %x) ' % (rTypeId, typeIdAssign))
       os._exit(7)
     if rSize <= 0: 
       print('Wrong event size (%d) ' % (rSize))
       os._exit(7)
    
     # --------- Receive Payload  ----------
     data = s.recv(rSize)
     if len(data) != rSize:
       print("Data size mismatch  %d / %d" % (len(data), rSize))
       os._exit(7)
     #fmt = '%dI' % (len(data)/4)
     #print 'fmt ', fmt
     #payload = struct.unpack( fmt , data)
     #print 'len ', len(data)
     #if verbose: print "<<<---", listHexDump(payload[:8])
     
     # - Unpacking data
     totalLen = 0 
     iterator = 0
     while totalLen < len(data):
       word1 = struct.unpack('I', data[totalLen:totalLen+4])
       if not int(word1[0]) == 3708957917:             # == 0xdd1234dd in hex
         print('*** Warning, error decoding Rob fragment ***')
         print('First word: ', listHexDump(word1) )
       word2 = struct.unpack('I', data[totalLen+4:totalLen+8])
       ThisRobSize = word2[0]    
       #if verbose > 3:
       #  print 'Number of words: ', word2[0]
       fmt = '%dI' % (ThisRobSize-2)                  # Read the remaining Size-2 words
       payload = struct.unpack( fmt , data[totalLen+8:totalLen+ThisRobSize*4])
       totalLen += ThisRobSize*4
       iterator = iterator + 1
       payload = word1 + word2 + payload
      
       # Access the list of ROBs 
       if len(payload) > 0:
         bindata = list(payload)
         rob = eformat.ROBFragment(eformat.u32list(bindata)) 
         if verbose > 3:
           dump_id = hex(rob.source_id().code())
           print("Exchange ID: ", rExchangeId)
           print("L1Id: ", rob.rod_lvl1_id(), "RobId: ", rob.source_id(), "  ", dump_id)
           print(listHexDump(payload[0:]))
           #print "-------- End Event ---------"

  
   #time.sleep(5)
   
# --- Main ---
if __name__ == "__main__":
 
  parser = OptionParser()
  parser.add_option('-V', '--verbose', dest='verbose', type='int', default=0, help='Verbose level')
  parser.add_option('-I', '--ip', dest='ip', type='string', help='IP number or host name')
  parser.add_option('-E', '--numberEv', dest='numberEv', type='int', default=1, help='Number of Rob to request, only 1 supported at the moment')
  parser.add_option('-P', '--port', dest='port', type='int', help='TCP server port')
  parser.add_option('-L', '--l1Id', dest='l1Id', type='int', default=-1, help='Level 1 Id, -1 to set random')


  (options, args) = parser.parse_args()  
  if (not options.numberEv):
    print('')
    print(' --- DCMtoROS emulator ---')
    print('')
    parser.print_help()
    print('')
    print('-> Please insert at least number of events, ip and port number')
    print('')
    sys.exit(0)
  
  if options.verbose :
    print('')
    print(' -------- DCMtoROS  emulator ----------')
    print(' --- Connect to: ', options.ip)
    print(' --- Port: ', options.port)
    print(' --- Number of events: ', options.numberEv)
    print('---------------------------------------')
    print('')

  print("===== Configuration ============")
  for k, v in vars(options).items():
    print("%-10s = %s" % (k, v))
  print("================================")


  # - Start TCP client
  client()

