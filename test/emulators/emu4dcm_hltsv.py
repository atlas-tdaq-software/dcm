#!/usr/bin/env tdaq_python
###############################################################################
# HLTSV emulator, server side: open a socket on a port, listen for a message 
# from a client, and send an echo reply; forks a process to handle each client 
# connection; child processes share parent's socket descriptors; fork is less 
# portable than threads--not yet on Windows, unless Cygwin or similar installed;
###############################################################################

import os, time, sys
import eformat                            # Event format 
import optparse
from optparse import OptionParser
import socket
import string
import struct                             # This module performs conversions between Python values and C structs represented as Python strings
from socket import *                      # get socket constructor and constants
import random
import datetime            
from ispy import *

# ------------- Data Format ------------------
#         ----------
#        | TypeID   | Header (Message Type)
#        | X ID     | Header (Transaction ID)
#        | Msg size | Header (Payload size)
#         ----------
#        | GID upper| GID 64 bits
#        | GID lower|
#        | flags    | Enumeration or bit mask
#        | #ofROBs  | Or RobArraySize
#        | dd1234dd | ROB-0 (event format)
#        | ......   |
#          ......
#          ......
#        | dd1234dd | ROB-n (event format)
#        | ......   |
#          ......
#          ......
#         ----------
# --------------------------------------------

# Protocols type IDs
typeIdUpdate   = 0x00DCDF00
typeIdAssign   = 0x00DCDF01
typeIdReAssign = 0x00DCDF0F
headerSize     = 12

#CTP fragment 
ctpFrag = [ 0x5e475d00, 0x50ccf013, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00800000, 0x0000003d, 
            0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00800000, 0x00020000, 
            0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00800000, 0x00020000, 
            0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
            0x00800001, 0x00000000, 0x00000000, 0x00000000, 0x00824000, 0x0000103e, 0x00020000, 0x00000000, 
            0x02000000, 0x00000000, 0x00000400, 0x00000000, 0x00800000, 0x00420000, 0x00000000, 0x00000000, 
            0x02000000, 0x00000000, 0x00000400, 0x00000000, 0x00800000, 0x00420000, 0x00000000, 0x00000000, 
            0x02000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00800000, 0x00002080, 
            0x00000000, 0x00000000, 0x00904000, 0x0000003f, 0x00000001, 0x00000000, 0x00000000, 0x00000002, 
            0x00000400, 0x00000000, 0x02800000, 0x00020000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
            0x00000400, 0x00000000, 0x02800000, 0x00020000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
            0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000006f9, 0x0488a23f, 0x00002656, 0x00000001, 
            0x00f70000
         ]

def publishMsgInfo(partition, isServer, port, nMask):
  part = IPCPartition(options.partition)
  name = isServer + ".MSG_HLTSV"
  print("Publishing in ISServer '%s@%s'" % (name, partition))
  try:
    msgInfo = ISObject(part, name, 'MsgInfo')
    msgInfo.Hostname = gethostname()
    msgInfo.Port     = port
    msgInfo.Addresses= [gethostbyname(msgInfo.Hostname) + "/" + nMask ]
    msgInfo.checkin(False)
    print("msgInfo = ", msgInfo)
  except UserWarning as details:   
    print("WARNING: Unable to publish in IS:", details)
    pass


##############################################################
# HLTSV Session Class
class HltsvSession:
  def __init__(self, connection, timeout_ms, verbose):
    self.reqCounter    = 0
    self.lvl1_id       = 0
    self.exchangeId    = 0
    self.pid           = os.getpid()
    self.gidLo         = 0
    self.gidUp         = self.pid
    self.verbose       = verbose
    self.connection    = connection
    self.table         = {}
    self.timeout_ms    = timeout_ms
    self.timeout       = datetime.timedelta(microseconds= self.timeout_ms * 1000)

  def l1id(self):
    self.lvl1_id = (self.lvl1_id+1) % 1000000
    return self.lvl1_id
  
  def gidLow(self):
    self.gidLo += 1
    return self.gidLo

  def dump(self):
    s = [k for k in self.table]
    l = len(self.table)
    print("  DUMP[%5d] gid=%d, #Reqs=%2d, #L1Ids=%2d %s" % (self.pid, self.gidLo, self.reqCounter, l, s  ))

  def checkTimeouts(self):
    t = datetime.datetime.now()
    for l1id in self.table:
      if t > self.table[l1id] + self.timeout :
        dt = t - self.table[l1id]
        ms = dt.seconds*1000 + dt.microseconds/1000 
        print("ERROR: timeout for L1ID = %d (ms = %d / %d)" % (l1id, ms, self.timeout_ms )) 
        return l1id
    return 0    

  def receiveData(self):
    # Parse the header
    data = self.connection.recv(headerSize)            
    if not data: return True

    header = struct.unpack('III', data)
    if self.verbose: print("<<<---[%5d] %s" % (self.pid, listHexDump(header)))
    rTypeId         = header[0]  
    self.exchangeId = header[1]  
    rSize           = header[2]
    if rTypeId != typeIdUpdate: 
      print('Data request from wrong client (%x / %x) ' % (rTypeId, typeIdUpdate))
      return False
    if rSize <= 0: 
      print('Wrong event size (%d) ' % (rSize))
      return False

    # Read the payload
    data    = self.connection.recv(rSize)            
    fmt     = "%dI" % (rSize/4)
    payload = struct.unpack(fmt, data)
    if self.verbose: print("<<<---[%5d] %s" % (self.pid, listHexDump(payload)))
   
    # Update the reqCounter
    self.reqCounter += payload[0]

    # Update the L1Id table
    for l1id in payload[1:]:
      del self.table[l1id]
   
    #if self.verbose: 
    #  s = [k for k in self.table]
    #  print "  dump[%5d] #Reqs = %d, #L1Ids = %d %s" % (self.pid, self.reqCounter, len(self.table), s)

    return True

  def sendEvents(self, number):
    # TODO:
    #  - Support multiple ROB per L1Result
    #  - Support CTPFragment as payload! NB: there is no CTPFragment write interface,
    #    therefore the data has to be hardplugged in the code
    for i in range(0, number):
      lvl1_id      = self.l1id()
      bc_id        = lvl1_id%1000
      gidLo        = self.gidLow()
      gidUp        = self.gidUp
      source_id    = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_CTP, 0x0)

      # Dedice the payload
      dummyPayload = options.robSize
      if options.robSize == 0:
        dummyPayload = ctpFrag 
        
      status       = [0x0]
      rob          = eformat.dummy.make_rob(source_id, dummyPayload, lvl1_id, bc_id, status)    # Make random rob data (see dummy.py)
      robArray     = rob
      robArraySz   = len(robArray)*4                          # - ROBs array size  
      sSize        = 12 + robArraySz 

      outData  = ''
      outData += struct.pack('I', typeIdAssign)
      outData += struct.pack('I', self.exchangeId) 
      outData += struct.pack('I', sSize)
      outData += struct.pack('I', gidUp)        # GID upper
      outData += struct.pack('I', gidLo)        # GID lower
      outData += struct.pack('I', lvl1_id)

      # - pack the events
      outData += ''.join((struct.pack('I', w) for w in robArray.__raw__()))

      # Send
      self.connection.send(outData)
      self.table[lvl1_id]=datetime.datetime.now()
      self.reqCounter -= 1
    
      if self.verbose: 
        s = listHexDump(struct.unpack('%dI' % (len(outData)/4), outData)[:8])
        print("--->>>[%5d] %s" % (self.pid, s))

##############################################################


# Helper function to dump in hexadecimal 
def listHexDump(l):
  return ''.join('0x%08x ' % x for x in l)


# ----------------------------------------------------------------
# --- HLTSV emulator: TCP server implementation
def server():                               
  # - Set running options from argv 
  readFromFile = options.file
  if readFromFile: 
    inputFileName = options.filename
  myHost = ''
  myPort = options.port                        # listen on a non-reserved port number  
  
  sockobj = socket(AF_INET, SOCK_STREAM)       # make a TCP socket object
  sockobj.bind((myHost, myPort))               # bind it to server port number
  sockobj.listen(5)                            # allow 5 pending connects

  if myPort == 0:
    myPort = sockobj.getsockname()[1]
    print("Random port assigned:", myPort)  

  # Try to publish in IS
  if options.partition:
    publishMsgInfo(options.partition, options.isServer, myPort, options.nMask )

  def now( ):                                  # current time on server
    return time.ctime(time.time( ))

  activeChildren = []
  def reapChildren( ):                         # reap any dead child processes
    while activeChildren:                      # else may fill up system table
      pid,stat = os.waitpid(0, os.WNOHANG)     # don't hang if no child exited
      if not pid: break
      activeChildren.remove(pid)

  # Worker
  def handleClient(connection):           # child process: reply, exit
    hltsvSession = HltsvSession(connection, options.timeout_ms, options.verbose)
    cnt = 0
    while True:
      cnt += 1
      
      # Dump stats
      if cnt%options.dumpStep == 0:
        hltsvSession.dump()

      # Receive a request (blocking)
      if not hltsvSession.receiveData():
        break
      
      # Check timeouts 
      # NB: just after recv since the mesg can be a "done"
      l1id = hltsvSession.checkTimeouts()  
      if l1id:
        print("FATAL: timeout, the connection will be closed!")
        return False

      # Sends some events: random number
      number = random.randint(1,hltsvSession.reqCounter)
      hltsvSession.sendEvents(number)

    connection.close( )
    os._exit(0)

  def dispatcher( ):                          # listen until process killed
    while True:                               # wait for next connection,
      connection, address = sockobj.accept( ) # pass to process for service
      print('Server connected by', address)
      print('at', now( ))
      reapChildren( )                         # clean up exited children now
      childPid = os.fork( )                   # copy this process
      if childPid == 0:                       # if in child process: handle
        handleClient(connection)
      else:                                   # else: go accept next connect
        activeChildren.append(childPid)       # add to active child pid list

  dispatcher( )
# ----------------------------------------------------------------


# --- Main ---
if __name__ == "__main__":
  
  parser = OptionParser()
  parser.add_option('-V', '--verbose', dest='verbose', type='int', default=0, help='Verbose mode')
  parser.add_option('-F', '--file', dest='file', help='Read from file')
  parser.add_option('-N', '--filename', dest='filename', type='string', help='Input file name, if read from file selected')
  parser.add_option('-E', '--robSize', dest='robSize', type='int',  default=0, help='CTP ROB fragment size, zero means preloaded')
  parser.add_option('-P', '--port', dest='port', type='int', default=0, help='TCP server port')
  parser.add_option('-p', '--partition', dest='partition', type='string', default='', help='partition name' )
  parser.add_option('-s', '--isServer',  dest='isServer', type='string', default='DF', help='IS server name' )
  parser.add_option('-m', '--nMask',  dest='nMask', type='string', default='255.255.255.0', help='Network mask' )
  parser.add_option('-d', '--dumpStep', dest='dumpStep',   type='int', default=100,  help='Dump stats every n events')
  parser.add_option('-t', '--timeout',  dest='timeout_ms', type='int', default=5000, help='Timeout is ms')


  (options, args) = parser.parse_args()  
  if options.verbose :
    print('')
    print(' --- HLTSV emulator ---')
    print('')
    if options.file: 
      print('Read from file: ', options.file)
  
  print("===== Configuration ============")
  for k, v in vars(options).items():
    print("%-10s = %s" % (k, v))

  if options.robSize == 0:
    print("N.B.       = Preloading CTP fragment of size: ", len(ctpFrag))
  print("================================")

  server()

