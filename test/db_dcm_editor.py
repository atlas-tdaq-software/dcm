#!/usr/bin/env tdaq_python

import sys
import pm.project
import pm.farm
import getopt
from config.dal import module as dal_module

map = {}
map['l1Source']      = ['DcmDummyL1Source','DcmHltsvL1Source']
map['processor']     = ['DcmDummyProcessor','DcmHltpuProcessor']
map['dataCollector'] = ['DcmDummyDataCollector','DcmRosDataCollector']
map['output']        = ['DcmFileOutput', 'DcmSfoOutput', 'DcmSfoEfioOutput']

def Usage():
  print('Edit dcm configuration')
  print('Usage:', sys.argv[0], '[options]')
  print('   -p  name      Partition name (mandatory)')
  print('   -d  dbfile    Database file (mandatory)')
  print('   -r  path      Set RepositoryRoot')
  print('   -s  index     Set L1Source :', map['l1Source'])
  print('   -x  index     Set Processor:', map['processor'])
  print('   -c  index     Set Collector:', map['dataCollector'])
  print('   -o  index     Set Output   :', map['output'])
  sys.exit(2)


pname     = ""
dbname    = ""
repo      = "empty"
idxProcessor = -1
idxL1source  = -1
idxOutput    = -1
idxCollector = -1

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hp:d:x:o:c:s:r:")
except getopt.GetoptError:
  print(str(err))
  Usage()
  sys.exit(2)

try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-h": Usage()
    elif  o == "-p": pname=a
    elif  o == "-d": dbname=a
    elif  o == "-r": repo=a
    elif  o == "-x": idxProcessor = int(a)
    elif  o == "-o": idxOutput    = int(a)
    elif  o == "-c": idxCollector = int(a)
    elif  o == "-s": idxL1source  = int(a)
except(AttributeError): pass
if len(dbname) == 0 : Usage()
if len(pname)  == 0 : Usage()

# Access the DB
db = pm.project.Project(dbname)
try: partition = db.getObject('Partition', pname)
except(RuntimeError): print('Partition object %s not found' % (pname)); sys.exit(7)
try: dcmList = db.getObject('DcmApplication')
except(RuntimeError): print('No DcmApplications found'); sys.exit(7)
if len(dcmList) > 1:
  print("WARNING: multiple DCMs in the partition. Using the 1st one")
dcm   = dcmList[0]  

# Operations:
print("--- Operations:") 

def update(rel, idx):
  if idx >= len(map[rel]):
    print("ERROR: wrong processor index for", rel); sys.exit(77)
  type = map[rel][idx]
  try: obj = db.getObject(type)[0]
  except(IndexError):
    tmp = db.create_obj(type, type+"-7")
    obj = db.get_dal   (type, type+"-7")
    db.addObjects((obj,))
  print("Setting dcm.%s = %s" % (rel, obj.fullName()))
  return obj  

# Update the processor
if idxProcessor >= 0:
  dcm.processor = update('processor', idxProcessor)
  db.updateObjects((partition, dcm))

# Update the l1Source
if idxL1source >= 0:
  dcm.l1Source = update('l1Source', idxL1source)
  db.updateObjects((partition, dcm))

# Update the output
if idxOutput >= 0:
  dcm.output = update('output', idxOutput)
  if(idxOutput == 0):
    db.updateObjects((partition, dcm))
  else: #Update DFParameters.DefaultDataNetworks  
    import fcntl
    import struct
    import socket
    iface = "eth0"
    netaddr = socket.gethostbyname(socket.gethostname())
    netmask = socket.inet_ntoa(fcntl.ioctl(socket.socket(socket.AF_INET, socket.SOCK_DGRAM), 35099, struct.pack('256s', iface))[20:24])
    #FIXME: Ugly gymnastic to match complex netmasks
    splAddr = netaddr.split(".")
    splMask = netmask.split(".")
    theAddr = ""
    for i in range(0,4):
      if int(splMask[i]) != 0: theAddr += "%d" % (int(splAddr[i]) & int(splMask[i]))
      else:                    theAddr += "%d" % (int(splAddr[i])) 
      if i < 3: theAddr += "."
    try: dfparList = db.getObject('DFParameters')
    except(RuntimeError): print('No DFParameters found'); sys.exit(7)
    if len(dfparList) > 1:
      print("WARNING: multiple DFParameters in the partition. Using the 1st one")
    dfpar = dfparList[0]
    defDataNet = "%s/%s" % (theAddr,netmask) 
    dfpar.DefaultDataNetworks = [defDataNet]
    print("DFParameters.DefaultDataNetworks --->", dfpar.DefaultDataNetworks)
    db.updateObjects((partition, dcm, dfpar))
    
# Update the collector
if idxCollector >= 0:
  dcm.dataCollector = update('dataCollector', idxCollector)
  db.updateObjects((partition, dcm))

# Update repository root
if repo != "empty" > 0:
  partition.RepositoryRoot=repo
  db.updateObjects((partition,))
  print("  + Set partition.RepositoryRoot =", partition.RepositoryRoot)

print("\n---- Current values:") 
print("DB file  =", dbname)
print("Partition=", partition.fullName())
print("            |- RepositoryRoot =", partition.RepositoryRoot)
print("Dcm      =", dcm.fullName())
print("            |- l1Source  ->", dcm.l1Source.fullName()) 
print("            |- processor ->", dcm.processor.fullName())
print("            |- dataColl  ->", dcm.dataCollector.fullName())
print("            |- output    ->", dcm.output.fullName())



