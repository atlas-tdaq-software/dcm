#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdio>
#include <cstdint>

#include "dcm/SharedBlockArray/Block.h"
#include "dcm/SharedBlockArray/Readable.h"
#include "dcm/SharedBlockArray/Writable.h"
#include "ers/ers.h"


namespace SBA = daq::dcm::SharedBlockArray;
using namespace std;

// Return empty string if the shared block array works as expected
// Otherwise the string returned is the exception string
std::string check( bool display, const std::string& fileName )
{
  // One mebibyte unit size (see http://en.wikipedia.org/wiki/Mebibyte)
  // const size_t MiB = 1024*1024;

  // Check values returned by getROB
  if( display )
    cout << endl << "Testing Write class." << endl;

  std::uint32_t dataChunk1, dataChunk2, blockId;
  uint32_t robIds[] = { 0x7C0000, 0x7C0001, 0x7C0002, 0x7C0003, 0x7C0004 };

  try
  {
    // Parameters of the SharedBlockArray
    std::uint32_t blockMBSize = 32;
    std::uint32_t numberOfBlocks = 32;

    // Output both index content
    if( display )
      cout << endl << "Instantiating SharedBlockArray with " << numberOfBlocks
                << " blocks of " << blockMBSize << " MB each" << endl;
    SBA::Writable sba( fileName, numberOfBlocks, blockMBSize );

    // Output both index content
    if( display )
      cout << endl << "Write class instantiation successful" << endl;

    // Allocate a block (throws if no more blocks to allocate)
    SBA::Writable::Block& block = sba.allocate();

    // save robId for readable check
    blockId = block.blockId();

    // Create 5 ROB Fragments with robId with sub detector HLT and module id 0, 1, 2, 3 and 4
    const size_t robWordSize = 1000;
    uint32_t robData[robWordSize] = { 0xdd1234dd, robWordSize, 20, 0, robIds[0] };
    const size_t robByteSize = robWordSize*sizeof(uint32_t);

    // Insert a ROB using appendRobData method
    if (display)
      cout << endl << "Adding the ROB Fragment with id " << eformat::helper::SourceIdentifier(robIds[0]).human() << endl;
    block.appendRobData( robData, robByteSize );

    // save data chunk 1 for readable block check
    dataChunk1 = block.dataWordSize();

    if (block.robInfoIndex().size() != 1)
      throw std::runtime_error( "SharedBlockArray::check : invalid number of ROB identifiers in index" );

    SBA::RobInfo* robInfo = block[robIds[0]];
    if (!robInfo)
      throw std::runtime_error( "SharedBlockArray::check : Failed to retrieve inserted ROB" );
    if (display)
    {
      cout << "Found robInfo with robId " << eformat::helper::SourceIdentifier(robInfo->robId).human() << endl;
      cout << "and ROB fragment with Source identifier " <<
                   eformat::helper::SourceIdentifier(robInfo->robFragment().source_id()).human() << endl;
    }

    if( display )
      cout << endl << "Adding a ROB Fragment using startAppendRobData" << endl;
    // Insert a ROB using in place write
    uint32_t *ptr = block.startAppendRobData( 5*robByteSize );
    robData[4] = robIds[1];
    ::memcpy( ptr, robData, robByteSize );
    block.endAppendRobData( robByteSize );

    const SBA::RobInfoIndex& robInfoIndex = block.robInfoIndex();
    if (robInfoIndex.size() != 2)
      throw std::runtime_error( "SharedBlockArray::check : invalid number of ROBs in index" );
    SBA::RobInfoIndex::const_iterator cit, eit;

    // Check Block content
    ptr = robIds;
    for( cit = robInfoIndex.begin(), eit = robInfoIndex.end(); cit != eit; ++cit, ++ptr )
    {
      if (cit->first != *ptr)
        throw std::runtime_error( "SharedBlockArray::check : unexpected robId in RobInfo" );
      if (!cit->second)
        throw std::runtime_error( "SharedBlockArray::check : null value in robIndex" );
      if (!cit->second->robPtr()) // Also checks marker
        throw std::runtime_error( "SharedBlockArray::check : null ROB pointer in robIndex" );
      if (cit->first != cit->second->robFragment().source_id())
        throw std::runtime_error( "SharedBlockArray::check : robId of index mismatch robId in ROB fragment" );
      if (display)
        cout << "key: " << eformat::helper::SourceIdentifier(cit->first).human() << " " <<
                "val: " << eformat::helper::SourceIdentifier(cit->second->robFragment().source_id()).human() <<
                std::endl;
    }

    if( display )
      cout << endl << "Adding multiple ROB Fragments with one startAppendRobData" << endl;
    ptr = block.startAppendRobData( 5*robByteSize );
    for( int i = 2; i < 5; ++i, ptr += robWordSize )
    {
      robData[4] = robIds[i];
      ::memcpy( ptr, robData, robByteSize );
    }
    block.endAppendRobData( 3*robByteSize );

    // Check Block content
    ptr = robIds;
    for( cit = robInfoIndex.begin(), eit = robInfoIndex.end(); cit != eit; ++cit, ++ptr )
    {
      if (cit->first != *ptr)
        throw std::runtime_error( "SharedBlockArray::check : unexpected robId in RobInfo" );
      if (!cit->second)
        throw std::runtime_error( "SharedBlockArray::check : null value in robIndex" );
      if (!cit->second->robPtr()) // Also checks marker
        throw std::runtime_error( "SharedBlockArray::check : null ROB pointer in robIndex" );
      if (cit->first != cit->second->robFragment().source_id())
        throw std::runtime_error( "SharedBlockArray::check : robId of index mismatch robId in ROB fragment" );
      if (display)
        cout << "key: " << eformat::helper::SourceIdentifier(cit->first).human() << " " <<
                "val: " << eformat::helper::SourceIdentifier(cit->second->robFragment().source_id()).human() <<
                std::endl;
    }

    // Se recoverable up to here
    block.setRecoverable();

    // save data chunk 2 for readable block check
    dataChunk2 = block.dataWordSize();

  }
  catch( std::exception& e )
  {
    ::unlink( fileName.c_str() );
    throw;
  }

  // Check values returned by getROB
  if( display )
    cout << endl << "Write class test completed. Testing Read class." << endl;

  // Test the Read class
  try
  {
    // Output both index content
    if( display )
      cout << endl << "Instantiating SharedBlockArray '" << fileName << "'" << endl;
    SBA::Readable sba( fileName );

    // Output both index content
    if( display )
      cout << endl << "Read class instantiation successful" << endl;

    // Get block with specified blockId
    SBA::Readable::Block block = sba.block(blockId);
    if( display )
      cout << endl << "Block mapping successful" << endl;

    if (!block.robInfoIndex().empty())
      throw std::runtime_error( "SharedBlockArray::check : Block should be initially empty" );

    // Output both index content
    if( display )
      cout << "Append data chunk 1 to Readable block" << endl;
    block.appendAndInsertRobInfos(dataChunk1);

    if (block.robInfoIndex().size() != 1)
      throw std::runtime_error( "SharedBlockArray::check : Block should contain one ROB" );

    if (block.robInfoIndex().begin()->first != robIds[0])
      throw std::runtime_error( "SharedBlockArray::check : Invalid robId in robIndex" );
    if (block.robInfoIndex().begin()->second->robFragment().source_id() != robIds[0])
      throw std::runtime_error( "SharedBlockArray::check : Invalid robId in robInfo ROB" );
    if( display )
      cout << "Append data chunk 1 successful" << endl;

    // Output both index content
    if( display )
      cout << endl << "Append data chunk 2 to Readable block" << endl;
    block.appendAndInsertRobInfos(dataChunk2);
    if (block.robInfoIndex().size() != 5)
      throw std::runtime_error( "SharedBlockArray::check : Invalid number of ROBs in robIndex" );


    // Check Block content
    std::uint32_t *ptr = robIds;
    const SBA::RobInfoIndex& robInfoIndex = block.robInfoIndex();
    SBA::RobInfoIndex::const_iterator cit, eit;
    for( cit = robInfoIndex.begin(), eit = robInfoIndex.end(); cit != eit; ++cit, ++ptr )
    {
      if (cit->first != *ptr)
        throw std::runtime_error( "SharedBlockArray::check : unexpected robId in RobInfo" );
      if (!cit->second)
        throw std::runtime_error( "SharedBlockArray::check : null value in robIndex" );
      if (!cit->second->robPtr()) // Also checks marker
        throw std::runtime_error( "SharedBlockArray::check : null ROB pointer in robIndex" );
      if (cit->first != cit->second->robFragment().source_id())
        throw std::runtime_error( "SharedBlockArray::check : robId of index mismatch robId in ROB fragment" );
      if (display)
        cout << "key: " << eformat::helper::SourceIdentifier(cit->first).human() << " " <<
                "val: " << eformat::helper::SourceIdentifier(cit->second->robFragment().source_id()).human() <<
                std::endl;
    }

    if (display)
      cout << endl << "Check recover" << endl;
    for (auto& entry : sba.recoverInfos())
    {
      // if (display)
      //  cout << "Recoverable words in block " << entry.blockId << " is " << entry.words << endl;
      if ((entry.blockId == 0 && entry.words != 5046) || (entry.blockId != 0 && entry.words != 0))
        throw std::runtime_error( "SharedBlockArray::check : Invalid number of recoverable words in a block" );
    }
    if (display)
      cout << endl << "Check recover successful" << endl;

  }
  catch( std::exception& e )
  {
    // Erase the Shared Block Array file
    ::unlink( fileName.c_str() );
    throw;
  }
  // Return empty string
  return std::string();
}


//! Main program. TODO : make the file removal optional
int main( int argc, char* argv[] )
{
  try
  {
    if( argc == 1 )
    {
        cout << "Error: requires the name of the temporary memory mapped file as argument.\n"
                  << "       The file name must be in a local disk, preferably in /tmp.\n"
                  << "       The file will be removed after the test." << endl;
        return 1;
    }

    // Name of SharedBlockArray to test
    std::string fileName( argv[1] );

    ERS_LOG("Testing write in a Shared block array");

    std::string errStr = check( true, fileName );

    if( errStr == std::string() )
      { ERS_LOG("Test passed successfully"); }
    else
      { cout << errStr << endl; }
  }
  catch( daq::dcm::Issue &ex){ ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught dcm::Issue",     ex )); }
  catch( ers::Issue &ex)     { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught ers::Issue",     ex )); }
  catch( std::exception &ex) { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught std::exception", ex )); }
  catch( ... )               { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught unknown exception"  )); }

  return 0;
}

