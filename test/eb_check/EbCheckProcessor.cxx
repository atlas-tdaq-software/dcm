#include "test/eb_check/EbCheckProcessor.h"
#include "dcm/EventBuilder.h"
#include "eformat/FullEventFragment.h"
#include "dcm/EventBuilderSingleStripping.h"


namespace daq
{
  namespace dcm
  {
    void EbCheckProcessor::processEvent( const uint32_t* inEvent )
    {
      std::cout << std::endl << std::endl;
      std::cout << "Start Processor::processEvent()" << std::endl;

      eformat::read::FullEventFragment event( inEvent );
      if( !event.check_noex() )
        throw std::runtime_error( "DummyProcess::processEvent: Event is not valid" );

      // Instantiate the event object with a pointer to the block
      std::unique_ptr<Event> outEvent(new Event( m_sba ));

      // Debugging hack so that output can compare the inputEvent and the builtEvent
      outEvent->inputEvent = inEvent;

      // Copy the global_id (should be given by HLTSV)
      outEvent->globalId = event.global_id();

      // Allocate a block to store the event data
      SharedBlockArray::Block* block = outEvent->block;

      // Parse all ROBs and insert them in the SBA event block
      std::vector<eformat::read::ROBFragment> robs;
      event.robs( robs );
      for( size_t i = 0; i < robs.size(); ++i )
      {
          //std::cout << "Store Rob " << i << "/" << robs.size() << " source_id: 0x" << std::hex << robs[i].source_id() << std::dec << std::endl;
          block->appendRobData( robs[i].start(), robs[i].fragment_size_word()*sizeof(uint32_t) );
      }
      //std::cout << "All robs stored" << std::endl;

      // TODO
      // Extract status into pcsError

      // Store HLTPU trigger info into the outEvent
      outEvent->hltpuTriggerInfo.assign(event.event_filter_info(),
                                        event.event_filter_info() + event.nevent_filter_info() );

      // Store the decoded streamTags in the output event
      eformat::helper::decode( event.nstream_tag(), event.stream_tag(), outEvent->streamTags );

      // Send the event output to the event builder
      if( m_builder )
        m_builder->asyncBuildAndSend( std::move(outEvent) );
      else
        throw std::runtime_error("DummyProcessor::processEvent: no event builder" );
    }

    void EbCheckProcessor::onBuildAndSend( std::unique_ptr<Event> )
    {
      // Delete the event
    }

    void EbCheckProcessor::onBuildAndSendError(const ers::Issue& /*issue*/,
                                 std::unique_ptr<Event> )
    {
      // Delete the event
    }
  } // namespace dcm
} // namespace daq
