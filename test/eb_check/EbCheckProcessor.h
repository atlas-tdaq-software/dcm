#ifndef DCM_EBCHECKPROCESSOR_H
#define DCM_EBCHECKPROCESSOR_H

#include "dcm/Processor.h"
#include "dcm/SharedBlockArray/Writable.h"

namespace daq
{
  namespace dcm
  {

    class EbCheckProcessor : public Processor
    {
    public:
      //! Constructor with a reference on the SharedBlockArray to use
      EbCheckProcessor( SharedBlockArray::Writable &sba ) : m_sba(sba), m_builder(0) {}

      //! Implementation of Processor::initialize()
      void initialize( L1Source* /* l1Source */, DataCollector* /* dataCollector */,
          EventBuilder* eventBuilder ) override
      {
        m_builder = eventBuilder;
      }

      //! Inject the event to process
      void processEvent( const uint32_t* event );

      // Transitions (not implemented)

      //! Implementation of Processor::asyncConnect()
      void asyncConnect() override
      {
        // This is a mock class: implementation not needed
      }

      //! Implementation of Processor::asyncStart()
      void asyncStart() override
      {
        // This is a mock class: implementation not needed
      }

      //! Implementation of Processor::asyncStop()
      void asyncStop() override
      {
        // This is a mock class: implementation not needed
      }

      //! Implementation of Processor::asyncDisconnect()
      void asyncDisconnect() override
      {
        // This is a mock class: implementation not needed
      }

      // L1Source completion methods (not implemented)

      //! Implementation of Processor::onFetchL1R()
      void onFetchL1R( std::unique_ptr<Event> /* event */, void* /* context */ ) override
      {
        // This is a mock class: implementation not needed
      }

      //! Implementation of Processor::onFetchL1RError()
      void onFetchL1RError( ers::Issue& /* issue */,
          std::unique_ptr<Event> /* event */, void* /* context */ ) override
      {
        // This is a mock class: implementation not needed
      }

      // DataCollector completion methods (not implemented)

      //! Implementation of Processor::onFetchRobs()
      void onFetchRobs( Event* /* event */, void* /* context */ ) override
      {
        // This is a mock class: implementation not needed
      }

      //! Implementation of Processor::onFetchRobsError()
      void onFetchRobsError( const ers::Issue& /* error */,
          Event* /* event */, void* /* context */ ) override
      {
        // This is a mock class: implementation not needed
      }

      // EventBuilder completion methods

      //! Implementation of Processor::onBuildAndSend()
      void onBuildAndSend( std::unique_ptr<Event> event ) override;

      //! Implementation of Processor::onBuildAndSendError()
      void onBuildAndSendError(const ers::Issue& issue, std::unique_ptr<Event> event ) override;

    private:
      //! Reference on the SharedBlockArray
      SharedBlockArray::Writable& m_sba;

      //! Pointer on the EventBuilder
      EventBuilder *m_builder;
    };
  } // namespace dcm
} // namespace daq
#endif // !defined( DCM_EBCHECKPROCESSOR_H )
