#ifndef EBCHECKEVENTBUILDERSINGLESTRIPPING_H
#define EBCHECKEVENTBUILDERSINGLESTRIPPING_H

#include "dcm/EventBuilder.h"
#include "dcm/Processor.h"
#include "dcm/Output.h"
#include "dcm/BuildSingleStrippingEvent.h"

namespace daq
{
  namespace dcm
  {
    // Forward declaration
    class Processor;
    class Output;
    class Main;


    /*! @class EventBuilder is a pure abstract class for Event building
        @brief Build events given by the Processor and pass them to the Output
        @author Christophe Meessen <meessen@cppm.in2p3.fr>
        @author Tommaso Colombo <Tommaso.Colombo@cern.ch>
        @author Andrea Negri <Andrea.Negri@pv.infn.it>

        The EventBuilder class is intended to interconnect the Processor and Output object
        instances with the following call scenarios

        <pre>
        Processor                EventBuilder                 Output
           |                          |                          |
           |---- getMissingRobList--->|                          |
           |<-------------------------|                          |
           |                          |                          |
           |----asyncBuildAndSend---->|                          |
           |                          |------asyncEventSend----->|
           =                          =                          =
           |                          |<-------onEventSent-------| (callback)
           |<-------onEventSent-------|                          |
         </pre>

         The method getMissingRobIds() is called only if all the robs are not all loaded. The list
         of missing ROB Ids is established based on the ROBs already in memory and the required ROBs
         for the output based on the StreamTag.

         Once all the ROBs are available, the Processor calls the EventBuilder::asyncBuildAndSend()
         method. This method builds the FullEventHeader fragment(s) for the events, pass them to
         the Monitoring system and then forward the event to the Output.

         Some time later, when the event has been sent, or failed to be sent because of an error,
         the Output calls back the method EventBuilder::onEventSent() to notify that the event has
         been sent. When the event data may be discarded and its storage space may be reused, the
         EventBuilder forwards the call back to the Processor by calling Processor::onEventSent().
      */
    class EbCheckEventBuilderSingleStripping : public EventBuilder
    {
    public:
      //! Constructor initializes EventBuilder with NULL processor and output
      EbCheckEventBuilderSingleStripping( uint32_t runType ) : m_build(runType) {}

      /*! @brief Configures the EventBuilder
          @param processor Pointer to the Processor submitting Events to build and send to Output
          @param output Pointer to the Output to which built Events will be submitted

          The EventBuilder instance must be configured with non null processor and output pointer,
          otherwise a misconfiguration issue must be raised.

          The EventBuilder may be reconfigured, but the behavior is undefined if there are pending
          callbacks.

          If output is null, the event is silently deleted after construction and the processor is
          notified of a successfull event build and send operation.
       */
      void initialize( Processor* processor, Output* output ) override
        { m_processor = processor; m_output = output; }

      /*! @brief Return the robIds with no data for ROBs in the StreamTag list
          @param block A reference on the block containing the ROB data
          @param streamTags a reference on the vector containing the streamTags

          Should be called only if a prior call to collectAllRobs() returned false.
       */
      std::vector<uint32_t> getMissingRobIds( const SharedBlockArray::Block &block,
                                    const std::vector<eformat::helper::StreamTag>& streamTags )
        { return m_build.getMissingRobIds( block, streamTags ); }

      /*! @brief Build the submitted event and forward it to the output
          @param event The event to build and pass to the output instance

          The method Processor::onEventSent(event) is called when the event has been successfuly
          sent, or Processor::onEventSent(error, event) is called when an error occured.
       */
      void asyncBuildAndSend( std::unique_ptr<Event> event ) override
      {
        m_build.buildEvent( event.get() );
        if( m_output )
          m_output->asyncEventSend( std::move(event) );
      }

      /*! @brief Informs the EventBuilder that the given event was successfully sent by the Output
          @param event The event that was sent by the Output

          Forwards the call to the Processor instance, if any.
       */
      void onEventSent( std::unique_ptr<Event> event ) override
      {
        if( m_processor)
          m_processor->onBuildAndSend( std::move(event) );
      }

      /*! @brief Informs the EventBuilder that the given event failed to be sent by the Output
          @param error The error code
          @param event Event that failed to be sent by the Output

          Forwards the call to the Processor instance, if any
       */
      void onEventSentError( const ers::Issue& issue,
                          std::unique_ptr<Event> event ) override
      {
        if( m_processor)
          m_processor->onBuildAndSendError( issue, std::move(event) );
      }

    private:
      Processor *m_processor = nullptr; //!< Pointer to event input
      Output    *m_output = nullptr;    //!< Pointer to event output
      BuildSingleStrippingEvent m_build;//!< Builder of single stripping events
    };
  } // namespace dcm
} // namespace daq


#endif // EBCHECKEVENTBUILDERSINGLESTRIPPING_H
