#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "eformat/FullEventFragmentNoTemplates.h"
#include "eformat/RunType.h"
#include "test/eb_check/EbCheckProcessor.h"
#include "dcm/DummyOutput.h"
#include "dcm/EventBuilderSingleStripping.h"
#include "test/eb_check/EbCheckEventBuilderSingleStripping.h"

typedef unsigned int uint32_t;

using namespace std;
using namespace daq::dcm;
using namespace eformat;


/* Maps a file containing a sequence of contiguous events.
   8 bytes with value 0 are appended after the last event as event marker and size value.
 */
uint32_t* mapEventSampleFile( const char* fileName, size_t &fileSize )
{

  // Try open file for read only access
  int fd = ::open( fileName, O_RDONLY );
  if( fd < 0 )
  {
    cerr << "Failed to open the event sample file '"
         << fileName << "' : " << strerror( errno ) << endl;
    return NULL;
  }

  struct stat st;
  if( fstat( fd, &st ) < 0 )
  {
    cerr << "Failed to open the event sample file '"
         << fileName << "' : " << strerror( errno )<< endl;
    return NULL;
  }

  //size_t pageByteSize = ::sysconf( _SC_PAGESIZE );
  fileSize = (size_t)st.st_size;

  void *data = ::mmap( NULL, fileSize, PROT_READ, MAP_SHARED, fd, 0 );
  if( data == MAP_FAILED )
  {
    cerr << "Failed mapping event sample file '"
         << fileName << "' : " << strerror( errno )<< endl;
    return NULL;
  }

  return (uint32_t*)data;
}


/* Extract all the robIds (sourceId) from the event fragments,
   store them in the robIds vectore and finally sort them in ascending order */
void collectRobIds( const uint32_t* inputEvent, SharedBlockArray::ROBIdVect& robIds )
{
  robIds.clear();
  eformat::read::FullEventFragment event(inputEvent);
  std::vector<eformat::read::ROBFragment> robs;
  event.robs(robs);
  //for( size_t i = 0; i < robs.size(); ++i )
  for( eformat::read::ROBFragment rob : robs )
  {
    uint32_t robId = eformat::helper::SourceIdentifier( rob.source_id() ).simple_code();
    //uint32_t srcId = robs[i].source_id();
    //uint32_t size = robs[i].fragment_size_word() * sizeof(uint32_t);
    // cout << "Collect robId "<< i << "/" << robs.size() << " 0x" << hex << robId << " 0x" << srcId << dec << " size(Bytes) " << size << endl;
    robIds.push_back( robId );
  }
}



int main( int, char* [] )
{
  std::string ebSbaFileName = "/tmp/eb_test_sba.data";
  std::string ebOutFileName = "/tmp/eb_test_out.data";
  ::unlink( ebSbaFileName.c_str() );
  ::unlink( ebOutFileName.c_str() );
  std::cout << "Initializing ..." << std::endl;

  try
  {
    size_t size = 0;
    uint32_t* data = mapEventSampleFile( "../test/eventSamples.data", size );
    if( data == NULL )
      exit(1);

    // Collect the robIds from the first event in the event sample file
    SharedBlockArray::ROBIdVect robIds;
    collectRobIds( data, robIds );

    // Instantiate the ShareBlockArray of 10 blocks of 32MiB
    SharedBlockArray::Write sba( ebSbaFileName, 10, 32*1024*1024, robIds );

    // Instantiate the DCM modules for testing
    EbCheckProcessor *pr = new EbCheckProcessor( sba );
    // Set runType to PHYSICS to match what is in the sampleEvents
    EventBuilder *eb = new EbCheckEventBuilderSingleStripping( eformat::PHYSICS );
    DummyOutput *out = new DummyOutput( ebOutFileName );

    // Configure the DCM modules
    pr->initialize( nullptr, nullptr, eb );
    eb->initialize( pr, out );
    out->initialize( eb );

    std::cout << "Configuration done!" << std::endl;
    std::cout << "DummyOutput compares the inputEvent (first column) and the built event (second column)." << std::endl <<
                 "Only different value pairs are shown with a following !." << std::endl;
    std::cout << "Note: Event and header size differ because lvl2 trigger info is not included." << std::endl;
    std::cout << "      The difference must be equal 254." << std::endl;


    // Process the events
    for( uint32_t* evt = data; *evt != 0; evt += evt[1] )
        pr->processEvent( evt );
  }
  catch( daq::dcm::Issue &ex){ ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught dcm::Issue",     ex )); }
  catch( ers::Issue &ex)     { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught ers::Issue",     ex )); }
  catch( std::exception &ex) { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught std::exception", ex )); }
  catch( ... )               { ers::fatal(daq::dcm::GenericIssue(ERS_HERE, "Caught unknown exception"  )); }

  ::unlink( ebSbaFileName.c_str() );
  //::unlink( ebOutFileName.c_str() );
  return 0;
}
