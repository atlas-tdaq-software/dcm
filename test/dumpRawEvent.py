#!/usr/bin/env tdaq_python

import sys
import struct
import eformat
import eformat.dump
import libpyeformat
import libpyeformat_helper
import argparse

checkword=0xaa1234aa
word=4

parser = argparse.ArgumentParser(description='Decode an eformat file even if the it is not closed or corrupted')
parser.add_argument('file', help="EventStorare or EventFormat file")
parser.add_argument('-r', '--robs',   action="store_true", help='Dump robs IDs')
parser.add_argument('-s', '--tags',   action="store_true", help='Dump stream tags')
parser.add_argument('-S', '--status', action="store_true", help='Dump status words')
parser.add_argument('-P', '--pretty', type=int, default=0, help='Eformat pretty print: 1=dump full event header, 2=dump robs header')
args = parser.parse_args()

# Open file
dataFile = args.file
f=open(dataFile, "rb")

size=0
#Loop
while(1):
  # Find the checkword and get the size (2nd word)
  for i in range(1,1000): #
    try: w=struct.unpack('I', f.read(word))[0]
    except(struct.error): 
      print ("EOF")
      sys.exit(0)
    if w == checkword:
      size=struct.unpack('I', f.read(word))[0]
      break
  if not size:
    print ("Checkword", hex(checkword), "not found")
    sys.exit(1)

  # Go back to the beginning and fill the buffer
  f.seek(-word*2, 1)
  buffer = []
  for i in range(0, size):
    buffer.append(struct.unpack('I', f.read(word))[0])

  # Instantiate the fullevent
  ulist = libpyeformat.u32list(buffer)
  fe = libpyeformat.FullEventFragment(ulist)
  stags = fe.stream_tag()


  # Pretty print
  if args.pretty >0 :
    eformat.dump.fullevent_handler(fe)
    if args.pretty >1 :
      for rob in fe:  
        eformat.dump.rob_handler(rob)
    continue

  # Print generic informations
  print ("L1id=%8d, Gid=%8d, bytes=%6d, #stags=%2d, #ROBs=%3d, #status=%2d" % (
    fe.lvl1_id(), fe.global_id(), fe.fragment_size_word()*4, len(stags), fe.nchildren(), len(fe.status())))

  # Build the streamTag line
  if args.tags:
    str="\tStags: "
    for s in stags:
      sR =  "".join('x%08x, ' % i for i in s.robs)
      sD =  "".join('x%08x, ' % i for i in s.dets)
      str+="%s@%s [{%s},{%s}]; " % (s.name, s.type, sR, sD)
    print(str)  

  # Build the ROBs line
  if args.robs:
    str="\tROBs: "
    for r in fe.children():
      str+="0x%08x " % r.source_id()
    print(str)

  # Build the status line
  if args.status:
    str="\t Status: "
    for s in fe.status():
      str += "0x%08x, " % s
    print(str)

