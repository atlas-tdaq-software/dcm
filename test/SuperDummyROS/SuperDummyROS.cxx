#include "asyncmsg/Error.h"
#include "asyncmsg/NameService.h"
#include "asyncmsg/Server.h"
#include "asyncmsg/Session.h"
#include "dccommon/Distribution.h"
#include "dal/Detector.h"
#include "dal/Partition.h"
#include "dal/ResourceSet.h"
#include "dcm/dal/SuperDummyChannel.h"
#include "DFdal/DFParameters.h"
#include "DFdal/ROS.h"
#include "eformat/write/ROBFragment.h"
#include "eformat/Status.h"
#include "ers/ers.h"
#include "ipc/partition.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

#include <boost/asio/io_service.hpp>
#include <TRandom.h>
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <chrono>
#include <memory>
#include <random>
#include <thread>
#include <vector>

template<class T, class ... Args>
typename std::unique_ptr<T> make_unique(Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template<class T1, class T2>
std::unique_ptr<T1> static_pointer_cast(std::unique_ptr<T2>&& ptr)
{
  return std::unique_ptr<T1>(static_cast<T1*>(ptr.release()));
}

namespace daq
{

ERS_DECLARE_ISSUE(SuperDummyROS,
    SystemError,
    message << " " << "(" << category << ": " << number << ")",
    ((std::string) message) ((std::string) category) ((int) number))

ERS_DECLARE_ISSUE(SuperDummyROS,
    ServerError,
    "Input server problem " << action,
    ((std::string) action))

ERS_DECLARE_ISSUE(SuperDummyROS,
    SessionError,
    "Problem communicating with " << remoteName << " (" << remoteEndpoint << ") while " << action,
    ((std::string) remoteName) ((std::string) remoteEndpoint) ((std::string) action))

ERS_DECLARE_ISSUE(SuperDummyROS,
    UnexpectedMessageType,
    "Unexpected message type 0x" << std::hex << type,
    ((std::uint32_t) type))

ERS_DECLARE_ISSUE(SuperDummyROS,
    InvalidMessageSize,
    "Invalid size " << size << " for message type 0x" << std::hex << type,
    ((std::uint32_t) type) ((std::uint32_t) size))

ERS_DECLARE_ISSUE(SuperDummyROS,
    InvalidROBId,
    "Invalid ROB ID 0x" << std::hex << robId << " requested",
    ((std::uint32_t) robId))

namespace SuperDummyROS
{

SystemError makeSystemError(const ers::Context& context,
    const boost::system::error_code& error)
{
  return SystemError(context, error.message(), error.category().name(), error.value());
}

class Config
{

public:

  void refresh()
  {
    auto detId = application().get_Detector()->get_LogicalId();
    for (auto appResource : application().get_Contains())
    {
      auto module = database().cast<daq::core::ResourceSet>(appResource);
      if (module != nullptr && !module->disabled(partition()))
      {
        for (auto moduleResource : module->get_Contains())
        {
          auto channel = database().cast<dcm::dal::SuperDummyChannel>(moduleResource);
          if (channel != nullptr && !channel->disabled(partition()))
          {
            // XXX: the following logical OR works in the following cases:
            // - The ROS sub-detector ID is 0 and the channel ID contains the full ROB ID
            // - The ROS sub-detector ID is set and the channel ID contains just the module ID
            // - The ROS sub-detector ID is set and the channel ID contains the full ROB ID,
            //   with its sub-detector ID part equal to the sub-detector ID.
            std::uint32_t robId = channel->get_Id() | ((std::uint32_t) detId << 16);
            m_channels.insert({robId, channel->get_PayloadSizeDistribution_words()});
          }
        }
      }
    }
    gRandom->SetSeed(std::rand());

    // Compute the size of an ROB fragment with no payload
    m_headerSize_words = eformat::write::ROBFragment(0, 0, 0, 0, 0, 0,
        0, nullptr, eformat::STATUS_FRONT).size_word();
  }

  inline const std::string name() const
  {
    return daq::rc::OnlineServices::instance().applicationName();
  }

  inline Configuration& database() const
  {
    return daq::rc::OnlineServices::instance().getConfiguration();
  }

  inline const daq::core::Partition& partition() const
  {
    return daq::rc::OnlineServices::instance().getPartition();
  }

  inline const daq::core::Segment& segment() const
  {
    return daq::rc::OnlineServices::instance().getSegment();
  }

  inline const std::vector<std::string>& dataNetworks() const
  {
    const daq::df::DFParameters *dfparams =
        database().cast<daq::df::DFParameters>(partition().get_DataFlowParameters());
    return dfparams->get_DefaultDataNetworks();
  }

  inline const daq::df::ROS& application() const
  {
    return *database().cast<daq::df::ROS>(
        &daq::rc::OnlineServices::instance().getApplication());
  }

  std::uint32_t nSessionThreads() const
  {
    // TODO: make it configurable
    return 16;
  }

  std::vector<std::uint32_t> payloadSizes_words(const std::vector<std::uint32_t>& robIds)
  {
    std::lock_guard<std::mutex> lock(m_rootMutex);
    std::vector<std::uint32_t> sizes;
    sizes.reserve(robIds.size());
    for (auto robId : robIds)
    {
      try
      {
        std::uint32_t fragmentSize_words = m_channels.at(robId).next_value();
        if (fragmentSize_words > m_headerSize_words)
        {
          sizes.push_back(fragmentSize_words - m_headerSize_words);
        }
        else
        {
          sizes.push_back(0);
        }
      }
      catch (std::out_of_range&)
      {
        throw InvalidROBId(ERS_HERE, robId);
      }
    }
    return sizes;
  }

  std::uint32_t runNumber() const
  {
    // TODO: get it from IS
    return 0;
  }

private:

  std::map<std::uint32_t, DC::Distribution> m_channels;
  std::uint32_t m_headerSize_words;
  std::mutex m_rootMutex;

};

class Session: public daq::asyncmsg::Session
{

private:

  struct RequestMessage: public asyncmsg::InputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF20;

    explicit RequestMessage(std::uint32_t transactionId, std::uint32_t nRobIds) :
        tId(transactionId),
        l1Id(0),
        robIds(nRobIds)
    {
    }

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return tId;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      buffers.emplace_back(static_cast<void*>(&l1Id), sizeof(l1Id));
      buffers.emplace_back(robIds.data(), robIds.size() * sizeof(std::uint32_t));
    }

    std::uint32_t tId;
    std::uint32_t l1Id;
    std::vector<std::uint32_t> robIds;

  };

  struct ResponseMessage: public daq::asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF21;

    ResponseMessage(std::uint32_t transactionId,
        std::uint32_t runNumber,
        std::uint32_t l1Id,
        const std::vector<std::uint32_t>& robIds,
        const std::vector<std::uint32_t>& payloadSizes_words) :
        tId(transactionId)
    {
      ERS_ASSERT(robIds.size() == payloadSizes_words.size());

      const std::uint32_t bunchCrossingId = 0;
      const std::uint32_t l1TriggerType = 0;
      const std::uint32_t eventType = 0;
      // For each ROB, we choose a payload size from the configured distribution and use
      // uninitialized memory as payload.
      auto maxPayloadSize_words = *std::max_element(payloadSizes_words.begin(), payloadSizes_words.end());
      data.resize(maxPayloadSize_words);
      robs.reserve(robIds.size());
      for (std::uint32_t i = 0; i < robIds.size(); ++i)
      {
        robs.emplace_back(robIds[i], runNumber, l1Id,
            bunchCrossingId, l1TriggerType, eventType, payloadSizes_words[i], data.data(),
            eformat::STATUS_FRONT);
      }
    }

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return tId;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      for (auto& rob : robs)
      {
        for (auto node = rob.bind(); node; node = node->next)
        {
          buffers.emplace_back(node->base, node->size_word * sizeof(std::uint32_t));
        }
      }
    }

    std::uint32_t tId;
    std::vector<std::uint32_t> data;
    mutable std::vector<eformat::write::ROBFragment> robs;

  };

public:

  Session(Config& config, boost::asio::io_service& ioService) :
      daq::asyncmsg::Session(ioService),
      m_config(config)
  {
  }

private:

  void onOpen() noexcept
  {
    ERS_LOG("Connection to " << remoteName() << " open.");
    asyncReceive();
  }

  void onOpenError(const boost::system::error_code& error) noexcept
  {
    abort(ERS_HERE, "opening session", makeSystemError(ERS_HERE, error));
  }

  void onClose() noexcept
  {
    ERS_LOG("Connection to " << remoteName() << " closed.");
  }

  void onCloseError(const boost::system::error_code& error) noexcept
  {
    // Ignore errors caused by calling asyncClose() twice
    if (error != daq::asyncmsg::Error::SESSION_NOT_OPEN)
    {
      abort(ERS_HERE, "closing session", makeSystemError(ERS_HERE, error));
    }
  }

  std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
  std::uint32_t transactionId, std::uint32_t size) noexcept override
  {
    if (typeId == RequestMessage::TYPE_ID)
    {
      if ((size % sizeof(std::uint32_t) != 0) || (size < 2 * sizeof(std::uint32_t)))
      {
        abort(ERS_HERE, "receiving Request message", InvalidMessageSize(ERS_HERE, typeId, size));
        return std::unique_ptr<daq::asyncmsg::InputMessage>();
      }
      std::uint32_t nRobIds = size / sizeof(std::uint32_t) - 1;
      return make_unique<RequestMessage>(transactionId, nRobIds);
    }
    else
    {
      abort(ERS_HERE, "receiving unknown message", UnexpectedMessageType(ERS_HERE, typeId));
      return std::unique_ptr<daq::asyncmsg::InputMessage>();
    }
  }

  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override
  {
    if (message->typeId() == RequestMessage::TYPE_ID)
    {
      auto request = static_pointer_cast<RequestMessage>(std::move(message));

      try {
        std::vector<std::uint32_t> payloadSize_words = m_config.payloadSizes_words(request->robIds);
        asyncSend(std::make_unique<ResponseMessage>(request->transactionId(), m_config.runNumber(),
            request->l1Id, request->robIds, payloadSize_words));
        asyncReceive();
      }
      catch (InvalidROBId& ex)
      {
        abort(ERS_HERE, "receiving unknown message", ex);
        return;
      }
    }
    else
    {
      ERS_ASSERT_MSG(false, "This point should never be reached");
    }
  }

  void onReceiveError(const boost::system::error_code& error,
  std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override
  {
    if (message && message->typeId() == RequestMessage::TYPE_ID)
    {
      abort(ERS_HERE, "receiving Request message", makeSystemError(ERS_HERE, error));
    }
    else
    {
      abort(ERS_HERE, "receiving unknown message", makeSystemError(ERS_HERE, error));
    }
  }

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
  {
  }

  void onSendError(const boost::system::error_code& error,
  std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
  {
    abort(ERS_HERE, "sending Response message", makeSystemError(ERS_HERE, error));
  }

private:

  void abort(const ers::Context& context, const std::string& action, const std::exception& cause)
  {
    std::ostringstream iss;
    iss << remoteEndpoint();
    ers::error(SessionError(context, remoteName(), iss.str(), action, cause));

    // Reset
    if (state() == State::OPEN)
    {
      asyncClose();
    }
  }

private:

  Config& m_config;

};

class Server: public daq::asyncmsg::Server
{

public:

  Server(Config& config, boost::asio::io_service& ioService) :
      daq::asyncmsg::Server(ioService),
      m_config(config)
  {

    if (m_config.nSessionThreads() == 0)
    {
      throw ers::Message(ERS_HERE, "At least one session thread must be configured");
    }

    for (std::uint32_t i = 0; i < m_config.nSessionThreads(); ++i)
    {
      // Create io_service
      m_ioServices.emplace_back(1);
      auto& ioService = m_ioServices.back();
      // Prevent io_service::run() from returning when there are no pending asynchronous operations
      m_ioServiceWorks.emplace_back(ioService);
      // Run io_service
      m_ioServiceThreads.emplace_back([&] ()
      {
        ioService.run();
      });
    }
    m_nextIoService = m_ioServices.begin();
  }

  void open(const std::string& name)
  {
    asyncmsg::Server::listen(name);
    ERS_LOG("Server listening on endpoint: " << localEndpoint());
    startAccept();
  }

  void close()
  {
    asyncmsg::Server::close();
    ERS_LOG("Server closed");
  }

  ~Server()
  {
    // Close all connections
    for (auto& session : m_sessions) {
      session->asyncClose();
    }
    while (!m_sessions.empty()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      m_sessions.erase(std::remove_if(m_sessions.begin(), m_sessions.end(),
          [] (std::shared_ptr<Session> s)
          {
            return s->state() == asyncmsg::Session::State::CLOSED;
          }
      ), m_sessions.end());
    }

    // Allow io_service::run() to return when there are no pending asynchronous operations
    m_ioServiceWorks.clear();
    // Wait for io_service::run() to return (i.e. wait for all asynchronous operations to finish)
    for (auto& thread : m_ioServiceThreads) {
      thread.join();
    }
    m_ioServiceThreads.clear();
    // Destroy io_services.
    m_ioServices.clear();
  }

private:

  void startAccept()
  {
    // Accept the next incoming connection
    auto newSession = std::make_shared<Session>(m_config, *m_nextIoService);
    if (++m_nextIoService == m_ioServices.end())
    {
      m_nextIoService = m_ioServices.begin();
    }
    asyncAccept(newSession);
  }

  virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept override
  {
    m_sessions.emplace_back(std::static_pointer_cast<Session>(session));
    startAccept();
  }

  virtual void onAcceptError(const boost::system::error_code& error,
  std::shared_ptr<daq::asyncmsg::Session> session) noexcept override
  {
    if (error == boost::asio::error::operation_aborted)
    {
      return;
    }
    else if (error)
    {
      ers::warning(ServerError(ERS_HERE, "accepting incoming connection",
          makeSystemError(ERS_HERE, error)));
    }

    session.reset();

    startAccept();
  }

  Config& m_config;

  std::list<boost::asio::io_service> m_ioServices;
  std::vector<std::thread> m_ioServiceThreads;
  std::vector<boost::asio::io_service::work> m_ioServiceWorks;
  std::list<boost::asio::io_service>::iterator m_nextIoService;

  std::vector<std::shared_ptr<Session>> m_sessions;
};

class App: public daq::rc::Controllable
{

public:

  void configure(const daq::rc::TransitionCmd&) override
  {
    m_config.refresh();

    m_ipcPartition = IPCPartition(m_config.partition().UID());

    // Create io_service
    m_ioServices.emplace_back(1);
    auto& ioService = m_ioServices.back();
    // Prevent io_service::run() from returning when there are no pending asynchronous operations
    m_ioServiceWorks.emplace_back(ioService);
    // Run io_service
    m_ioServiceThreads.emplace_back([&] ()
    {
      ioService.run();
    });

    // Create server
    m_server.reset(new Server(m_config, ioService));
    m_server->open(m_config.name());

    // Publish server endpoint via asyncmsg::NameService
    daq::asyncmsg::NameService ns(m_ipcPartition, m_config.dataNetworks());
    try
    {
      ns.publish(m_config.name(), m_server->localEndpoint().port());
    }
    catch (daq::asyncmsg::CannotPublish& e)
    {
      throw ServerError(ERS_HERE,"publishing endpoint on NameService", e);
    }
    catch (std::exception &e)
    {
      throw ServerError(ERS_HERE, "publishing endpoint on NameService", e);
    }
  }

  void unconfigure(const daq::rc::TransitionCmd&) override
  {
    // Destroy server
    m_server->close();
    m_server.reset();

    // Allow io_service::run() to return when there are no pending asynchronous operations
    m_ioServiceWorks.clear();
    // Wait for io_service::run() to return (i.e. wait for all asynchronous operations to finish)
    for (auto& thread : m_ioServiceThreads) {
      thread.join();
    }
    m_ioServiceThreads.clear();
    // Destroy io_services.
    m_ioServices.clear();
  }

private:

  // Configuration
  Config m_config;

  // Services
  IPCPartition m_ipcPartition;

  // I/O
  std::list<boost::asio::io_service> m_ioServices;
  std::vector<std::thread> m_ioServiceThreads;
  std::vector<boost::asio::io_service::work> m_ioServiceWorks;
  std::shared_ptr<Server> m_server;

};

} // namespace SuperDummyROS
} // namespace daq

int main(int argc, char** argv)
{
  // Seed random number generator
  std::random_device rd;
  std::srand(rd() ^ std::chrono::steady_clock::now().time_since_epoch().count());
  try
  {
    // Create the main controllable object
    auto main = std::make_shared<daq::SuperDummyROS::App>();
    // Create, initialize and start the ItemCtrl
    daq::rc::ItemCtrl itemCtrl(daq::rc::CmdLineParser(argc, argv, true), main);
    itemCtrl.init();
    itemCtrl.run();
  }
  catch (daq::rc::CmdLineHelp& ex)
  {
    ers::info(ex);
    return EXIT_SUCCESS;
  }
  catch (daq::rc::CmdLineError& ex)
  {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch (ers::Issue& ex)
  {
    ers::fatal(ex);
    std::abort();
  }

  return EXIT_SUCCESS;
}
