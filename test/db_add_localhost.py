#!/usr/bin/env tdaq_python

import sys
import pm.project
import pm.farm

if len(sys.argv) < 2 :
    print('Usage:', sys.argv[0], 'dbfile.data.xml /path/to/local/installed')
    sys.exit(1)
dbname = sys.argv[1]

repo=""
if len(sys.argv) == 3:
  repo = sys.argv[2]

db = pm.project.Project(dbname)

localhost = pm.farm.local_computer()

partition = db.getObject('Partition', 'part_dcm')

partition.DefaultHost = localhost
segment = db.getObject('HLTSegment', 'DCM-Segment-1')
segment.DefaultHost = localhost
segment.TemplateHosts = [localhost]

print("Set localhost:", localhost.id)

if repo:
  partition.RepositoryRoot=repo
  print("partition.RepositoryRoot =", partition.RepositoryRoot)

# Update counters
# TODO: get the DCM full name from DAL
base = "DF-EF-Segment-0-iss.DCM-1:DCM-Segment-1:%s:1.info." % (localhost.id.split(".")[0])

L1Cnt =  db.getObject('IS_EventsAndRates', 'L1-counter')
L1Cnt.EventCounter = base + "ProxL1Events"
L1Cnt.Rate         = base + "L1Rate"
#print L1Cnt

EBCnt =  db.getObject('IS_EventsAndRates', 'EB-counter')
EBCnt.EventCounter = base + "EbEvents"
EBCnt.Rate         = base + "EbRate"
#print EBCnt

EFCnt =  db.getObject('IS_EventsAndRates', 'EF-counter')
EFCnt.EventCounter = base + "ProxAccEvents"
EFCnt.Rate         = base + "OutRate"
#print EFCnt

print("Updated partition counters ( eg: L1rate =", L1Cnt.Rate, ")")

db.addObjects((localhost,))
db.updateObjects((partition, segment, L1Cnt, EFCnt))
